#! /bin/bash

. Configuration.sh
Pairs=Normal-Stage_0,Normal-Stage_I_II,Normal-Stage_III_IV,Normal-Cancer
EvalMetric=Accuracy

if [ "$K" -le 10 ];then
    export KmerCountsDir=KmerCountRawReads
    MiscConds=All
    Classifiers=LogRegL2,RandomForest,GaussianNB
    FilterMetrics=Frequency
    KmerCountsDir=KmerCountRawReads
elif [ "$K" -eq 14 ];then
    #MiscConds=Top_50,Top_100,Top_200,Top_1000,Top_10000
    MiscConds=Top_10000
    #FilterMetrics=Correlation,MannWhitneyUTest
    FilterMetrics=MannWhitneyUTest
    Classifiers=LogRegL2,RandomForest
elif [ "$K" -eq 20 ];then
    #MiscConds=Top_100,Top_200,Top_300,Top_400,Top_500,Top_1000
    MiscConds=MRMR_100,MRMR_200,MRMR_300,MRMR_400,MRMR_500
    #MiscConds=Top_10000
    #MiscConds=MRMR_100_100000,MRMR_200_100000,MRMR_300_100000,MRMR_400_100000,MRMR_500_100000
    #FilterMetrics=Correlation,MannWhitneyUTest,MWUTestFreq
    #FilterMetrics=MWUTestFreq
    #FilterMetrics=FreqDiff
    FilterMetrics=MannWhitneyUTest,FreqDiff,MWUTestFreq
    #Classifiers=LogRegL2,RandomForest,MultinomialNB,GaussianNB
    Classifiers=LogRegL2,RandomForest,GaussianNB
    KmerCountsDir=KMC
elif [ "$K" -eq 28 ];then
    MiscConds=Top_10000
    #Classifiers=SVM,LogRegL2,RandomForest
    #FilterMetrics=InfoGain,MutualInfo,ProbabilityRatio
    FilterMetrics=MannWhitneyUTest,FreqDiff,MWUTestFreq
    Classifiers=LogRegL2,RandomForest,GaussianNB
    KmerCountsDir=KmerFreqWithCoverage
elif [ "$K" -eq 32 ];then
    #MiscConds=Top_100,Top_200,Top_300,Top_400,Top_500,Top_1000
    MiscConds=MRMR_100,MRMR_200,MRMR_300,MRMR_400,MRMR_500
    #MiscConds=Top_10000
    #MiscConds=MRMR_100_100000,MRMR_200_100000,MRMR_300_100000,MRMR_400_100000,MRMR_500_100000
    #FilterMetrics=Correlation,MannWhitneyUTest,MWUTestFreq
    #FilterMetrics=MWUTestFreq
    #FilterMetrics=FreqDiff
    FilterMetrics=MannWhitneyUTest,FreqDiff,MWUTestFreq
    #Classifiers=LogRegL2,RandomForest,MultinomialNB,GaussianNB
    Classifiers=LogRegL2,RandomForest,GaussianNB
    KmerCountsDir=KMC
fi
export KmerCountsDir
#export OMP_NUM_THREADS=$NPROC

get_param_files(){
    IFS=',' read -a pair_list <<<${Pairs?}
    IFS=',' read -a misc_cond_list <<<${MiscConds?}
    IFS=',' read -a filter_metric_list <<<${FilterMetrics?}
    IFS=',' read -a classifier_list <<<${Classifiers?}
    for pair in ${pair_list[@]};do
      for misc_cond in ${misc_cond_list[@]};do
        for filter_metric in ${filter_metric_list[@]};do
          for classifier in ${classifier_list[@]};do
            printf " KmerMarker/$K/Param/CV.${pair}.${misc_cond}.${filter_metric}.${classifier}.json"
          done
        done
      done
    done
    #echo "Pairs: ${pair_list[@]}"
    #echo "MiscConds: ${misc_cond_list[@]}"
    #echo "FilterMetrics: ${filter_metric_list[@]}"
    #echo "Classifiers: ${classifier_list[@]}"
}

SelectKmerCV(){
    if [ -n "$min_count_ratio" ];then
        min_count_ratio="--min-count-ratio $min_count_ratio"
    fi
    python PrepareCV.py --kmer-count-dir $KmerCountsDir $MinCountRatio --classifiers $Classifiers --misc-conds $MiscConds --filter-metrics $FilterMetrics -k $K SelectKmerCV
    $BINPATH/SelectKmerCV 'select' KmerMarker/$K/Param/SelectKmerCV.json
}

CreateMRMRInput(){
    python PrepareCV.py --kmer-count-dir $KmerCountsDir $MinCountRatio --classifiers $Classifiers --misc-conds $MiscConds --filter-metrics $FilterMetrics -k $K SelectKmerCV
    ./CreateJobs.sh job_CreateMRMRInput
}

CalculateRelevance(){
    python PrepareCV.py --kmer-count-dir $KmerCountsDir $MinCountRatio --classifiers $Classifiers --misc-conds $MiscConds --filter-metrics $FilterMetrics -k $K SelectKmerCV
    n_tasks=64 ./CreateJobs.sh job_CalculateRelevance
    n_tasks_relevance=64 n_tasks=$NPROC ./CreateJobs.sh job_SelectKmerByRelevance
}

SelectKmerByRelevance(){
    python PrepareCV.py --kmer-count-dir $KmerCountsDir $MinCountRatio --classifiers $Classifiers --misc-conds $MiscConds --filter-metrics $FilterMetrics -k $K SelectKmerCV
    n_tasks_relevance=64 n_tasks=$NPROC ./CreateJobs.sh job_SelectKmerByRelevance
}

CreateKmerMatrix(){
    python PrepareCV.py --kmer-count-dir $KmerCountsDir --classifiers $Classifiers --misc-conds $MiscConds --filter-metrics $FilterMetrics -k $K CreateKmerMatrix
    ./CreateJobs.sh job_CreateKmerMatrix
}

CrossValidation(){
    python PrepareCV.py --kmer-count-dir $KmerCountsDir --classifiers $Classifiers --misc-conds $MiscConds --filter-metrics $FilterMetrics -k $K CrossValidation
    #param_files=$(ls KmerMarker/$K/Param/ | grep '^CV' | cut -d'.')
    param_files=$(get_param_files) ./CreateJobs.sh job_CrossValidation
    #python CrossValidation.py --parallel-engine $PARALLEL_ENGINE \
    #    -j $NPROC KmerMarker/$K/Param/CV.*.$MiscCond.*.json
}

CVIndex(){
    python PrepareCV.py --kmer-count-dir $KmerCountsDir -k $K --cvindex-file CVIndex
}

Report(){
    mkdir -p KmerMarker/$K/Report 2>/dev/null
    python CrossValidation.py --eval-metric $EvalMetric --report $(get_param_files) | tee KmerMarker/$K/Report/CrossValidation.txt
}


if [ "$#" -lt 1 ];then
    echo "Usage: $0 command1 [command2]..."
    echo "Commands: {Report|CVIndex|CalculateRelevance|CreateMRMRInput|SelectKmerCV|SelectKmerByRelevance|CreateKmerMatrix|CrossValidation}"
    exit 1
fi
for cmd in $@;do
    $cmd
done

