aspect_ratio=4.0/3.0

# get cummulative distribution of occurence of k-mers
metadata <- read.table('metadata/metadata.txt', header=TRUE)
cumdist_kmc <- function(k){
    cumdist <- NULL
    for(i in 1:nrow(metadata)){
        kmcdist <- read.table(sprintf('KmerMarker/%d/KMCHist/%s.txt', k, metadata$DataId[i]), header=FALSE)
        cumdist <- cbind(cumdist, cumsum(kmcdist[,2])/sum(kmcdist[,2]))
        rownames(cumdist) <- kmcdist[,1]
    }
    colnames(cumdist) <- metadata$DataId
    return(cumdist)
}

coverage_analysis <- function(k){
    coverage <- NULL
    threshs <- c(2, 5, 10, 20)
    coefs <- data.frame()
    for(thresh in c(2, 5, 10, 20)){
        dat <- read.table(sprintf('KmerMarker/%d/Report/coverage_analysis.ci%d.txt', k, thresh),
            header=FALSE, col.names=c('NumSamples', 'NumKmers'))
        dat$NumKmers <- dat$NumKmers/1e8
        dat$Threshold <- factor(as.character(thresh), levels=as.character(threshs))
        coverage <- rbind(coverage, dat)
        co <- coef(lm(NumKmers ~ NumSamples, data=dat))
        kmerset_size <- co[1] + co[2]*nrow(metadata)
        coefs <- rbind(coefs, list(co[1], co[2], thresh, kmerset_size))
    }
    colnames(coefs) <- c('Intercept', 'Slope', 'Threshold', 'KmerSetSize')
    coefs$Threshold <- factor(as.character(coefs$Threshold), levels=as.character(threshs))
    print(coefs)
    library(cowplot)
    plt <- ggplot(coverage, aes(x=NumSamples, y=NumKmers)) +
        geom_abline(data=coefs, aes(intercept=Intercept, slope=Slope)) +
        geom_point(aes(colour=Threshold)) +
        scale_y_continuous(breaks=seq(0, 25, 5)) +
        xlab('Number of samples') + ylab(expression(paste('Number of k-mers/', 10^8)))
    save_plot(sprintf('KmerMarker/%d/Report/coverage_analysis.pdf', k), plt, base_aspect_ratio=aspect_ratio)
}

coverage_analysis(32)
#for(k in c(20, 32)){
#    cumdist <- cumdist_kmc(k)
    
