#! /usr/bin/env python
import h5py
import math, argparse, sys
from parallelmap import parallelmap

def LoadJSON(filename):
    import json
    f = open(filename, 'r')
    p = json.load(f)
    f.close()
    return p

def ScaleMatrixByMean(kmermat_file, update_file=True):
    h5file = h5py.File(kmermat_file, 'r')
    mat = h5file['data'][:]
    h5file.close()
    mat_mean = mat.mean()
    scale = 10**(-math.floor(math.log10(mat_mean)))
    scale = 1.0/mat.max()
    mat *= scale
    if update_file:
        h5file = h5py.File(kmermat_file, 'w')
        h5file.create_dataset('data', data=mat)
        h5file.close()
    print 'Scaled matrix file {0} by {1}'.format(kmermat_file, scale)

def ParseParamFile(param_file, parallel_engine):
    p = LoadJSON(param_file)
    kmermat_files = []
    for params in p['Params']:
        kmermat_files.append(params['KmerMatrixFile'])
    parallelmap(ScaleMatrixByMean, kmermat_files, engine=parallel_engine)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('kmermat_files', type=str, nargs='*')
    parser.add_argument('-f', '--param-file', type=str, required=False,
            help='JSON file for CreateKmerMatrix')
    parser.add_argument('--parallel-engine', type=str,
                       choices=('sge_array', 'mpi', 'python', 'multiprocessing'),
                       default='sge_array')
    args = parser.parse_args()
    
    if args.param_file:
        ParseParamFile(args.param_file, args.parallel_engine)
    else:
        if args.kmermat_files is not None:
            for kmermat_file in args.kmermat_files:
                ScaleMatrixByMean(kmermat_file)
        else:
            print 'read file names from stdin'
            for line in sys.stdin:
                kmermat_file = line.strip()
                ScaleMatrixByMean(kmermat_file)


