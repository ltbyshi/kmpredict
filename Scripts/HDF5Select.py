#! /usr/bin/env python

import argparse
import signal

signal.signal(signal.SIGPIPE, signal.SIG_DFL)
signal.signal(signal.SIGINT, signal.SIG_DFL)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Select a subset of a HDF5 dataset using another HDF5 dataset as index')
    parser.add_argument('filename', type=str)
    parser.add_argument('dataname', type=str, help='dataset name of the dataset')
    parser.add_argument('index_filename', type=str)
    parser.add_argument('index_dataname', type=str, help='dataset name of the index dataset')
    args = parser.parse_args()

    import h5py
    import numpy as np

    h5file = h5py.File(args.filename, 'r')
    h5dset = h5file[args.dataname]
    data = np.empty(h5dset.shape, h5dset.dtype)
    h5dset.read_direct(data)

    ifile = h5py.File(args.index_filename, 'r')
    idset = ifile[args.index_dataname]
    indices = np.empty(idset.shape, idset.dtype)
    idset.read_direct(indices)
    for i in indices:
        print data[i]
