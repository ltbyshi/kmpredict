#! /usr/bin/env python
import json, sys, os

def LoadJSON(jsonfile):
    f = open(jsonfile, 'r')
    data = json.load(f)
    f.close()
    return data

def MakeDirs(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == 17:
            pass
        else:
            raise e

def RunMRMR(param_file):
    p = LoadJSON(param_file)
    params = p['Params']
    for param in p['Params']:
        n_features = param['FeatureNum']
        outdir = os.path.dirname(param['MRMROutputFile'])
        if not os.path.exists(outdir):
            MakeDirs(outdir)
        print 'echo "MRMROutputFile: {2}";  mrmr -i {0} -t 0.5 -n {1} -v {3} > {2}'.format(param['MRMRInputFile'], n_features, param['MRMROutputFile'], param['MaxFeatureNum'])

def ParseMRMRTask(param_file, task_id=0, n_tasks=1):
    '''read output of mRMR and extract the feature list (direct index)
    k-mer list is written to a HDF5 file.
    '''
    import numpy as np
    import h5py
    p = LoadJSON(param_file)
    for i, param in enumerate(p['Params']):
        # skip param set that does not belong to this task
        if (i % n_tasks) != task_id:
            continue
        features = []
        with open(param['MRMROutputFile'], 'r') as f:
            mrmr_started = False
            for line in f:
                line = line.strip()
                if not mrmr_started:
                    if line.startswith('*** mRMR features ***'):
                        mrmr_started = True
                else:
                    if len(line) == 0:
                        break
                    fields = line.split()
                    try:
                        fi = int(fields[0])
                        ki = int(fields[1])
                        features.append(ki - 1)
                    except:
                        pass
        features = np.asarray(features, dtype='uint64')
        # read direct index
        h5f_kmerlist = h5py.File(param['KmerListFile'], 'r')
        kmerlist = h5f_kmerlist['data'][:]
        features = kmerlist[features]
        h5f_kmerlist.close()
        print 'WriteKmerList:', param['MRMRKmerListFile']
        MakeDirs(os.path.dirname(param['MRMRKmerListFile']))
        h5f = h5py.File(param['MRMRKmerListFile'], 'w')
        h5f.create_dataset('data', data=features)
        h5f.close()

def ParseMRMR(param_file):
    '''create task files.
    Required environments: $ScriptDir, $K, $NPROC
    '''
    p = LoadJSON(param_file)
    n_tasks = int(os.environ['NPROC'])
    for i in xrange(n_tasks):
        print '''PYTHONPATH=$PYTHONPATH:{0} python -B -c 'from CreateTaskFiles import ParseMRMRTask; ParseMRMRTask("{1}", {2}, {3})' '''.format(os.environ['ScriptDir'], param_file, i, n_tasks)

