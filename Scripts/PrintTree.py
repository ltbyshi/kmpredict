#! /usr/bin/env python

import sys, os
import json
import argparse

def PrintIndent(f, indent):
    for i in xrange(indent):
        f.write(' ')
        
def PrintTree(f, obj, prefix='/', name='', indent=0, full=False):
    if full:
        prefix = os.path.join(prefix, name)
    else:
        prefix += name
    if isinstance(obj, list):
        f.write('%s: list(%d)\n'%(prefix, len(obj)))
        for i in xrange(len(obj)):
            item = obj[i]
            if isinstance(item, dict) or isinstance(item, list):
                '''for j in xrange(indent):
                    sys.stdout.write(' ')
                sys.stdout.write('%d: %d\n'%(i, len(item)))'''
                if full:
                    newprefix = '%s'%(prefix)
                else:
                    newprefix = '%s'%(' '*indent)
                PrintTree(f, item, newprefix, str(i), indent + 2, full)
    elif isinstance(obj, dict):
        f.write('%s: dict(%d)\n'%(prefix, len(obj)))
        for key in obj.iterkeys():
            item = obj[key]
            if isinstance(item, dict) or isinstance(item, list):
                '''for j in xrange(indent):
                    sys.stdout.write(' ')
                sys.stdout.write('%s: %d\n'%(key, len(item)))'''
                if full:
                    newprefix = '%s'%(prefix)
                else:
                    newprefix = '%s'%(' '*indent)
                PrintTree(f, item, newprefix, str(key), indent + 2, full)
    else:
        pass
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Print object sizes in JSON file as a tree')
    parser.add_argument('filename', type=str)
    args = parser.parse_args()
    
    data = json.load(open(args.filename, 'r'))
    PrintTree(sys.stdout, data, full=True)
    