#! /usr/bin/env python
import json, argparse
import h5py, sys
from kmpredict import counts_to_vw

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('param_file', type=str)
    args = parser.parse_args()

    f = open(args.param_file, 'r')
    p = json.load(f)
    f.close()

    fout = sys.stdout
    for info in p['SampleInfo']:
        h5file = h5py.File(info['KmerCountFile'], 'r')
        mat = h5file['data'][:]
        h5file.close()
        print >>sys.stderr, 'CountsToVW: %s'%info['KmerCountFile']
        counts_to_vw(mat, info['Group'])
        break
        #fout.write(str(info['Group']) + ' | ')
        #for i in xrange(mat.shape[0]):
        #    fout.write(' %d:%f'%(i, mat[i]))

