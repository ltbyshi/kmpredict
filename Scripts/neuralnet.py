import theano
import theano.tensor as T
from theano.tensor.signal import downsample
from theano.tensor.nnet import conv
import numpy as np
import cPickle, gzip
import time
from ptools import ShowInfo

class Classifier(object):
    def cost(self):
        pass
    
    def errors(self):
        pass
    
    def updates(self, learning_rate):
        pass
    
class LogisticRegression(Classifier):
    def __init__(self, x, n_in, n_out):
        '''Initialize the model with hyperparameters
        data: a matrix with shape (n_samples, n_features)
        '''
        self.x = x
        self.y = T.ivector('y')
        self.n_in = n_in
        self.n_out = n_out
        # add a columns of 1's before the input matrix
        # declare symbolic variables
        self.W = theano.shared(value=np.zeros((n_in, n_out), dtype=theano.config.floatX),
                               name='W', borrow=True)
        self.b = theano.shared(value=np.zeros((n_out,), dtype=theano.config.floatX),
                               name='b',borrow=True)
        # posterior probability given the data
        self.p = T.nnet.softmax(T.dot(x, self.W) + self.b)
        self.y_pred = T.argmax(self.p, axis=1)
        # parameter array
        self.params = [self.W, self.b]
        self.cost = self.negative_log_likelihood
        
    def negative_log_likelihood(self):
        '''An error function
        y: the true class labels
        '''
        return -T.mean(T.log(self.p)[T.arange(self.y.shape[0]), self.y])
    
    def errors(self):
        '''Calculate the error rate in a minibatch
        Assume that the class labels are integers
        '''
        return T.mean(T.neq(self.y, self.y_pred))

    def accuracy(self):
        return 1.0-self.errors()
    
    def updates(self, learning_rate):
        '''return parameters to update after each iteration 
        '''
        # gradients
        cost = self.negative_log_likelihood()
        g_W = T.grad(cost=cost, wrt=self.W)
        g_b = T.grad(cost=cost, wrt=self.b)
        return [(self.W, self.W - learning_rate*g_W),
                (self.b, self.b - learning_rate*g_b)]
      

class HiddenLayer(object):
    def __init__(self, input, n_in, n_out, W=None, b=None,
                 activation=T.tanh):
        self.input = input
        # parameter initialization
        if W is None:
            W_values = np.random.uniform(low=-np.sqrt(6.0/(n_in + n_out)),
                                         high=np.sqrt(6.0/(n_in + n_out)),
                                         size=(n_in, n_out))
            W_values = W_values.astype(theano.config.floatX)
            if activation == T.nnet.sigmoid:
                W_values *= 4
            W = theano.shared(value=W_values, name='W', borrow=True)
        
        if b is None:
            b_values = np.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)
        
        self.W = W
        self.b = b
        self.params = [self.W, self.b]

        output_sum = T.dot(input, self.W) + self.b
        if activation is None:
            self.output = output_sum
        else:
            self.output = activation(output_sum)
            
class MLP(object):
    def __init__(self, x, n_in, n_hidden, n_out,
                 L1_reg=0.0, L2_reg=0.0001):
        self.x = x
        self.L1_reg = L1_reg
        self.L2_reg = L2_reg
        # hidden layer and top layer
        self.hiddenLayer = HiddenLayer(input=x, 
                    n_in=n_in, n_out=n_hidden, activation=T.tanh)
        self.topLayer = LogisticRegression(x=self.hiddenLayer.output,
                    n_in=n_hidden, n_out=n_out)
        # L1 and L2 regularization
        self.L1 = (abs(self.hiddenLayer.W).sum() +
                   abs(self.topLayer.W).sum())
        self.L2_sqr = ((self.hiddenLayer.W**2).sum() +
                       (self.topLayer.W**2).sum())
        self.negative_log_likelihood = self.topLayer.negative_log_likelihood
        self.params = self.hiddenLayer.params + self.topLayer.params
        self.errors = self.topLayer.errors
        self.y = self.topLayer.y

    def cost(self):
        return (self.negative_log_likelihood() + 
                self.L1_reg*self.L1 + self.L2_reg*self.L2_sqr)
    
    def updates(self, learning_rate):
        g_params = [T.grad(cost=self.cost(), wrt=param)
                    for param in self.params]
        return [(param, param - learning_rate*g_param)
                for (param, g_param) in zip(self.params, g_params)]
    
class LeNetConvPoolLayer(object):
    def __init__(self, input, filter_shape, image_shape, pool_size=(2, 2)):
        '''input: of shape image_shape
        filter_shape: (number of filters, number of input feature maps,
                        filter height, filter width)
        image_shape: (minibatch size, number of feature maps,
                        image height, image width)
        '''
        self.input = input
        # fan_in is filter height * filter width * number of input feature maps
        fan_in = np.prod(filter_shape[1:])
        # fan_out is number of filters * filter height * filter width / poolsize
        fan_out = filter_shape[0]*np.prod(filter_shape[2:])/np.prod(pool_size)
        # initialize weights
        w_bound = np.sqrt(6.0/(fan_in + fan_out))
        self.W = theano.shared(value=np.random.uniform(low=-w_bound, high=w_bound,
                               size=filter_shape).astype(theano.config.floatX),
                            borrow=True)
        self.b = theano.shared(value=np.zeros((filter_shape[0],),
                                              dtype=theano.config.floatX),
                            borrow=True)
        # convolve the input image with the filters
        conv_out = conv.conv2d(input=input,
                               filters=self.W,
                               filter_shape=filter_shape,
                               image_shape=image_shape)
        # downsample the output feature maps
        if pool_size[0] > 1:
            pooled_out = downsample.max_pool_2d(input=conv_out,
                                            ds=pool_size,
                                            ignore_border=True)
        else:
            pooled_out = conv_out
        self.output = T.tanh(pooled_out + self.b.dimshuffle('x', 0, 'x', 'x'))
        self.params = [self.W, self.b]
        
class LeNet5Simple(object):
    def __init__(self, x, batch_size, n_filters=[20, 50]):
        self.x = x
        layer0 = LeNetConvPoolLayer(input=x.reshape((batch_size, 1, 28, 28)),
                                         filter_shape=(n_filters[0], 1, 5, 5),
                                         image_shape=(batch_size, 1, 28, 28),
                                         pool_size=(2, 2))
        layer1 = LeNetConvPoolLayer(input=layer0.output,
                                         filter_shape=(n_filters[1], n_filters[0], 5, 5),
                                         image_shape=(batch_size, n_filters[0], 12, 12),
                                         pool_size=(2, 2))
        # flatten the output of Layer 1
        layer2 = HiddenLayer(input=layer1.output.flatten(2),
                                  n_in=n_filters[1]*4*4,
                                  n_out=500,
                                  activation=T.tanh)
        # last layer
        topLayer = LogisticRegression(x=layer2.output,
                                         n_in=500,
                                         n_out=10)
        self.y = topLayer.y
        # cost function
        self.cost = topLayer.negative_log_likelihood
        # loss function
        self.errors = topLayer.errors
        
        self.params = topLayer.params + layer2.params + layer1.params + layer0.params
        
    def updates(self, learning_rate):
        g_params = T.grad(cost=self.cost(), wrt=self.params)
        return [(param, param - learning_rate*g_param)
                for param, g_param in zip(self.params, g_params)]
    
class LeNetConvPool1DLayer(object):
    def __init__(self, input, filter_shape, image_shape, pool_size=(2,)):
        '''
        filter_shape: (number of filters, number of input feature maps, filter length)
        image_shape: (minibatch size, number of feature maps, image length)
        '''
        self.input = input
        fan_in = np.prod(filter_shape[1:])
        fan_out = filter_shape[0]*filter_shape[2]/pool_size[0]
        # initialize weights
        w_bound = np.sqrt(6.0/(fan_in + fan_out))
        self.W = theano.shared(value=np.random.uniform(low=-w_bound, high=w_bound,
                                   size=filter_shape).astype(theano.config.floatX))
        self.b = theano.shared(value=np.zeros((filter_shape[0],),
                                              dtype=theano.config.floatX))
        # convolution
        conv_out = conv.conv()
        
class LeNet5Seq(object):
    def __init__(self, x, batch_size, seqlen, alphabet_size, 
                 n_filters=[40, 80, 100], filter_shapes=[5, 5, 5],
                 pool_sizes=[2, 2, 1],
                 hidden_sizes=[20]):
        '''
        x: shape (minibatch size, alphabet size, sequence length, 1)
        '''
        self.x = x
        layer0 = LeNetConvPoolLayer(input=x,
                                    filter_shape=(n_filters[0], alphabet_size, filter_shapes[0], 1),
                                    image_shape=(batch_size, alphabet_size, seqlen, 1),
                                    pool_size=(pool_sizes[0], 1))
        out_size0 = int((seqlen - filter_shapes[0] + 1)/pool_sizes[0])
        layer1 = LeNetConvPoolLayer(input=layer0.output,
                                    filter_shape=(n_filters[1], n_filters[0], filter_shapes[1], 1),
                                    image_shape=(batch_size, n_filters[0], out_size0, 1),
                                    pool_size=(pool_sizes[1], 1))
        out_size1 = int((out_size0 - filter_shapes[1] + 1)/pool_sizes[1])
        layer2 = LeNetConvPoolLayer(input=layer1.output,
                                    filter_shape=(n_filters[2], n_filters[1], filter_shapes[2], 1),
                                    image_shape=(batch_size, n_filters[1], out_size1, 1),
                                    pool_size=(pool_sizes[2], 1))
        out_size2 = int((out_size1 - filter_shapes[2] + 1)/pool_sizes[2])
        # hidden layer
        out_size3 = hidden_sizes[0]
        layer3 = HiddenLayer(input=layer2.output.flatten(2),
                             n_in=n_filters[2]*out_size2,
                             n_out=out_size3,
                             activation=T.tanh)
        topLayer = LogisticRegression(x=layer3.output,
                                      n_in=out_size3,
                                      n_out=2)
        self.y = topLayer.y
        self.cost = topLayer.negative_log_likelihood
        self.errors = topLayer.errors
        self.params = topLayer.params + layer3.params + layer2.params + layer1.params + layer0.params
        
    def updates(self, learning_rate):
        g_params = T.grad(cost=self.cost(), wrt=self.params)
        return [(param, param - learning_rate*g_param)
                for param, g_param in zip(self.params, g_params)]
    
    
class AutoEncoder(object):
    def __init__(self, input, n_visible, n_hidden, 
                 corruption_level=0.3, learning_rate=0.1,
                 W=None, b_hid=None, b_vis=None):
        self.n_visible = n_visible
        self.n_hidden = n_hidden
        
        if not W:
            W_bound = 4.0*np.sqrt(6.0/(n_visible + n_hidden))
            W_values = np.random.uniform(low=-W_bound, high=W_bound,
                                            size=(n_visible, n_hidden))
            W_values = W_values.astype(theano.config.floatX)
            W = theano.shared(value=W_values, name='W', borrow=True)
        if not b_vis:
            b_vis = theano.shared(value=np.zeros(n_visible, dtype=theano.config.floatX),
                                 borrow=True)
        if not b_hid:
            b_hid = theano.shared(value=np.zeros(n_hidden, dtype=theano.config.floatX),
                                 borrow=True)
        
        self.theano_rng = T.shared_randomstreams.RandomStreams(np.random.randint(2**30))
        self.W = W
        self.b = b_hid
        self.b_prime = b_vis
        self.W_prime = W.T
        
        if input is None:
            self.x = T.matrix(name='x')
        else:
            self.x = input
            
        self.params = [self.W, self.b, self.b_prime]
        # cost function
        self.corruption_level = corruption_level
        tilde_x = self.get_corrupted_input(self.x, self.corruption_level)
        hidden = self.get_hidden_values(tilde_x)
        z = self.get_reconstructed_input(hidden)
        self.z = z
        
        L = -T.sum(self.x*T.log(z) + (1 - self.x)*T.log(1 - z), axis=1)
        self.cost = T.mean(L)
        # updates
        g_params = T.grad(cost=self.cost, wrt=self.params)
        self.learning_rate = learning_rate
        self.updates = [(param, param - self.learning_rate*g_param)
                   for param, g_param in zip(self.params, g_params)]
   
    def transform(self, input):
        # transform from input to output
        ShowInfo('CompileFunction', 'transform')
        _transform = theano.function(inputs=[],
                                          name='transform',
                                          outputs=self.z,
                                          updates=None,
                                          givens={self.x: input})
        return _transform()
    
    def get_hidden_values(self, input):
        return T.nnet.sigmoid(T.dot(input, self.W) + self.b)
    
    def get_reconstructed_input(self, hidden):
        return T.nnet.sigmoid(T.dot(hidden, self.W_prime) + self.b_prime)
    
    def get_corrupted_input(self, input, corruption_level):
        return self.theano_rng.binomial(size=input.shape, n=1,
                                        p=1 - corruption_level,
                                        dtype=theano.config.floatX)*input
    
    def save_image(self, data, outfile):
        from PIL import Image
        from training import TileImages
        
        
    def train(self, train_set_x, batch_size=20, n_epochs=15, callback_epoch=None):
        index = T.lscalar('index')
        # compute the number of minibatches
        n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size

        ShowInfo('CompileFunction', 'train_model')
        train_model = theano.function(inputs=[index],
                                      name='train_model',
                                      outputs=[self.cost],
                                      updates=self.updates,
                                      givens={self.x: train_set_x[index*batch_size:(index + 1)*batch_size]})
                              
        start_time = time.clock()
        for epoch in xrange(n_epochs):
            costs = []
            for batch_index in xrange(n_train_batches):
                cost = train_model(batch_size)
                costs.append(cost)
            if callback_epoch:
                callback_epoch(self, epoch)

            ShowInfo('TrainModel', 'epoch %d, cost %.4f'%(epoch, np.mean(costs)))
        end_time = time.clock()
        total_time = end_time - start_time
        ShowInfo('Summary',
                 'corruption_level %.1f%%, total time %.1fs, %.2fs/epoch'%(self.corruption_level*100,
                                                                           total_time,
                                                                           total_time/n_epochs))