import theano
import theano.tensor as T
import numpy as np
import cPickle, gzip, os, itertools, time
from ptools import ShowInfo

def CreateSharedDataset(dataset):
    train_set, valid_set, test_set = dataset
    def shared_dataset(data_xy, borrow=True):
        x, y = data_xy
        # store the dataset as floats
        shared_x = theano.shared(x, borrow=borrow)
        shared_y = theano.shared(y, borrow=borrow)
        # cast the class labels to integers
        return shared_x, T.cast(shared_y, 'int32')
    
    train_set_x, train_set_y = shared_dataset(train_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    test_set_x, test_set_y = shared_dataset(test_set)
    
    return (train_set_x, train_set_y,
            valid_set_x, valid_set_y,
            test_set_x, test_set_y)

def CreateSharedDataset(data, borrow=True):
    shared_data = {}
    shared_data['train_set_x'] = theano.shared(data['train_set_x'], borrow=borrow)
    shared_data['valid_set_x'] = theano.shared(data['valid_set_x'], borrow=borrow)
    shared_data['test_set_x'] = theano.shared(data['test_set_x'], borrow=borrow)
    shared_data['train_set_y'] = T.cast(theano.shared(data['train_set_y'], borrow=borrow), 'int32')
    shared_data['valid_set_y'] = T.cast(theano.shared(data['valid_set_y'], borrow=borrow), 'int32')
    shared_data['test_set_y'] = T.cast(theano.shared(data['test_set_y'], borrow=borrow), 'int32')
    
    return shared_data

def LoadMNISTFromPkl(fname='data/mnist.pkl.gz'):
    ShowInfo('LoadData', fname)
    f = gzip.open(fname, 'rb')
    data = cPickle.load(f)
    f.close()
    data_array = []
    for i in xrange(len(data)):
        dataset_array = []
        for j in xrange(len(data[i])):
            dataset_array.append(np.asarray(data[i][j], dtype=theano.config.floatX))
        data_array.append(dataset_array)
    return data_array

def LoadMNIST(fname='data/mnist.npz'):
    ShowInfo('LoadMNIST', fname)
    data = {name:value for name, value in np.load(fname).iteritems()}
    return data

def TransformMNIST(data, fname='data/mnist.transformed.npz', offset=7, rotation=30, scale=0.5):
    import itertools
    ShowInfo('TransformMNIST', 'offset=%d, rotation=%.1f, scale=%.1f'%(offset, rotation, scale))
    coords = np.asarray(tuple(itertools.product(xrange(28), xrange(28))), dtype='int32')
    def transform(x, offset, rotation, scale):
        offset = np.random.randint(low=-offset, high=offset, size=2)
        rotation = np.random.uniform(low=-rotation, high=rotation)*2.0*np.pi/360
        scale = np.random.uniform(low=scale, high=1.0)
        new_coords = (coords - 14.5)*scale
        rot_m = np.array([[np.cos(rotation), np.sin(rotation)],
                               [-np.sin(rotation), np.cos(rotation)]])
        new_coords = np.dot(rot_m, (coords.T - 14.5)) + 14.5
        new_coords = (new_coords.T + offset).astype('int32')
        new_coords_ind = np.all((new_coords >= 0) & (new_coords < 28), axis=1)
        new_x = np.zeros((28, 28))
        new_coords_x, new_coords_y = new_coords[new_coords_ind].T
        coords_x, coords_y = coords[new_coords_ind].T
        new_x[new_coords_x, new_coords_y] = x.reshape((28, 28))[coords_x, coords_y]
        return new_x.reshape((28*28,))
    
    ind = np.arange(28, dtype='int32')
    transformed = {}
    for i in xrange(len(data)):
        for j in xrange(len(data[i][0])):
            """
            x_offset, y_offset = np.random.randint(low=-offset, high=offset, size=2)
            x = data[i][0][j].reshape((28, 28))
            x = x[:, np.mod(ind + x_offset, 28)]
            x = x[np.mod(ind + y_offset, 28)]
            if x_offset > 0:
                x[:, :x_offset] = 0
            elif x_offset < 0:
                x[:, 28-x_offset:] = 0
            if y_offset > 0:
                x[:y_offset] = 0
            elif y_offset < 0:
                x[28-y_offset:] = 0
            x = x.reshape((28*28,))
            """
            data[i][0][j] = transform(data[i][0][j], offset, rotation, scale)
    np.savez('data/mnist.shifted.npz', train_set_x=data[0][0])
    return data

def GenNormalData(size, fname='data/normal.10.npz',
                  fold=5, dim=500, n_classes=10):
    ShowInfo('GenNormalData', 'size %d, dim %d, classes %d'%(size, dim, n_classes))
    if os.path.isfile(fname):
        npzfile = np.load(fname)
        return ((npzfile['train_set_x'], npzfile['train_set_y']),
                (npzfile['valid_set_x'], npzfile['valid_set_y']),
                (npzfile['test_set_x'], npzfile['test_set_y']))
    
    size_class = size/n_classes
    x = np.random.randn(size, dim).astype(config.theano.floatX)
    y = np.zeros(size, dtype='int64')
    for i in xrange(n_classes):
        mu = 10*np.random.rand(dim)
        sigma = 5*np.random.rand()+1.0
        x[i*size_class:(i+1)*size_class] = sigma*x[i*size_class:(i+1)*size_class] + mu
        y[i*size_class:(i+1)*size_class] = i
    # shuffle the dataset
    order = np.random.permutation(size)
    x = x[order]
    y = y[order]
    
    train_set = (x[0:size*fold/(fold+2)], y[0:size*fold/(fold+2)])
    valid_set = (x[size*fold/(fold+2):size*(fold+1)/(fold+2)], y[size*fold/(fold+2):size*(fold+1)/(fold+2)])
    test_set = (x[size*(fold+1)/(fold+2):], y[size*(fold+1)/(fold+2):])
    
    np.savez(fname,
            train_set_x=train_set[0], train_set_y=train_set[1],
            valid_set_x=valid_set[0], valid_set_y=valid_set[1],
            test_set_x=test_set[0], test_set_y=test_set[1])
    #cPickle.dump((train_set, valid_set, test_set), open('data/normal.10.pkl', 'wb'))
    return (train_set, valid_set, test_set)

def GenSeqWithMotif(seqlen, motif_len, size, alphabet):
    # random sequences
    seqs = np.random.choice(alphabet, size=seqlen*size).reshape(size, seqlen)
    # generated PWM
    pwm = np.zeros((motif_len, len(alphabet)))
    motif = np.random.randint(low=0, high=len(alphabet), size=motif_len)
    for i in xrange(motif_len):
        p_major = np.random.uniform(low=0.80, high=0.99)
        p_minor = (1.0 - p_major)/(len(alphabet) - 1)
        pwm[i] = p_minor
        pwm[i][motif[i]] = p_major
    
    # insert motifs into sequences
    motif_pos = np.random.randint(seqlen - motif_len, size=size)
    motif_seqs = np.empty((size, motif_len), dtype='S1')
    for i in xrange(motif_len):
        motif_seqs[:, i] = np.random.choice(alphabet, size=size, p=pwm[i])
    for i in xrange(size):
        seqs[i][motif_pos[i]:(motif_pos[i] + motif_len)] = motif_seqs[i]
        
    return (motif, pwm, seqs)
        
def CreateSeqData(seqlen=50, motif_len=8, size=70000, alphabet='ATCG',
                  prefix = 'data/sequences', pos_seqfile=None):
    datafile = prefix + '.npz'
    if os.path.isfile(datafile):
        return np.load(datafile)
    
    alphabet = np.asarray(list(alphabet))
    size_class = size/2
    x = np.zeros((size, len(alphabet), seqlen, 1), dtype='int8')
    y = np.zeros((size,), dtype='int32')
    """
    # random sequences
    seqs = np.random.choice(alphabet, size=seqlen*size).reshape(size, seqlen)
    # generated PWM
    pwm = np.zeros((motif_len, len(alphabet)))
    motif = np.random.randint(low=0, high=len(alphabet), size=motif_len)
    for i in xrange(motif_len):
        p_major = np.random.uniform(low=0.80, high=0.99)
        p_minor = (1.0 - p_major)/(len(alphabet) - 1)
        pwm[i] = p_minor
        pwm[i][motif[i]] = p_major
    
    # insert motifs into sequences
    motif_pos = np.random.randint(seqlen - motif_len, size=size)
    motif_seqs = np.empty((size_class, motif_len), dtype='S1')
    for i in xrange(motif_len):
        motif_seqs[:, i] = np.random.choice(alphabet, size=size_class, p=pwm[i])
    for i in xrange(size_class):
        seqs[i][motif_pos[i]:(motif_pos[i] + motif_len)] = motif_seqs[i]
    """
    if pos_seqfile:
        pos_seqs = []
        with open(pos_seqfile, 'r') as f:
            for line in f:
                seq = line.strip()
                pos_seqs.append(np.asarray(list(seq)).reshape(1, len(seq)))
        pos_seqs = np.concatenate(pos_seqs)
    else:
        motif, pwm, pos_seqs = GenSeqWithMotif(seqlen, motif_len, size_class, alphabet)
        np.savetxt(prefix + '.pwm.txt', pwm)
        open(prefix + '.motif.txt', 'w').write(''.join(alphabet[motif]) + '\n')
    
    # random sequences
    neg_seqs = np.random.choice(alphabet, size=seqlen*size_class).reshape(size_class, seqlen)
    seqs = np.concatenate([pos_seqs, neg_seqs])
    
    # prepare dataset for training
    x = np.zeros((size, len(alphabet), seqlen, 1),
                 dtype=theano.config.floatX)
    for i in xrange(size):
        for j in xrange(len(alphabet)):
            x[i][j][seqs[i] == alphabet[j]] = 1
    y[:size_class] = 1
    rand_indices = np.random.permutation(size)
    x = x[rand_indices]
    y = y[rand_indices]
    seqs = seqs[rand_indices]
    
    with open(prefix + '.txt', 'w') as f:
        for i in xrange(size):
            f.write(''.join(seqs[i]) + '\n')
    # write to fasta files
    ffasta_pos = open(prefix + '.pos.fa', 'w')
    ffasta_neg = open(prefix + '.neg.fa', 'w')
    ffasta = open(prefix + '.fa', 'w')
    for i in xrange(size):
        seq = ''.join(seqs[i])
        if y[i] == 1:
            record = '>PO_%08d\n%s\n'%(i+1, seq)
        else:
            record = '>NG_%08d\n%s\n'%(i+1, seq)
        ffasta.write(record)
        if y[i] == 1:
            ffasta_pos.write(record)
        else:
            ffasta_neg.write(record)
    ffasta.close()
    ffasta_neg.close()
    ffasta_pos.close()
    
    train_size = size*5/7
    valid_size = size*6/7
    test_size = size*7/7
    
    data = {}
    #permutate labels
    #y = y[np.random.permutation(size)]
    data['train_set_x'] = x[:train_size]
    data['train_set_y'] = y[:train_size]
    data['valid_set_x'] = x[train_size:valid_size]
    data['valid_set_y'] = y[train_size:valid_size]
    data['test_set_x'] = x[valid_size:]
    data['test_set_y'] = y[valid_size:]
    data['sequences'] = seqs
    np.savez(datafile, **data)
    return data

def TrainSGD(classifier, data,
              learning_rate, batch_size=500, n_epochs=1000,
              maxiter=5000, maxiter_increase=2,
              improvement_threshold=0.995):
    '''data: (train_set_x, train_set_y, valid_set_x, valid_set_y, test_set_x, test_set_y)
    '''
    #train_set_x, train_set_y, valid_set_x, valid_set_y, test_set_x, test_set_y = data
    train_set_x = data['train_set_x']
    train_set_y = data['train_set_y']
    valid_set_x = data['valid_set_x']
    valid_set_y = data['valid_set_y']
    test_set_x = data['test_set_x']
    test_set_y = data['test_set_y']
    
    # symbolic variables
    index = T.lscalar()
    
    ShowInfo('CompileFunction', 'train_model')
    # compile the training, validating and testing functions
    train_model = theano.function(inputs=[index], name='train_model',
                outputs=classifier.cost(),
                updates=classifier.updates(learning_rate),
                givens={classifier.x: train_set_x[index*batch_size:(index+1)*batch_size],
                        classifier.y: train_set_y[index*batch_size:(index+1)*batch_size]})
    ShowInfo('CompileFunction', 'validate_model')
    validate_model = theano.function(inputs=[index], name='validate_model',
                outputs=classifier.errors(),
                givens={classifier.x: valid_set_x[index*batch_size:(index+1)*batch_size],
                        classifier.y: valid_set_y[index*batch_size:(index+1)*batch_size]})
    ShowInfo('CompileFunction', 'test_model')
    test_model = theano.function(inputs=[index], name='test_model',
                outputs=classifier.errors(),
                givens={classifier.x: test_set_x[index*batch_size:(index+1)*batch_size],
                        classifier.y: test_set_y[index*batch_size:(index+1)*batch_size]})
                
    # compute the number of minibatches
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] / batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] / batch_size
    
    stop = False
    epoch = 0
    validation_freq = min(n_train_batches, maxiter/2)
    best_validation_loss = np.inf
    
    start_time = time.clock()
    while (epoch < n_epochs) & (not stop):
        epoch += 1
        for minibatch_index in xrange(n_train_batches):
            avg_cost = train_model(minibatch_index)
            iiter = (epoch - 1)*n_train_batches + minibatch_index
            
            if (iiter + 1)%validation_freq == 0:
                validation_loss = np.mean([validate_model(i) 
                                              for i in xrange(n_valid_batches)])
                ShowInfo('Validation', 'Epoch %d, minibatch %d/%d, error %f %%'%(
                    epoch, minibatch_index + 1, n_train_batches, validation_loss*100))
                
                if validation_loss < best_validation_loss:
                    # the validation score is improved significantly
                    if validation_loss < best_validation_loss*improvement_threshold:
                        maxiter = max(maxiter, iiter*maxiter_increase)
                    best_validation_loss = validation_loss
                    
                    # check test error
                    test_loss = np.mean([test_model(i) for i in xrange(n_test_batches)])
                    ShowInfo('Test', 'Epoch %d, minibatch %d/%d, error %f %%'%(
                        epoch, minibatch_index, n_train_batches, test_loss*100))
            
            if iiter >= maxiter:
                stop = True
                break
    end_time = time.clock()
    ShowInfo('FinalPerformance', 'validation error: %f %%, test error: %f %%'%(
        best_validation_loss*100, test_loss*100))
    ShowInfo('Epochs', '%d epochs in total, %f epochs/s'%(
        epoch, float(epoch)/(end_time - start_time)))
    ShowInfo('TotalTime', '%.1fs'%(end_time - start_time))
    
def NormalizeImage(x):
    return (x - x.min())/(x.max() - x.min() + 1e-8)

def TileImages(x, image_shape, tile_shape, 
               tile_spacing=(0, 0),
               normalize=False):
    out_shape = [(ishp + tsp)*tshp - tsp for ishp, tshp, tsp in
                 zip(image_shape, tile_shape, tile_spacing)]
    H, W = image_shape
    Hs, Ws = tile_spacing
    out_img = np.ones(out_shape, dtype='uint8')
    for tile_row, tile_col in itertools.product(xrange(tile_shape[0]),
                                                xrange(tile_shape[1])):
        img_index = tile_row*tile_shape[0] + tile_col
        if img_index < x.shape[0]:
            img = NormalizeImage(x[img_index].reshape(image_shape))
            out_img[tile_row*(H + Hs):tile_row*(H + Hs) + H,
                    tile_col*(W + Ws):tile_col*(W + Ws) + W] = img*255
    return out_img