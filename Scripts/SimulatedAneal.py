#! /usr/bin/env python

import numpy as np
class SAUpdateSchedule(object):
    pass

class FastSchedule(object):
    def __init__(self, ndims, xmin, xmax, T0, m=1.0, n=1.0, quench=1.0):
        self.ndims = ndims
        self.xmin = xmin
        self.xmax = xmax
        self.T0 = T0
        self.m = m
        self.n = n
        self.quench = quench
        self.k = 0
        self.c = self.m * np.exp(-self.n * self.quench)

    def update_x(self, x, T):
        u = np.random.uniform(0, 1, size=self.ndims)
        y = np.sign(u - 0.5)*T*((1 + 1.0/T)**abs(2*u - 1) - 1.0)
        xc = y*(self.xmax - self.xmin)
        xnew = x + xc
        return xnew

    def update_temp(self, T):
        T = self.T0*np.exp(-self.c*self.k**(self.quench))
        self.k += 1
        return T

class CauchySchedule(object):
    def __init__(self, ndims, T0, learn_rate=0.5):
        self.ndims = ndims
        self.T0 = T0
        self.learn_rate = learn_rate
        self.k = 0

    def update_x(self, x, T):
        u = np.random.uniform(-np.pi/2, np.pi/2, size=self.ndims)
        xc = self.learn_rate*self.T*np.tan(u)
        xnew = x + xc
        return xnew

    def update_temp(self, T):
        T = self.T0/(1 + self.k)
        self.k += 1
        return T

class BoltzmannSchedule(object):
    def __init__(self, ndims, T0, learn_rate=0.5):
        self.ndims = ndims
        self.T0 = T0
        self.learn_rate = learn_rate
        self.k = 0

    def update_x(self, x, T):
        std = np.mininum(np.sqrt(T)*np.ones(self.ndims),
                         (self.xmax - self.xmin)/3.0/self.learn_rate)
        xc = np.random.normal(0, std, size=self.ndims)
        xnew = x + xc*self.learn_rate
        return xnew

    def update_temp(self, T):
        self.k += 1
        T = self.T0 / np.log(self.k + 1.0)
        return T


def Accept(dE, T):
    if dE < 0:
        return True
    if np.exp(-dE*1.0/T) > np.random.uniform(0.0, 1.0):
        return True
    return False

def SimulatedAnneal(func, x0, xmin, xmax, 
                    schedule='fast', T0=None, Tf=None, maxiter=400, niter_step=50,
                    feps=1e-6):
    # estimate the range of the function
    x0 = np.asarray(x0)
    ndims = x0.shape[0] if len(x0.shape) > 0 else 1
    n_init = 50
    fmin = np.finfo(float).max
    fmax = np.finfo(float).min
    best_x = None
    best_cost = np.Inf
    # estimate the range of the function by random input
    for i in xrange(n_init):
        x = np.random.uniform(xmin, xmax, size=ndims)
        fval = func(x)
        if fval > fmax:
            fmax = fval
        if fval < fmin:
            fmin = fval
            best_cost = fval
            best_x = x
    # determine initial temperature
    if not T0:
        T0 = (fmax - fmin)*1.5
    # initial evaluation
    fval = func(x0)
    last_cost = fval
    last_x = x0.copy()
    if last_cost < best_cost:
        best_cost = last_cost
        best_x = x0.copy()
    T = T0
    if schedule == 'fast':
        schedule = FastSchedule(ndims, xmin, xmax, T0)
    elif schedule == 'cauchy':
        schedule = CauchySchedule(ndims, T0)
    elif schedule == 'boltzmann':
        schedule = BoltzmannSchedule(ndims, T0)
    else:
        raise ValueError('No such schedule: ' + schedule)
    last_values = [100, 300, 500, 700]
    for iters in xrange(maxiter):
        for itemp in xrange(niter_step):
            current_x = schedule.update_x(last_x, T)
            current_cost = func(current_x)
            dE = current_cost - last_cost
            if Accept(dE, T):
                last_x = current_x.copy()
                last_cost = current_cost
                if last_cost < best_cost:
                    best_x = last_x.copy()
                    best_cost = last_cost
        T = schedule.update_temp(T)
        last_values.append(last_cost)
        last_values.pop(0)
        lv = np.asarray(last_values, dtype='float')
        if np.all(np.abs((lv - lv[0])/lv[0]) < feps):
            retval = 0
            break
        if iters == (maxiter - 1):
            retval = 3
            break
    return (best_x, best_cost, retval)

class FixedRateSchedule(object):
    '''for feature selection
    randomly change every feature at fixed probability
    '''
    def __init__(self, n_features, n_select, T0, p=None, m=1.0, n=1.0, quench=1.0):
        self.n_features = n_features
        self.n_select = n_select
        self.T0 = T0
        self.p = p if p is not None else min(1.0/n_select, 1.0)
        self.m = m
        self.n = n
        self.quench = quench
        self.c = self.m * np.exp(-self.n * self.quench)
        self.k = 1

    def update_x(self, fs, T):
        mutated = np.random.choice([0, 1], size=self.n_select,
                                   p=[1.0 - self.p, self.p])
        fs = fs.copy()
        fs[mutated] = np.random.choice(self.n_features, size=mutated.sum(),
                               replace=False)
        return fs

class FastTempUpdater(object):
    def __init__(self, T0, m=1.0, n=1.0, quench=1.0):
        self.T0 = T0
        self.m = m
        self.n = n
        self.quench = quench
        self.k = 0
        self.c = m*np.exp(-n*quench)

    def update(self):
        T = self.T0*np.exp(-self.c*self.k**(self.quench))
        self.k += 1
        return T

class CauchyTempUpdater(object):
    def __init__(self, T0):
        self.T0 = T0
        self.k = 0

    def update(self):
        self.k += 1
        return self.T0/(1 + self.k)

class BoltzmannTempUpdater(object):
    def __init__(self, T0):
        self.T0 = T0
        self.k = 0

    def update(self):
        self.k += 1
        return self.T0/np.log(1 + self.k)


def SimulatedAnnealFS(classifier, X_train, y_train, n_select,
                    X_test=None, y_test=None,
                    schedule='fixed_rate', temp_updater='fast',
                    T0=None, Tf=None, maxiter=400, niter_step=100,
                    feps=1e-6):
    '''X: shape of (n_samples, n_features)
    y: array of length n_samples
    classifier: an object that has fit(X, y) and score(X, y) methods (sklearn.estimator like)
    '''
    n_features = X.shape[1]
    print 'n_features={0}, temp_updater={1}'.format(n_features, temp_updater)
    best_fs = None
    best_score = -1
    # determine initial temperature
    if not T0:
        T0 = 1.0*1.5
    
    X_test = X_train if X_test is None else X_test
    y_test = y_train if y_test is None else y_test
    # initial feature set
    fs = np.random.choice(n_features, size=n_select)
    # initial evaluation
    classifier.fit(X_train[:, fs], y_train)
    score = classifier.score(X_train[:, fs], y_train)
    last_score = score
    last_fs = fs.copy()
    if last_score < best_score:
        best_score = last_score
        best_fs = fs.copy()
    T = T0
    # select feature set updating schedule
    if schedule == 'fixed_rate':
        schedule = FixedRateSchedule(n_features, n_select, T0, p=0.01)
    else:
        raise ValueError('No such schedule: ' + schedule)
    # select temperature updater
    if temp_updater == 'fast':
        temp_updater = FastTempUpdater(T0)
    elif temp_updater == 'cauchy':
        temp_updater = CauchyTempUpdater(T0)
    elif temp_updater == 'boltzmann':
        temp_updater = BoltzmannTempUpdater(T0)
    else:
        raise ValueError('No such temperature updater: ' + temp_updater)
    
    last_values = [-i for i in xrange(1, 6)]
    for iters in xrange(maxiter):
        for itemp in xrange(niter_step):
            current_fs = schedule.update_x(last_fs, T)
            X_train_fs = X_train[:, current_fs]
            classifier.fit(X_train_fs, y_train)
            current_score = classifier.score(X_train_fs, y_train)
            dE = last_score - current_score
            if Accept(dE, T):
                last_fs = current_fs.copy()
                last_score = current_score
                if last_score > best_score:
                    best_fs = last_fs.copy()
                    best_score = last_score
        T = temp_updater.update()
        last_values.append(last_score)
        last_values.pop(0)
        lv = np.asarray(last_values, dtype='float')

        classifier.fit(X_train[:, last_fs], y_train)
        score_test = classifier.score(X_test[:, last_fs], y_test)
        print 'T={0}, train_score={1}, test_score={2}, fs={3}'.format(T, 
                last_score, score_test, repr(last_fs[:4]))
        if np.all(np.abs((lv - lv[0])/lv[0]) < feps):
            retval = 0
            break
        if iters > maxiter:
            retval = 3
            break
    return (best_fs, best_score, retval)


def LoadKmerMatrix(param_file, kmermat_file):
    import json
    import h5py
    p = None
    with open(param_file, 'r') as f:
        p = json.load(f)
    groups = [info['Group'] for info in p['SampleInfo']]
    print 'LoadKmerMatrix', kmermat_file
    h5file = h5py.File(kmermat_file, 'r')
    X = h5file['data'][:].T
    h5file.close()
    return X, np.asarray(groups)

if __name__ == '__main__':
    '''
    func = lambda x: np.cos(14.5 * x - 0.3) + (x + 0.2) * x
    func = lambda x: np.exp(-np.power((x-1.0),2.0))*np.sin(32*x)
    for schedule in ('fast', 'cauchy', 'boltzmann'):
        print schedule, SimulatedAnneal(func, 1.0, xmin=-3.0, xmax=3.0, schedule='fast')
    '''
    import os
    os.chdir(os.path.join(os.environ['HOME'], 'projects', 'kmer'))
    # test feature selection
    import sklearn
    from sklearn.datasets import make_classification
    from sklearn.cross_validation import KFold
    
    simulate_data = True
    if simulate_data:
        X, y = make_classification(n_samples=600, n_features=10000,
            n_informative=10, n_redundant=1000, n_repeated=7000,
            random_state=np.random.RandomState(1340))
    else:
        X, y = LoadKmerMatrix('KmerMarker/32/Param/SelectKmerCV.json',
            'KmerMarker/32/KmerMatrix/Normal-Stage_0/Top_10000/MannWhitneyUTest/outer/1.h5')

    n_samples = X.shape[0]
    n_features = X.shape[1]

    n_train = int(n_samples*0.9)
    
    random_state_old = np.random.get_state()
    samples_train, samples_test = list(KFold(n_samples, n_folds=2, random_state=1396))[0]
    X_train = X[samples_train, :]
    y_train = y[samples_train]
    X_test = X[samples_test, :]
    y_test = y[samples_test]
    classifier = 'LogRegL2'
    for schedule in ('fixed_rate',):
        if classifier == 'LogRegL2':
            from sklearn.linear_model import LogisticRegression
            classifier = LogisticRegression(penalty='l2')
        elif classifier == 'MLP':
            from sklearn.neural_network import MLPClassifier
            classifier = MLPClassifier(hidden_layer_sizes=(50,))
        else:
            raise ValueError('No such classifier: ' + classifier)
        result = SimulatedAnnealFS(classifier, X_train, y_train, n_select=200,
                X_test=X_test, y_test=y_test, temp_updater='fast')
        print schedule, result
        classifier.fit(X_train, y_train)
        score_train = classifier.score(X_train, y_train)
        score_test = classifier.score(X_test, y_test)
        print 'all features: score_train={0}, score_test={1}'.format(score_train, score_test)


