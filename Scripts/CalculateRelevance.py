#! /usr/bin/env python
import sys, argparse, os
import prettylog as logger

def _DeferedImport():
    '''Accelerate argument parsing
    '''
    global json, math, np, pearsonr, h5py
    import json, math
    import numpy as np
    from scipy.stats import pearsonr
    import h5py

def MakeDir(pathname):
    try:
        os.makedirs(pathname)
        print >>sys.stderr, 'MakeDir: ' + pathname
    except OSError as e:
        if e.errno == 17:
            pass
        else:
            print >>sys.stderr, 'Failed to create: ' + pathname

def CalculateRelevance(m, samples, groups, metric):
    '''m: (n_samples, n_kmers)
    returns: relevance with shape (m.shape[1],)
    '''
    from _relevance import Correlation, MannWhitneyUTest
    m = m[samples, :]
    r = None
    if metric == 'Correlation':
        r = Correlation(m, groups)
    elif metric == 'MannWhitneyUTest':
        r = MannWhitneyUTest(m, groups)
        group1 = np.nonzero(groups == -1)[0]
        group2 = np.nonzero(groups == 1)[0]
        #for i in xrange(10):
        #    print (m[group1, i], m[group2, i], r[i])
    else:
        raise ValueError('Invalid relevance metric: ' + metric)

    return r

def InferHashSize(kmerfile):
    h5f = h5py.File(kmerfile, 'r')
    hashsize = h5f['data'].shape[0]
    h5f.close()
    return hashsize

def GetTaskRange(hashsize, n_tasks, taskid):
    unitsize = int(math.ceil(float(hashsize)/n_tasks))
    start = unitsize*taskid
    end = min(hashsize, start + unitsize)
    return (start, end)

def CalculateRelevanceTask(param_file, n_tasks, taskid):
    p = json.load(open(param_file, 'r'))
    groupset = (-1, 1)
    kmerfiles = []
    groups = []
    for info in p['SampleInfo']:
        kmerfiles.append(info['KmerCountFile'])
        groups.append(info['Group'])
    groups = np.asarray(groups, dtype='int')
    n_samples = len(kmerfiles)
    # detect number of kmers
    hashsize = InferHashSize(kmerfiles[0])
    logger.info('HashSize', hashsize)

    start, end = GetTaskRange(hashsize, n_tasks, taskid)
    n_kmers = end - start

    logger.info('TaskRange', '%d (%d, %d)'%(n_kmers, start, end))

    logger.info('ReadKmerCount', 'started')
    m = np.zeros((n_samples, n_kmers), dtype='float32')
    for i in xrange(n_samples):
        f = h5py.File(kmerfiles[i], 'r')
        m[i, :] = f['data'][start:end].astype('float32')/p['AverageCoverage'][i]
        f.close()
    logger.info('ReadKmerCount', 'finished')

    for params in p['Params']:
        samples_task = np.asarray(params['TrainIndex'])
        groups_task = groups[samples_task]
        metric = params['FilterMetric']
        relevance = CalculateRelevance(m, samples_task, groups_task, metric)
        rele_file = '%s_%d'%(params['RelevanceFile'], taskid)
        logger.info('WriteRelevanceFile', rele_file)
        MakeDir(os.path.dirname(rele_file))
        f = h5py.File(rele_file, 'w')
        f.create_dataset('data', data=relevance)
        f.close()

def MergeRelevance(param_file, n_tasks):
    p = json.load(open(param_file, 'r'))
    hashsize = InferHashSize(p['SampleInfo'][0]['KmerCountFile'])
    for param in p['Params']:
        relevance = np.zeros(hashsize, dtype='float32')
        for taskid in xrange(n_tasks):
            start, end = GetTaskRange(hashsize, n_tasks, taskid)
            rele_file = '%s.task_%d'%(params['RelevanceFile'], taskid)
            f = h5py.File(rele_file, 'r')
            f['data'].read_direct(relevance, np._s[0:(end - start)], np._s[start:end])
            f.close()
        f = h5py.File(params['RelevanceFile'], 'w')
        f.create_dataset('data', data=relevance)
        f.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub-commands')

    parser_calc = subparsers.add_parser('calc',
            help='calculate relevance from k-mer counts')
    parser_calc.add_argument('param_file', type=str)
    parser_calc.add_argument('n_tasks', type=int)
    parser_calc.add_argument('taskid', type=int,
            help='0-based task id between 0 and (n_tasks - 1)')

    parser_merge = subparsers.add_parser('merge', 
            help='Merge relevance files from separate tasks into a single file')
    parser_merge.add_argument('param_file', type=str)
    parser_merge.add_argument('n_tasks', type=int)

    args = parser.parse_args()

    _DeferedImport()
    if args.subcommand == 'calc':
        CalculateRelevanceTask(args.param_file, args.n_tasks, args.taskid)
    elif args.subcommand == 'merge':
        MergeRelevance(args.param_file, args.n_tasks)


