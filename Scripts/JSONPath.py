#! /usr/bin/env python

import json
import argparse

def JSONPath(obj, path, filename):
    
    if path.startswith('/'):
        for name in path.split('/')[1:]:
            if isinstance(obj, dict):
                if not name in obj:
                    raise ValueError('No such path: ' + path)
                obj = obj[name]
            elif isinstance(obj, list):
                index = int(name)
                if index > len(obj):
                    raise ValueError('No such path: ' + path)
                obj = obj[int(name)]
            else:
                raise ValueError('No such path: ' + path)
        return obj
    else:
        if path == 'FileName':
            return filename
     
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', metavar='FILE', nargs='+', type=str,
                        help='JSON files')
    parser.add_argument('-p', '--paths', type=str)
    args = parser.parse_args()
    
    paths = args.paths.split(':')
    for jsonfile in args.infiles:
        obj = json.load(open(jsonfile, 'r'))
        for path in paths:
            print path, JSONPath(obj, path, jsonfile)
            