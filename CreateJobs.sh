#! /bin/bash

. Configuration.sh

check_bool(){
    if [ -z "$1" ];then
        return 1
    elif [ "$1" -eq 1 ];then
        return 0
    elif [ "$1" -eq 0 ];then
        return 1
    else
        echo "Error: varible value is not valid"
        return 1
    fi
}

create_jobscript_array(){
    local enable_array=${enable_array:=1}
    JobName=$1
    [ -d "$LogDir/$JobName" ] || mkdir -p "$LogDir/$JobName"
    {
        printf -- "-e\ns/{JobName}/$JobName/\n"
        printf -- "-e\ns/{LogDir}/$LogDir/\n"
        if [ -n "$s_wd" ];then
            printf -- "-e\n1 a #\$ -wd $s_wd\n"
        else
            printf -- "-e\n1 a #\$ -cwd\n"
        fi
        if [ "$enable_array" -eq 1 ];then
            printf -- "-e\ns#{TaskFile}#$JobDir/${JobName}.tasklist.txt#\n"
            printf -- "-e\ns/{NumJobs}/$NPROC/\n"
        fi
        if [ -n "$s_array" ];then printf -- "-e\n1 a #\$ -t 1-$s_array\n";fi
        if [ -n "$s_memory" ];then printf -- "-e\n1 a #\$ -l mem_req=$s_memory,s_vmem=$s_memory\n";fi
        if [ -n "$s_slots" ];then printf -- "-e\n1 a #\$ -pe def_slot $s_slots\n";fi
        if [ "$enable_array" -eq 1 ];then
            printf -- "$TemplateDir/qsub/array.sh"
        else
            if [ -n "$s_array" ];then
                printf -- "$TemplateDir/qsub/array_no_taskfile.sh"
            else
                printf -- "$TemplateDir/qsub/default.sh"
            fi
        fi
    } | xargs -d'\n' sed > ${JobDir}/${JobName}.qsub.sh
    # read commands from standard input for non-array jobs
    if [ "$enable_array" -eq 0 ];then cat >> ${JobDir}/${JobName}.qsub.sh;fi
    echo "Job script: ${JobDir}/${JobName}.qsub.sh"
}

create_jobscript_array_noexec(){
    JobName=$1
    sed -e "s/{JobName}/$JobName/" \
        -e "s/{NumJobs}/$NPROC/" \
        -e "s/{LogDir}/$LogDir/" \
        $TemplateDir/qsub/array_noexec.sh > $JobDir/$JobName.qsub.sh
}
create_jobscript_mpi(){
    JobName=$1
    sed -e "s/{JobName}/$JobName/" \
        -e "s/{NumJobs}/$NPROC/" \
        -e "s/{LogDir}/$LogDir/" \
        $TemplateDir/qsub/mpi.sh > $JobDir/$JobName.qsub.sh
}

create_jobscript(){
    enable_array=0 create_jobscript_array $1
}

job_GetAverageCoverage(){
    local count_type=${count_type:=default}
    JobName=GetAverageCoverage.$K
    if [ "$count_type" = "default" ];then
        #s_memory=$(python -c "print (4**$K)*4/(2**30) + 1")G
        printf "K=$K KmerCountsDir=$KmerCountsDir ./CountKmers.sh average_coverage\n" \
            | NPROC=1 create_jobscript $JobName
    elif [ "$count_type" = "kmc" ];then
        printf "K=$K KmerCountsDir=$KmerCountsDir ./CountKmers.sh average_coverage_kmc\n" \
            | NPROC=1 create_jobscript $JobName
    else
        echo "Error: invalid count_type: $count_type"
        exit 1
    fi
}


job_CountKmerExact() {
    local multiple_run=${multiple_run:=0}
    if [ "$multiple_run" -eq 1 ];then
        echo "Enable multiple run"
        : ${min_k?} ${max_k?}
        JobName=CountKmerExact.${min_k}-${max_k}
        {
        for dataid in $(select_dataid);do
            seqfile=$(get_seq_file $dataid)
            for K in $(seq $min_k $max_k);do
                echo "$BINPATH/KmerCounter count $K $seqfile $KmerCountsDir/$dataid/kmer.${K}.counts"
            done
        done
        } > $JobDir/$JobName.tasklist.txt
    else
        JobName=CountKmerExact.$K
        {
        for dataid in $(select_dataid);do
            seqfile=$(get_seq_file $dataid)
            echo "$BINPATH/KmerCounter count $K $seqfile $KmerCountsDir/$dataid/kmer.${K}.counts"
        done
        } > $JobDir/$JobName.tasklist.txt
    fi
    create_jobscript_array $JobName
}

job_CountKmerHashedScaffold() {
    local weighted=${weighted:=1}
    JobName=CountKmerHashedScaffold.$K
    create_jobscript_array $JobName
    {
    for DataId in `select_dataid`;do
        if [ "$weighted" -eq 1 ];then
            echo $BINPATH/KmerCounter hashedcountw $K $HashSize Hyperplanes/$K.$HashSize \
                $ScaffoldDir/$DataId/scaffold.fa \
                $KmerCountsDir/$DataId/AverageCoverage.txt \
                $KmerCountsDir/$DataId/kmer.$K.counts
        else
            echo $BINPATH/KmerCounter hashedcount $K $HashSize Hyperplanes/$K.$HashSize \
                $ScaffoldDir/$DataId/scaffold.fa \
                $KmerCountsDir/$DataId/kmer.$K.counts
        fi
    done
    } > $JobDir/$JobName.tasklist.txt
}

job_CountKmerLSHRawReads(){
    : ${HashSize?:}
    KmerCountsDir=KmerCountLSHRawReads
    JobName=CountKmerLSHRawReads.$K
    create_jobscript_array $JobName
    {
    for dataid in `select_dataid`;do
        seqfile=$(ls $RawReadsDir/$dataid/*.fasta)
        echo $SURUN $BINPATH/KmerCounter hashedcount $K $HashSize Hyperplanes/$K.$HashSize \
            $RawReadsDir/$dataid/$seqfile $KmerCountsDir/kmer.$K.counts
    done
    } > $JobDir/$JobName.tasklist.txt
}

job_CreateKmerMatrix(){
    # maximum memory per task in GB
    local max_memory=${max_memory:=20}
    JobName=CreateKmerMatrix.$K
    local total_memory=$(estimate_memory=1 $BINPATH/SelectKmerCV mat KmerMarker/$K/Param/CreateKmerMatrix.json)
    local task_num=1
    local s_memory=${total_memory}G
    if [ "$total_memory" -gt "$max_memory" ];then
        s_memory=${max_memory}G
        task_num=$(estimate_task_num=1 max_memory=$max_memory $SURUN $BINPATH/SelectKmerCV mat KmerMarker/$K/Param/CreateKmerMatrix.json)
    fi
    NPROC=$task_num s_memory=$s_memory create_jobscript_array $JobName
    {
    for task_id in $(seq 0 $(($task_num - 1)));do
        echo "pe_task_id=$task_id pe_task_num=$task_num $SURUN $BINPATH/SelectKmerCV mat KmerMarker/$K/Param/CreateKmerMatrix.json"
    done
    } > $JobDir/${JobName}.tasklist.txt
}

job_KmerCounterHashedRaw(){
    JobName=KmerCounterRaw.$K
    set -e
    create_jobscript_array $JobName
    {
    for dataid in $(select_dataid);do
        local seqfile=$(ls $RawReadsDir/$dataid/*.fasta)
        echo $BINPATH/KmerCounter hashedcount $K ${HashSize?} Hyperplanes/$K.${HashSize?} \
            $seqfile $KmerCountsDir/$dataid/kmer.$K.counts
    done
    } > $JobDir/$JobName.tasklist.txt
}

job_RemoveLowAbunKmer() {
    JobName=RemoveLowAbunKmer.$K
    MinAbun=2
    create_jobscript_array $JobName
    {
    for DataId in `select_dataid`;do
        echo $BINPATH/KmerFilter removelowabunkmer $K \
            $HashSize Hyperplanes/$K.$HashSize \
            $ScaffoldDir/$DataId/scaffold.fa \
            $KmerCountsDir/$DataId/kmer.$K.MinAbun$MinAbun.counts \
            $MinAbun
    done
    } > $JobDir/$JobName.tasklist.txt
}

job_CrossValidation() {
    : ${param_files?}
    other_options=
    # estimate memory
    local param_file=$(echo $param_files | cut -d' ' -f1)
    local kmermat_file=$($BINPATH/ReadJSON get $param_file /CVOuter/0/KmerMatrixFile)
    local s_memory=$(($(stat -c '%s' $kmermat_file) * 6 / 1073741824 + 3))G
    for cvloop in inner outer;do
        JobName=CrossValidation.$K.$cvloop
        {
        if [ "$PARALLELMAP_ENGINE" = "mpi" ];then
            printf "mpirun -np $NPROC "
        fi
        printf "python CrossValidation.py $other_options --$cvloop --parallel-engine $PARALLELMAP_ENGINE $param_files\n"
        } | s_memory=$s_memory s_array=$NPROC create_jobscript $JobName
    done
}

job_ScaleKmerMatrix(){
    JobName=ScaleKmerMatrix.$K
    echo "python $ScriptDir/ScaleKmerMatrix.py -f KmerMarker/$K/Param/CreateKmerMatrix.json --parallel-engine sge_array" \
    | s_array=$NPROC create_jobscript $JobName
}

job_CrossValidation_array(){
    JobName=CrossValidation.$K
    create_jobscript_array $JobName
    for MiscCond in "Top_5000";do
        Files="$Files $(echo KmerMarker/$K/Param/CV.*.$MiscCond.*.json)"
    done
    {
    for f in $Files;do
        echo "python --parallel-engine python $f"
    done
    } > $JobDir/$JobName.tasklist.txt
}

job_CounterToFilter(){
    min_ratio=${min_ratio?}
    JobName=CounterToFilter.${K}.minr${min_ratio}
    create_jobscript_array $JobName
    {
    for dataid in $(select_dataid);do
        threshold=$(awk "{if(\$1 == \"$dataid\") print \$2}" KmerMarker/$K/CountThreshold/${min_ratio}.txt)
        echo $BINPATH/CounterToFilter $KmerCountsDir/$dataid/kmer.${K}.counts $KmerCountsDir/$dataid/kmer.${K}.minr${min_ratio}.filter $threshold
    done
    } > $JobDir/${JobName}.tasklist.txt
}

job_KmerCountHist(){
    JobName=KmerCountHist.$K
    create_jobscript_array $JobName
    {
    for dataid in $(select_dataid);do
        echo "bincount=10000 binwidth=1 $BINPATH/KmerCounter hist $KmerCountsDir/$dataid/kmer.${K}.counts > $KmerCountsDir/$dataid/kmer.${K}.hist"
    done
    } > $JobDir/${JobName}.tasklist.txt
}

job_CalculateRelevance(){
    n_tasks=${n_tasks:?}
    enable_python=${enable_python:=0}
    JobName=CalculateRelevance.$K
    s_memory=8G create_jobscript_array $JobName
    {
    for taskid in $(seq 0 $(($n_tasks-1)));do
        if [ "$enable_python" -eq 1 ];then
            echo "$SURUN python $ScriptDir/CalculateRelevance.py calc KmerMarker/$K/Param/SelectKmerCV.json $n_tasks $taskid"
        else
            echo "$SURUN $BINPATH/CalculateRelevance calc KmerMarker/$K/Param/SelectKmerCV.json $n_tasks $taskid"
        fi
    done
    } > $JobDir/${JobName}.tasklist.txt
}

job_SelectKmerByRelevance(){
    n_tasks_relevance=${n_tasks_relevance?}
    n_tasks=${n_tasks?}
    JobName=SelectKmerByRelevance.$K
    s_memory=10G create_jobscript_array $JobName
    {
        for taskid in $(seq 0 $((n_tasks-1)));do
            echo "pe_task_id=$taskid pe_task_num=$n_tasks $BINPATH/CalculateRelevance select KmerMarker/$K/Param/SelectKmerCV.json $n_tasks_relevance"
        done
    } > $JobDir/${JobName}.tasklist.txt
}

# get the k-mer existence (0's and 1's) of each sample to the unified k-mer set
job_QueryKmerSet(){
    : ${min_count?}
    overwrite=${overwrite:=0}
    JobName=QueryKmerSet.${K}.ci${min_count}
    s_memory=8G create_jobscript_array $JobName
    {
        for dataid in $(select_dataid);do
            local outfile=KMC/$dataid/kmer_exists.${K}.ci${min_count}
            if [ "$overwrite" -eq 0 ] && [ -f "$outfile" ];then continue;fi
            echo "$SURUN $BINPATH/CreateKmerMatrixFromKMC query KmerSet/$K/ci${min_count}/merged KMC/$dataid/kmer.$K KMC/$dataid/kmer_exists.${K}.ci${min_count}"
        done
    } > $JobDir/${JobName}.tasklist.txt
}

job_CountKmerKMC(){
    memory=${memory:=20}
    threads=${threads:=8}
    # max value of k-mer counts
    if [ "$K" -lt 24 ];then
        max_value=${max_value:=65535}
    else
        max_value=${max_value:=255}
    fi
    JobName=CountKmerKMC.$K
    s_memory=${memory}G s_slots=${threads} create_jobscript_array $JobName
    local seqfile=
    local dataid=
    {
    while read dataid seqfile;do
        echo "$SURUN mkdir -p "KMC/$dataid" KMCTmp/$K/$dataid 2>/dev/null; \
            echo KMC count $dataid; \
            $SURUN kmc -cs$max_value -t$threads -fa -k$K -m$memory \
            $seqfile KMC/$dataid/kmer.${K} KMCTmp/$K/$dataid"
    done < metadata/rawreads_list.txt
    } > $JobDir/${JobName}.tasklist.txt
}

job_FreqHistKMC(){
    : ${min_count?}
    set -e
    OutDir=KmerMarker/$K/Filter/ci${min_count}
    local enable_freq_hist=${enable_freq_hist:=1}
    # memory in GB
    local dataid0=$(select_dataid | head -n 1)
    local nsamples=$(select_dataid | wc -l)
    local nkmers=$($BINPATH/CreateKmerMatrixFromKMC size_query KMC/$dataid0/kmer_exists.$K.ci${min_count})
    local n_tasks=${n_tasks:=60}
    local nkmers_per_task=$(($nkmers / $n_tasks))
    if [ "$enable_freq_hist" -eq 1 ];then
        JobName=FreqHistKMC.$K
        local memory=$(($nkmers_per_task * $nsamples / 8 + $nkmers_per_task * 8))
    else
        max_freq=${max_freq:=0}
        min_freq=$(cat $OutDir/min_freq.txt)
        JobName=FilterKmerKMC.$K
        local memory=$(($nkmers_per_task * $nsamples / 8))
    fi
    memory=$(($memory / 1073741824 + 1))
    s_memory=$(($memory + 1))G create_jobscript_array $JobName
    mkdir -p $OutDir 2>/dev/null
    local filelist=$OutDir/kmer_exists_list.txt
    {
    for dataid in $(select_dataid);do
        echo "KMC/$dataid/kmer_exists.${K}.ci${min_count}"
    done
    } > $filelist
    {
    for task_id in $(seq 0 $(($n_tasks-1)));do
        if [ "$enable_freq_hist" -eq 1 ];then
            echo "$BINPATH/CreateKmerMatrixFromKMC freq_hist $OutDir/freq_hist.t${task_id}.h5 $n_tasks $task_id < $filelist"
        else
            echo "$BINPATH/CreateKmerMatrixFromKMC filter $OutDir/filter.t${task_id}.h5 $min_freq $max_freq $n_tasks $task_id < $filelist"
        fi
    done
    } > $JobDir/${JobName}.tasklist.txt
}

job_FilterKmerKMC(){
    enable_freq_hist=0 job_FreqHistKMC
}

job_KMCHist(){
    JobName=KMCHist.$K
    OutDir=KmerMarker/$K/KMCHist
    mkdir -p $OutDir 2>/dev/null
    create_jobscript_array $JobName
    {
    for dataid in $(select_dataid);do
        echo "kmc_tools histogram KMC/$dataid/kmer.${K} $OutDir/${dataid}.txt"
    done
    } > $JobDir/${JobName}.tasklist.txt
}

job_ExtractCountsKMC(){
    : ${min_count}
    overwrite=${overwrite:=0}
    JobName=ExtractCountsKMC.${K}.ci${min_count}
    s_memory=8G create_jobscript_array $JobName
    {
    for dataid in $(select_dataid);do
        # skip i
        if [ "$overwrite" -eq 0 ] && [ -f "KMC/$dataid/kmer.${K}.ci${min_count}.counts" ];then
            continue
        fi
        echo "$SURUN $BINPATH/CreateKmerMatrixFromKMC extract_counts KMC/$dataid/kmer.$K KmerSet/$K/ci${min_count}/merged KmerMarker/$K/Filter/ci${min_count}/filter.h5 KMC/$dataid/kmer.${K}.ci${min_count}.counts"
    done
    } > $JobDir/${JobName}.tasklist.txt
}

job_CreateMRMRInput(){
    JobName=CreateMRMRInput.${K}
    create_jobscript_array $JobName
    {
    for i in $(seq 0 $(($NPROC - 1)));do
        echo "pe_task_id=$i pe_task_num=$NPROC $BINPATH/SelectKmerCV mat_mrmr KmerMarker/$K/Param/SelectKmerCV.json"
    done
    } > $JobDir/${JobName}.tasklist.txt
}

job_RunMRMR(){
    JobName=RunMRMR.${K}
    create_jobscript_array $JobName
    PYTHONPATH=$PYTHONPATH:$ScriptDir python -B -c "from CreateTaskFiles import RunMRMR; RunMRMR('KmerMarker/$K/Param/SelectKmerCV.json')" > $JobDir/${JobName}.tasklist.txt
}

job_ParseMRMR(){
    JobName=ParseMRMR.${K}
    create_jobscript_array $JobName
    PYTHONPATH=$PYTHONPATH:$ScriptDir python -B -c "from CreateTaskFiles import ParseMRMR; ParseMRMR('KmerMarker/$K/Param/SelectKmerCV.json')" > $JobDir/${JobName}.tasklist.txt
}

if [ "$#" -gt 0 ];then
    for cmd in $@;do
        $cmd
    done
else
    echo "Error: missing job names"
    echo "Usage: $0 job1 [job2]..."
    echo "Available jobs:"
    for job in $(declare -F | awk '{print $NF}' | grep '^job_');do
        echo "  $job"
    done
    exit 1

    #job_KmerCounter
    #job_RemoveLowAbunKmer
    #job_CrossValidation 'Top_10000 Bootstrap_10000' 'TopBootstrap'
    #job_CrossValidation 'All' 'AllFrequency'
    #job_CounterToFilter
    #job_KmerCountHist
    #job_CounterToFilter
fi
