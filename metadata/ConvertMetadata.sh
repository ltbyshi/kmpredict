#! /bin/bash

awk 'BEGIN{OFS="\t"}{split($2,a,"."); print $1,a[1],a[2]}' metadata.new.txt \
    | sort -k1,1 > metadata.new.1.txt
sed '1 d' metadata.old.txt | cut -f1,4 | sort -k2,2 > metadata.old.1.txt
echo -e 'DataId\tState\tUnknown\tCode' > metadata.txt
join -1 1 -2 2 -o 2.1,1.2,1.1,1.3 metadata.new.1.txt metadata.old.1.txt \
    | tr ' ' '\t' | sort -k1,1 >> metadata.txt


