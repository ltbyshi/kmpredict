#! /bin/bash

. Configuration.sh

OutDir=KmerMarker/$K
mkdir -p $OutDir 2>/dev/null

# Do PCA on K-mer counts
# K-mer matrix is constructed from raw counts files and normalized
pca(){
    mkdir -p $OutDir/Reports $OutDir/KmerMatrix/ 2>/dev/null
    R --vanilla <<RSCRIPT
library(rhdf5)
library(cowplot)
matrix_file <- '$OutDir/KmerMatrix/All/0.h5'
metadata <- read.table('metadata/metadata.txt', header=TRUE)
message('Read kmer counts: ', matrix_file)
m <- h5read(matrix_file, 'data')
rownames(m) <- metadata[['DataId']]
groups <- metadata[['State']]
groups[groups == 'Few_polyps'] <- 'Normal'
for(method in c('PCA')){
    message('Perform ', method)
    x <- NULL
    fit <- NULL
    if(method == 'PCA'){
        fit <- prcomp(m)
        x <- fit[['x']][, 1:2]
    }else{
        library(tsne)
        x <- tsne(m)
    }
    print(dim(fit[['x']]))
    plt.data <- data.frame(DataId=rownames(m),
        PC1=x[, 1], PC2=x[, 2],
        Group=groups)
    plt <- ggplot(plt.data) + geom_point(aes(x=PC1, y=PC2, color=Group)) +
        xlab(sprintf('PC1 (%.0f%%)', fit[['sdev']][1]*100)) +
        ylab(sprintf('PC2 (%.0f%%)', fit[['sdev']][2]*100)) +
        ggtitle(sprintf('%s analysis of k-mer frequency (K=$K)', method))
    save_plot(sprintf('$OutDir/Report/%s.pdf', method), plt,  base_aspect_ratio=4.0/3.0)
}
RSCRIPT
}

# Do PCA on k-mer matrix (loaded from HDF5 files)
pca_kmer_matrix(){
    local pair=${pair:=Normal-Cancer}
    local misc_cond=${misc_cond:=Top_10000}
    local filter_metric=${filter_metric:=MannWhitneyUTest}
    local outer_index=${outer_index:=0}
    local inner_index=${inner_index:=0}
    
    mkdir -p $OutDir/Report 2>/dev/null
    R --vanilla <<RSCRIPT
library(rhdf5)
library(ggplot2)
matrix_file <- '$OutDir/KmerMatrix/$pair/$misc_cond/$filter_metric/outer/${outer_index}.h5'
metadata <- read.table('metadata/metadata.txt', header=TRUE)
message('Read k-mer matrix file: ', matrix_file)
m <- h5read(matrix_file, 'data')
rownames(m) <- metadata[['DataId']]
message('Perform PCA')
fit <- prcomp(m)
groups <- metadata[['State']]
groups[groups == 'Few_polyps'] <- 'Normal'
plt.data <- data.frame(PC1=fit[['x']][, 1], PC2=fit[['x']][, 2], Group=groups)
plt <- ggplot(plt.data) + geom_point(aes(x=PC1, y=PC2, color=Group)) +
    ggtitle('PCA analysis of k-mer frequency (K=$K)')
ggsave('$OutDir/Report/PCA.${pair}.${misc_cond}.${filter_metric}.outer.${outer_index}.png', plot=plt)
RSCRIPT
}

# clustering k-mers based on counts in samples
pca_kmer_clustering(){
    local pair=${pair:=Normal-Cancer}
    local misc_cond=${misc_cond:=Top_10000}
    local filter_metric=${filter_metric:=MannWhitneyUTest}
    local outer_index=${outer_index:=0}
    local inner_index=${inner_index:=0}
    
    mkdir -p $OutDir/Report 2>/dev/null
    R --vanilla <<RSCRIPT
library(rhdf5)
library(ggplot2)
matrix_file <- '$OutDir/KmerMatrix/$pair/$misc_cond/$filter_metric/outer/${outer_index}.h5'
metadata <- read.table('metadata/metadata.txt', header=TRUE)
message('Read k-mer matrix file: ', matrix_file)
m <- h5read(matrix_file, 'data')
message('Perform PCA')
m <- t(m)
fit <- prcomp(m)
plt.data <- data.frame(PC1=fit[['x']][, 1], PC2=fit[['x']][, 2])
plt <- ggplot(plt.data) + geom_point(aes(x=PC1, y=PC2), size=0.2) +
    ggtitle('PCA analysis of k-mers (feature space)')
    ggsave('$OutDir/Report/KmerPCA.${pair}.${misc_cond}.${filter_metric}.outer.${outer_index}.png', plot=plt)
RSCRIPT
}

extract_kmer_seq(){
    local pair=${pair:=Normal-Cancer}
    local misc_cond=${misc_cond:=Top_10000}
    local filter_metric=${filter_metric:=MannWhitneyUTest}
    local outer_index=${outer_index:=0}
    local inner_index=${inner_index:=0}
    local cvloop=${cvloop:=outer}

    local kmer_seq_dir=$OutDir/KmerSeq//$pair/$misc_cond/$filter_metric/$cvloop
    local kmer_seq_file=
    if [ "$cvloop" = outer ];then
        kmer_seq_file=$kmer_seq_dir/${outer_index}.txt
    else
        kmer_seq_file=$kmer_seq_dir/${outer_index}.${inner_index}.txt
    fi
    # extract k-mer sequences from k-mer list file
    mkdir -p $kmer_seq_dir 2>/dev/null
    local min_count=${min_count:=20}
    $BINPATH/KMCDump KmerSet/$K/ci${min_count}/merged \
        $OutDir/KmerList/$pair/$misc_cond/$filter_metric/$cvloop/${outer_index}.h5 \
        > $kmer_seq_file
}
# plot heatmap of the k-mer matrix with only training samples
# the maximum number of k-mers is determined by $maxnkmers
heatmap_kmer_matrix(){
    local pair=${pair:=Normal-Cancer}
    local misc_cond=${misc_cond:=Top_10000}
    local filter_metric=${filter_metric:=MannWhitneyUTest}
    local outer_index=${outer_index:=0}
    local inner_index=${inner_index:=0}
    local cvloop=${cvloop:=outer}
    local maxnkmers=${maxnkmers:=500}

    local kmer_seq_dir=$OutDir/KmerSeq//$pair/$misc_cond/$filter_metric/$cvloop
    local kmer_seq_file=
    if [ "$cvloop" = outer ];then
        kmer_seq_file=$kmer_seq_dir/${outer_index}.txt
    else
        kmer_seq_file=$kmer_seq_dir/${outer_index}.${inner_index}.txt
    fi
    local samples=$($BINPATH/ReadJSON list $OutDir/Param/CVIndex.json /$pair/$outer_index/TrainIndex | xargs)
    mkdir -p $OutDir/Report 2>/dev/null
    R --vanilla <<RSCRIPT
library(rhdf5)
library(RColorBrewer)
matrix_file <- '$OutDir/KmerMatrix/$pair/$misc_cond/$filter_metric/outer/${outer_index}.h5'
metadata <- read.table('metadata/metadata.txt', header=TRUE)
# read k-mer sequences
seqs <- read.table('$kmer_seq_file', header=FALSE)[,2]
samples <- as.numeric(strsplit('$samples', ' ')[[1]]) + 1
message('Read k-mer matrix file: ', matrix_file)
m <- t(h5read(matrix_file, 'data'))
rownames(m) <- seqs
groups <- metadata[['State']]
groups[groups == 'Few_polyps'] <- 'Normal'
colnames(m) <- groups
s_order <- NULL
for(g in c('Normal', 'Stage_0', 'Stage_I_II', 'Stage_III_IV')){
    s_order <- c(s_order, which(groups == g))
}
s_order <- s_order[samples]

# mrmr feature selection
enable_mrmr = FALSE
if(enable_mrmr){
    mrmr_kmerlist <- h5read('$OutDir/MRMR/$pair/$misc_cond/$filter_metric/outer/${outer_index}.kmerlist.h5', 'data')
    m <- m[mrmr_kmerlist, ]
}

#print(samples)
png('$OutDir/Report/Heatmap.${pair}.${misc_cond}.${filter_metric}.outer.${outer_index}.png', width=2400, height=2400)
#pdf('$OutDir/Report/Heatmap.${pair}.${misc_cond}.${filter_metric}.outer.${outer_index}.pdf')
heatmap(m[1:$maxnkmers, s_order], Colv=NA, margins=c(6, 15), labRow=NA,
    col=colorRampPalette(brewer.pal(5, 'Greys'))(100),
    main='Kmer matrix', keep.dendro=FALSE)
dev.off()
RSCRIPT
}

kmer_cluster_kmeans(){
    local pair=${pair:=Normal-Cancer}
    local misc_cond=${misc_cond:=Top_10000}
    local filter_metric=${filter_metric:=MannWhitneyUTest}
    local outer_index=${outer_index:=0}
    local inner_index=${inner_index:=0}
    local cvloop=${cvloop:=outer}
    python <<PYTHON
from sklearn.metrics import silhouette_score
from sklearn.cluster import KMeans, DBSCAN
import h5py
import numpy as np
h5file = h5py.File('$OutDir/KmerMatrix/$pair/$misc_cond/$filter_metric/$cvloop/${outer_index}.h5', 'r')
data = h5file['data'][:]
h5file.close()
n_runs = 5
for n_clusters in xrange(10, 100, 5):
    inertia = np.zeros(n_runs, dtype='float')
    models = []
    for irun in xrange(n_runs):
        model = KMeans(n_clusters)
        #model = DBSCAN()
        model.fit(data)
        inertia[irun] = model.inertia_
        models.append(model)
    best_run = inertia.argmin()
    score = silhouette_score(data, models[best_run].labels_, metric='euclidean')
    print '{0}\t{1}\t{2}'.format(n_clusters, score, inertia[best_run])
    del models
PYTHON
}

coverage_analysis(){
    enable_report=${enable_report:=1}
    enable_kmc=${enable_kmc:=1}
    min_count=${min_count:=10}
    max_samples=20
    mapfile -t dataids < <(select_dataid)
    OutDir=KmerSet/$K/CoverageAnalysis/ci${min_count}
    $SURUN mkdir -p $OutDir 2>/dev/null
    if [ "$enable_kmc" -eq 1 ];then
        for i in $(seq $max_samples);do
            echo "Analyzing coverage for $i samples"
            if [ "$i" -eq 1 ];then
                $SURUN kmc_tools reduce KMC/${dataids[0]}/kmer.$K -ci$min_count $OutDir/union.1
            elif [ "$i" -eq 2 ];then
                $SURUN kmc_tools union KMC/${dataids[0]}/kmer.$K -ci$min_count \
                    KMC/${dataids[1]}/kmer.$K -ci$min_count \
                    $OutDir/union.2
            else
                $SURUN kmc_tools union KMC/${dataids[$(($i-1))]}/kmer.$K -ci$min_count \
                    $OutDir/union.$((i-1)) -ci$min_count \
                    $OutDir/union.$i
            fi
        done
    fi
    if [ -n "$enable_report" ];then
        mkdir -p KmerMarker/$K/Report 2>/dev/null
        {
        for i in $(seq $max_samples);do
            nkmers=$($BINPATH/KMCInfo $OutDir/union.$i | grep '^total_kmers' | cut -d$'\t' -f2)
            printf "$i\t$nkmers\n"
        done
        } > KmerMarker/$K/Report/coverage_analysis.ci${min_count}.txt
    fi
}

usage(){
    echo "Usage: $script_name command [args...]"
    echo "Available commands: coverage_analysis pca extract_kmer_seq pca_kmer_matrix pca_kmer_clustering heatmap_kmer_matrix kmer_cluster_kmeans"
}

script_name=$0
cmd=$1
shift
case "$cmd" in
    pca)
        pca
        ;;
    heatmap_kmer_matrix|pca_kmer_matrix|pca_kmer_clustering|extract_kmer_seq)
        set -e
        for stage in Cancer Stage_0 Stage_I_II Stage_III_IV;do
            pair=Normal-$stage $cmd
        done
        ;;
    kmer_cluster_kmeans)
        set -e
        $cmd
        ;;
    coverage_analysis)
        coverage_analysis
        ;;
    *)
        if [ "$#" -eq 0 ];then
            echo "Error: missing command"
        else
            echo "Error: invalid command"
        fi
        usage
        exit 1
esac

