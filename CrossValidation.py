#! /usr/bin/env python

import sys, os, argparse
#from sklearn.svm import SVC, LinearSVC
#from sklearn.linear_model import LogisticRegression
#from sklearn.ensemble import RandomForestClassifier
#from sklearn import metrics
#import h5py
#import numpy as np
import json
import multiprocessing
import gc
from itertools import product
from parallelmap import parallelmap

def DeferredImport():
    global h5py, np
    import h5py
    import numpy as np
    global metrics
    from sklearn import metrics

def RemoveFile(filename):
    try:
        os.unlink(filename)
        print >>sys.stderr, 'Removed: ' + filename
    except OSError:
        print >>sys.stderr, 'Failed to remove: ' + filename
        pass
    
def MakeDir(pathname):
    try:
        os.makedirs(pathname)
        print >>sys.stderr, 'MakeDir: ' + pathname
    except OSError as e:
        if e.errno == 17:
            pass
        else:
            print >>sys.stderr, 'Failed to create: ' + pathname
    
def SaveJSON(obj, filename):
    print >>sys.stderr, 'SaveJSON:', filename
    json.dump(obj, open(filename, 'w'))     

def LoadJSON(filename):
    #print >>sys.stderr, 'LoadJSON:', filename
    return json.load(open(filename, 'r'))

def LoadHDF5(filename, name):
    f = h5py.File(filename, 'r')
    ds = f[name]
    m = np.zeros(shape=ds.shape, dtype=ds.dtype)
    ds.read_direct(m)
    f.close()
    return m
    
def CreateModel(classifier, hp):
    if classifier == 'SVM':
        from sklearn.svm import SVC
        return SVC(C=hp['C'], probability=True, class_weight='balanced')
    elif classifier == 'SVMLinear':
        from sklearn.svm import LinearSVC
        return LinearSVC(C=hp['C'], class_weight='balanced')
    elif classifier == 'LogRegL1':
        from sklearn.linear_model import LogisticRegression
        return LogisticRegression(penalty='l1', C=hp['C'], class_weight='balanced')
    elif classifier == 'LogRegL2':
        from sklearn.linear_model import LogisticRegression
        return LogisticRegression(penalty='l2', C=hp['C'], class_weight='balanced')
    elif classifier == 'RandomForest':
        from sklearn.ensemble import RandomForestClassifier
        return RandomForestClassifier(n_estimators=hp['N'], class_weight='balanced')
    elif classifier == 'MultinomialNB':
        from sklearn.naive_bayes import MultinomialNB
        return MultinomialNB(alpha=hp['alpha'])
    elif classifier == 'GaussianNB':
        from sklearn.naive_bayes import GaussianNB
        return GaussianNB()
    else:
        raise ValueError('Unknown classifier: %s'%classifier)
    
def EvaluateModel(classifier, params, eval_metric, eval_train):
    hp = params['HyperParams']
    model = CreateModel(classifier, hp)
    X = LoadHDF5(params['KmerMatrixFile'], 'data').T
    y = np.asarray(params['Y'])
    
    imin = params['KmerRankMin']
    if params['KmerRankMax'] > 0:
        X_train = X[params['TrainIndex'], params['KmerRankMin']:params['KmerRankMax']]
        X_test  = X[params['TestIndex'],  params['KmerRankMin']:params['KmerRankMax']]
    else:
        X_train = X[params['TrainIndex'], params['KmerRankMin']:]
        X_test  = X[params['TestIndex'],  params['KmerRankMin']:]
    y_train = y[params['TrainIndex']]
    y_test = y[params['TestIndex']]
    if eval_train:
        X_test = X_train
        y_test = y_train
    model.fit(X_train, y_train)
    result = {}
    result['HyperParams'] = hp
    
    # calculate AUC (call predict_proba)
    if classifier == 'SVMLinear':
        scores = model.decision_function(X_test)
    else:
        scores = model.predict_proba(X_test)[:, 1]
    fpr, tpr, thresholds = metrics.roc_curve(y_test, scores)
    result['AUC'] = metrics.auc(fpr, tpr)
    result['Scores'] = scores.tolist()
    # calculate weighted accuracy (call score)
    classes, counts = np.unique(y_test, return_counts=True)
    weights = 1.0/counts.astype('float')
    sample_weight = np.empty(len(y_test))
    for c, w in zip(classes, weights):
        sample_weight[y_test == c] = w
    y_pred = model.predict(X_test)
    score = metrics.accuracy_score(y_test, y_pred, sample_weight=sample_weight)
    #score = model.score(X_test, y_test, sample_weight=sample_weight)
    result['PredictedLabels'] = y_pred.tolist()
    result['TrueLabels'] = y_test.tolist()
    result['Accuracy'] = score

    result['Performance'] = result[eval_metric]
    result_dir = os.path.dirname(params['ResultFile'])
    MakeDir(result_dir)       
    SaveJSON(result, params['ResultFile'])
    # clean up
    del X, X_train, y_train, X_test, y_test
    gc.collect()

    return result
   
def InnerTask(args):
    classifier, params, eval_metric, eval_train = args
    EvaluateModel(classifier, params, eval_metric, eval_train)
        
def OuterTask(args):
    classifier, outer, eval_metric, eval_train = args
    perf_hyper = []

    for HyperIndex, hyper in enumerate(outer['Hyper']):
        perfs = []
        for InnerIndex, inner in enumerate(hyper['Inner']):
            perfs.append(LoadJSON(inner['ResultFile'])['Performance'])
        perf = np.mean(perfs)
        perf_hyper.append(perf)
    best_hyper = outer['Hyper'][np.argmax(perf_hyper)]['HyperParams']
    outer['HyperParams'] = best_hyper
    EvaluateModel(classifier, outer, eval_metric, eval_train)
    
def CrossValidationInner(p, jobs, dry_run=False, parallel_engine='python', eval_train=False):
    # inner
    #print 'CVInner started'
    tasks = list(product((p['Classifier'],),  p['CVInner'], (p['EvalMetric'],), (eval_train,)))
    if dry_run:
        for classifier, params, eval_metric, eval_train in tasks:
            print 'Inner\t{0}\t{1}\t{2}\t{3}\t{4}\t{5}'.format(p['Pair'],
                  p['EvalMetric'], p['Classifier'], p['FilterMetric'], 
                  params['OuterIndex'], params['InnerIndex'])
        return
    parallelmap(InnerTask, tasks, n_jobs=jobs, engine=parallel_engine)
    #for args in enumerate(p['CVInner']):
    #    Task(args)
    
def CrossValidationOuter(p, jobs, dry_run=False, parallel_engine='python', eval_train=False):
    # outer
    #print 'CVOuter started'
    tasks = list(product((p['Classifier'],), p['CVOuter'], (p['EvalMetric'],), (eval_train,)))
    if dry_run:
        for classifier, params, eval_metric, eval_train in tasks:
            print 'Outer\t{0}\t{1}\t{2}\t{3}\t{4}'.format(p['Pair'],
                  p['EvalMetric'], p['Classifier'], p['FilterMetric'],
                  params['OuterIndex'])
        return
    
    parallelmap(OuterTask, tasks, n_jobs=jobs, engine=parallel_engine)
    
def CrossValidation(p, inner_only=False, outer_only=False, jobs=8, 
                    dry_run=False, parallel_engine='python',
                    eval_train=False):
    print 'FilterMetric:', p['FilterMetric']
    print 'Classifier:', p['Classifier']
    print 'EvalMetric:', p['EvalMetric']
    print 'Jobs: {0}'.format(jobs)
    if not outer_only:
        CrossValidationInner(p, jobs, dry_run, parallel_engine, eval_train)
    if not inner_only:
        CrossValidationOuter(p, jobs, dry_run, parallel_engine, eval_train)

def Report(param_files, eval_metric):
    print 'Pair\tMiscCond\tFilterMetric\tClassifier\tMean\tStd'
    for param_file in param_files:
        p = LoadJSON(param_file)
        perf = []
        try:
            for outer in p['CVOuter']:
                result = LoadJSON(outer['ResultFile'])
                perf.append(result[eval_metric])
            print '{0}\t{1}\t{2}\t{3}\t{4}\t{5}'.format(p['Pair'], p['MiscCond'],
                            p['FilterMetric'], p['Classifier'],
                            np.mean(perf), np.std(perf))
        except IOError:
            pass
    
def Clean(param_files):
    def CleanOne(param_file):
        p = LoadJSON(param_file)
        for params in p['CVInner']:
            RemoveFile(params['ResultFile'])
        for params in p['CVOuter']:
            RemoveFile(params['ResultFile'])
    map(CleanOne, param_files)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    #parser.add_argument('-k', type=int, required=True)
    parser.add_argument('param_files', type=str, nargs='+')
    parser.add_argument('--outer', action='store_true', required=False, default=False,
                        help='Only run outer loop')
    parser.add_argument('--inner', action='store_true', required=False, default=False,
                        help='Only run inner loop')
    parser.add_argument('--eval-metric', type=str, required=False, default='Accuracy')
    parser.add_argument('-j', '--jobs', type=int, required=False, default=8,
                        help='Number of parallel jobs')
    parser.add_argument('--dry-run', action='store_true', required=False)
    parser.add_argument('--parallel-engine', type=str, 
                        choices=('sge_array', 'mpi', 'python', 'multiprocessing'),
                        default='multiprocessing')
    parser.add_argument('--eval-train', action='store_true', required=False,
                        help='Use training set as test set')
    commands = parser.add_mutually_exclusive_group()
    commands.add_argument('--report', action='store_true')
    commands.add_argument('--clean', action='store_true')
    args = parser.parse_args()
    DeferredImport()
    
    if args.report:
        Report(args.param_files, args.eval_metric)
    elif args.clean:
        Clean(args.param_files)
    else:
        for param_file in args.param_files:
            p = LoadJSON(param_file)
            p['EvalMetric'] = args.eval_metric
            CrossValidation(p, args.inner, args.outer, 
                            jobs=args.jobs, 
                            dry_run=args.dry_run,
                            parallel_engine=args.parallel_engine,
                            eval_train=args.eval_train)
    
    
