#! /usr/bin/env python
import argparse, sys
from kmpredict.utils import ArgDependChecker

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Count k-mers in sequences')
    parser.add_argument('-k', type=int, required=True, help='k-mer length')
    parser.add_argument('-c', '--canonical', action='store_true',
        required=False, default=False,
        help='use lexicographical smaller one of the k-mer and its reverse complement')
    group_method = parser.add_mutually_exclusive_group(required=True)
    group_method.add_argument('--exact', action='store_true',
        help='count all possible k-mers with a given length')
    group_method.add_argument('--lsh', action='store_true',
        help='count k-mers using locality-sensitive hashing')

    parser.add_argument('--lshfile', type=str, required=False,
        help='coefficient files needed for the LSH method')
    parser.add_argument('-n', '--hash-nbits', type=int, required=False,
        help='number of bits for LSH')
    parser.add_argument('--batch-size', type=int, required=False,
        default=10000, help='number of sequences to process at a batch for LSH')
    parser.add_argument('seqfile', type=str,
        help='sequence file in FASTA format')
    parser.add_argument('countfile', type=str,
        help='output file storing k-mer counts in HDF5 format')

    depends = ArgDependChecker()
    depends.add_depend('lsh', 'lshfile')
    depends.add_depend('lsh', 'hash_nbits')
    depends.add_depend('lsh', 'batch_size')
    args = parser.parse_args()
    depends.check(parser, args)

    if args.exact:
        from kmpredict.kmercounter import ExactKmerCounter
        counter = ExactKmerCounter(args.k)
        counter.CountFasta(args.seqfile)
        counter.Save(args.countfile)
    elif args.lsh:
        from kmpredict.kmercounter import LSHKmerCounter
        counter = LSHKmerCounter(0, args.k)
        counter.CountFasta(args.seqfile, args.hash_nbits, args.batch_size)
        counter.Save(args.countfile)
