#! /usr/bin/env python
import json, argparse
import h5py, sys
from kmpredict.utils import kmercounts_to_vw

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    group_method = parser.add_mutually_exclusive_group(required=True)
    group_method.add_argument('-f', '--files', type=str, required=False,
        help='read k-mer count file names from argument (comma-separated string like group_id:file_name)')
    group_method.add_argument('-i', '--stdin', action='store_true', required=False,
        help='read k-mer counts file names from standard input (one file per line like group_id:file_name)')
    group_method.add_argument('-p', '--param-file', type=str, required=False,
        help='read k-mer count files given in a JSON file (/SampleInfo/$i/KmerCountFile)')

    parser.add_argument('--print-groups', action='store_true', required=False,
        help='print class labels one per line')

    parser.add_argument('-o', '--outfile', type=str, required=False,
        help='write the output to a file')
    args = parser.parse_args()
    # get file names
    filelist = []
    if args.files:
        for line in args.files.split(','):
            group, filename = line.strip().split(':')
            filelist.append([int(group), filename])
    elif args.stdin:
        for line in sys.stdin:
            group, filename = line.strip().split(':')
            filelist.append([int(group), filename])
    elif args.param_file:
        f = open(args.param_file, 'r')
        p = json.load(f)
        f.close()
        for info in p['SampleInfo']:
            filelist.append([info['Group'], info['KmerCountFile']])

    fileno = 1
    groups = zip(*filelist)[0]
    map_group = {min(groups): 0, max(groups): 1}
    # print groups
    if args.print_groups:
        for group in groups:
            print map_group[group]
    # convert files
    else:
        for group, countfile in filelist:
            h5file = h5py.File(countfile, 'r')
            counts = h5file['data'][:]
            h5file.close()
            print >>sys.stderr, 'KmerCountsToVW[%d]: %s'%(fileno, countfile)
            kmercounts_to_vw(counts, map_group[group])
            fileno += 1
