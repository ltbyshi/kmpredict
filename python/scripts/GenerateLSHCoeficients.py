#! /usr/bin/env python
import argparse, sys

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Generate coefficient file for LSH')
    parser.add_argument('-k', type=int, required=True, help='k-mer length')
    parser.add_argument('-n', '--nbits', type=int, required=True,
        help='number of bits for LSH')
    parser.add_argument('--sample-size', type=int, required=False,
        default=1000, help='number of random sequences to sample')
    parser.add_argument('lshfile', type=str, help='output file')
    args = parser.parse_args()

    from kmpredict.lsh import LSH
    lsh_hash = LSH(args.k, args.nbits)
    lsh_hash.generate_coefs(args.sample_size)
    lsh_hash.save_coefs(args.lshfile)
