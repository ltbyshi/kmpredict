from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext
import os

def source_path(filenames):
    paths = []
    for f in filenames:
        if f.endswith('.cpp'):
            paths.append(os.path.join('src', f))
        else:
            paths.append(os.path.join('kmpredict', f))
    return paths

depends = {'Eigen': {'Include': ['/home/shibinbin/apps/include'], 'Library': []}}
cppflags = ['-std=c++11', '-Wno-deprecated']

setup(
  name = 'kmpredict',
  version = '1.0',
  description='python modules for k-mer-based metagenomic sample prediction',
  cmdclass= {'build_ext': build_ext},
  ext_modules = [Extension('kmpredict._relevance',
                    source_path(['_relevance.pyx', '_relevance_helper.cpp']),
                    language='c++', include_dirs=['src'], extra_compile_args=cppflags),
                 Extension('kmpredict._counts_to_vw', source_path(['_counts_to_vw.pyx'])),
                 Extension('kmpredict.kmercounter',
                     source_path(['kmercounter.pyx', 'KmerCounter.cpp']),
                     language='c++',
                     include_dirs=['src'] + depends['Eigen']['Include'],
                     extra_compile_args=cppflags),
                 Extension('kmpredict.lsh', source_path(['lsh.pyx', 'HyperplaneHash.cpp']),
                     include_dirs=['src'] + depends['Eigen']['Include'],
                     language='c++',
                     extra_compile_args=cppflags),
                 Extension('kmpredict.carray', source_path(['carray.pyx']),
                     extra_compile_args=cppflags),
                 Extension('kmpredict._utils', source_path(['_utils.pyx']),
                     extra_compile_args=cppflags),
                 Extension('kmpredict._filter_metrics', source_path(['_filter_metrics.pyx']),
                     extra_compile_args=cppflags)],
  packages=['kmpredict'],
  package_dir={'kmpredict': 'kmpredict'}
)

