#include <iostream>
#include <cstdlib>
#include "HyperplaneHash.h"

HyperplaneHash::HyperplaneHash(int k, int hashsize)
{
    _k = k;
    _hashsize = hashsize;
    _tablesize = (1 << (2*hashsize));
    //_W.resize(hashsize, k);
    //_w0.resize(hashsize);
    InitTranstable();
}

void HyperplaneHash::CreateHyperplanes(int nRandomKmers)
{
    _W.resize(_hashsize, _k);
    _w0.resize(_hashsize);
    for(int h = 0; h < _hashsize; h ++)
    {
        MatrixXcd m(nRandomKmers + 1, _k + 1);
        GenRandomKmers(m);
        for(int i = 0; i < nRandomKmers; i ++)
            m(i, _k) = std::complex<double>(-1, 0);
        for(int i = 0; i <= _k; i ++)
            m(nRandomKmers, i) = std::complex<double>(0, 0);
        // PCA
        JacobiSVD<MatrixXcd> svd(m, ComputeThinV);
        MatrixXcd V = svd.matrixV();
        for(int i = 0; i < _k; i ++)
            _W(h, i) = V(i, _k);
        _w0(h) = V(_k, _k);
    }
}

void HyperplaneHash::GenRandomKmers(MatrixXcd& m)
{
    for(int i = 0; i < m.rows(); i ++)
        for(int j = 0; j < m.cols(); j ++)
            m(i, j) = _transtable(int(DNAStr[rand() % 3]));
}


void HyperplaneHash::InitTranstable()
{
    _transtable.resize(256);
    _transtable.fill(std::complex<double>(0, 0));
    _transtable(int('A')) = std::complex<double>(0.995, 0);
    _transtable(int('C')) = std::complex<double>(0, 0.995);
    _transtable(int('G')) = std::complex<double>(0, -0.995);
    _transtable(int('T')) = std::complex<double>(-0.995, 0);
}


void HyperplaneHash::LoadHyperplanes(const std::string& filename)
{
    LoadEigenMatrix(_W, filename + ".W");
    LoadEigenMatrix(_w0, filename + ".w0");
    bool status = true;
    status &= (_W.rows() == _hashsize);
    status &= (_W.cols() == _k);
    status &= (_w0.rows() == _hashsize);
    if(!status)
        throw ValueError("invalid hyperplane files " + filename);
}

void HyperplaneHash::SaveHyperplanes(const std::string& filename) const
{
    SaveEigenMatrix(_W, filename + ".W");
    SaveEigenMatrix(_w0, filename + ".w0");
}
