#include <iostream>
#include <fstream>
#include <string>
#include <map>
using namespace std;
#include <stdlib.h>
#include <math.h>

#include "Fasta.h"
#include "KmerFilter.h"
#include "Utils.h"
#include "Thread.h"
#include "HyperplaneHash.h"

#define OPT_EXCLUSIVE 0
#define OPT_FISHER  0
#define OPT_ENTROPY 1
#define OPT_PROGRESS 1
#define PROGNAME "KmerFilter"

#if ENABLE_MPI
#include <mpi.h>
#include <MPIUtil.h>
using MPIUtil::mpienv;
void DistributeJobs(int n_jobs, int n_workers, int rank,
                    int& id_start, int& id_end)
{
    int unit_size = n_jobs/n_workers;
    id_start = rank*unit_size;
    id_end = std::min(n_jobs, id_start + unit_size);
}
// rapidjson's DOM-style APIs
#include <rapidjson/document.h>     
using rapidjson::Document;
using rapidjson::Value;
using rapidjson::SizeType;
#endif

int KmerFilterCount(int argc, char** argv)
{
#if ENABLE_MPI
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmer frequencies" << endl;
        cerr << "Usage: " << PROGNAME << " count param_file" << endl;
        exit(1);
    }
    string param_file = string(argv[1]);
    Document params;
    ReadJSON(param_file, params);
    int jobid_start, jobid_end;
    DistributeJobs(params.GetSize(), mpienv.world_size, mpi.world_rank,
                   jobid_start, jobid_end);
    for(int jobid = jobid_start; jobid < jobid_end; jobid ++)
    {
        int k = params[jobid]["K"].GetInt();
        string seqfile = params[jobid]["SeqFile"].GetString();
        string kmerfile = params[jobid]["KmerFile"].GetString();
        
#else
    if(argc != 4)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmer frequencies" << endl;
        cerr << "Usage: " << PROGNAME << " count k seq_file kmer_file" << endl;
        cerr << "\tseq_file\tsequence in FASTA format." << endl;
        cerr << "\tkmer_file\toutput file containing kmer frequencies." << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    const char* seqfile = argv[2];
    const char* kmerfile = argv[3];
#endif
    
    FastaReader reader(seqfile);
    DefaultKmerFilter counter(k);
    // count kmers
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        const char* seq = record->seq.c_str();
        size_t nkmer = record->seq.size() - k + 1;
        // skip if the sequence is shorter than kmer length
        if(nkmer <= 0)
            continue;
        DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
        counter.Add(kmer);
        for(size_t i = 0; i < nkmer - 1; i++)
        {
            kmer = OverlapKmer(kmer, k, seq[k + i]);
            counter.Add(kmer);
        }
        delete record;
        record = reader.ReadNext();
    }
    reader.Close();
    counter.Save(kmerfile, false);
#if ENABLE_MPI
    }
#endif
    return 0;
}

int KmerFilterCreateHash(int argc, char** argv)
{
    if(argc != 4)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Create random hyperplanes for hashing" << endl;
        cerr << "Usage: " << PROGNAME << " createhash k hashsize hashfile" << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    int hashsize = atoi(argv[2]);
    const char* hashfile = argv[3];
    
    HyperplaneHash<DefaultKmerType> hash(k, hashsize);
    hash.CreateHyperplanes();
    if(hash.SaveHyperplanes(hashfile))
        return 0;
    else
        return 1;
}

int KmerFilterHashedCount(int argc, char** argv)
{
    if(argc != 6)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmer frequencies" << endl;
        cerr << "Usage: " << PROGNAME << " hash k hashsize hashfile seqfile kmerfile" << endl;
        cerr << "\thashfile\tfile created by createhash" << endl;
        cerr << "\tseqfile\tsequence in FASTA format." << endl;
        cerr << "\tkmerfile\toutput file containing kmer frequencies." << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    int hashsize = atoi(argv[2]);
    const char* hashfile = argv[3];
    const char* seqfile = argv[4];
    const char* kmerfile = argv[5];
    
    HyperplaneHash<DefaultKmerType> hash(k, hashsize);
    if(!hash.LoadHyperplanes(hashfile))
        exit(1);
    
    FastaReader reader(seqfile);
    DefaultKmerFilter counter(k, (1UL << hashsize));
    // count kmers
#if ENABLE_PROGRESS
    size_t filesize = FileSize(seqfile);
    ProgressBar progressbar(filesize);
#endif
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        const char* seq = record->seq.c_str();
        size_t nkmers = record->seq.size() - k + 1;
        for(size_t i = 0; i < nkmers; i ++)
            counter.Add(hash.Hash(&(seq[i])));
        delete record;
        record = reader.ReadNext();
#if ENABLE_PROGRESS
        progressbar.Update(reader.GetFilePos());
#endif
    }
#if ENABLE_PROGRESS
    progressbar.Finish();
#endif
    reader.Close();
    counter.Save(kmerfile, false);
    return 0;
}

#define MAX_KMER_ABUN   255
int RemoveLowAbunKmer(int argc, char** argv)
{
    if(argc != 7)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmer frequencies" << endl;
        cerr << "Usage: " << PROGNAME << " hash k hashsize hashfile seqfile kmerfile" << endl;
        cerr << "\thashfile\tfile created by createhash" << endl;
        cerr << "\tseqfile\tsequence in FASTA format." << endl;
        cerr << "\tkmerfile\toutput file containing kmer frequencies." << endl;
        cerr << "\tmin_abun\tabundance filter" << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    int hashsize = atoi(argv[2]);
    const char* hashfile = argv[3];
    const char* seqfile = argv[4];
    const char* kmerfile = argv[5];
    int min_abun = atoi(argv[6]);
    
    HyperplaneHash<DefaultKmerType> hash(k, hashsize);
    if(!hash.LoadHyperplanes(hashfile))
        exit(1);
    
    FastaReader reader(seqfile);
    // count k-mer frequencies
    #if ENABLE_PROGRESS
    size_t filesize = FileSize(seqfile);
    ProgressBar progressbar(filesize);
    #endif
    vector<unsigned char> counts((1UL << hashsize));
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        const char* seq = record->seq.c_str();
        size_t nkmers = record->seq.size() - k + 1;
        for(size_t i = 0; i < nkmers; i ++)
        {
            DefaultKmerType hashval = hash.Hash(&(seq[i]));
            if(counts[hashval] < MAX_KMER_ABUN)
                counts[hashval] ++;
        }
        delete record;
        record = reader.ReadNext();
        #if ENABLE_PROGRESS
        progressbar.Update(reader.GetFilePos());
        #endif
    }
    reader.Close();
    #if ENABLE_PROGRESS
    progressbar.Finish();
    #endif
    // filter k-mers by frequencies
    DefaultKmerFilter counter(k, (1UL << hashsize));
    for(DefaultKmerType kmer = 0; kmer < counter.GetHashSize(); kmer ++)
    {
        if(counts[kmer] >= min_abun)
            counter.Add(kmer);
    }
    counter.Save(kmerfile, false);
    return 0;
}

int KmerFilterDump(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " dump kmer_file" << endl;
        cerr << "\tkmer_file\toutput file from k-mer counter." << endl;
        exit(1);
    }
    else if(argc == 2)
    {
        const char* kmerfile = argv[1];
        DefaultKmerFilter counter;
        counter.Load(kmerfile);
        counter.DumpToTable();
    }
    return 0;
}

int KmerFilterGetInfo(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " info kmer_file" << endl;
        cerr << "\tkmer_file\tkmer binary file generated from count" << endl;
        exit(1);
    }
    else
    {
        const char* kmerfile = argv[1];
        
        DefaultKmerFilter::GetInfo(kmerfile);
    }
    return 0;
}

int KmerFilterCoverage(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " info kmer_file" << endl;
        cerr << "\tkmer_file\tkmer binary file generated from count" << endl;
        exit(1);
    }
    else
    {
        const char* kmerfile = argv[1];
        
        DefaultKmerFilter counter;
        counter.Load(kmerfile);
        DefaultKmerType hashsize = counter.GetHashSize();
        DefaultKmerType kmernum = counter.GetKmerNum();
        double coverage = double(kmernum)/hashsize;
        cout << "KmerNum\t" << kmernum << endl;
        cout << "HashSize\t" << hashsize << endl;
        cout << "Coverage\t" << coverage << endl;
    }
    return 0;
}


class FisherExactTestJob: public Job
{
public:
    FisherExactTestJob() {}
    virtual ~FisherExactTestJob() {}
    virtual int Start()
    {
        return 0;
    }
public:
    // input
    DefaultKmerType kmer_start, kmer_end;
    vector<vector<unsigned char> >* nkmer_per_group;
    vector<int>* nsamples_pg;
    vector<int>* nsamples_other_pg;
    // output
    vector<int>* sig_group;
    vector<double>* pvalue;
};

int KmerFilterUniqueKmerCount(int argc, char** argv)
{
    if(argc < 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " uniqkmercount kmer_file [kmer_file...]" << endl;
        cerr << "\tkmer_file\tkmer binary file generated from count" << endl;
        exit(1);
    }
    else if(argc == 2)
    {
        const char* kmerfile = argv[1];
        
        DefaultKmerFilter counter;
        counter.Load(kmerfile);
        cout << counter.GetUniqueKmerCount() << endl;
    }
    else
    {
        DefaultKmerFilter counter;
        counter.Load(argv[1]);
        for(int i = 2; i < argc; i ++)
        {
            DefaultKmerFilter b;
            b.Load(argv[i]);
            counter.Merge(b);
        }
        cout << counter.GetUniqueKmerCount() << endl;
    }
    return 0;
}

int KmerFilterConvert(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " convert kmerfile" << endl;
        cerr << "\tkmerfile\told kmerfile format" << endl;
    }
    else
    {
        const char* kmerfile = argv[1];
        DefaultKmerFilter counter;
        counter.Convert(kmerfile);
    }
    return 0;
}

template <typename Callback>
void ScanFasta(int k, const string& seqfile, const map<string, int>& coverage, 
               Callback& callback)
{
    FastaReader reader(seqfile);
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        map<string, int>::const_iterator it = coverage.find(record->name);
        if(it == coverage.end())
        {
            // cerr << "Warning: sequence " << record->name << " has no coverage value" << endl;
        }
        else
        {
            const char* seq = record->seq.c_str();
            size_t nkmer = record->seq.size() - k + 1;
            // skip if the sequence is shorter than kmer length
            if(nkmer <= 0)
                continue;
            DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
            callback(kmer, it->second);
            for(size_t i = 0; i < nkmer - 1; i++)
            {
                kmer = OverlapKmer(kmer, k, seq[k + i]);
                callback(kmer, it->second);
            }
         }
        delete record;
        record = reader.ReadNext();
    }
    reader.Close();
}

struct CountWeightedMean
{
    DefaultKmerType _totalcounts;
    double _totalnkmers;
    CountWeightedMean(double totalnkmers): 
        _totalcounts(0), _totalnkmers(totalnkmers) {}
    void operator()(DefaultKmerType kmer, int cov){
        _totalcounts += cov;
    }
    double mean() const{
        return (_totalcounts/_totalnkmers);
    }
};

struct CountWeightedVariance
{
    double _variance;
    double _mean;
    long _actualnkmers;
    double _totalnkmers;
    CountWeightedVariance(double mean, double actualnkmers, double totalnkmers):
        _variance(0), _mean(mean), _actualnkmers(actualnkmers), _totalnkmers(totalnkmers) {}
    void operator()(DefaultKmerType kmer, int cov){
        _variance += (cov - _mean)*(cov - _mean);
    }
    double variance() const{
        return (_variance + _mean*_mean*(_totalnkmers - _actualnkmers))/_totalnkmers;
    }
};

struct CountWeightedNorm
{
    double _hashsize;
    double _norm;
    CountWeightedNorm(double hashsize):
        _hashsize(hashsize), _norm(0) {}
    void operator()(DefaultKmerType kmer, int cov){
        _norm += cov*cov;
    }
    double norm() const{
        return sqrt(_norm/_hashsize);
    }
};

int WeightedAverageCountMain(int argc, char** argv)
{
    if(argc != 5)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Average kmer count with sequence weight" << endl;
        cerr << "Usage: " << PROGNAME << " countw k seq_file coverage_file kmer_file" << endl;
        cerr << "\tseq_file\tsequence in FASTA format." << endl;
        cerr << "\tcoverage_file\tfile with two columns (name, coverage)." << endl;
        
        exit(1);
    }
    int k = atoi(argv[1]);
    const char* seqfile = argv[2];
    const char* covfile = argv[3];
    const char* kmerfile = argv[4];
    
    // read coverage file
    map<string, int> coverage = ReadCoverage(covfile);
    KmerFilterHeader header = DefaultKmerFilter::ReadHeader(kmerfile);
    DefaultKmerType totalnkmers = header.hashsize;
    /*
    DefaultKmerFilter counter;
    counter.Load(kmerfile);
    DefaultKmerType actualnkmers = counter.GetKmerNum();
    // count kmers
    DefaultKmerType totalnkmers = counter.GetHashSize();
    
    CountWeightedMean count_mean(totalnkmers);
    ScanFasta(k, seqfile, coverage, count_mean);
    double mean = count_mean.mean();
    CountWeightedVariance count_variance(mean, actualnkmers, totalnkmers);
    ScanFasta(k, seqfile, coverage, count_variance);
    double variance = count_variance.variance();
    */
    CountWeightedNorm count_norm(totalnkmers);
    ScanFasta(k, seqfile, coverage, count_norm);
    double norm = count_norm.norm();
    //cout << count_mean._totalcounts << endl;
    //cout << count_variance._variance << endl;
    //cout << mean << endl;
    //cout << variance << endl;
    cout << norm << endl;
    return 0;
}

int main(int argc, char** argv)
{
    HandlerMap handlers;
    handlers["count"] = KmerFilterCount;
    handlers["dump"]  = KmerFilterDump;
    handlers["info"]  = KmerFilterGetInfo;
    handlers["coverage"] = KmerFilterCoverage;
    handlers["createhash"] = KmerFilterCreateHash;
    handlers["hashedcount"] = KmerFilterHashedCount;
    handlers["unikmercount"] = KmerFilterUniqueKmerCount;
    handlers["convert"] = KmerFilterConvert;
    handlers["avgcountw"] = WeightedAverageCountMain;
    handlers["removelowabunkmer"] = RemoveLowAbunKmer;
    
    ParseArguments(handlers, PROGNAME, argc, argv);
    
    return 0;
}