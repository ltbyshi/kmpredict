#ifndef __KMERCOUNTER_H__
#define __KMERCOUNTER_H__
#include <vector>
#include <string>

#include "Nucleotide.h"
#include "Kmer.h"

class KmerCounter
{
public:
    KmerCounter(int k);
    KmerCounter(const KmerCounter& counter);

    ~KmerCounter()
    {
        Clear();
    }


    inline void AddKmer(KmerType kmer, ValType value)
    {
        if(kmer < _hash_size)
            _hash[kmer] += value;
    }

    inline void AddKmer(const char* seq, ValType value)
    {
        KmerType kmer = StringToKmer(seq, _k);
        AddKmer(kmer, value);
    }

    inline ValType GetKmer(KmerType kmer) const
    {
        return _hash[kmer];
    }

    inline ValType operator[](KmerType kmer) const
    {
        return _hash[kmer];
    }

    void Clear();

    ValType* GetData() { return _hash; }

    inline int GetK() const { return _k; }

    inline KmerType GetHashSize() const { return _hash_size; }
    // allocate memory for a new hash table
    void Allocate(size_t hash_size);
    // use an existing hash table
    void SetData(size_t hash_size, ValType* data);
    // get number of distinct k-mers (non-zero counts)
    KmerType GetUniqueKmerCount() const;
    // merge counts of a k-mer and its reverse complement
    void MergeRC();
    // count k-mers
    void CountFasta(const char* seqfile);
    // count k-mers with weights supplied, n_seq is the number of sequences
    void WeightedCountFasta(const char* seqfile, int* weights, size_t n_seq);
    // generate a histogram with binwidth of 1
    void KmerCountHistogram(ValType bincount, unsigned long* values);
    // calculate the total count of k-mers divided by the hash size
    unsigned long AverageCount();
    // use locality-sensitivy hashing for k-mer counting
    void LSHCountFasta(const char* seqfile, const char* hyperplane_file, size_t n_bit);

private:
    ValType* _hash;
    KmerType _hash_size;
    int _k;
    bool _delete_hash;
};

#endif
