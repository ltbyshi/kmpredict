#include <stdlib.h>
#include "KmerCounter.h"
#include "Fasta.h"
#include "Exception.h"

KmerCounter::KmerCounter(int k)
{
    _hash = NULL;
    _k = k;
    _hash_size = 0;
    _delete_hash = false;
}

KmerCounter::KmerCounter(const KmerCounter& counter)
{
    _k = counter._k;
    _hash_size = counter._hash_size;
    if((_hash_size > 0) && _delete_hash)
    {
        _hash = new ValType[_hash_size];
        memcpy(_hash, counter._hash, _hash_size*sizeof(ValType));
    }
    else
        _hash = counter._hash;
    _delete_hash = counter._delete_hash;
}

void KmerCounter::Clear()
{
    if((_hash != NULL) & _delete_hash)
        delete[] _hash;
    _hash = NULL;
    _hash_size = 0;
}

void KmerCounter::Allocate(size_t hash_size)
{
    Clear();
    _hash = new ValType[hash_size];
    _hash_size = hash_size;
    _delete_hash = true;
    memset(_hash, 0, _hash_size*sizeof(ValType));
}

void KmerCounter::MergeRC()
{
    for(KmerType kmer = 0; kmer < _hash_size; kmer ++)
    {
        KmerType kmerrc = KmerRC(kmer, _k);
        if(kmer <= kmerrc)
        {
            ValType count;
            if(kmer < kmerrc)
                count = _hash[kmer] + _hash[kmerrc];
            else
                count = _hash[kmer];
            _hash[kmer] = count;
            _hash[kmerrc] = 0;
        }
    }
}

void KmerCounter::SetData(size_t hash_size, ValType* data)
{
    Clear();
    _hash_size = hash_size;
    _hash = data;
    _delete_hash = false;
}

KmerType KmerCounter::GetUniqueKmerCount() const
{
    KmerType uniqc = 0;
    for(KmerType kmer = 0; kmer < _hash_size; kmer ++)
    {
        KmerType kmerrc = KmerRC(kmer, _k);
        if((kmer < kmerrc) && (_hash[kmer] + _hash[kmerrc] > 0))
            uniqc ++;
        else if((kmer == kmerrc) && (_hash[kmer] > 0))
            uniqc ++;
    }
    return uniqc;
}

void KmerCounter::CountFasta(const char* seqfile)
{
    if(!_hash)
        Allocate((1UL << 2*_k));
    FastaReader reader(seqfile);
    if(!reader)
        throw IOError(std::string("cannot open the fasta file ") + seqfile);
    // count kmers
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        const char* seq = record->seq.c_str();
        size_t nkmer = record->seq.size() - _k + 1;
        // skip if the sequence is shorter than kmer length
        if(nkmer <= 0)
            continue;
        KmerType kmer = StringToKmer(seq, _k);
        AddKmer(kmer, 1);
        for(size_t i = 0; i < nkmer - 1; i++)
        {
            kmer = OverlapKmer(kmer, _k, seq[_k + i]);
            AddKmer(kmer, 1);
        }
        delete record;
        record = reader.ReadNext();
    }
    reader.Close();
}

void KmerCounter::WeightedCountFasta(const char* seqfile, int* coverage, size_t n_seq)
{
    FastaReader reader(seqfile);
    FastaRecord* record = reader.ReadNext();
    for(size_t iseq = 0; iseq < n_seq; iseq ++)
    {
        if(coverage[iseq] == 0)
            continue;
        const char* seq = record->seq.c_str();
        size_t nkmer = record->seq.size() - _k + 1;
        // skip if the sequence is shorter than kmer length
        if(nkmer <= 0)
            continue;
        KmerType kmer = StringToKmer(seq, _k);
        AddKmer(kmer, coverage[iseq]);
        for(size_t i = 0; i < nkmer - 1; i++)
        {
            kmer = OverlapKmer(kmer, _k, seq[_k + i]);
            AddKmer(kmer, coverage[iseq]);
        }
        delete record;
        record = reader.ReadNext();
    }
    reader.Close();
}

// calculate a histogram and store the values in the given array (of length bincount)
void KmerCounter::KmerCountHistogram(ValType bincount, unsigned long* values)
{
    int lastbin = bincount - 1;
    for(KmerType kmer = 0; kmer < _hash_size; kmer ++)
    {
        ValType bin = GetKmer(kmer);
        if(bin < bincount)
            values[bin] ++;
        else
            values[lastbin] ++;
    }
}

unsigned long KmerCounter::AverageCount()
{
    unsigned long total_count = 0;
    for(KmerType kmer = 0; kmer < _hash_size; kmer ++)
        total_count += _hash[kmer];
    return double(total_count)/double(_hash_size);
}

/*
#include "HyperplaneHash.h"
void KmerCounter::LSHCountFasta(const char* seqfile, const char* hyperplane_file, size_t n_bit)
{
    HyperplaneHash hash(_k, n_bit);
    hash.LoadHyperplanes(hyperplane_file);

    FastaReader reader(seqfile);

    FastaRecord* record = reader.ReadNext();
    VectorXcd kmer_code(_k);
    while(record)
    {
        const char* seq = record->seq.c_str();
        size_t nkmers = record->seq.size() - _k + 1;
        for(size_t i = 0; i < nkmers; i ++)
            AddKmer(hash.Hash(&(seq[i]), kmer_code), 1);
        delete record;
        record = reader.ReadNext();
    }
    reader.Close();
}
*/
