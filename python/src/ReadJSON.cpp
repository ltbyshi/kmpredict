#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;
// rapidjson's DOM-style APIs
#define RAPIDJSON_HAS_STDSTRING 1
#include <rapidjson/document.h>     
#include <rapidjson/filereadstream.h>
using rapidjson::Document;
using rapidjson::Value;
using rapidjson::SizeType;
using rapidjson::Reader;
using rapidjson::FileReadStream;
#include "Exception.h"

static inline std::vector<std::string> StringSplit(const std::string& s,
        char delimiter='\t')
{
    std::istringstream is(s);
    std::string tok;
    std::vector<std::string> v;
    while(std::getline(is, tok, delimiter))
        v.push_back(tok);
    return v;
}

static inline string StringJoin(const vector<std::string>& strs, const string& delimiter)
{
    size_t length = 0;
    for(size_t i = 0; i < strs.size(); i ++)
        length += strs[i].size();
    length += delimiter.size()*(strs.size() - 1);
    string s(length, 0);
    string::iterator it = s.begin();
    for(size_t i = 0; i < strs.size(); i ++)
    {
        if(i > 0)
            it = std::copy(delimiter.begin(), delimiter.end(), it);
        it = std::copy(strs[i].begin(), strs[i].end(), it);
    }
    return s;
}

static string GetValueTypeString(const Value& v)
{
    if(v.IsString()) return "string";
    else if(v.IsInt()) return "int";
    else if(v.IsNull()) return "null";
    else if(v.IsDouble()) return "double";
    else if(v.IsArray()) return "array";
    else if(v.IsObject()) return "object";
    else if(v.IsBool()) return "bool";
    else return "unknown";
}

static string GetValueString(const Value& v)
{
    ostringstream os;
    if(v.IsString()) os << v.GetString();
    else if(v.IsInt()) os << v.GetInt();
    else if(v.IsBool()) os << v.GetBool();
    else if(v.IsNull()) os << "null";
    else if(v.IsDouble()) os << v.GetDouble();
    else if(v.IsObject()) os << "<object>";
    else if(v.IsArray()) os << "<array>";
    else os << "<unknown_type>";
    return os.str();
}

static size_t GetValueSize(const Value& v)
{
    if(v.IsObject())
        return v.MemberCount();
    else if(v.IsArray())
        return v.Size();
    else
        return 1;
}

static void FindJSONByPath(Document& json,
                           const string& path,
                           const string& operation)
{
    vector<string> subpaths = StringSplit(path, '/');
    Value& e = json;
    for(size_t i = 0; i < subpaths.size(); i ++)
    {
        if(subpaths[i].size() == 0)
            continue;
        if(e.IsArray())
        {
            SizeType index;
            istringstream is(subpaths[i]);
            is >> index;
            if(index > e.Size())
            {
                vector<string> strs(i + 1);
                std::copy(subpaths.begin(), subpaths.begin() + i + 1, strs.begin());
                string subpath = StringJoin(strs, "/");
                cerr << "Error: index out of bounds in " << subpath << endl;
                exit(1);
            }
            e = e[index];
        }
        else if(e.IsObject())
        {
            if(!json.HasMember(subpaths[i]))
            {
                vector<string> strs(i + 1);
                std::copy(subpaths.begin(), subpaths.begin() + i + 1, strs.begin());
                string subpath = StringJoin(strs, "/");
                cerr << "Error: JSON path " << subpath << " does not exist" << endl;
                exit(1);
            }
            e = e[subpaths[i]];
        }
        else
        {
            vector<string> strs(i + 1);
            std::copy(subpaths.begin(), subpaths.begin() + i + 1, strs.begin());
            string subpath = StringJoin(strs, "/");
            cerr << "Error: JSON path " << subpath << " does not exist" << endl;
            exit(1);
        }
    }
    if(operation == "get")
        cout << GetValueString(e) << endl;
    else if(operation == "list")
    {
        if(e.IsObject())
        {
            for(Value::MemberIterator it = e.MemberBegin(); it < e.MemberEnd(); ++it)
                cout << it->name.GetString() << endl;
        }
        else if(e.IsArray())
        {
            for(SizeType i = 0; i < e.Size(); i ++)
                cout << GetValueString(e[i]) << endl;
        }
        else
            cout << GetValueString(e) << endl;
    }
    else if(operation == "size")
        cout << GetValueSize(e) << endl;
    else if(operation == "type")
        cout << GetValueTypeString(e) << endl;
    else
    {
        cerr << "Error: unknown operation: " << operation << endl;
        exit(1);
    }
}

int main(int argc, char** argv)
{
    if(argc != 4)
    {
        cerr << "Error: missing arguments" << endl;
        cerr << "Usage: " << argv[0] << " command json_file json_path" << endl;
        cerr << "Available commands: get list size type" << endl;
        exit(1);
    }
    string command(argv[1]);
    string json_file(argv[2]);
    string json_path(argv[3]);

    FILE* fp = fopen(json_file.c_str(), "r");
    if(!fp)
    {
        throw IOError("cannot open the JSON file " + json_file);
        exit(1);
    }
    Document json;
    Reader reader;
    char* buffer = new char[64*1024];
    FileReadStream is(fp, buffer, sizeof(buffer));
    json.ParseStream(is);
#if 0
    {
        cerr << "Error: cannot parse the JSON file" << endl;
        exit(1);
    }
#endif
    FindJSONByPath(json, json_path, command);
    delete[] buffer;
    fclose(fp);
    
    return 0;
}
