#ifndef __RELEVANCE_HELPER_H__
#define __RELEVANCE_HELPER_H__
#include <iostream>
template <typename DType>
void print_array1d(DType* data, int n)
{
    int i;
    std::cout << "array(" << n << ") = [";
    for(i = 0; i < n; i ++)
    {
        if(i > 0) std::cout << ", ";
        std::cout << data[i];
    }
    std::cout << ")" << std::endl;
}
#endif
