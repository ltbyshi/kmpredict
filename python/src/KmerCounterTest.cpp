#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <map>
#include <list>
#include <bitset>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
using namespace std;
#include <sys/time.h>
#include "Fasta.h"
#include "KmerCounter.h"
#include "Utils.h"
#include "KmerFilter.h"

#define PROGNAME "KmerCounterTest"


void TestKmerConversion()
{
    const char* sequences[] = {"ATGCG", "GCGTA", "TACGC", "CGCAT"};
    for(int i = 0; i < 4; i ++)
    {
        const char* seq = sequences[i];
        DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, 5);
        cout << "StringToKmer(" << seq << "): " << ToBinaryString(kmer) << endl;
        cout << "KmerToString(" << ToBinaryString(kmer) << "): " << KmerToString(kmer, 5) << endl;
    }
}

void TestKmerRC()
{
    const char* seq = "GAAA";
    int k = strlen(seq);
    DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
    DefaultKmerType kmerrc = KmerRC(kmer, k);
    string seqrc = KmerToString(kmerrc, k);
    cout << "Seq:     " << seq << endl;
    cout << "Kmer:    " << ToBinaryString(kmer) << endl;
    cout << "RC seq:  " << seqrc << endl;
    cout << "RC kmer: " << ToBinaryString(kmerrc) << endl;
}

void TestKmerRC2()
{
    int k = 4;
    cout << "Kmer\tKmerRC\tKmerRC2" << endl;
    for(DefaultKmerType kmer = 0; kmer < 4*4*4*4; kmer ++)
    {
        DefaultKmerType kmerrc = KmerRC(kmer, k, false);
        DefaultKmerType kmerrc2 = 0;
        string seq = KmerToString(kmer, k);
        for(int j = k - 1; j >= 0; j --)
        {
            kmerrc2 <<= 2;
            kmerrc2 |= DNACodeC[int(seq[j])];
        }
        if(kmerrc != kmerrc2)
        {
            cout << KmerToString(kmer, k) 
                << "\t" << KmerToString(kmerrc, k)
                << "\t" << KmerToString(kmerrc2, k) << endl;
            cout << "[ Kmer ] " << ToBinaryString(kmer) << endl;
            kmerrc = KmerRC(kmer, k, true);
            cout << endl;
        }
    }       
}       

void TestFastaReader()
{
    const char* fastaFileName = "test.fa";
    FastaReader reader(fastaFileName);
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        cout << "Name: " << record->name << endl;
        cout << record->seq << endl;
        delete record;
        record = reader.ReadNext();
    }
}

void TestKmerCounter()
{
    const char* fastaFileName = "test.fa";
    int k = 4;
    // calculate hash size
    unsigned int hashSize = 1;
    for(int i = 0; i < k; i ++)
        hashSize *= 4;
    
    FastaReader reader(fastaFileName);
    DefaultKmerCounter counter(hashSize, k);
    // count kmers
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        size_t nkmer = record->seq.size() - k + 1;
        for(size_t i = 0; i < nkmer; i ++)
            counter.Add(&(record->seq.c_str()[i]), 1);
        delete record;
        record = reader.ReadNext();
    }
    // output kmer counts
    for(unsigned int kmer = 0; kmer < hashSize; kmer ++)
    {
        if(counter.Get(kmer) > 0)
            cout << KmerToString(kmer, k) << "\t" << counter.Get(kmer) << endl;
    }
}

void TestOverlapKmer()
{
    const char* seq = "AGTCGTACGTAGCTAGCTGATCGATCGAT";
    int k = 4;
    int nkmer = strlen(seq) - k + 1;
    DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
    cout << seq << endl;
    cout << KmerToString(kmer, k) << endl;
    for(int i = 0; i < nkmer - 1; i ++)
    {
        kmer = OverlapKmer(kmer, k, seq[i + k]);
        for(int j = 0; j < i + 1; j ++)
            cout << " ";
        cout << KmerToString(kmer, k);
        for(int j = 0; j < nkmer - i; j ++)
            cout << " ";
        cout << ToBinaryString(kmer) << endl;
    }
}

void TestCalcKmerSize()
{
    cout << "K\tHashSize\tUniqueKmerCount" << endl;
    for(int k = 4; k <= 12; k ++)
        cout << k 
            << "\t" << CalcKmerHashSize<DefaultKmerType>(k)
            << "\t" << CalcUniqueKmerCount<DefaultKmerType>(k) << endl; 
}

void TestKmerRCSpeed()
{
    int k = 4;
    
    struct timeval tvstart;
    gettimeofday(&tvstart, NULL);
    
    MappedFile mfile("tmp/urandom");
    DefaultKmerType* kmers = (DefaultKmerType*)mfile.GetData();
    DefaultKmerType maxkmer = mfile.GetLength()/sizeof(DefaultKmerType);
    
    DefaultKmerType count = 0;
    for(DefaultKmerType kmer = 0; kmer < maxkmer; kmer ++)
    {
        DefaultKmerType kmerrc = KmerRC(kmers[kmer], k);
        if(kmer <= kmerrc)
            count ++;
    }
    
    struct timeval tvend;
    gettimeofday(&tvend, NULL);
    double totalms = ((1e6*tvend.tv_sec) + tvend.tv_usec - (1e6*tvstart.tv_sec) - tvstart.tv_usec);
    cout << "Maximum " << k << "-mer count: " << count << endl;
    cout << "(" << tvstart.tv_sec << "," << tvstart.tv_usec << ") => ("
        << tvend.tv_sec << "," << tvend.tv_usec << ")" << endl;
    cout << "RC operation speed: " << double(maxkmer)/totalms*1e6 << "/s" << endl;
}

void TimeIt(const char* message, void (*func)())
{
    cout << "Time it: " << message << endl;
    struct timeval tvstart;
    gettimeofday(&tvstart, NULL);
    
    func();
    
    struct timeval tvend;
    gettimeofday(&tvend, NULL);
    double totalus = ((1e6*tvend.tv_sec) + tvend.tv_usec - (1e6*tvstart.tv_sec) - tvstart.tv_usec);
    cout << "Total time: " << totalus/1e3 << "ms" << endl;
}

void TestReadFileSpeed_Direct()
{
    const char* filename = "tmp/urandom";
    ifstream fin(filename, ios::binary);
    fin.seekg(0, fin.end);
    size_t length = fin.tellg();
    char* buf = new char[length];
    fin.seekg(0, fin.beg);
    fin.read(buf, length);
    fin.close();
    
    unsigned int* data = (unsigned int*)buf;
    double sum = 0;
    for(size_t i = 0; i < length/sizeof(unsigned int); i ++)
        sum += data[i];
    cout << "Sum: " << sum << endl;
    delete[] data;
}

void TestReadFileSpeed_Mapped()
{
    MappedFile mfile("tmp/urandom");
    size_t length = mfile.GetLength();
    unsigned int* data = (unsigned int*)mfile.GetData();
    
    double sum = 0;
    for(size_t i = 0; i < length/sizeof(unsigned int); i ++)
        sum += data[i];
    cout << "Sum: " << sum << endl;
}

void TestReadFileSpeed()
{
    TimeIt("DirectReadFile", TestReadFileSpeed_Direct);
    TimeIt("MappedReadFile", TestReadFileSpeed_Mapped);
}

void TestBitSet()
{
    const size_t size = (1L << 24);
    bitset<size> bs1;
    BitSet bs2(size);
    for(size_t i = 0; i < 10240; i ++)
    {
        int index = rand() % size;
        bs1[index] = true;
        bs2.Set(index);
        
        index = rand() % size;
        bs1[index] = false;
        bs2.Clear(index);
    }
    for(size_t i = 0; i < size; i ++)
    {
        if(bs1[i] != bs2[i])
        {
            cout << "Bitset unequal: " << i << endl;
            break;
        }
    }
}

void TestBitSetUnpack()
{
    const size_t size = (1L << 32);
    BitSet bs(size);
    for(size_t i = 0; i < 10240; i ++)
        bs.Set(rand() % 24);
    vector<bool> values;
    bs.Unpack(values);
    cout << bs[rand() % size] << endl;
}

void PerfBitSet()
{
    const size_t size = (1L << 32);
    BitSet bs(size);
    for(size_t i = 0; i < size; i ++)
        bs.Set(i);
    cout << "BitSet size: " << size << ", all true: " << bs.All() << endl;
}

void TestKmerFilter(int k = 15)
{
    const char* seqfile = "Scaffold/random_01/scaffold.fa";
    const char* kmerfile = "tmp/random_01.kmer.filter";
    
    FastaReader reader(seqfile);
    DefaultKmerFilter filter(k);
    // count kmers
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        const char* seq = record->seq.c_str();
        size_t nkmer = record->seq.size() - k + 1;
        // skip if the sequence is shorter than kmer length
        if(nkmer <= 0)
            continue;
        DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
        filter.Add(kmer);
        for(size_t i = 0; i < nkmer - 1; i++)
        {
            kmer = OverlapKmer(kmer, k, seq[k + i]);
            filter.Add(kmer);
        }
        delete record;
        record = reader.ReadNext();
    }
    reader.Close();
    filter.Save(kmerfile, false);
    //filter.DumpToTable();
    
    filter.Load(kmerfile);
    //filter.DumpToTable();
    DefaultKmerType hashsize = filter.GetHashSize();
    DefaultKmerType kmernum = filter.GetKmerNum();
    double occupancy = double(kmernum)/hashsize;
    cout << "K=" << k << ", HashSize=" << hashsize
        << ", Kmers=" << kmernum
        << ", Occupancy=" << occupancy << endl;
}

void TestFisherExactTest()
{
    int k = 250, m = 300, n = 200;
    FisherExactTest fisher(m + n);
    for(int x = 0; x <= k; x ++)
        printf("FisherTest(%d,%d,%d,%d): %lf\n", x, m, n, k, 
               (double)fisher.TestGreater(x, m, n, k));
}

#if 0
void TestEntropy()
{

    double a[6*5] = {
        1.0, 1.0, 1.0, 1.0, 1.0,
        1.0, 0.5, 0.5, 0.5, 0.5,
        20.0, 1.0, 1.0, 1.0, 1.0,
        8.0, 3.0, 5.0, 4.0, 1.0,
        1.0, 2.0, 3.0, 4.0, 5.0,
        8.0, 8.0, 2.0, 1.0, 2.0
    };
    for(int i = 0; i < 6; i ++)
    {
        vector<double> x(a + i*5, a + (i+1)*5);

    for(int i = 0; i < 10; i ++)
    {
        vector<double> x(5);
        for(int j = 0; j < 5; j ++)
        {
            x[j] = drand48();
            cout << x[j] << " ";
        }
        double e = Entropy(x);
        cout << ": " << e << endl;
    }
}
#endif

    
void ConvertKmerFile()
{
    const char* kmerfile = "tmp/kmer.20.hashed";
    KmerFilterHeader_old header;
    ifstream fin(kmerfile, ios::binary);
    fin.read((char*)&header, sizeof(header));
    fin.close();
    
    cout << "Old header of " << kmerfile << endl;
    cout << "HeaderSize\t" << header.headersize << endl;
    cout << "K\t" << header.k << endl;
    cout << "UnitSize\t" << header.unitsize << endl;
    cout << "HashSize\t" << header.hashsize << endl;
    cout << "NumUnits\t" << header.nunits << endl;
}

void ProfileKmerEncoder()
{
    const int k = 32;
    const char* seqfile = "Scaffold/random_01/scaffold.fa";
    size_t hashsize = (1UL << 10);
    size_t mask = hashsize - 1;
    long totalkmers = 0;
    vector<int> counts(hashsize);
    FastaReader reader(seqfile);
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        const char* seq = record->seq.c_str();
        size_t nkmer = record->seq.size() - k + 1;
        if(nkmer > 0)
        {
            DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
            counts[kmer & mask] += 1;
            totalkmers ++;
            for(size_t i = 0; i < nkmer - 1; i++)
            {
                kmer = OverlapKmer(kmer, k, seq[k + i]);
                counts[kmer & mask] += 1;
                totalkmers ++;
            }
        }
        delete record;
        record = reader.ReadNext();
    }
    delete record;
    reader.Close();
    
    DefaultKmerType randkmer = rand() & mask;
    cout << "count(" << randkmer << "): " << counts[randkmer] << endl;
    cout << "totalkmers: " << totalkmers << endl;
}

int main(int argc, char** argv)
{
    // TestKmerRCSpeed();
    // TestReadFileSpeed();
    srand(time(0));
    srand48(time(0));
    //TimeIt("PerfBitSet", PerfBitSet);
    //TestBitSet();
    //for(int k = 15; k <= 15; k ++)
    //    TestKmerFilter(k);
    // TestBitSetUnpack();
    //TestFisherExactTest();
    // TestEntropy();
    //ConvertKmerFile();
    ProfileKmerEncoder();
    return 0;
}