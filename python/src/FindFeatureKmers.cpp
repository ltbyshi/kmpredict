#include <iostream>
#include <fstream>
#include <string>
#include <map>
using namespace std;
#include <stdlib.h>
#include <math.h>

#include "Fasta.h"
#include "KmerFilter.h"
#include "Utils.h"
#include "Thread.h"
#include "HDF5Utils.h"


#define OPT_EXCLUSIVE 0
#define OPT_FISHER  0
#define OPT_ENTROPY 1
#define OPT_PROGRESS 0
#define VERBOSE 0
#define PROGNAME "FindFeatureKmers"

class FisherExactTestJob: public Job
{
public:
    FisherExactTestJob() {}
    virtual ~FisherExactTestJob() {}
    virtual int Start()
    {
        return 0;
    }
public:
    // input
    DefaultKmerType kmer_start, kmer_end;
    vector<vector<unsigned char> >* nkmer_per_group;
    vector<int>* nsamples_pg;
    vector<int>* nsamples_other_pg;
    // output
    vector<int>* sig_group;
    vector<double>* pvalue;
};

class EntropyJob: public Job
{
public:
    EntropyJob(DefaultKmerType kmer_start, DefaultKmerType kmer_end,
                        int ngroups,
                        const vector<int>& nsamples_pg,
                        const vector<unsigned char>& nkmer_per_group,
                        vector<float>& entropies,
                        vector<float>& max_nkmer_ratios
              )
    : kmer_start(kmer_start), kmer_end(kmer_end),
    ngroups(ngroups), nsamples_pg(nsamples_pg),
    nkmer_per_group(nkmer_per_group),
    entropies(entropies),
    max_nkmer_ratios(max_nkmer_ratios)
    {}
    virtual ~EntropyJob() {}
    virtual int Start()
    {
        cerr << "Calculating entropy of each kmer" << endl;
        vector<double> c1(ngroups);
        vector<double> nsamples_pg_c(ngroups);
        for(int i = 0; i < ngroups; i ++)
        {
            double c2 = 1;
            nsamples_pg_c[i] = nsamples_pg[i] + c2;
            c1[i] = 1;
        }
        
#if OPT_PROGRESS
        DefaultKmerType totalkmers = kmer_end - kmer_start;
        ProgressBar progressbar(totalkmers, 50);
        DefaultKmerType counter = 0;
        DefaultKmerType blocksize = totalkmers/1000;
#endif
        for(size_t kmer = kmer_start, index = kmer_start*ngroups;
            kmer < kmer_end;
            kmer ++, index += ngroups)
        {
            vector<double> kmer_ratio(ngroups);
            max_nkmer_ratios[kmer] = 0;
            for(int i = 0; i < ngroups; i ++)
            {
                //int nkmer = nkmer_per_group[i][kmer];
                int nkmer = nkmer_per_group[index + i];
                kmer_ratio[i] = (double)(c1[i] + nkmer)/nsamples_pg_c[i];
                if(kmer_ratio[i] > max_nkmer_ratios[kmer])
                    max_nkmer_ratios[kmer] = kmer_ratio[i];
            }
            
            double entropy = Entropy(kmer_ratio);
            entropies[kmer] = entropy;

#if OPT_PROGRESS
            counter ++;
            if(counter >= blocksize)
            {
                counter = 0;
                progressbar.Update(kmer);
            }
            
#endif
        }
#if OPT_PROGRESS
        progressbar.Finish();
#endif
        cerr << "Calculating entropy finished" << endl;
        return 0;
    }
public:
    // input
    DefaultKmerType kmer_start, kmer_end;
    int ngroups;
    const vector<int>& nsamples_pg;
    const vector<unsigned char>& nkmer_per_group;
    // output
    vector<float>& entropies;
    vector<float>& max_nkmer_ratios;
};

int CountSamplesPerKmer(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Get characteristic kmers for each group" << endl;
        cerr << "Usage: " << PROGNAME << " count groupfile prefix" << endl;
        cerr << "\tgroupfile\ta text file with group labels and kmerfile name per line" << endl;
        cerr << "\tprefix\tstring prepended to the output filenames" << endl;
        exit(1);
    }
    const char* groupfile = argv[1];
    string prefix = argv[2];
    vector<string> groupmap;
    vector<int> groups;
    vector<string> kmerfiles;
    
    ReadGroupFile(groupfile, groupmap, groups, kmerfiles);
    if(groups.size() != kmerfiles.size())
        Die("number of samples in the groupfile does not match the number of kmerfiles");
    
    int ngroups = groupmap.size();
    int nsamples = kmerfiles.size();
    // detect K
    cerr << "Detect K and hashsize" << endl;
    DefaultKmerFilter* counter = new DefaultKmerFilter;
    counter->Load(kmerfiles[0].c_str());
    int k = counter->GetK();
    DefaultKmerType hashsize = counter->GetHashSize();
    delete counter;
    counter = NULL;
    cerr << "K=" << k << ", hashsize=" << hashsize << endl;
    
    vector<int> nsamples_pg(ngroups);
    // number of samples in all other groups
    vector<int> nsamples_other_pg(ngroups);
    for(int i = 0; i < nsamples; i ++)
        nsamples_pg[groups[i]] ++;
    for(int i = 0; i < ngroups; i ++)
        nsamples_other_pg[i] = nsamples - nsamples_pg[i];
    // count the number of samples that contain each kmers
    cerr << "Read kmerfiles" << endl;
    vector<unsigned char> nkmer_per_group(size_t(ngroups)*hashsize);
    ReadKmerFiles(groups, ngroups, kmerfiles, hashsize, nsamples, nkmer_per_group);
    cerr << "Save samples per kmer" << endl;
    SaveVector(nkmer_per_group, prefix + "nkmerpg");
    
    ofstream fout(prefix + "groupinfo.txt");
    for(int i = 0; i < ngroups; i ++)
        fout << groupmap[i] << "\t" << nsamples_pg[i] << endl;
    fout.close();
    
    return 0;
}

int CalculateEntropy(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Get characteristic kmers for each group" << endl;
        cerr << "Usage: " << PROGNAME << " entropy groupfile prefix" << endl;
        cerr << "\tgroupfile\ta text file with group labels and kmerfile name per line" << endl;
        cerr << "\tprefix\tstring prepended to the output filenames" << endl;
        exit(1);
    }
    const char* groupfile = argv[1];
    string prefix = argv[2];

    vector<string> groupnames;
    vector<int> groupids;
    vector<string> kmerfiles;
    ReadGroupFile(groupfile, groupnames, groupids, kmerfiles);
    int ngroups = groupnames.size();
    int nsamples = kmerfiles.size();
    vector<int> nsamples_pg(ngroups);
    for(int i = 0; i < nsamples; i ++)
        nsamples_pg[groupids[i]] ++;
    
    cerr << "Read number of samples that contain each kmer" << endl;
    vector<unsigned char> nkmer_per_group;
    LoadVector(nkmer_per_group, prefix + "nkmerpg");
    size_t hashsize = nkmer_per_group.size() / size_t(ngroups);
    // pseudo counts
    vector<double> c1(ngroups);
    vector<double> nsamples_pg_c(ngroups);
    for(int i = 0; i < ngroups; i ++)
    {
        double c2 = 1;
        nsamples_pg_c[i] = nsamples_pg[i] + c2;
        c1[i] = 1;
    }
    // calculate entropy
    vector<float> entropies(hashsize);
    vector<float> max_nkmer_ratios(hashsize);
    const int nthreads = 16;
    ThreadPool pool(nthreads);
    vector<EntropyJob*> entropy_jobs(nthreads);
    size_t blocksize = hashsize / nthreads;
    for(int i = 0; i < nthreads - 1; i ++)
    {
        entropy_jobs[i] = new EntropyJob(i*blocksize, (i + 1)*blocksize, ngroups,
                 nsamples_pg, nkmer_per_group, entropies, max_nkmer_ratios);
    }
    entropy_jobs[nthreads - 1] = new EntropyJob(blocksize*(nthreads - 1),
            hashsize, ngroups, nsamples_pg, nkmer_per_group, entropies,
            max_nkmer_ratios);
    cerr << "Calculating entropies" << endl;
    for(int i = 0; i < nthreads; i ++)
        pool.Submit(entropy_jobs[i]);
    pool.Wait();
    for(int i = 0; i < nthreads; i ++)
        delete entropy_jobs[i];
    
    pool.Shutdown();
    pool.Join();
    // save the entropy values
    cerr << "Save entropies" << endl;
    SaveVector(entropies, prefix + "entropies");
    cerr << "Save maxratio" << endl;
    SaveVector(max_nkmer_ratios, prefix + "maxratio");
    
    return 0;
}

int FindFeatureKmers(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Get characteristic kmers for each group" << endl;
        cerr << "Usage: " << PROGNAME << " groupfile prefix" << endl;
        cerr << "\tgroupfile\ta text file with group labels and kmerfile name per line" << endl;
        cerr << "\tprefix\tstring prepended to the output filenames" << endl;
        exit(1);
    }
    else
    {
        const char* groupfile = argv[1];
        string prefix = argv[2];
        vector<string> groupmap;
        vector<int> groups;
        vector<string> kmerfiles;
        
        ReadGroupFile(groupfile, groupmap, groups, kmerfiles);
        if(groups.size() != kmerfiles.size())
            Die("number of samples in the groupfile does not match the number of kmerfiles");
        
        int ngroups = groupmap.size();
        int nsamples = kmerfiles.size();
        // detect K
        cerr << "Detect K and hashsize" << endl;
        DefaultKmerFilter* counter = new DefaultKmerFilter;
        counter->Load(kmerfiles[0].c_str());
        int k = counter->GetK();
        DefaultKmerType hashsize = counter->GetHashSize();
        delete counter;
        counter = NULL;
        cerr << "K=" << k << ", hashsize=" << hashsize << endl;
        // number of samples that has a particular kmer in each group
        // the array is composed of hashsize chunks (ngroups elements per chunk)
        vector<unsigned char> nkmer_per_group(size_t(ngroups)*hashsize);
        //vector<vector<unsigned char> > nkmer_per_group(ngroups);
        //for(int i = 0; i < ngroups; i ++)
        //    nkmer_per_group[i].resize(hashsize);
        /*
        vector<DefaultKmerFilter> counters(ngroups);
        for(int i = 0; i < ngroups; i ++)
            counters[i].Create(k);
        */
        // number of samples in one group
        vector<int> nsamples_pg(ngroups);
        // number of samples in all other groups
        vector<int> nsamples_other_pg(ngroups);
        for(int i = 0; i < nsamples; i ++)
            nsamples_pg[groups[i]] ++;
        for(int i = 0; i < ngroups; i ++)
            nsamples_other_pg[i] = nsamples - nsamples_pg[i];
        
        // read kmer files and count the number of samples that has a kmer
        if(getenv("ReadKmerFiles"))
        {
            cerr << "Read kmerfiles" << endl;
            ReadKmerFiles(groups, ngroups, kmerfiles, hashsize, nsamples, nkmer_per_group);
            SaveVector(nkmer_per_group, prefix + "nkmerpg");
        }
        else
        {
            cerr << "Load kmer counts" << endl;
            LoadVector(nkmer_per_group, prefix + "nkmerpg");
        }
        // count feature kmers
#if OPT_FISHER
        FisherExactTest fishertest(nsamples+1);
        vector<double> pvalues(ngroups);
#endif
#if OPT_ENTROPY
        // pseudo counts
        vector<double> c1(ngroups);
        vector<double> nsamples_pg_c(ngroups);
        for(int i = 0; i < ngroups; i ++)
        {
            double c2 = 1;
            nsamples_pg_c[i] = nsamples_pg[i] + c2;
            c1[i] = 1;
        }
        vector<float> entropies(hashsize);
        /*
        cout << "Kmer";
        for(int i = 0; i < ngroups; i ++)
            cout << "\tGroup_" << i;
        cout << "\tCharGroup\tEntropy" << endl;*/
        vector<float> max_nkmer_ratios(hashsize);
        vector<float> avg_nkmer_ratios(hashsize);
        if(getenv("CalcEntropy"))
        {
            ThreadPool pool(4);
            EntropyJob* entropy_job = new EntropyJob(0, hashsize, ngroups,
                                                     nsamples_pg,
                                                     nkmer_per_group,
                                                     entropies,
                                                     max_nkmer_ratios
            );
            pool.Submit(entropy_job);
            pool.Wait();
            pool.Shutdown();
            pool.Join();
            delete entropy_job;
            SaveVector(entropies, prefix + "entropies");
        }
        else
        {
            cerr << "Load entropy values" << endl;
            LoadVector(entropies, prefix + "entropies");
        }
        
        //LoadVector(char_group, prefix + "group");
        for(size_t kmer = 0, index= 0;
            kmer < hashsize;
        kmer ++, index += ngroups)
        {
            float max_nkmer_ratio = 0;
            float avg_nkmer_ratio = 0;
            vector<double> kmer_ratio(ngroups);
            for(int i = 0; i < ngroups; i ++)
            {
                int nkmer = nkmer_per_group[index + i];
                kmer_ratio[i] = (double)(c1[i] + nkmer)/nsamples_pg_c[i];
                if(kmer_ratio[i] > max_nkmer_ratio)
                {
                    max_nkmer_ratio = kmer_ratio[i];
                    avg_nkmer_ratio += kmer_ratio[i];
                }
            }
            max_nkmer_ratios[kmer] = max_nkmer_ratio;
            avg_nkmer_ratio /= ngroups;
            avg_nkmer_ratios[kmer] = avg_nkmer_ratio;
        }
        cerr << "Save max kmer ratios" << endl;
        SaveVector(max_nkmer_ratios, prefix + "maxratio");
        //cerr << "Save average kmer ratios" << endl;
        //SaveVector(avg_nkmer_ratios, prefix + "avgratio");
                
#endif
    }
    return 0;
}

int SelectByEntropy(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Select feature kmers by entropy and maxratio" << endl;
        cerr << "Usage: " << PROGNAME << "select_en groupfile prefix" << endl;
        cerr << "\tprefix\tstring prepended to the output filenames" << endl;
        exit(1);
    }
    
    string groupfile = argv[1];
    string prefix = argv[2];
    
    vector<string> groupnames;
    vector<int> groupids;
    vector<string> kmerfiles;
    ReadGroupFile(groupfile, groupnames, groupids, kmerfiles);
    int ngroups = groupnames.size();
    int nsamples = kmerfiles.size();
    vector<int> nsamples_pg(ngroups);
    for(int i = 0; i < nsamples; i ++)
        nsamples_pg[groupids[i]] ++;
    
    cerr << "Load max kmer ratios" << endl;
    vector<float> max_nkmer_ratios;
    LoadVector(max_nkmer_ratios, prefix + "maxratio");

    vector<float> entropies;
    cerr << "Load entropy values" << endl;
    LoadVector(entropies, prefix + "entropies");
    
    vector<unsigned char> nkmer_per_group;
    cerr << "Load number of samples that contain each kmer" << endl;
    LoadVector(nkmer_per_group, prefix + "nkmerpg");
    
    size_t hashsize = entropies.size();

    //double quantile_proportion = 0.2;
    
    //double entropy_thresh = Quantile(entropies, 0, hashsize*quantile_proportion, 0.999);
    double entropy_thresh = 0.1;
    double maxratio_thresh = 0.2;
    DefaultKmerType feature_kmer_count = 0;
    cerr << "Entropy threshold: " << entropy_thresh << endl;
    cerr << "MaxRatio threashold: " << maxratio_thresh << endl;
    //exit(1);
    cerr << "Select feature kmers" << endl;
    FILE* fout_featkmers = fopen((prefix + "featkmers.txt").c_str(), "w");
    if(!fout_featkmers)
        Die("cannot open the output file ", (prefix + "featkmers.txt").c_str());
    for(DefaultKmerType kmer = 0, index = 0;
        kmer < hashsize;
        kmer ++, index += ngroups)
    {
        if((entropies[kmer] >= entropy_thresh) 
            && (max_nkmer_ratios[kmer] >= maxratio_thresh))
        {
            fprintf(fout_featkmers, "%08lX", kmer);
            for(int i = 0; i < ngroups; i ++)
                fprintf(fout_featkmers, "\t%d(%.3f)", int(nkmer_per_group[index + i]),
                       double(nkmer_per_group[index + i])/nsamples_pg[i]);
            fprintf(fout_featkmers, "\t%f\n", entropies[kmer]);
            feature_kmer_count ++;
        }
    }
    fclose(fout_featkmers);
    
    {
        ofstream fout(prefix + "summary.txt");
        fout << "FeatureKmerCount\t" << feature_kmer_count << endl;
        fout.close();
    }
    
    return 0;
}

int SelectByOccurence(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Select feature kmers by entropy and maxratio" << endl;
        cerr << "Usage: " << PROGNAME << " select_oc groupfile prefix" << endl;
        cerr << "\tprefix\tstring prepended to the output filenames" << endl;
        exit(1);
    }
    
    string groupfile = argv[1];
    string prefix = argv[2];
    
    vector<string> groupnames;
    vector<int> groupids;
    vector<string> kmerfiles;
    ReadGroupFile(groupfile, groupnames, groupids, kmerfiles);
    int ngroups = groupnames.size();
    int nsamples = kmerfiles.size();
    
    vector<int> nsamples_pg(ngroups);
    for(int i = 0; i < nsamples; i ++)
        nsamples_pg[groupids[i]] ++;
    
    cerr << "Load max kmer ratios" << endl;
    vector<float> max_nkmer_ratios;
    LoadVector(max_nkmer_ratios, prefix + "maxratio");

    vector<float> entropies;
    cerr << "Load entropy values" << endl;
    LoadVector(entropies, prefix + "entropies");
    
    vector<unsigned char> nkmer_per_group;
    cerr << "Load number of samples that contain each kmer" << endl;
    LoadVector(nkmer_per_group, prefix + "nkmerpg");
    DefaultKmerType hashsize = nkmer_per_group.size()/ngroups;
    cerr << "Hashsize: " << hashsize << endl;
    
    vector<int> nsamples_per_kmer(hashsize);
    //int nfeature = 0;
    for(DefaultKmerType kmer = 0, index = 0;
        kmer < hashsize;
        kmer ++, index += ngroups)
    {
        nsamples_per_kmer[kmer] = 0;
        for(int i = 0; i < ngroups; i ++)
            nsamples_per_kmer[i] += nkmer_per_group[index + i];
    }
    
    cerr << "Sort kmers by occurence" << endl;
    const unsigned int nselect = 1000000;
    vector<unsigned int> indices(hashsize);
    for(size_t i = 0; i < hashsize; i ++)
        indices[i] = i;
    std::sort(indices.begin(), indices.end(), 
              ArgGreater<vector<int> >(nsamples_per_kmer));
    
    cerr << "Output kmers" << endl;
    vector<unsigned int> selected(nselect);
    vector<float> selected_entropies(nselect);
    for(size_t i = 0; i < nselect; i ++)
    {
        selected[i] = indices[i];
        unsigned long kmer = indices[i];
        size_t offset = kmer*ngroups;
#if 0
        fprintf(stdout, "%08lX", kmer);
        for(int g = 0; g < ngroups; g ++)
            fprintf(stdout, "\t%d(%.3f)", int(nkmer_per_group[offset + g]),
                   double(nkmer_per_group[offset + g])/nsamples_pg[g]);
        fprintf(stdout, "\t%f", entropies[kmer]);
        fprintf(stdout, "\n");
        if(i > 10)
            break;
#endif
        selected_entropies[i] = entropies[kmer];
    }
    //WriteVectorToHDF5<float>(prefix + "select_oc.entropies", "data", selected_entropies);
    WriteVectorToHDF5<unsigned int>(prefix + "selected_oc.kmer", "kmer", selected);
    
    return 0;
}

int CreateKmerFreqMatrix(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Select feature kmers by entropy and maxratio" << endl;
        cerr << "Usage: " << PROGNAME << " mat groupfile prefix" << endl;
        cerr << "\tprefix\tstring prepended to the output filenames" << endl;
        exit(1);
    }
    
    string groupfile = argv[1];
    string prefix = argv[2];
    
    vector<string> groupnames;
    vector<int> groupids;
    vector<string> kmerfiles;
    ReadGroupFile(groupfile, groupnames, groupids, kmerfiles);
    int ngroups = groupnames.size();
    int nsamples = kmerfiles.size();
    
    vector<unsigned int> selected;
    logger.Info() << "Read selected kmers " << prefix + "selected_oc.kmer" << Endl;
    
    ReadVectorFromHDF5(prefix + "selected_oc.kmer", "kmer", selected);
    vector<DefaultValType> mat(selected.size()*nsamples);
    
#if MULTITHREAD
    class ReadKmerFileJob: public Job {
    public:
        ReadKmerFileJob(const string& kmerfile, size_t i,
                        vector<DefaultValType>& mat,
                        vector<unsigned int> selected
                       ) 
            : _kmerfile(kmerfile), _i(i), _mat(mat), _selected(selected) {}
        int Start()
        {
            vector<DefaultValType> counts;
            logger.Info() << "Read kmer counts file " << _kmerfile << Endl;
            ReadVectorFromHDF5<DefaultValType>(_kmerfile, "data", counts);
            for(size_t si = 0; si < _selected.size(); si ++)
                _mat[_i + si] = counts[_selected[si]];
            return 0;
        }
    private:
        string _kmerfile;
        size_t _i;
        vector<DefaultValType>& _mat;
        vector<unsigned int>& _selected;
    };
    {
        ThreadPool pool(1);
        vector<Job*> jobs(nsamples);
        for(int i = 0; i < nsamples; i ++)
        {
            jobs[i] = new ReadKmerFileJob(kmerfiles[i], i, mat, selected);
            pool.Submit(jobs[i]);
        }
        pool.Wait();
        for(int i = 0; i < nsamples; i ++)
            delete jobs[i];
        pool.Shutdown();
    }
#else
    ProgressBar progressbar(nsamples, 50);
    for(int i = 0; i < nsamples; i ++)
    {
        vector<DefaultValType> counts;
        logger.Info() << "Read kmer counts file " << kmerfiles[i] << Endl;
        ReadVectorFromHDF5<DefaultValType>(kmerfiles[i], "data", counts);
        for(size_t si = 0; si < selected.size(); si ++)
        {
            mat[i + si] = counts[selected[si]];
        }
        progressbar.Increment(1);
    }
    progressbar.Finish();
#endif
    logger.Info() << "Write kmer freq matrix file " << prefix + "selected_oc.mat" << Endl;
    WriteMatrixToHDF5<DefaultValType>(prefix + "selected_oc.mat", "data", 
                                      selected.size(), nsamples, mat);
    
    return 0;
}

int main(int argc, char** argv)
{
    HandlerMap handlers;
    handlers["count"] = CountSamplesPerKmer;
    handlers["entropy"] = CalculateEntropy;
    handlers["select_en"] = SelectByEntropy;
    handlers["select_oc"] = SelectByOccurence;
    handlers["mat"] = CreateKmerFreqMatrix;
    ParseArguments(handlers, PROGNAME, argc, argv);
    //FindFeatureKmers(argc, argv);
    
    return 0;
}
