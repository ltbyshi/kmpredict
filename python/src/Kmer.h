#ifndef __KMER_H__
#define __KMER_H__
#include <vector>
#include <string>
#include <map>
#include <stdint.h>
#include "Nucleotide.h"
// kmer calculation utility functions
typedef uint64_t KmerType;
typedef uint32_t ValType;

typedef uint64_t DefaultKmerType;
typedef uint32_t DefaultValType;

const uint32_t LowBitMask[17] = {
    0x00000000,0x00000003,0x0000000F,0x0000003F,
    0x000000FF,0x000003FF,0x00000FFF,0x00003FFF,
    0x0000FFFF,0x0003FFFF,0x000FFFFF,0x003FFFFF,
    0x00FFFFFF,0x03FFFFFF,0x0FFFFFFF,0x3FFFFFFF,
    0xFFFFFFFF
};

inline KmerType CalcKmerHashSize(int k)
{
    return (1UL << (k*2));
}

inline KmerType CalcUniqueKmerCount(int k)
{
    if(k % 2 == 0)
        return (((1L << (k*2)) + (1L << k)) >> 1);
    else
        return (1L << (k*2 - 1));
}

// reverse complement
inline KmerType KmerRC(KmerType kmer, int k, bool debug = false)
{
    KmerType rc = 0;
    for(int i = 0; i < k*2; i += 2)
    {
        rc <<= 2;
        rc += (~(kmer >> i) & 0x03);
    }
    return rc;
}

inline KmerType StringToKmer(const char* seq, int k)
{
    KmerType kmer = 0;
    for(int i = 0; i < k; i ++)
    {
        kmer <<= 2;
        kmer |= DNACode[int(seq[i])];
    }
    return kmer;
}

inline std::string KmerToString(KmerType kmer, int32_t k)
{
    std::string seq(k, 0);
    for(int i = k - 1; i >= 0; i --)
    {
        seq[i] = DNAStr[kmer & 0x03];
        kmer >>= 2;
    }
    return seq;
}  

inline KmerType OverlapKmer(KmerType kmer, int k, int s)
{
    return (((kmer << 2) + DNACode[s]) & LowBitMask[k]);
}

struct ExactHash
{
    int k;
    KmerType kmer;
    
    ExactHash(int k): k(k) {}
    
    inline KmerType operator()(const char* s) {
        kmer = 0;
        for(int i = 0; i < k; i ++)
        {
            kmer <<= 2;
            kmer |= DNACode[int(s[i])];
        }
        return kmer;
    }
    
    inline KmerType IncHash(int c) {
        kmer = (((kmer << 2) + DNACode[c]) & LowBitMask[k]);
        return kmer;
    }
};

#endif
