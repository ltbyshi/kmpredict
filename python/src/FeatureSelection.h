#ifndef __FEATURESELECTION_H__
#define __FEATURESELECTION_H__
#include <cmath>

template <typename Type>
inline Type Entropy(Type x, Type y)
{
    Type total = x + y;
    Type px = x/total;
    Type py = y/total;
    return -px*std::log(px) - py*std::log(py);
}

template <typename Type>
inline Type InfoGain(Type tp, Type tn, Type fp, Type fn)
{
    Type total = tp + tn + fp + fn;
    Type pos = tp + fn;
    Type neg = fp + tn;
    Type ptru = (tp + fp)/total;
    Type pfal = 1 - ptru;
    return Entropy<Type>(pos, neg)
        - ptru*Entropy<Type>(tp, fp)
        - pfal*Entropy<Type>(fn, tn);
}

template <typename Type>
inline Type MutualInfo(Type tp, Type tn, Type fp, Type fn)
{
    Type total = tp + tn + fp + fn;
    Type pos = tp + fn;
    Type neg = fp + tn;
    Type tru = tp + fp;
    Type fal = fn + tn;
    Type mi = (tp)*std::log(tp/(tru*pos))
            + (fn)*std::log(fn/(tru*neg))
            + (fp)*std::log(fp/(fal*pos))
            + (tn)*std::log(tn/(fal*neg));
    mi /= total;
    return mi;
}

template <typename Type>
inline Type Accuracy(Type tp, Type tn, Type fp, Type fn)
{
    return tp - fp;
}

template <typename Type>
inline Type BalancedAccuracy(Type tp, Type tn, Type fp, Type fn)
{
    Type tpr = tp/(tp + tn);
    Type fpr = fp/(fp + fn);
    return std::abs(tpr - fpr);
}

template <typename Type>
inline Type ProbabilityRatio(Type tp, Type tn, Type fp, Type fn)
{
    Type tpr = tp/(tp + tn);
    Type fpr = fp/(fp + fn);
    return tpr/fpr;
}

template <typename Type>
inline Type BiNormalSeperation(Type tp, Type tn, Type fp, Type fn)
{
    return 0;
}

template <typename Type>
inline Type OddsRatio(Type tp, Type tn, Type fp, Type fn)
{
    return (tp*tn)/(fp*fn);
}

template <typename Type>
inline Type Frequency(Type tp, Type tn, Type fp, Type fn)
{
    return tp + fp;
}

template <typename Type>
inline Type Correlation(Type tp, Type tn, Type fp, Type fn)
{
    Type tpr = tp/(tp + tn);
    Type fpr = fp/(fp + fn);
    return 0;
}

template <typename Type>
inline Type ChiSquaredTerm(Type count, Type expected)
{
    Type diff = count - expected;
    return diff*diff/expected;
}

template <typename Type>
inline Type ChiSquared(Type tp, Type tn, Type fp, Type fn)
{
    Type total = tp + tn + fp + fn;
    Type ppos = (tp + fn)/total;
    Type pneg = (fp + tn)/total;
    return ChiSquaredTerm(tp, (tp + fp)/ppos) +
           ChiSquaredTerm(fn, (tn + fn)/ppos) +
           ChiSquaredTerm(fp, (tp + fp)/pneg) +
           ChiSquaredTerm(tn, (fn + tn)/pneg);
}


#endif
