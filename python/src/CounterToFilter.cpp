#include <iostream>
using namespace std;
// user library
#include "KmerFilter.h"
#include "KmerCounter.h"
#include "Utils.h"

#define PROGNAME "CounterToFilter"

int CounterToFilter(int argc, char** argv)
{
    if(argc != 4)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Convert KmerCounter format to KmerFilter format" << endl;
        cerr << "Usage: " << PROGNAME << " counter_file filter_file threshold" << endl;
        cerr << "\tcounter_file:\toutput file from KmerCounter" << endl;
        cerr << "\tfilter_file:\toutput file from KmerFilter" << endl;
        cerr << "\tthreshold:\tleast counts required for each kmer" << endl;
        exit(1);
    }
    const char* counter_file = argv[1];
    const char* filter_file = argv[2];
    DefaultValType threshold = atoi(argv[3]);
    
    DefaultKmerCounter counter;
    counter.LoadFromHDF5(counter_file);
    //cout << "K=" << counter.GetK()  << ", HashSize=" << counter.GetHashSize() << endl;
    DefaultKmerFilter filter(counter.GetK(), counter.GetHashSize());
    DefaultKmerType hashsize = counter.GetHashSize();
    for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
    {
        if(counter.Get(kmer) >= threshold)
            filter.Add(kmer);
    }
    filter.Save(filter_file, false);
    return 0;
}


int main(int argc, char** argv)
{
    return CounterToFilter(argc, argv);
}
