#include <iostream>
using namespace std;
#include <time.h>
#include "HyperplaneHash.h"
#include "Utils.h"

#undef KMER_ENUM

class SeedInitializer
{
public:
    SeedInitializer() { srand((unsigned int)time(0)); }
};

SeedInitializer sinit;

typedef unsigned long KmerType;

void TestCollisions(int k, int hashsize)
{
    HyperplaneHash<KmerType> hphash(k, hashsize);
    
    if(!hphash.LoadHyperplanes("tmp/Hyperplanes"))
    {
        hphash.CreateHyperplanes();
        hphash.SaveHyperplanes("tmp/Hyperplanes");
    }

    char* seq = new char[k + 1];
    seq[k] = 0;
#ifdef KMER_ENUM
    KmerType nKmers = 1 << (2*k);
#else
    // KmerType nKmers = 1 << (hashsize);
    KmerType nKmers = 1000000;
#endif
    KmerType tablesize = 1UL << hashsize;
    bool* slot = new bool[tablesize];
    memset(slot, 0, tablesize);
    KmerType collisions = 0;
    KmerType occupied = 0;
    
    for(KmerType i = 0; i < nKmers; i ++)
    {
#ifdef KMER_ENUM
        KmerType kmer = i;
        for(int j = 0; j < k; j ++)
        {
            seq[k - 1 - j] = DNAStr[kmer & 0x03];
            kmer >>= 2;
        }
#else
        for(int j = 0; j < k; j ++)
            seq[j] = DNAStr[rand() & 0x03];
#endif
        KmerType hashed = hphash.Hash(seq);
        if(slot[hashed])
            collisions ++;
        else
        {
            slot[hashed] = true;
            occupied ++;
        }
    //    cout << seq << " => " << ToBinaryString(hashed) << endl;
    }
     cout << "collisions: " << double(collisions)/nKmers
       << " (" << collisions << "/" << nKmers << "), " 
       << ", occupied: " << double(occupied)/tablesize << "(" << occupied << "/" << tablesize
       << "), load factor: " << double(nKmers) / (occupied)<< endl;
    delete[] slot;
    delete[] seq;
}

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Usage: HyperplaneHashTest k hashsize" << endl;
        exit(1);
    }
    
    int k = atoi(argv[1]);
    int hashsize = atoi(argv[2]);
    cout << "k=" << k << ", hashsize=" << hashsize << endl;
    /*
    HyperplaneHash<KmerType> hphash(k, hashsize);
    hphash.CreateHyperplanes();
    const char* seq = "ATGCT";
    VectorXcd code = hphash.Encode(seq);
    cout << "Code: " << code << endl;
    KmerType hashed = hphash.Hash(seq);
    cout << seq << " => " << hashed << endl;
    */
    TestCollisions(k, hashsize);
    return 0;
}
    