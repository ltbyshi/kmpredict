#ifndef __HYPERPLANEHASH_H__
#define __HYPERPLANEHASH_H__
#include <string>
#include <complex>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/SVD>
using namespace Eigen;

#include "Kmer.h"
#include "Nucleotide.h"
#include "EigenUtils.h"

class HyperplaneHash
{
public:
    HyperplaneHash(int k, int hashsize);
    ~HyperplaneHash() {}
    void CreateHyperplanes(int nRandomKmers = 1000);
    void GenRandomKmers(MatrixXcd& m);
    void SaveHyperplanes(const std::string& filename) const;
    void LoadHyperplanes(const std::string& filename);
    inline KmerType Hash(const char* s) const;
    inline KmerType Hash(const char* s, VectorXcd& code) const;
    inline void HashSeq(const char* s, size_t seqlen, bool canonical = false) const;
    inline KmerType operator()(const char* s) const {return Hash(s);}
    inline VectorXcd Encode(const char* s) const;
    inline void Encode(const char* s, VectorXcd& code) const;
protected:
    void InitTranstable();
private:
    int _k;
    int _hashsize;
    KmerType _tablesize;
    MatrixXcd _W;
    VectorXcd _w0;
    VectorXcd _transtable;
};

VectorXcd HyperplaneHash::Encode(const char* s) const
{
    VectorXcd code(_k);
    for(int i = 0; i < _k; i ++)
        code(i) = _transtable(int(s[i]));
    return code;
}

void HyperplaneHash::Encode(const char* s, VectorXcd& code) const
{
    for(int i = 0; i < _k; i ++)
        code(i) = _transtable(int(s[i]));
}

KmerType HyperplaneHash::Hash(const char* s) const
{
    VectorXcd code = Encode(s);
    VectorXcd t = _W*code - _w0;
    KmerType hashed = 0;
    for(int i = 0; i < _hashsize; i ++)
    {
        hashed <<= 1;
        if((t(i).real() > 0) && (t(i).imag() > 0))
            hashed += 1;
    }
    return hashed;
}

KmerType HyperplaneHash::Hash(const char* s, VectorXcd& code) const
{
    Encode(s, code);
    VectorXcd t = _W*code - _w0;
    KmerType hashed = 0;
    for(int i = 0; i < _hashsize; i ++)
    {
        hashed <<= 1;
        if((t(i).real() > 0) && (t(i).imag() > 0))
            hashed += 1;
    }
    return hashed;
}

void HyperplaneHash::HashSeq(const char* seq, size_t seqlen, bool canonical) const
{
    size_t n_seq = seqlen - _k + 1;
    MatrixXcd x(n_seq, _k);
    for(size_t iseq = 0; iseq < n_seq; iseq ++)
    {
        for(int ik = 0; ik < _k; ik ++)
            x(iseq, ik) = _transtable[int(seq[iseq + ik])];
    }
}


#endif
