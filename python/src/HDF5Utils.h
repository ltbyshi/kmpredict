#ifndef __HDF5UTILS_H__
#define __HDF5UTILS_H__
#include <vector>
#include <H5Cpp.h>
#include "Exception.h"
#include "IOUtils.h"
#if 0
using namespace H5;
#endif

DEFINE_EXCEPTION(HDF5Error)

template <typename Type>
H5::DataType InferDataType() { throw "Bad datatype in InferDataType"; return H5::DataType(); }

template <> H5::DataType inline InferDataType<char>() { return H5::PredType::NATIVE_CHAR; }
template <> H5::DataType inline InferDataType<unsigned char>() { return H5::PredType::NATIVE_UCHAR; }
template <> H5::DataType inline InferDataType<short>() { return H5::PredType::NATIVE_SHORT; }
template <> H5::DataType inline InferDataType<unsigned short>() { return H5::PredType::NATIVE_USHORT; }
template <> H5::DataType inline InferDataType<int>() { return H5::PredType::NATIVE_INT; }
template <> H5::DataType inline InferDataType<unsigned int>() { return H5::PredType::NATIVE_UINT; }
template <> H5::DataType inline InferDataType<long>() { return H5::PredType::NATIVE_LONG; }
template <> H5::DataType inline InferDataType<unsigned long>() { return H5::PredType::NATIVE_ULONG; }
template <> H5::DataType inline InferDataType<long long>() { return H5::PredType::NATIVE_LLONG; }
template <> H5::DataType inline InferDataType<unsigned long long>() { return H5::PredType::NATIVE_ULLONG; }
template <> H5::DataType inline InferDataType<float>() { return H5::PredType::NATIVE_FLOAT; }
template <> H5::DataType inline InferDataType<double>() { return H5::PredType::NATIVE_DOUBLE; }
template <> H5::DataType inline InferDataType<long double>() { return H5::PredType::NATIVE_LDOUBLE; }
template <> H5::DataType inline InferDataType<H5std_string>() { return H5::StrType(H5::PredType::C_S1, H5T_VARIABLE); }


// HDF5 functions that does not depend on type
std::vector<size_t> GetHDF5DatasetDims(const H5std_string& filename,
                                       const H5std_string& dataname);
size_t GetHDF5DatasetTypeSize(const H5std_string& filename, 
                              const H5std_string& dataname);

// Create a new HDF5 file if it does not exist
// Add a new dataset to the HDF5 file
template <typename Type>
inline void CreateHDF5DataSet(const H5std_string& filename,
                       const H5std_string& dataname,
                       int rank, const hsize_t* dims)
{
    H5::H5File h5file;
    if(FileExists(filename))
        h5file.openFile(filename, H5F_ACC_RDWR);
    else
        h5file.openFile(filename, H5F_ACC_TRUNC);
    H5::DataSpace file_space(rank, dims);
    H5::DataType memtype = InferDataType<Type>();
    H5::DataSet dataset = h5file.createDataSet(dataname, memtype, file_space);
    dataset.close();
    h5file.close();
}


// buffer shold be pre-allocated
template <typename Type>
inline void ReadHDF5(const H5std_string& filename, 
              const H5std_string& dataname,
              int& rank, hsize_t* dims,
              Type* data)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataSpace dataspace = dataset.getSpace();
    rank = dataspace.getSimpleExtentNdims();
    dataspace.getSimpleExtentDims(dims, NULL);
    H5::DataType memtype = InferDataType<Type>();
    dataset.read(data, memtype);
    dataset.close();
    h5file.close();
}

template <typename Type>
inline void ReadHDF5(const H5std_string& filename, 
              const H5std_string& dataname,
              int& rank, hsize_t* dims,
              std::vector<Type>& data)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataSpace dataspace = dataset.getSpace();
    rank = dataspace.getSimpleExtentNdims();
    dataspace.getSimpleExtentDims(dims, NULL);
    hsize_t count = 1;
    for(int i = 0; i < rank; i ++)
        count *= dims[i];
    data.resize(count);
    H5::DataType memtype = InferDataType<Type>();
    if(!(memtype == dataset.getDataType()))
        throw HDF5Error("Unmatched data type in ReadHDF5");
    dataset.read(data.data(), memtype);
    dataset.close();
    h5file.close();
}

template <typename Type>
inline void ReadAttributeFromHDF5(const H5std_string& filename,
                           const H5std_string& dataname,
                           const H5std_string& name,
                           Type& result)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    dataset.openAttribute(name).read(InferDataType<Type>(), &result);
    dataset.close();
    h5file.close();
}

template <>
inline void ReadAttributeFromHDF5<H5std_string>(const H5std_string& filename,
                                         const H5std_string& dataname,
                                         const H5std_string& name,
                                         H5std_string& result)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    dataset.openAttribute(name).read(InferDataType<H5std_string>(), result);
    dataset.close();
    h5file.close();
}

template <typename Type>
inline void WriteAttributeToHDF5(const H5std_string& filename,
                          const H5std_string& dataname,
                          const H5std_string& name,
                          const Type& data)
{
    H5::H5File h5file(filename, H5F_ACC_RDWR);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataType datatype = InferDataType<Type>();
    if(!dataset.attrExists(name))
        dataset.createAttribute(name, datatype, H5::DataSpace()).write(datatype, &data);
    else
        dataset.openAttribute(name).write(datatype, &data);
    dataset.close();
    h5file.close();
}

template <>
inline void WriteAttributeToHDF5<H5std_string>(const H5std_string& filename,
                                        const H5std_string& dataname,
                                        const H5std_string& name,
                                        const H5std_string& data)
{
    H5::H5File h5file(filename, H5F_ACC_RDWR);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataType datatype = H5::StrType(H5::PredType::C_S1, H5T_VARIABLE);
    if(!dataset.attrExists(name))
        dataset.openAttribute(name).write(datatype, data);
    dataset.close();
    h5file.close();
}

template <typename Type>
inline void ReadVectorFromHDF5(const H5std_string& filename,
                        const H5std_string& dataname,
                        std::vector<Type>& data)
{
    int rank;
    hsize_t dims[1];
    ReadHDF5<Type>(filename, dataname, rank, dims, data);
}


template <typename Type>
inline void ReadVectorFromHDF5(const H5std_string& filename,
                        const H5std_string& dataname,
                        Type* data,
                        hsize_t start = 0, hsize_t count = 0)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataSpace file_space = dataset.getSpace();
    int rank = file_space.getSimpleExtentNdims();
    if(rank != 1)
        throw Exception("HDF5 dataset is not a vector", "HDF5Error");
    hsize_t length;
    file_space.getSimpleExtentDims(&length, NULL);
    if(count <= 0)
        count = length;
    file_space.selectHyperslab(H5S_SELECT_SET, &count, &start);
    H5::DataType memtype = InferDataType<Type>();
    H5::DataSpace mem_space(1, &count);
    dataset.read(data, memtype, mem_space, file_space);
    dataset.close();
    h5file.close();
}

// count: number of elements to read
// coord: array of positions of length count
template <typename Type>
inline void ReadVectorFromHDF5(const H5std_string& filename,
                        const H5std_string& dataname,
                        Type* data,
                        hsize_t count, 
                        const hsize_t* coord)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataSpace file_space = dataset.getSpace();
    int rank = file_space.getSimpleExtentNdims();
    if(rank != 1)
        throw Exception("HDF5 dataset is not a vector", "HDF5Error");
    file_space.selectElements(H5S_SELECT_SET, count, coord);
    H5::DataType memtype = InferDataType<Type>();
    H5::DataSpace mem_space(1, &count);
    dataset.read(data, memtype, mem_space, file_space);
    dataset.close();
    h5file.close();
}

template <typename Type>
inline void ReadMatrixFromHDF5(const H5std_string& filename,
                        const H5std_string& dataname,
                        hsize_t& nrow, hsize_t& ncol,
                        std::vector<Type>& data)
{
    int rank;
    hsize_t dims[2];
    ReadHDF5<Type>(filename, dataname, rank, dims, data);
    nrow = dims[0];
    ncol = dims[1];
}

template <typename Type>
inline void WriteHDF5(const H5std_string& filename,
               const H5std_string& dataname,
               int rank, const hsize_t* dims,
               const Type* data)
{   
    hsize_t count = 1;
    for(int i = 0; i < rank; i ++)
        count *= dims[i];
    H5::H5File h5file(filename, H5F_ACC_TRUNC);
    H5::DataSpace dataspace(rank, dims);
    H5::DataType memtype = InferDataType<Type>();
    H5::DataSet dataset = h5file.createDataSet(dataname, memtype, dataspace);
    dataset.write(data, memtype);
    dataset.close();
    h5file.close();
}

template <typename Type>
inline void WriteHDF5(const H5std_string& filename,
               const H5std_string& dataname,
               int rank, const hsize_t* dims,
               const std::vector<Type>& data)
{   
    hsize_t count = 1;
    for(int i = 0; i < rank; i ++)
        count *= dims[i];
    H5::H5File h5file(filename, H5F_ACC_TRUNC);
    H5::DataSpace dataspace(rank, dims);
    H5::DataType memtype = InferDataType<Type>();
    H5::DataSet dataset = h5file.createDataSet(dataname, memtype, dataspace);
    dataset.write(data.data(), memtype);
    dataset.close();
    h5file.close();
}

template <typename Type>
inline void WriteVectorToHDF5(const H5std_string& filename,
                       const H5std_string& dataname,
                       Type* data,
                       hsize_t start = 0, 
                       hsize_t count = 0)
{
    H5::H5File h5file(filename, H5F_ACC_RDWR);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataSpace file_space = dataset.getSpace();
    int rank = file_space.getSimpleExtentNdims();
    if(rank != 1)
        throw HDF5Error("HDF5 dataset is not a vector");
    hsize_t ds_size;
    file_space.getSimpleExtentDims(&ds_size, NULL);
    if(count <= 0)
        count = ds_size;
    file_space.selectHyperslab(H5S_SELECT_SET, &count, &start);
    H5::DataType memtype = InferDataType<Type>();
    H5::DataSpace mem_space(1, &count);
    dataset.write(data, memtype, mem_space, file_space);
    dataset.close();
    h5file.close();
}


template <typename Type>
inline void WriteVectorToHDF5(const H5std_string& filename,
                       const H5std_string& dataname,
                       const std::vector<Type>& data)
{
    int rank = 1;
    hsize_t dims[1];
    dims[0] = data.size();
    WriteHDF5<Type>(filename, dataname, rank, dims, data);
}

/*
template <typename Type>
void WriteVectorToHDF5(const H5std_string& filename,
                       const H5std_string& dataname,
                       const Type* data,
                       size_t length)
{
    int rank = 1;
    hsize_t dims[1];
    dims[0] = length;
    WriteHDF5<Type>(filename, dataname, rank, dims, data);
}
*/
template <typename Type>
inline void WriteMatrixToHDF5(const H5std_string& filename,
                       const H5std_string& dataname,
                       hsize_t nrow, hsize_t ncol,
                       const std::vector<Type>& data)
{
    int rank = 2;
    hsize_t dims[2];
    dims[0] = nrow;
    dims[1] = ncol;
    WriteHDF5<Type>(filename, dataname, rank, dims, data);
}

#endif
