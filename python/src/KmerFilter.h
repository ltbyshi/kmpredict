#ifndef __KMERFILTER_H__
#define __KMERFILTER_H__
#include "Nucleotide.h"
#include "Utils.h"
#include "Kmer.h"

struct KmerFilterHeader_old
{
    uint32_t magic;
    uint32_t headersize;
    uint32_t k;
    uint32_t unitsize;
    uint64_t hashsize;
    uint64_t nunits;
};

struct KmerFilterHeader
{
    uint32_t magic;
    uint32_t headersize;
    uint32_t k;
    uint32_t kmersize;
    uint32_t unitsize;
    uint64_t hashsize;
    uint64_t nunits;
    
    static const uint32_t MagicNumber = 0x46454D4B;
    
    KmerFilterHeader()
    {
        magic = KmerFilterHeader::MagicNumber;
        headersize = GetSize();
    }
    
    void Read(std::istream& is)
    {
        ReadItem(is, magic);
        ReadItem(is, headersize);
        ReadItem(is, k);
        ReadItem(is, kmersize);
        ReadItem(is, unitsize);
        ReadItem(is, hashsize);
        ReadItem(is, nunits);
    }
    
    void Write(std::ostream& os) const
    {
        WriteItem(os, magic);
        WriteItem(os, headersize);
        WriteItem(os, k);
        WriteItem(os, kmersize);
        WriteItem(os, unitsize);
        WriteItem(os, hashsize);
        WriteItem(os, nunits);
    }
    
    uint32_t GetSize() const
    {
        return (sizeof(magic) + sizeof(headersize) + sizeof(k)
            + sizeof(kmersize) + sizeof(unitsize) + sizeof(hashsize)
            + sizeof(nunits));
    }
};

inline std::ostream& operator<<(std::ostream& os, const KmerFilterHeader& header)
{
    header.Write(os);
    return os;
}

inline std::istream& operator>>(std::istream& is, KmerFilterHeader& header)
{
    header.Read(is);
    return is;
}
    

// a filter to indicate whether a kmer exist in a dataset
template <typename KmerType>
class KmerFilter
{
public:
    KmerFilter() { _k = 0; _hashsize = 0; }
    KmerFilter(const char* filename) { Load(filename); }
    
    KmerFilter(int k, KmerType hashsize = 0) { Create(k, hashsize); }
    
    void Create(int k, KmerType hashsize = 0) {
        _k = k;
        if(hashsize > 0)
            _hashsize = hashsize;
        else
            _hashsize = CalcKmerHashSize<KmerType>(k);
        _data.Resize(_hashsize);
    }
    
    void Add(const char* s) {
        _data.Set(StringToKmer<KmerType>(s, _k));
    }
    
    void Add(KmerType kmer) {
        _data.Set(kmer);
    }
    
    void Clear(KmerType kmer) {
        _data.Clear(kmer);
    }
    
    bool Get(const char* s) const {
        return _data[StringToKmer<KmerType>(s, _k)]; 
    }
    void Unpack(std::vector<bool>& unpacked) const { _data.Unpack(unpacked); }
    
    bool Get(KmerType kmer) const { return _data[kmer]; }
    bool operator[](KmerType kmer) const { return _data[kmer]; }
    
    KmerType GetKmerNum() const { return _data.Sum(); }
    KmerType GetHashSize() const { return _hashsize; }
    KmerType GetK() const { return _k; }
    KmerType GetUniqueKmerCount() const { return _data.Sum(); }
    
    void DumpToTable() const;
    bool Merge(const KmerFilter& b);
    void Load(const char* filename);
    void Save(const char* filename, bool mergerc = true);
    void Convert(const char* filename);
    static void GetInfo(const char* filename);
    static KmerFilterHeader ReadHeader(const std::string& filename);
public:
    static const uint32_t MagicNumber = 0x46454D4B;
private:
    BitSet _data;
    int _k;
    KmerType _hashsize;
};

typedef KmerFilter<DefaultKmerType> DefaultKmerFilter;

template <typename KmerType>
bool KmerFilter<KmerType>::Merge(const KmerFilter& b)
{
    if((_k != 0) && (_k == b._k))
    {
        _data.Or(b._data);
        return true;
    }
    else
        return false;
}

template <typename KmerType>
void KmerFilter<KmerType>::Load(const char* filename)
{
    std::ifstream fin(filename, std::ios::binary);
    if(!fin)
        Die("cannot open the input file %s", filename);
    KmerFilterHeader header;
    fin >> header;
    _k = header.k;
    _hashsize = header.hashsize;
    _data.Resize(_hashsize);
    fin.read((char*)_data.GetData(),
             _data.GetUnitSize()*_data.GetNumUnits());
    fin.close();
}

template <typename KmerType>
void KmerFilter<KmerType>::Convert(const char* filename)
{
    std::ifstream fin(filename, std::ios::binary);
    if(!fin)
        Die("cannot open the input file %s", filename);
    KmerFilterHeader_old header;
    fin.read((char*)&header, sizeof(header));
    _k = header.k;
    _hashsize = header.hashsize;
    if(_hashsize > CalcKmerHashSize<KmerType>(_k))
        Die("not old kmer format, cannot convert");
    _data.Resize(_hashsize);
    fin.read((char*)_data.GetData(),
             _data.GetUnitSize()*_data.GetNumUnits());
    fin.close();
    Save(filename, false);
}

template <typename KmerType>
void KmerFilter<KmerType>::Save(const char* filename, bool mergerc)
{
    std::ofstream fout(filename, std::ios::binary);
    if(!fout)
        Die("cannot open the output file %s", filename);
    KmerFilterHeader header;
    header.k = _k;
    header.unitsize = 8;
    header.kmersize = sizeof(KmerType);
    header.hashsize = _hashsize;
    header.nunits = _data.GetNumUnits();
    fout << header;
    if(mergerc)
    {
        for(KmerType kmer = 0; kmer < _hashsize; kmer ++)
        {
            KmerType kmerrc = KmerRC(kmer, _k);
            if(kmer < kmerrc)
            {
                if(_data[kmer] | _data[kmerrc])
                    _data.Set(kmer);
                _data.Clear(kmerrc);
            }
        }
        fout.write(reinterpret_cast<const char*>(_data.GetData()),
                   _data.GetUnitSize()*_data.GetNumUnits());
    }
    else
    {
        fout.write(reinterpret_cast<const char*>(_data.GetData()), 
                _data.GetUnitSize()*_data.GetNumUnits());
    }
    fout.close();
}

template <typename KmerType>
void KmerFilter<KmerType>::GetInfo(const char* filename) 
{
    ifstream fin(filename, std::ios::binary);
    if(!fin)
        Die("cannot open the input file %s", filename);
    KmerFilterHeader header;
    fin >> header;
    fin.close();
    
    cout << "HeaderSize\t" << header.headersize << endl;
    cout << "K\t" << header.k << endl;
    cout << "UnitSize\t" << header.unitsize << endl;
    cout << "KmerSize\t" << header.kmersize << endl;
    cout << "HashSize\t" << header.hashsize << endl;
    cout << "NumUnits\t" << header.nunits << endl;
}

template <typename KmerType>
KmerFilterHeader KmerFilter<KmerType>::ReadHeader(const std::string& filename)
{
    ifstream fin(filename, std::ios::binary);
    if(!fin)
        Die("cannot open the input file %s", filename.c_str());
    KmerFilterHeader header;
    fin >> header;
    fin.close();
    return header;
}

template <typename KmerType>
void KmerFilter<KmerType>::DumpToTable() const
{
    for(KmerType kmer = 0; kmer < _hashsize; kmer ++)
    {
        cout << KmerToString<KmerType>(kmer, _k) << "\t" << int(_data[kmer]) << endl;
    }
}


#endif
