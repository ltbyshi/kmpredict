#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
// Linux
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/file.h>
#include <unistd.h>
#include <wait.h>
#include <errno.h>

#include "IOUtils.h"
#include "Exception.h"

// OS
bool FileExists(const std::string& filename)
{
    struct stat st;
    if(stat(filename.c_str(), &st) == 0)
        return true;
    else
        return false;
}

bool IsDirectory(const std::string& pathname)
{
    struct stat st;
    if(stat(pathname.c_str(), &st) == 0)
    {
        if((st.st_mode & S_IFMT) == S_IFDIR)
            return true;
    }
    return false;
}

size_t FileSize(const std::string& filename)
{
    struct stat st;
    if(stat(filename.c_str(), &st) == 0)
        return st.st_size;
    else
        return 0;
}

// class FileLock
FileLock::FileLock()
    : fd(-1), fp(NULL)
{
}

FileLock::FileLock(const std::string& filename)
    : filename(filename)
{
    Open(filename);
}

void FileLock::Open(const std::string& filename)
{
    fp = fopen(filename.c_str(), "w");
    if(!fp)
        throw IOError("cannot lock file " + filename);
    fd = fileno(fp);
}

FileLock::~FileLock()
{
    Close();
}

void FileLock::Close()
{
    if(fp)
    {
        fclose(fp);
        fp = NULL;
    }
}

bool FileLock::Lock(bool blocking)
{
    if(blocking)
    {
        if(flock(fd, LOCK_EX) == -1)
            throw IOError("cannot lock file " + filename);
    }
    else
    {
        if(flock(fd, LOCK_EX | LOCK_NB) == -1)
        {
            if(errno != EWOULDBLOCK)
                throw IOError("cannot lock file " + filename);
            else
                return false;
        }
    }
    return true;
}

void FileLock::Unlock()
{
    if(flock(fd, LOCK_UN) == -1)
        throw IOError("cannot unlock file " + filename);
}
// end of class FileLock

// begin of class DistLock
DistLock::DistLock()
    : pathname(""), locked(false)
{
}

DistLock::DistLock(const std::string& path)
    : pathname(path), locked(false)
{
}

DistLock::~DistLock()
{
    if(locked)
        Release();
}

void DistLock::Open(const std::string& path)
{
    pathname = path;
    locked = false;
}


bool DistLock::Acquire(bool blocking)
{
    while(mkdir(pathname.c_str(), 0777) != 0)
    {
        if(errno != EEXIST)
            std::cerr << "Warning: DistLock::Acquire: " << strerror(errno) << std::endl;
        if(blocking)
            sleep(1);
        else
            return false;
    }
    locked = true;
    return true;
}

bool DistLock::Release()
{
    if(rmdir(pathname.c_str()) != 0)
    {
        if(errno == EEXIST)
            std::cerr << "Warning: DistLock::Release: release unlocked lock" << std::endl;
        else
            std::cerr << "Warning: DistLock::Release: " << strerror(errno) << std::endl;
        return false;
    }
    locked = false;
    return true;
}

// end of class DistLock

int Execute(const std::string& path, std::vector<std::string>& args)
{
    pid_t pid;
    int status;

    pid = fork();
    if(pid < 0)
        throw OSError("fork() failed");
    else if(pid == 0)
    {
        char** argv = new char*[args.size() + 1];
        for(size_t i = 0; i < args.size(); i ++)
        {
            if(args[i].size() > 0)
            {
                argv[i] = new char[args[i].size() + 1];
                strcpy(argv[i], args[i].c_str());
            }
            else
                argv[i] = NULL;
        }
        argv[args.size()] = NULL;
        if(execvp(path.c_str(), argv) < 0)
            throw OSError("execvp() failed");
        for(size_t i = 0; i < args.size(); i ++)
        {
            if(argv[i])
                delete[] argv[i];
        }
        delete[] argv;
    }
    else
    {
        while(::wait(&status) != pid) {}
    }
    return status;
}

void MakeDir(const std::string& path)
{
    if(FileExists(path))
        return;

    if(mkdir(path.c_str(), 0777) != 0)
        throw OSError("cannot create directory " + path);
}

void MakeDirs(const std::string& path)
{
    if(FileExists(path))
        return;
    std::vector<std::string> argv(3);
    argv[0] = "mkdir";
    argv[1] = "-p";
    argv[2] = path;
    if(Execute("mkdir", argv) != 0)
        throw OSError("cannot create directory " + path, false);
}

void RemoveFile(const std::string& path)
{
    if(unlink(path.c_str()) != 0)
        throw OSError("cannot remove file " + path);
}

void RemoveDir(const std::string& path, bool recursive)
{
    if(recursive)
    {
        if(system((std::string("rm -r ") + path).c_str()) != 0)
            throw OSError("cannot remove directory " + path, false);
    }
    else
    {
        if(rmdir(path.c_str()) != 0)
            throw OSError("cannot remove directory " + path);
    }
}

std::string DirName(const std::string& path)
{
    size_t pos = path.find_last_of('/');
    if(pos == std::string::npos)
        return std::string(".");
    else
        return path.substr(0, pos);
}

void CheckWrite(std::ostream& os, const char* s, std::streamsize n)
{
    os.write(s, n);
    if(!os)
    {
        if(os.eof())
            throw IOError("end of file reached");
        if(os.bad())
            throw IOError("logical error on I/O operation");
        if(os.bad())
            throw IOError("writing error on I/O operation");
    }
}

// class ProcessStatus
ProcessStatus& ProcessStatus::Update()
{
    FILE* fp = fopen("/proc/self/stat", "r");
    if(!fp)
        throw IOError("cannot open /proc/self/stat");
    fscanf(fp, "%d", &(this->pid));
    fscanf(fp, "%s", this->comm);
    fscanf(fp, " %c", &(this->state));
    fscanf(fp, "%d", &(this->ppid));
    fscanf(fp, "%d", &(this->pgrp));
    fscanf(fp, "%d", &(this->session));
    fscanf(fp, "%d", &(this->tty_nr));
    fscanf(fp, "%d", &(this->tpgid));
    fscanf(fp, "%u", &(this->flags));
    fscanf(fp, "%lu", &(this->minflt));
    fscanf(fp, "%lu", &(this->cminflt));
    fscanf(fp, "%lu", &(this->majflt));
    fscanf(fp, "%lu", &(this->cmajflt));
    fscanf(fp, "%lu", &(this->utime));
    fscanf(fp, "%lu", &(this->stime));
    fscanf(fp, "%ld", &(this->cutime));
    fscanf(fp, "%ld", &(this->cstime));
    fscanf(fp, "%ld", &(this->priority));
    fscanf(fp, "%ld", &(this->nice));
    fscanf(fp, "%ld", &(this->num_threads));
    fscanf(fp, "%ld", &(this->itrealvalue));
    fscanf(fp, "%llu", &(this->starttime));
    fscanf(fp, "%lu", &(this->vsize));
    fscanf(fp, "%ld", &(this->rss));
    fscanf(fp, "%lu", &(this->rsslim));
    fscanf(fp, "%lu", &(this->startcode));
    fscanf(fp, "%lu", &(this->endcode));
    fscanf(fp, "%lu", &(this->startstack));
    fscanf(fp, "%lu", &(this->kstkesp));
    fscanf(fp, "%lu", &(this->kstkeip));
    fscanf(fp, "%lu", &(this->signal));
    fscanf(fp, "%lu", &(this->blocked));
    fscanf(fp, "%lu", &(this->sigignore));
    fscanf(fp, "%lu", &(this->sigcatch));
    fscanf(fp, "%lu", &(this->wchan));
    fscanf(fp, "%lu", &(this->nswap));
    fscanf(fp, "%lu", &(this->cnswap));
    fscanf(fp, "%d", &(this->exit_signal));
    fscanf(fp, "%d", &(this->processor));
    fscanf(fp, "%u", &(this->rt_priority));
    fclose(fp);
    return *this;
}
// end of ProcessStatus

MappedFile::MappedFile(const std::string& filename, bool readonly)
{
    if(readonly)
        _fp = fopen(filename.c_str(), "rb");
    else
        _fp = fopen(filename.c_str(), "rb+");
    if(!_fp)
        IOError("cannot open the input file " + filename);
    int fd = fileno(_fp);
    struct stat filestat;
    fstat(fd, &filestat);
    _length = filestat.st_size;
    _data = mmap(NULL, filestat.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if(_data == MAP_FAILED)
        OSError("mmap failed");
}

MappedFile::~MappedFile()
{
    munmap(_data, _length);
    fclose(_fp);
}

char* ReadFileAsString(const std::string& filename)
{
    FILE* fp = fopen(filename.c_str(), "rb");
    if(!fp)
        throw IOError("cannot open file " + filename);
    if(fseek(fp, 0, SEEK_END) != 0)
        throw IOError("cannot seek in file " + filename);
    unsigned long fsize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    char* s = new char[fsize + 1];
    if(fread(s, 1, fsize, fp) < fsize)
    {
        delete s;
        throw IOError("read less than file size " + filename);
    }
    fclose(fp);
    return s;
}
