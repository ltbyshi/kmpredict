#include <iostream>
#include <fstream>
#include <string>
using namespace std;
#include <stdlib.h>

#include "Utils.h"
#include "KmerCounter.h"

#define PROGNAME "DecodeKmer"

int DecodeKmerExact(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Convert the hashed kmer to sequence" << endl;
        cerr << "Usage: " << PROGNAME << "exact k kmerhashfile" << endl;
        cerr << "\tkmerhashfile\ta text file that contains the hashed kmers in the first column" << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    string kmerhashfile(argv[2]);
    
    FILE* fin = fopen(kmerhashfile.c_str(), "r");
    if(!fin)
        Die("cannot open the input file ", kmerhashfile.c_str());
    const size_t bufsize = 10240;
    char* line = new char[bufsize];
    while(!feof(fin))
    {
        fgets(line, bufsize, fin);
        DefaultKmerType kmer;
        if(sscanf(line, "%lx", &kmer) > 0)
            cout << KmerToString<DefaultKmerType>(kmer, k) << endl;
    }
    delete[] line;
    fclose(fin);
    
    return 0;
}

int main(int argc, char** argv)
{
    HandlerMap handlers;
    handlers["exact"] = DecodeKmerExact;
    ParseArguments(handlers, PROGNAME, argc, argv);
        
    return 0;
}
