#include <iostream>
#include <fstream>
#include <string>
#include <map>
using namespace std;
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "Fasta.h"
#include "KmerFilter.h"
#include "HyperplaneHash.h"
#include "Utils.h"
#include "Thread.h"

#define OPT_PROGRESS 0
#define PROGNAME "ScanKmers"

void ReadKmerList(const string& filename, vector<DefaultKmerType>& kmerlist)
{
    FILE* fin = fopen(filename.c_str(), "r");
    if(!fin)
        Die("cannot open the input file ", filename.c_str());
    const size_t bufsize = 10240;
    char* line = new char[bufsize];
    while(!feof(fin))
    {
        fgets(line, bufsize, fin);
        unsigned long kmer;
        if(sscanf(line, "%lx", &kmer) > 0)
            kmerlist.push_back(kmer);
    }
    delete[] line;
    fclose(fin);
}

void SaveKmerCounts(const string& filename, map<string, int>& counts)
{
    ofstream fout(filename);
    if(!fout)
        Die("cannot open the output file ", filename.c_str());
    for(map<string, int>::iterator it = counts.begin();
        it != counts.end();
        ++ it)
    {
        fout << it->first << "\t" << it->second << endl;
    }
    fout.close();
}

int ScanKmersHashed(int argc, char** argv)
{
    if(argc != 8)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmers in a give list" << endl;
        cerr << "Usage: " << PROGNAME << " hashed k hashsize kmerlistfile hashfile seqfile coveragefile outfile" << endl;
        cerr << "\tkmerlistfile\ta text file in which the first column contains the hex values of the kmer hash" << endl;
        cerr << "\tseqfile\tsequence in FASTA format." << endl;
        cerr << "\tkmerfile\toutput text file containing kmer frequencies." << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    int hashsize = atoi(argv[2]);
    string kmerlistfile = argv[3];
    string hashfile = argv[4];
    string seqfile = argv[5];
    string covfile = argv[6];
    string outfile = argv[7];
    
    // read coverage file
    map<string, int> coverage = ReadCoverage(covfile);
    
    vector<DefaultKmerType> kmerlist;
    ReadKmerList(kmerlistfile, kmerlist);
    
    HyperplaneHash<DefaultKmerType> hash(k, hashsize);
    if(!hash.LoadHyperplanes(hashfile))
        exit(1);
    vector<DefaultValType> counts(1UL << hashsize);
    
    FastaReader reader(seqfile);
    FastaRecord* record = reader.ReadNext();
#if OPT_PROGRESS
    ProgressBar progressbar(FileSize(seqfile), 50);
    progressbar.Start();
#endif
    while(record)
    {
        map<string, int>::iterator it = coverage.find(record->name);
        if(it == coverage.end())
        {
            delete record;
            record = reader.ReadNext();
            continue;
        }
        const char* seq = record->seq.c_str();
        size_t nkmer = record->seq.size() - k + 1;
        if(nkmer <= 0)
        {
            delete record;
            record = reader.ReadNext();
            continue;
        }
        for(size_t i = 0; i < nkmer; i++)
        {
            DefaultKmerType kmer = hash.Hash(&(seq[i]));
            counts[kmer] += it->second;
        }
        delete record;
#if OPT_PROGRESS
        progressbar.Update(reader.GetFilePos());
#endif
        record = reader.ReadNext();
    }
    reader.Close();
#if OPT_PROGRESS
    progressbar.Finish();
#endif
    
    FILE* fout = fopen(outfile.c_str(), "w");
    if(!fout)
        Die("cannot open the output file ", outfile.c_str());
    for(size_t i = 0; i < kmerlist.size(); i ++)
    {
        DefaultKmerType kmer = kmerlist[i];
        fprintf(fout, "%08lX\t%u\n", kmer, counts[kmer]);
    }
    fclose(fout);
    
    return 0;
}

int ScanKmersExact(int argc, char** argv)
{
    if(argc != 6)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmers in a give list" << endl;
        cerr << "Usage: " << PROGNAME << "exact k kmerlistfile seqfile coveragefile outfile" << endl;
        cerr << "\tkmerlistfile\ta text file in which the first column contains the hex values of the kmer hash" << endl;
        cerr << "\tseqfile\tsequence in FASTA format." << endl;
        cerr << "\tkmerfile\toutput text file containing kmer frequencies." << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    string kmerlistfile = argv[2];
    string seqfile = argv[3];
    string covfile = argv[4];
    string outfile = argv[5];
    
    // read coverage file
    map<string, int> coverage = ReadCoverage(covfile);
    
    vector<DefaultKmerType> kmerlist;
    ReadKmerList(kmerlistfile, kmerlist);
    
    FastaReader reader(seqfile);
    FastaRecord* record = reader.ReadNext();
    //cerr << "Count kmers" << endl;
    
    DefaultKmerType hashsize = CalcKmerHashSize<DefaultKmerType>(k);
    vector<DefaultValType> counts(hashsize);
#if OPT_PROGRESS
    ProgressBar progressbar(FileSize(seqfile), 50);
    progressbar.Start();
#endif
    while(record)
    {
        map<string, int>::iterator it = coverage.find(record->name);
        if(it == coverage.end())
        {
            record = reader.ReadNext();
            continue;
        }
        const char* seq = record->seq.c_str();
        size_t nkmer = record->seq.size() - k + 1;
        if(nkmer <= 0)
        {
            record = reader.ReadNext();
            continue;
        }
        DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
        counts[kmer] += it->second;
        for(size_t i = 0; i < nkmer - 1; i++)
        {
            kmer = OverlapKmer(kmer, k, seq[k + i]);
            counts[kmer] += it->second;
        }
        delete record;
#if OPT_PROGRESS
        progressbar.Update(reader.GetFilePos());
#endif
        record = reader.ReadNext();
    }
    reader.Close();
#if OPT_PROGRESS
    progressbar.Finish();
#endif
    //cerr << "Save kmer counts" << endl;
    // save
    FILE* fout = fopen(outfile.c_str(), "w");
    if(!fout)
        Die("cannot open the output file ", outfile.c_str());
    for(size_t i = 0; i < kmerlist.size(); i ++)
    {
        DefaultKmerType kmer = kmerlist[i];
        fprintf(fout, "%08lX\t%u\n", kmer, counts[kmer]);
    }
    fclose(fout);
    
    return 0;
}

int GenKmerFreqMatrix(int argc, char** argv)
{
    if(argc < 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmers in a give list" << endl;
        cerr << "Usage: " << PROGNAME << "matrix kmerfreqfiles" << endl;
        exit(1);
    }
    int nfiles = argc - 1;
    vector<vector<string> > matrix(nfiles);
    size_t nrow = 0;
#ifdef OPT_PROGRESS
    ProgressBar progressbar(nfiles, 50);
    progressbar.Start();
#endif
    for(int i = 0; i < nfiles; i ++)
    {
        const char* filename = argv[i + 1];
        vector<string>& row = matrix[i];
        ifstream fin(filename);
        if(!fin)
            Die("cannot open the input file ", filename);
        string line;
        size_t rownum = 0;
        while(!fin.eof())
        {
            getline(fin, line);
            if(line.size() < 1)
                continue;
            istringstream is(line);
            string col1;
            rownum ++;
            row.resize(rownum);
            is >> col1 >> row[rownum - 1];
        }
        fin.close();
        if(nrow == 0)
            nrow = row.size();
        else
        {
            if(nrow != row.size())
                Die("different number of rows in ", filename);
        }
#if OPT_PROGRESS
        progressbar.Increment(1);
#endif
    }
#if OPT_PROGRESS
    progressbar.Finish()
#endif
    for(size_t i = 0; i < nrow; i ++)
    {
        for(int j = 0; j < nfiles; j ++)
        {
            if(j > 0)
                cout << "\t";
            cout << matrix[j][i];
        }
        cout << endl;
    }
        
    return 0;
}

int main(int argc, char** argv)
{
    HandlerMap handlers;
    handlers["exact"] = ScanKmersExact;
    handlers["hashed"] = ScanKmersHashed;
    handlers["matrix"] = GenKmerFreqMatrix;
    ParseArguments(handlers, PROGNAME, argc, argv);
    
    return 0;
}