#ifndef __UNIVERSALHASH_H__
#define __UNIVERSALHASH_H__

template <typename KeyType>
class UniversalHash
{
public:
    UniversalHash(int k, int hashsize);
    ~UniversalHash() {}
};

#endif