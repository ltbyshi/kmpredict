#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
#include "Utils.h"
#include "IOUtils.h"
#include "HDF5Utils.h"
#include "Statistics.h"
#include "FeatureSelection.h"
#include "Exception.h"

template <typename Container>
static void PrintVector(const Container& x, size_t n)
{
    for(size_t i = 0; i < n; i ++)
    {
        if(i > 0)
            cout << ", ";
        cout << x[i];
    }
    cout << endl;
}

template <typename Type>
static ostream& operator<<(ostream& out, const vector<Type>& x)
{
    for(size_t i = 0; i < x.size(); i ++)
    {
        if(i > 0)
            out << ", ";
        out << x[i];
    }
    return out;
}

void TestIOStream()
{
    //FileInputStream fin("/etc/NOTFOUND");
    FileOutputStream fout("/etc/NOTFOUND");
}

void TestHDF5()
{
    hsize_t n = 40;
    hsize_t ncol = 5, nrow = 4;
    vector<int> v(n);
    for(size_t i = 0; i < n; i ++)
        v[i] = i + 1;
    vector<int> m(ncol*nrow);
    for(size_t i = 0; i < nrow; i ++)
        for(size_t j = 0; j < ncol; j ++)
            m[i*ncol + j] = 10*i + j;
    const string outdir = "";
    cout << "Original vector: " << v << endl;
    
    WriteVectorToHDF5<int>(outdir + "hdf5test.vector.h5", "vector", v);
    ReadVectorFromHDF5<int>(outdir + "hdf5test.vector.h5", "vector", v);
    WriteMatrixToHDF5<int>(outdir + "hdf5test.matrix.h5", "matrix", nrow, ncol, m);
    ReadMatrixFromHDF5<int>(outdir + "hdf5test.matrix.h5", "matrix", nrow, ncol, m);

    // partial read
    int* buf = new int[n];
    for(int i = 0; i < n/4; i ++)
        ReadVectorFromHDF5<int>(outdir + "hdf5test.vector.h5", "vector", buf, i*4, 4);
    cout << "Read parts of a vector and reassemble: " << vector<int>(buf, buf + n) << endl;
    delete[] buf;
}

void TestHDF5Hyperslab()
{
    typedef unsigned int ValType;
    string filename("output/AHCNC2ADXX.N702_CGTACTAG/kmer.14.counts");
    string dataname("data");

    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataSpace file_space = dataset.getSpace();
    int rank = file_space.getSimpleExtentNdims();
    if(rank != 1)
        throw ::Exception("HDF5 dataset rank should be 1", "HDF5Error");
    hsize_t length;
    file_space.getSimpleExtentDims(&length, NULL);
    hsize_t nunits = (length > 1024)? 1024 : length;
    hsize_t unitsize = length/nunits;
    cout << "number of units: " << nunits << ", unit size: " << unitsize << endl;
    H5::DataType memtype = H5::PredType::NATIVE_UINT;
    H5::DataSpace mem_space(1, &unitsize);
    ValType* data = new ValType[unitsize];
    for(hsize_t i = 0; i < nunits; i ++)
    {
        hsize_t offset = i*unitsize;
        cout << "Read hyperslab offset=" << offset << ", count=" << unitsize << endl;
        file_space.selectHyperslab(H5S_SELECT_SET, &unitsize, &offset);
        dataset.read(data, memtype, mem_space, file_space);
    }
    dataset.close();
    h5file.close();
    delete[] data;
}

void _TestMutualInfo(const double x[4])
{
    double p[4];
    std::copy(x, x + 4, p);
    double sum = 0;
    for(int i = 0; i < 4; i ++)
        sum += p[i];
    for(int i = 0; i < 4; i ++)
        p[i] /= sum;
    double mi = NormalizedMutualInfo(p[0], p[1], p[2], p[3]);
    cout << "   " << 0 << " " << 1 << endl;
    cout << "0: " << p[0] << " " << p[1] << endl;
    cout << "1: " << p[2] << " " << p[3] << endl;
    cout << "MI: " << mi << endl;
}

void TestMutualInfo()
{
    double p[4];
    for(int i = 0; i < 4; i ++)
    {
        p[i] = drand48();
    }
    cout << "Random: " << endl;
    _TestMutualInfo(p);
    p[0] = 1e6; p[1] = 1; p[2] = 1; p[3] = 1e6;
    cout << "Test 1: " << endl;
    _TestMutualInfo(p);
    p[0] = 1;
    cout << "Test 2: " << endl;
    _TestMutualInfo(p);
    cout << "Test 3: " << endl;
    p[0] = 0.525641; p[1] = 0.376923; p[2] = 0.0025641; p[3] = 0.0025641;
    _TestMutualInfo(p);
    p[0] = 1; p[1] = 1; p[2] = 1; p[3] = 1;
    cout << "Uniform: " << endl;
    _TestMutualInfo(p);
}

void _TestFeatureSelection(double tp, double tn, double fp, double fn)
{
    cout << "TP=" << tp << ", TN=" << tn << ", FP=" << fp << ", FN=" << fn << endl;
    cout << "InfoGain: "         << InfoGain(tp, tn, fp, fn) << endl;
    cout << "MutualInfo: "       << MutualInfo(tp, tn, fp, fn) << endl;
    cout << "Accuracy: "         << Accuracy(tp, tn, fp, fn) << endl;
    cout << "BalancedAccuracy: " << BalancedAccuracy(tp, tn, fp, fn) << endl;
    cout << "ProbabilityRatio: " << ProbabilityRatio(tp, tn, fp, fn) << endl;
    cout << "OddsRatio: "        << OddsRatio(tp, tn, fp, fn) << endl;
    cout << "Frequency: "        << Frequency(tp, tn, fp, fn) << endl;
    cout << "ChiSquared: "       << ChiSquared(tp, tn, fp, fn) << endl;
}

void TestFeatureSelection()
{
    double tp, tn, fp, fn;
    tp = 100; tn = 100; fp = 1; fn = 1;
    _TestFeatureSelection(tp, tn, fp, fn);
    tp = 1; tn = 1; fp = 100; fn = 100;
    _TestFeatureSelection(tp, tn, fp, fn);
    tp = 1; tn = 100; fp = 1; fn = 100;
    _TestFeatureSelection(tp, tn, fp, fn);
    tp = 1; tn = 100; fp = 100; fn = 1;
    _TestFeatureSelection(tp, tn, fp, fn);
    tp = 50; tn = 50; fp = 50; fn = 50;
    _TestFeatureSelection(tp, tn, fp, fn);
}

void TestFileLock()
{
    FileLock fl("lock");
    if(!fl.Lock(true))
    {
        cerr << "Failed to lock file" << endl;
        exit(1);
    }
    else
        cerr << "Aquired file lock" << endl;
    getchar();
    fl.Unlock();
}

void TestCorrelation()
{
    const size_t n = 50;
    vector<double> x(n), y(n);
    for(size_t i = 0; i < n; i ++)
    {
        x[i] = i;
        y[i] = i + drand48();
    }
    cout << "x=[" << x << "]" << endl;
    cout << "y=[" << y << "]" << endl;
    cout << "Correlation=" << Correlation<vector<double>, double>(x, y, n) << endl;
}

void TestException()
{
    throw Exception("you are too simple", "TooSimpleError");
}

void TestMannWhitneyU()
{
    const size_t n = 50;
    vector<double> x(n), y(n), combined(n + n);
    for(size_t i = 0; i < n; i ++)
    {
        x[i] = drand48();
        y[i] = drand48();
    }
    std::copy(x.begin(), x.end(), combined.begin());
    std::copy(y.begin(), y.end(), combined.begin() + n);
    cout << "x = [" << x << "]" << endl;
    cout << "y = [" << y << "]" << endl;
    MannWhitneyUTest mwutest(x.size(), y.size());
    double pvalue = mwutest(combined);
    cout << "p-value of Mann Whitney U test: " << pvalue << endl;
}

void TestIOUtils()
{
    MakeDirs("tmp/test_ioutils");
}

void TestProcessStatus()
{
    ProcessStatus ps;
    ps.Update();

    cout << "pid = " << ps.pid << endl;
    cout << "comm = " << ps.comm << endl;
    cout << "state = " << ps.state << endl;
    cout << "ppid = " << ps.ppid << endl;
    cout << "pgrp = " << ps.pgrp << endl;
    cout << "session = " << ps.session << endl;
    cout << "tty_nr = " << ps.tty_nr << endl;
    cout << "tpgid = " << ps.tpgid << endl;
    cout << "flags = " << ps.flags << endl;
    cout << "minflt = " << ps.minflt << endl;
    cout << "cminflt = " << ps.cminflt << endl;
    cout << "majflt = " << ps.majflt << endl;
    cout << "cmajflt = " << ps.cmajflt << endl;
    cout << "utime = " << ps.utime << endl;
    cout << "stime = " << ps.stime << endl;
    cout << "cutime = " << ps.cutime << endl;
    cout << "cstime = " << ps.cstime << endl;
    cout << "priority = " << ps.priority << endl;
    cout << "nice = " << ps.nice << endl;
    cout << "num_threads = " << ps.num_threads << endl;
    cout << "itrealvalue = " << ps.itrealvalue << endl;
    cout << "starttime = " << ps.starttime << endl;
    cout << "vsize = " << ps.vsize << endl;
    cout << "rss = " << ps.rss << endl;
    cout << "rsslim = " << ps.rsslim << endl;
    cout << "startcode = " << ps.startcode << endl;
    cout << "endcode = " << ps.endcode << endl;
    cout << "startstack = " << ps.startstack << endl;
    cout << "kstkesp = " << ps.kstkesp << endl;
    cout << "kstkeip = " << ps.kstkeip << endl;
    cout << "signal = " << ps.signal << endl;
    cout << "blocked = " << ps.blocked << endl;
    cout << "sigignore = " << ps.sigignore << endl;
    cout << "sigcatch = " << ps.sigcatch << endl;
    cout << "wchan = " << ps.wchan << endl;
    cout << "nswap = " << ps.nswap << endl;
    cout << "cnswap = " << ps.cnswap << endl;
    cout << "exit_signal = " << ps.exit_signal << endl;
    cout << "processor = " << ps.processor << endl;
    cout << "rt_priority = " << ps.rt_priority << endl;
}

int main(int argc, char** argv)
{
    srand(time(0));
    srand48(time(0));
    
    //TestMutualInfo();
    //TestFeatureSelection();
    //TestFileLock();
    //TestCorrelation();
    //TestException();
    //TestHDF5();
    //TestHDF5Hyperslab();
    //TestMannWhitneyU();
    //TestIOUtils();
    TestProcessStatus();

    return 0;
}
