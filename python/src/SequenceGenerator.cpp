#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
using namespace std;

// options
int numSeq = 5000;
int minLength = 10000;
int maxLength = 100000;
int fixedLength = 0;
char alphaSize = 4;
int wrap = 80;
const char* alphabet = "ATCG";
enum SeqFormat {SEQ_FASTA, SEQ_TEXT};
const char* prefix = "RN_";

void GenerateSequence()
{
    char* seq = NULL;
    if(fixedLength <= 0)
        seq = new char[maxLength + 1];
    else
        seq = new char[fixedLength + 1];
    
    char* fragment = NULL;
    if(wrap > 0)
        fragment = new char[wrap + 1];
    for(int i = 0; i < numSeq; i ++)
    {
        int length = fixedLength;
        if(fixedLength <= 0)
            length = rand() % (maxLength - minLength) + minLength;
        for(int j = 0; j < length; j ++)
            seq[j] = alphabet[rand() % alphaSize];
        seq[length] = 0;
        cout << ">" << prefix << setw(10) << setfill('0') << i + 1 << endl;
        if(wrap > 0)
        {
            int p = 0;
            while(length - p >= wrap)
            {
                strncpy(fragment, &(seq[p]), wrap);
                fragment[wrap] = 0;
                cout << fragment << endl;
                p += wrap;
            }
            if(length > p)
            {
                strncpy(fragment, &(seq[p]), length - p);
                fragment[length - p] = 0;
                cout << fragment << endl;
            }
        }
        else
            cout << seq << endl;
    }
    delete[] seq;
    if(wrap > 0)
        delete[] fragment;
}

int main(int argc, char** argv)
{
    srand((unsigned int)time(0));
    GenerateSequence();
    
    return 0;
}
    