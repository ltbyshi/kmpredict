#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
using namespace std;

#define PROGNAME "ReadBin"
const size_t bufsize = 1024*1024;

void Usage()
{
    cerr << "Usage: " << PROGNAME << " file1:start:end [file2:start:end]" << endl;
    exit(1);
}

void ReadBin(istream& fin, const string& filename, size_t start, size_t length)
{
    // cerr << "Info: " << filename << ", " << start << ", " << length << endl;
    if(!fin)
    {
        cerr << "Error: cannot open the input file: " << filename << endl;
        return;
    }
    
    char* buffer = new char[bufsize];
    if(start > 0)
    {
        fin.seekg(start);
        // cannot seek in stream input
        if(!fin)
        {
            cerr << "Warning: cannot seek in file: " << filename << endl;
            fin.clear();
            
            size_t blocks = start / bufsize;
            size_t remain = start - blocks*bufsize;
            for(size_t i = 0; i < blocks; i ++)
            {
                fin.read(buffer, bufsize);
                if(!fin)
                    break;
            }
            if(fin)
                fin.read(buffer, remain);
        }
        if(!fin)
        {
            cerr << "Error: less than " << start << " bytes in the file: " << filename << endl;
            delete[] buffer;
            return;
        }
    }
    
    size_t totalread = 0;
    while(fin.good())
    {
        fin.read(buffer, bufsize);
        size_t nread = fin.gcount();
        totalread += nread;
        if((length == 0) || 
            ((length > 0) &&(totalread <= length)))
            cout.write(buffer, nread);
        else
        {
            cout.write(buffer, length - (totalread - nread));
            break;
        }
    }
    if((length > 0) && (totalread < length))
        cerr << "Warning: less bytes read than requested from file: " << filename << endl;

    delete[] buffer;
}

int main(int argc, char** argv)
{
    if(argc < 2)
    {
        cerr << "Error: missing arguments\n" << endl;
        Usage();
    }
    
    for(int i = 1; i < argc; i ++)
    {
        string spec(argv[i]);
        string filename;
        size_t start, end, length;
        size_t sep[2];
        size_t nseps = 0;
        
        for(size_t pos = 0; pos < spec.size(); pos ++)
        {
            if(spec[pos] == ':')
            {
                if(nseps >= 2)
                    break;
                else
                {
                    sep[nseps] = pos;
                    nseps ++;
                }
            }
        }
        if(nseps != 2)
        {
            cerr << "Error: invalid argument\n" << endl;
            Usage();
        }
        filename = spec.substr(0, sep[0]);
        if(sep[1] - sep[0] == 1)
            start = 0;
        else
            start = atoi(spec.substr(sep[0] + 1, sep[1] - sep[0] -1).c_str());
        if(spec.size() - sep[1] == 1)
            end = 0;
        else
            end = atoi(spec.substr(sep[1] + 1, spec.size() - sep[1] - 1).c_str());
        
        if(end > 0)
            length = end - start;
        else
            length = 0;
        
        if(filename == "-")
            ReadBin(cin, "(stdin)", start, length);
        else
        {
            ifstream fin(filename.c_str(), ios::binary);
            ReadBin(fin, filename, start, length);
        }
    }
    return 0;
}