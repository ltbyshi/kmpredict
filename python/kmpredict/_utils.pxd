from libc cimport stdlib
from libc.string cimport memcpy

cdef int rc_dna[256]
cdef int nuc_dna[4]

cdef inline void* malloc_s(size_t size):
    cdef void* mem = stdlib.malloc(size)
    if mem == NULL:
        raise MemoryError('malloc returned NULL')

cdef inline void free_s(void* mem):
    if mem != NULL:
        stdlib.free(mem)

cdef inline int _choose_canonical(const char* seq, size_t length, const int* rc):
    """Compare the sequence and its reverse complement and choose the smaller one.
    Args:
        seq, length: input sequence
        rc: an array of shape (256,) containing the complement of a nucleotide
    Returns:
        0 if the original sequence should be chosen or 1 if its RC should be chosen.
    """
    cdef size_t i, ri
    cdef int c, c_rc
    cdef int choice = 0
    ri = length -  1
    for i in range(length):
        c = seq[i]
        c_rc = rc[<int>seq[ri]]
        if c < c_rc:
            choice = 0
            break
        elif c > c_rc:
            choice = 1
            break
        ri -= 1
    return choice

cdef inline void _get_reverse_complement(const char* seq, size_t length, char* rc_seq, const int* rc):
    """Convert a sequence to its reverse-complement
    """
    cdef size_t i, ri
    ri = length - 1
    for i in range(length):
        rc_seq[ri] = rc[<int>seq[i]]
        ri -= 1

cdef inline size_t _get_canonical_kmers(const char* seq, size_t length, size_t k, char* kmer_seq, const int* rc):
    """Get all overlapping k-mers on the sequence
    Returns:
        the number of k-mers in the sequence
    """
    cdef size_t i, n_kmers
    if length < k:
        return 0
    n_kmers = length - k + 1
    for i in range(n_kmers):
        if _choose_canonical(seq, k, rc) == 0:
            memcpy(kmer_seq, seq, k)
        else:
            _get_reverse_complement(seq, length, kmer_seq, rc)
        seq += 1
    return n_kmers
