cdef extern from "stdio.h" nogil:

    ctypedef struct FILE

    int feof_unlocked      (FILE *stream)

    char *gets_unlocked  (char *s)
    char *fgets_unlocked (char *s, int count, FILE *stream)
    int getchar_unlocked ()
    int fgetc_unlocked   (FILE *stream)
    int getc_unlocked    (FILE *stream)
    int ungetc_unlocked  (int c, FILE *stream)

    int puts_unlocked    (const char *s)
    int fputs_unlocked   (const char *s, FILE *stream)
    int putchar_unlocked (int c)
    int fputc_unlocked   (int c, FILE *stream)
    int putc_unlocked    (int c, FILE *stream)
