cimport ckmercounter
from ckmercounter cimport ValType, KmerType
cimport numpy as np
import numpy as np
from lsh cimport LSH
import time
import h5py
cimport cython

cdef class FastaReader:
    cdef bint single_line
    cdef str seqname, seq, filename
    cdef object f
    def __cinit__(self, filename, single_line=False):
        self.filename = filename
        self.f = open(filename, 'r')
        self.single_line = single_line

    def __iter__(self):
        return self
        
    def __next__(self):
        line = self.f.readline().strip()
        if len(line) == 0:
            raise StopIteration
        if line[0] == '>':
            if self.seqname is None:
                self.seqname = line
            else:
                raise ValueError('empty record in fasta file: %s'%self.filename)
            self.seq = ''
        else:
            self.seq = line
        while True:
            line = self.f.readline()
            if len(line) == 0:
                raise StopIteration()
            if line[0] == '>':
                self.seqname = line
                break
            else:
                self.seq += line
        return self.seq

    def read_seqs(self, int max_seqs):
        cdef int i
        cdef object seqs = []
        for i in range(max_seqs):
            try:
                seqs.append(next(self))
            except StopIteration:
                break
        return seqs

    def close(self):
        self.f.close()

cdef class KmerCounter:
    cdef np.ndarray data
    cdef int k
    def Save(self, filename, dataname='data'):
        """Save the k-mer counts into an HDF5 file
        """
        f = h5py.File(filename, 'w')
        dset = f.create_dataset(dataname, data=self.data)
        dset.attrs['k'] = self.k
        f.close()
    def Load(self, filename, dataname='data'):
        """Load the k-mer counts from an HDF5 file
        """
        f = h5py.File(filename, 'r')
        self.data = f[dataname]
        f.close()

cdef class ExactKmerCounter(KmerCounter):
    cdef ckmercounter.KmerCounter *thisptr
    def __cinit__(self, int k):
        self.k = k
        self.thisptr = new ckmercounter.KmerCounter(k)
    def __dealloc__(self):
        del self.thisptr
    def Clear(self):
        """Free memory and invalidate the data pointer.

        Detach the associated numpy array if it is set.
        """
        self.thisptr.Clear()
        self.data = None
    def Allocate(self, hash_size):
        """Allocate memory for a new hash table.

        Clear a previous hash table if it exists.
        """
        self.thisptr.Allocate(hash_size)

    def GetK(self):
        return self.thisptr.GetK()

    def GetHashSize(self):
        return self.thisptr.GetHashSize()

    def MergeRC(self):
        self.thisptr.MergeRC()

    def CountFasta(self, seqfile):
        cdef size_t hash_size = 4**self.k
        self.data = np.zeros(hash_size, dtype='uint32')
        self.thisptr.SetData(self.data.shape[0], <ValType*>self.data.data)
        return self.thisptr.CountFasta(seqfile)

    def GetKmer(self, kmer):
        return self.thisptr.GetKmer(kmer)


cdef class LSHKmerCounter(KmerCounter):
    cdef int n_bits

    def __cinit__(self, k, n_bits):
        self.k = k
        self.n_bits = n_bits
        self.data = np.zeros(2**n_bits, dtype='uint32')

    def __dealloc__(self):
        pass

    @cython.boundscheck(False)
    def CountFasta(self, seqfile, lshfile=None, chunk_size=5000):
        cdef LSH lsh_hash
        cdef np.ndarray[np.uint32_t, ndim=1] data = self.data
        cdef np.ndarray[np.int64_t] index
        cdef np.int64_t i
        cdef size_t n_seqs = 0
        lsh_hash = LSH(self.k, self.n_bits)
        if lshfile:
            lsh_hash.load_coefs(lshfile)
        else:
            lsh_hash.generate_coefs()
        reader = FastaReader(seqfile)
        print 'Started hashing sequences'
        cdef double start_time, elapsed_time
        start_time = time.clock()
        seqs = reader.read_seqs(chunk_size)
        n_seqs += len(seqs)
        while len(seqs) > 0:
            index = lsh_hash.hash_seqs(seqs)
            for i in range(index.shape[0]):
                data[index[i]] += 1
            seqs = reader.read_seqs(chunk_size)
            n_seqs += len(seqs)
        elapsed_time = time.clock() - start_time
        print 'Finished hashing sequences (%d, %f/s)'%(n_seqs, n_seqs/elapsed_time)
        reader.close()
