cimport numpy as np
import numpy as np
import h5py
from fasta cimport FastaRecord, FastaReader
from libc cimport stdlib
cimport lsh
from carray cimport CArray
cimport cython
from _utils cimport _choose_canonical, rc_dna, nuc_dna

cdef class LSH:
    def __cinit__(self, k, n_bits):
        self.k = k
        self.n_bits = n_bits
        self.init_transtable()

    cdef init_transtable(self):
        cdef np.ndarray[np.complex128_t] transtable = np.zeros(256, dtype='complex128')
        transtable[ord('A')] = 0.995 + 0j
        transtable[ord('C')] = 0 + 0.995j
        transtable[ord('G')] = 0 - 0.995j
        transtable[ord('T')] = -0.995 + 0j
        self.transtable = transtable

    def load_coefs(self, filename):
        f = h5py.File(filename, 'r')
        self.w0 = f['w0'][:]
        self.W = f['W'][:]
        if (f['w0'].shape[0] != self.n_bit) or (f['W'].shape[0] != self.k):
            raise ValueError('unmatched array shapes in coefficient file')
        f.close()

    def save_coefs(self, filename):
        f = h5py.File(filename, 'w')
        f.create_dataset('W', data=self.W)
        f.create_dataset('w0', data=self.w0)
        f.close()

    def generate_coefs(self, n_samples=1000):
        cdef np.ndarray[np.complex128_t, ndim=2] X
        cdef np.ndarray[np.complex128_t, ndim=1] nucs
        cdef np.ndarray U, S, V
        self.w0 = np.zeros((self.n_bits, 1), dtype='complex128')
        self.W = np.zeros((self.n_bits, self.k), dtype='complex128')
        for h in range(self.n_bits):
            nucs = np.asarray([0.995 + 0j, 0 + 0.995j, 0 - 0.995j, -0.995 + 0j],
                    dtype='complex128')
            X = np.random.choice(nucs, size=(n_samples + 1, self.k + 1))
            X[n_samples, :] = 0
            X[:, self.k] = -1 + 0j
            U, S, V = np.linalg.svd(X)
            self.w0[h, 0] = V[self.k, self.k]
            self.W[h, :] = V[:-1, self.k]

    cdef np.ndarray encode_seq(self, const char* seq, size_t seqlen):
        """ convert a sequence to complex vectors
        Args:
            seq: a DNA sequence
        Returns:
            an array with shape (k, number of k-mers)
        """
        cdef size_t n_kmers = seqlen - self.k + 1
        cdef np.ndarray[np.complex128_t] transtable = self.transtable
        if n_kmers < 1:
            return None
        cdef np.ndarray[np.complex128_t, ndim=2] X = np.empty((self.k, n_kmers), dtype='complex128')
        cdef size_t i, ikmer
        for i in range(self.k):
            X[i, 0] = transtable[<int>seq[i]]
        for ikmer in range(1, n_kmers):
            X[:-1, ikmer] = X[1:, ikmer - 1]
            X[-1,  ikmer] = transtable[<int>seq[self.k + ikmer - 1]]
        return X

    @cython.boundscheck(False)
    cdef np.ndarray encode_seqs(self, size_t n_seqs,
            const char** seqs, size_t* seqlens,
            int canonical):
        """ convert a list of sequences to complex vectors
        Args:
            seqs: a list of sequences
            canonical: 0 or 1.
                If canonical is 1, each k-mer is compared to its reverse-complement
                and only the smaller one is counted.
        Returns:
            an array with shape (k, number of k-mers)
        """
        cdef size_t iseq, ikmer, ikmer_s, n_kmers_s
        cdef int i, ri
        cdef np.ndarray[np.uint64_t, ndim=1] n_kmers
        cdef np.ndarray[np.complex128_t, ndim=2] X
        cdef np.ndarray[np.complex128_t] transtable = self.transtable
        cdef const char* cseq
        n_kmers = np.zeros(n_seqs, dtype=np.uint64)
        for iseq in range(n_seqs):
            n_kmers[iseq] = seqlens[iseq] - self.k + 1
            if n_kmers[iseq] < 0:
                n_kmers[iseq] = 0
        X = np.empty((self.k, n_kmers.sum()), dtype='complex128')
        ikmer = 0
        if canonical:
            for iseq in range(n_seqs):
                if n_kmers[iseq] <= 0:
                    continue
                cseq = seqs[iseq]
                for ikmer_s in range(n_kmers[iseq]):
                    if _choose_canonical(cseq + ikmer_s, self.k, rc_dna) == 0:
                        for i in range(self.k):
                            X[i, ikmer] = transtable[<int>cseq[i]]
                    else:
                        ri = self.k - 1
                        for i in range(self.k):
                            X[i, ikmer] = -transtable[<int>cseq[ri]]
                            ri -= 1
                    ikmer += 1
                    cseq += 1
        else:
            for iseq in range(n_seqs):
                if n_kmers[iseq] <= 0:
                    continue
                cseq = seqs[iseq]
                for i in range(self.k):
                    X[i, ikmer] = transtable[<int>cseq[i]]
                cseq += self.k
                ikmer += 1
                for ikmer_s in range(1, n_kmers[iseq]):
                    for i in range(self.k - 1):
                        X[i, ikmer] = X[i + 1, ikmer - 1]
                    #X[:(self.k - 1), ikmer] = X[1:, ikmer - 1]
                    X[(self.k - 1),  ikmer] = transtable[<int>cseq[0]]
                    cseq += 1
                    ikmer += 1
        return X

    @cython.boundscheck(False)
    cdef np.ndarray hash_vec(self, np.ndarray[np.complex128_t, ndim=2] X):
        """ convert complex vectors to hash values
        Args:
            X: a matrix of complex vectors with shape (k, number of k-mers)
        Returns:
            an array with shape (number of k-mers)
        """
        if X is None:
            return
        cdef size_t n_kmers = X.shape[1]
        cdef np.ndarray[np.uint8_t, ndim=2] y
        cdef np.ndarray[np.int64_t] values = np.zeros(n_kmers, dtype='int64')
        cdef unsigned long ikmer, i, h

        cdef np.ndarray y_ = ((np.dot(self.W, X) - self.w0) > 0)
        y = y_.astype(np.uint8, copy=False)
        for ikmer in range(n_kmers):
            h = 0
            for i in range(self.n_bits):
                h <<= 1
                h += y[i, ikmer]
            values[ikmer] = h
        return values

    def hash_seq(self, seq):
        """ hash overlapping k-mers in a sequence
        """
        cdef np.ndarray X = self.encode_seq(seq, len(seq))
        return self.hash_vec(X)

    def hash_seqs(self, seqs, canonical=False):
        """ hash overlapping k-mers in a list of sequence
        """
        cdef size_t n_seqs = len(seqs)
        cdef np.npy_intp dims_seqs = n_seqs * sizeof(char*)
        cdef np.npy_intp dims_seqlens = n_seqs * sizeof(size_t)
        #cdef np.ndarray cseqs = np.empty(n_seqs * sizeof(char*), dtype=np.void)
        cdef CArray cseqs = CArray(n_seqs * sizeof(char*))
        #cdef np.ndarray cseqs = np.PyArray_SimpleNew(1, &dims_seqs, np.NPY_VOID)
        #cdef np.ndarray[size_t] seqlens = np.empty(n_seqs * sizeof(size_t), dtype=np.void)
        cdef CArray seqlens = CArray(n_seqs * sizeof(size_t))
        #cdef np.ndarray[size_t] seqlens = np.PyArray_SimpleNew(1, &dims_seqlens, np.NPY_VOID)
        cdef size_t i
        for i in range(n_seqs):
            (<const char**>cseqs.data)[i] = seqs[i]
            (<size_t*>seqlens.data)[i] = len(seqs[i])
        cdef np.ndarray X = self.encode_seqs(n_seqs, <const char**>cseqs.data, <size_t*>seqlens.data, canonical)
        return self.hash_vec(X)
