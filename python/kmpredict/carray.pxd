ctypedef union data_ptr:
    void* voids
    float* floats
    double* doubles
    int* ints
    unsigned int* uints
    char* chars
    unsigned char* uchars
    long* longs
    unsigned long* ulongs
    short* shorts
    unsigned short* ushorts
    const char* cchars
    char** pchars
    const char** cpchars

cdef class CArray:
    cdef char* data
    cdef data_ptr d
    cdef readonly size_t size
