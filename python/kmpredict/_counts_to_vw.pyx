import cython
import sys
cimport numpy as np
cimport libc.stdio as stdio

@cython.boundscheck(False)
def counts_to_vw(np.ndarray[np.uint32_t, ndim=1] x, group):
    cdef bint i
    cdef int g = group
    stdio.printf('%d | ', g)
    for i in range(x.shape[0]):
        if x[i] > 0:
            stdio.printf(' %d:%d', i, x[i])

