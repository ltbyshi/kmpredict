from libc.stdint cimport uint32_t, uint64_t

cdef extern from 'KmerCounter.h' nogil:
    ctypedef uint64_t KmerType
    ctypedef uint32_t ValType

    cdef cppclass KmerCounter:
        KmerCounter(size_t hash_size)
        void Clear()
        void Allocate(size_t hashsize)
        void SetData(size_t hashsize, ValType* data)
        KmerType GetUniqueKmerCount()
        KmerType GetHashSize()
        int GetK()
        ValType* GetData()
        void AddKmer(KmerType kmer, ValType value)
        void AddKmer(const char* seq, ValType value)
        ValType GetKmer(KmerType kmer)
        void MergeRC()
        void CountFasta(const char* seqfile)
        void WeightedCountFasta(const char* seqfile, int* weights, size_t n_seq)
        void KmerCountHistogram(int bincount, unsigned long* values)
        unsigned long AverageCount()
        void LSHCountFasta(const char* seqfile, const char* hyperplane_file, size_t n_bit)
