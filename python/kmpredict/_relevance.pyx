import cython
import numpy as np
cimport numpy as np
from scipy.stats import pearsonr, mannwhitneyu

@cython.boundscheck(False)
def Correlation(np.ndarray[np.float32_t, ndim=2] m, np.ndarray[np.int_t, ndim=1] groups):
    cdef unsigned int n_kmers = m.shape[1]
    cdef np.ndarray[np.float32_t, ndim=1] r = np.ndarray(n_kmers, dtype=np.float32)
    cdef unsigned int i
    for i in range(n_kmers):
        r[i] = pearsonr(m[:, i], groups)[0]
    return r

@cython.boundscheck(False)
def MannWhitneyUTest(np.ndarray[np.float32_t, ndim=2] m, np.ndarray[np.int_t, ndim=1] groups):
    cdef unsigned int n_kmers = m.shape[1]
    cdef np.ndarray[np.float32_t, ndim=1] r = np.ndarray(n_kmers, dtype=np.float32)
    cdef np.ndarray[np.int_t, ndim=1] group1 = np.nonzero(groups == -1)[0]
    cdef np.ndarray[np.int_t, ndim=1] group2 = np.nonzero(groups == 1)[0]
    cdef unsigned int i
    for i in range(n_kmers):
        try:
            r[i] = mannwhitneyu(m[group1, i], m[group2, i])[1]
        except ValueError:
            r[i] = 2.0

    return r

cdef extern from "_relevance_helper.h":
    void print_array1d[DType](int* data, int n) nogil

@cython.boundscheck(False)
def ndarray_c(np.ndarray m):
    if (m.dtype == 'int32') and (m.ndim == 1):
        print_array1d[int](<int*>m.data, m.shape[0])
    else:
        raise ValueError('array not match: should be int32 and 1d')


