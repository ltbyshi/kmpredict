# each filter metric takes a data matrix and optionally the class labels as input
# and returns an array of scores.
import numpy as np
cimport numpy as np

ctypedef float ValType

def frequency(np.ndarray X, np.ndarray y=None, int min_freq=1):
    """First convert the input matrix X into binary matrix.
    Then select features that has frequency higher than min_freq.
    """
    cdef np.ndarray X_bin = X > min_freq
    cdef np.ndarray freq = X_bin.sum(axis=1)
    cdef np.ndarray features = np.nonzero(freq >= min_freq)[0]
    return features

def correlation(np.ndarray X, np.ndarray y):
    pass

