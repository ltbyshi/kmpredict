cimport _utils
cimport cython
from libc cimport stdio, stdlib, time
from kmer cimport ValType
cimport numpy as np
from cpython cimport PyString_AS_STRING, PyString_FromStringAndSize

cdef extern from "stdio.h" nogil:
    ctypedef struct FILE
    int fflush(FILE *stream)

@cython.boundscheck(False)
def kmercounts_to_vw(np.ndarray[ValType, ndim=1] counts,
                     group, ValType min_count=1):
    """Convert k-mer counts to the input format of Vowpal Wabbit
    and print to standard output.
    Args:
        counts: a array of k-mer counts
        group: class label
        min_count: only keep k-mers that have counts of at least min_count
    """
    group = str(group)
    cdef size_t i
    cdef size_t n_kmers = counts.shape[0]
    cdef const char* group_str = group
    stdio.printf('%s | ', group_str)
    for i in range(n_kmers):
        if counts[i] >= min_count:
            stdio.printf(' %lu:%u', i, counts[i])
    stdio.printf('\n')
    fflush(stdio.stdout)

# init reverse-complement lookup table
cdef int rc_dna[256]
cdef init_rc_dna():
    cdef int i
    for i in range(256):
        rc_dna[i] = 0
    rc_dna[<int>'A'] = <int>'T'
    rc_dna[<int>'T'] = <int>'A'
    rc_dna[<int>'G'] = <int>'C'
    rc_dna[<int>'C'] = <int>'G'
init_rc_dna()

# init alphabet
cdef int nuc_dna[4]
cdef init_nuc_dna():
    nuc_dna[0] = <int>'A'
    nuc_dna[1] = <int>'T'
    nuc_dna[2] = <int>'C'
    nuc_dna[3] = <int>'G'
init_nuc_dna()

cdef init_module():
    stdlib.srand(time.time(NULL))
init_module()

def choose_canonical(seq):
    """Compare the sequence and its reverse complement and choose the smaller one.
    Args:
        seq: input sequence
    Returns:
        0 if the original sequence should be chosen or 1 if its RC should be chosen.
    """
    cdef const char* cseq = seq
    cdef size_t length = len(seq)
    return _choose_canonical(cseq, length, rc_dna)

def get_reverse_complement(seq):
    """Returns the reverse-complement of a sequence
    """
    cdef size_t length = len(seq)
    cdef object rc_seq = PyString_FromStringAndSize(NULL, length)
    cdef char* rc_cseq = PyString_AS_STRING(rc_seq)
    _get_reverse_complement(seq, length, rc_cseq, rc_dna)
    return rc_seq

def random_sequence(Py_ssize_t length):
    cdef object seq = PyString_FromStringAndSize(NULL, length)
    cdef char* cseq = PyString_AS_STRING(seq)
    cdef Py_ssize_t i
    for i in range(length):
        cseq[i] = nuc_dna[stdlib.rand() % 4]
    return seq

def get_canonical_kmers(seq, Py_ssize_t k):
    cdef Py_ssize_t length = len(seq)
    cdef const char* cseq = seq
    cdef object kmer_seq = PyString_FromStringAndSize(NULL, length)
    cdef char* kmer_cseq = PyString_AS_STRING(kmer_seq)

    cdef n_kmers = _get_canonical_kmers(cseq, length, k, kmer_cseq, rc_dna)
    if n_kmers > 0:
        return kmer_seq
