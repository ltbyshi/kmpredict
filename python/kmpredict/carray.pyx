from libc cimport stdlib

cdef class CArray:
    def __cinit__(CArray self, size_t size):
        self.size = size
        self.data = <char*>stdlib.malloc(size)
        self.d.voids = <void*>self.data
        if not self.data:
            raise MemoryError()
    def __dealloc__(CArray self):
        stdlib.free(<void*>self.data)
