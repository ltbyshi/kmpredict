from collections import defaultdict
from _utils import *
import json
import h5py
import os, errno

class ArgDependChecker(object):
    """Check dependency between arguments for argparse
    """
    def __init__(self):
        self.depends = defaultdict(list)

    def add_depend(self, first, second):
        """specify dependency between two arguments
        Args:
            first, second: first argument depends on the second argument
                the argument names must exist in the parsed Namespace object
        """
        self.depends[first].append(second)

    def check(self, parser, args):
        """check parsed arguments
        Prints error if any of the dependency requirement is not satisfied
        Args:
            parser: an argparse.ArgumentParser object
            args: an argsparse.Namespace object parsed by ArgumentParser
        """
        args_d = vars(args)
        for first, seconds in self.depends.iteritems():
            if args_d[first]:
                unsatisfied = []
                for second in seconds:
                    if args_d[second] is None:
                        unsatisfied.append(second)
                parser.error('option %s depends on %s'%(first, ' '.join(seconds)))

def require_options(name, args, options):
    """Check required options for the parsed arguments by the argparse module
    Args:
        args: a Namespace object returned by ArgumentParser.parse_args()
        options: a list of options (long options) to check
    Raises:
        ValueError if any of the options is not in args
    """
    args = vars(args)
    missing = []
    for option in options:
        if option.startswith('--'):
            option_name = option[2:]
        elif option.startswith('-'):
            option_name = option[1:]
        else:
            option_name = option
        option_name = option_name.replace('-', '_')
        if args.get(option_name) is None:
            missing.append(option)
    if len(missing) > 0:
        raise ValueError('options %s are required for "%s"'%(
            ','.join(['%s'%option for option in missing]),
            name))

def save_json(obj, filename):
    """Save a python object to JSON file
    """
    f = open(filename, 'w')
    json.dump(obj, f)
    f.close()

def load_json(filename):
    """Load a python object from a JSON file
    """
    f = open(filename, 'r')
    obj = json.load(f)
    f.close()
    return obj

def load_hdf5(filename, dataname):
    """Load a matrix from HDF5 file
    Args:
        filename: HDF5 file name
        dataname: dataset name of the matrix
    Returns:
        a numpy array
    """
    import numpy as np
    f = h5py.File(filename, 'r')
    ds = f[dataname]
    m = np.empty(shape=ds.shape, dtype=ds.dtype)
    ds.read_direct(m)
    f.close()
    return m

def save_hdf5(data, filename, dataname):
    """Save a matrix to HDF5 file
    Args:
        data: a numpy array
        filename: HDF5 file name
        dataname: dataset name of the matrix
    """
    f = h5py.File(filename, 'w')
    f.create_dataset(dataname, data=data)
    f.close()

def makedir(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST:
            pass
        else:
            raise e
