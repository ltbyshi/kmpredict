# framework for evaluation of machine learning models
from utils import load_json, save_json, load_hdf5, save_hdf5, makedir
from parallelmap import parallelmap
import itertools
import os
import string

# configuration
class Configuration(object):
    def __init__(self):
        self.d = {}

    def get(self, name, **kwargs):
        """Get a variable with template substituted.
        For template substitution, the variables in the global configuration is first searched,
        and then the other variables are given in kwargs.
        """
        value = string.Template(self.d[name]).safe_substitute(self.d)
        value = string.Template(value).safe_substitute(kwargs)
        return value

    def __getitem__(self, name):
        """Get the value or template string of a variable
        """
        return self.d[name]

    def set(self, name, value):
        """Add a new variable with name and value.
            value is a normal string or template (with ${name} for variables)
        """
        self.d[name] = value

# initialize configuration variables
config = Configuration()
config.set('RootDir', 'cross_validation')
config.set('CVIndexFile', '${RootDir}/cv_index.json')
config.set('FeatureFileInner', '${RootDir}/features/inner/${OuterIndex}.${InnerIndex}')
config.set('FeatureFileOuter', '${RootDir}/features/outer/${OuterIndex}')
config.set('MatrixFileInner',  '${RootDir}/matrix/inner/${OuterIndex}.${InnerIndex}')
config.set('MatrixFileOuter',  '${RootDir}/matrix/outer/${OuterIndex}')
config.set('ResultFileInner',  '${RootDir}/result/inner/${OuterIndex}.${ParamIndex}.${InnerIndex}.json')
config.set('ResultFileOuter',  '${RootDir}/result/outer/${OuterIndex}.json')

def _defered_import():
    global h5py, np, metrics
    import h5py
    import numpy as np
    from sklearn import metrics

def normalize(X, factors=None, method=None):
    """Transform the data matrix such that the feature values of the samples are of the same scale
    Args:
        X: data matrix of shape (#samples, #features)
        factors: an array of scaling factors (length: #samples)
        method: specify how to normalize the matrix.
    """
    from sklearn import preprecessing
    Y = None
    if method == 'ScaleSamples':
        if factors is not None:
            Y = X/factors
        else:
            Y = preprocessing.normalize(X)
    else:
        raise ValueError('unknown normalization method: %s'%method)

def feature_select(X, y=None, method=None, **kwargs):
    """Generic function for feature selection
    Args:
        X: data matrix of shape (#samples, #features)
        y: an array of class labels of shape (#samples,)
        method: name of the feature selection method
    Returns:
        an array of indices of the features in X
    """
    from sklearn import feature_selection
    import _filter_metrics
    features = None
    if method == 'frequency':
        min_freq = 1
        if 'min_freq' in kwargs:
            min_freq = kwargs['min_freq']
        features = _filter_metrics.frequency(X, y, min_freq)
    else:
        raise ValueError('unknown feature selection method: %s', method)
    return features

def create_model(classifier, hyper_params, class_weight='balanced'):
    """Create a classifier from name
    Args:
        classifer: name of the classifier
        hyper_params: a dict of values of hyper parameters for the classifier
    """
    if classifier == 'SVM':
        from sklearn.svm import SVC
        return SVC(C=hyper_params['C'], probability=True, class_weight=class_weight)
    elif classifier == 'SVMLinear':
        from sklearn.svm import LinearSVC
        return LinearSVC(C=hyper_params['C'], class_weight=class_weight)
    elif classifier == 'LogRegL1':
        from sklearn.linear_model import LogisticRegression
        return LogisticRegression(penalty='l1', C=hyper_params['C'], class_weight=class_weight)
    elif classifier == 'LogRegL2':
        from sklearn.linear_model import LogisticRegression
        return LogisticRegression(penalty='l2', C=hyper_params['C'], class_weight=class_weight)
    elif classifier == 'RandomForest':
        from sklearn.ensemble import RandomForestClassifier
        return RandomForestClassifier(n_estimators=hyper_params['N'], class_weight=class_weight)
    elif classifier == 'MultinomialNB':
        from sklearn.naive_bayes import MultinomialNB
        return MultinomialNB(alpha=hyper_params['alpha'])
    elif classifier == 'GaussianNB':
        from sklearn.naive_bayes import GaussianNB
        return GaussianNB()
    else:
        raise ValueError('Unknown classifier: %s'%classifier)

def train_model(model, X_train, y_train):
    """Train a classifier on a given training set
    """
    model.fit(X_train, y_train)
    return model

def evaluate_model(classifier, X_test, y_test, eval_metric=None,
                   hyper_params=None):
    model = create_model(classifier, hyper_params)
    result = {}
    result['HyperParams'] = hyper_params
    # calculate AUC (call predict_proba)
    if classifier == 'SVMLinear':
        scores = model.decision_function(X_test)
    else:
        scores = model.predict_proba(X_test)[:, 1]
    fpr, tpr, thresholds = metrics.roc_curve(y_test, scores)
    result['AUC'] = metrics.auc(fpr, tpr)
    result['Scores'] = scores.tolist()
    # calculate weighted accuracy (call score)
    classes, counts = np.unique(y_test, return_counts=True)
    weights = 1.0/counts.astype('float')
    sample_weight = np.empty(len(y_test))
    for c, w in zip(classes, weights):
        sample_weight[y_test == c] = w
    y_pred = model.predict(X_test)
    score = metrics.accuracy_score(y_test, y_pred, sample_weight=sample_weight)
    #score = model.score(X_test, y_test, sample_weight=sample_weight)
    result['PredictedLabels'] = y_pred.tolist()
    result['TrueLabels'] = y_test.tolist()
    result['Accuracy'] = score
    result['Performance'] = result[eval_metric]

    return result

def inner_task(args):
    """Run the inner loop of cross-validation
    Args:
        DataMatrixFile: the file containing training data and test data.
            The shape of the matrix should be (#samples, #features)
        DataMatrixFileType: data file type. One of 'hdf5', 'table'.
        TableHeader: True
        TrueLabels: an integer array of class labels.
        TrainIndex: an integer array of indices of training samples in the matrix.
        TestIndex: an integer array of indices of test samples in the matrix.
        HyperParams: a dict containing the hyper parameters for creating the classifier
        ResultFile: filename of the output file (JSON format) for storing results
    """
    if args['DataMatrixFileType'] == 'hdf5':
        X = load_hdf5(args['DataMatrixFile'], args['HDF5DataName'])
    elif args['DataMatrixFileType'] == 'table':
        import pandas as pd
        X = pd.read_table(args['DataMatrixFile'], header=args.get('TableHeader'))
    else:
        raise ValueError('invalid DataMatrixFileType: %s', args['DataMatrixFileType'])
    y = np.asarray(args['TrueLabels'])
    result = evaluate_model(classifier=args['Classifier'],
                   X_train=X[args['TrainIndex'], :],
                   y_train=y[args['TrainIndex']],
                   X_test=X[args['TestIndex'], :],
                   y_test=y[args['TestIndex']],
                   hyper_params=args['HyperParams'])
    save_json(result, args['ResultFile'])
    del X, y

def outer_task(args):
    """Run the outer loop of cross-validation
    Args:
        InnerResultFiles: a two-dimensional list of result file names.
            The shape of the list is (#set of hyper-parameters, #inner CV folds).
            The file names can be indexed by [hyper_index][inner_index],
            where hyper_index is the index of a certain set of hyper-parameters
            and inner_index is the index of a certain inner CV loop.
        EvalMetric: the name of the metric to select hyper-paramters
        Other variables are the same as inner_task()
    """
    # get best hyper-parameters from inner loop
    perfs_hyper = []
    hyper_params_list = []
    for inner_result_filelist in args['HyperResults']:
        perfs = []
        for inner_index, inner_result_file in enumerate(inner_result_filelist):
            result = load_json(inner_result_file)
            perfs.append(results[args['EvalMetric']])
            if inner_index == 0:
                hyper_params_list.append(results['HyperParams'])
        perfs_hyper.append(np.mean(perfs))
    best_hyper = hyper_params_list[np.argmax(perfs_hyper)]
    args['HyperParams'] = best_hyper
    # evaluate with the best hyperparameter
    inner_task(args)

def run_inner(data_matrix_file, true_labels,
        hyper_param_list,
        classifiers, eval_metrics, cv_index,
        n_jobs=1, parallel_engine='sge_array'):
    """Run all tasks of inner cross-validation, optionally in parallel
    Args:
        data_matrix_file: the data matrix in HDF5 format or text format
            with shape (#samples, #features)
        true_labels: a list of class labels with shape (#samples,)
        hyper_param_list: a dict of lists containing hyper-parameters to search from.
            Dict key is the name of the hyper-parameter.
            For example, {'C': [0.01, 0.1, 1, 10, 100]}
        classifiers: a list of classifier names to use
        eval_metrics: a list of metric names for evaluation of the classification result
        cv_index: an object created by create_cv_index
    """
    for classifier, outer_index, inner_index in itertools.product(classifiers,
            range(cv_index['NumOuter']),
            range(cv_index['NumInner'])):
        train_index = cv_index['Inner'][outer_index][inner_index]['TrainIndex']
        test_index = cv_index['Inner'][outer_index][inner_index]['TestIndex']
        args = {'TrainIndex': train_index,
                'TestIndex': test_index,
                'TrueLabels': true_labels,
                'Classifier': classifier,
                'EvalMetrics': eval_metrics}
        if len(hyper_param_list) > 0:
            param_index = 0
            param_names = hyper_param_list.keys()
            for param_values in itertools.product(*hyper_param_list.values()):
                args['HyperParams'] = dict(zip(param_names, param_values))
                args['ResultFile'] = config.get('ResultFileInner',
                    OuterIndex=outer_index,
                    InnerIndex=inner_index,
                    ParamIndex=param_index)
                args_list.append(args)
                param_index += 1
        else:
            args['HyperParams'] = None
            args['ResultFile'] = config.get('ResultFileInner',
                    OuterIndex=outer_index,
                    InnerIndex=inner_index,
                    ParamIndex=0)
            args_list.append(args)
    parallelmap(inner_task, args_list, engine=parallel_engine)    

def run_outer(classifiers, eval_metrics, cv_index,
              n_jobs=1, parallel_engine='sge_array'):
    """Run all tasks of outer cross-validation, optionally in parallel
    """
    pass

def feature_selection_task(args):
    X = args['Matrix'][args['TrainIndex'], :]
    y = np.asarray(args['TrueLabels'][args['TrainIndex']], dtype='int')
    features = feature_select(X, y, method=args['Method'])
    makedir(os.path.dirname(args['FeatureFile']))
    save_hdf5(features, args['FeatureFile'], 'data')

def run_feature_selection(X, y, filter_metrics, cv_index,
                          n_jobs=1, parallel_engine='sge_array'):
    """Run all tasks of feature selection, optionally in parallel
    """
    arglist = []
    for filter_metric, outer_index in itertools.product(filter_metrics,
            range(cv_index['NumOuter'])):
        index = cv_index['Outer'][outer_index]
        arglist.append({'OuterIndex': outer_index,
                        'TrainIndex': index['TrainIndex'],
                        'TestIndex': index['TestIndex'],
                        'Method': filter_metric,
                        'Matrix': X,
                        'TrueLabels': y,
                        'FeatureFile': config.get('FeatureFileOuter',
                            OuterIndex=outer_index)})
    for filter_metric, outer_index, inner_index in itertools.product(filter_metrics,
            range(cv_index['NumOuter']),
            range(cv_index['NumInner'])):
        index = cv_index['Inner'][outer_index][inner_index]
        arglist.append({'OuterIndex': outer_index,
                        'TrainIndex': index['TrainIndex'],
                        'TestIndex': index['TestIndex'],
                        'Method': filter_metric,
                        'Matrix': X,
                        'TrueLabels': y,
                        'FeatureFile': config.get('FeatureFileInner',
                            OuterIndex=outer_index,
                            InnerIndex=inner_index)})
    parallelmap(feature_selection_task, arglist)

def create_cv_index(sample_indices, n_outer_folds=10, n_inner_folds=5, random_state=None):
    """Create indicies of training and test data for cross-validation
    Args:
        n_outer_folds: number of folds for final evaluation of the model performance.
        n_inner_folds: number of folds for optimization of hyper-parameters.
            If n_inner_folds is 0, only indices for outer CV will be generated.
        sample_indices: an integer or an array.
            If an array is given, the samples indices will be used.
            If an integer is given, a range [0, sample_indices) will be generated.
        random_state: pseudo-random number generator state used for shuffling.
    Returns:
        A nested object:
        ['NumOuter']: number of folds in outer CV
        ['NumInner']: number of folds in inner CV
        ['Outer'][outer_index]['TrainIndex']: a list of training samples in outer CV
        ['Outer'][outer_index]['Testindex']: a list of test samples in outer CV
        ['Inner'][outer_index][inner_index]['TrainIndex'],
        ['Inner'][outer_index][inner_index]['TestIndex']: a list of training samples in inner CV
    """
    samples = None
    if hasattr(sample_indices, '__getitem__'):
        samples = np.asarray(sample_indices)
    elif isinstance(sample_indices, int):
        samples = np.arange(sample_indices)
    else:
        raise ValueError('sample indices should be an integer or an array')

    from sklearn.cross_validation import KFold
    outer = []
    inner = []
    for outer_train, outer_test in KFold(len(samples), n_folds=n_outer_folds, shuffle=True):
        if n_inner_folds > 0:
            inner_outer = []
            for inner_train, inner_test in KFold(len(outer_train),
                                             n_folds=n_inner_folds, shuffle=True):
                inner_outer.append({'TrainIndex': list(samples[inner_train]),
                              'TestIndex': list(samples[inner_test])})
            inner.append(inner_outer)
        outer.append({'TrainIndex': list(samples[outer_train]),
                      'TestIndex': list(samples[outer_test])})
    cvindex = {'NumOuter': n_outer_folds, 'NumInner': n_inner_folds,
               'Inner': inner, 'Outer': outer}
    return cvindex

def create_job(filename, name=None, **kwargs):
    pass

_defered_import()
