cimport numpy as np
import numpy as np
import h5py
from fasta cimport FastaRecord, FastaReader

cdef class LSH:
    cdef int k, n_bits
    cdef np.ndarray w0, W, transtable

    cdef init_transtable(self)
    cdef np.ndarray encode_seq(self, const char* seq, size_t seqlen)
    cdef np.ndarray encode_seqs(self, size_t n_seqs,
            const char** seqs, size_t* seqlens,
            int canonical)
    cdef np.ndarray hash_vec(self, np.ndarray[np.complex128_t, ndim=2] X)
