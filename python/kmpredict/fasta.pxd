from libcpp.string cimport string

cdef extern from 'Fasta.h' nogil:
    cdef cppclass FastaRecord:
        string name
        string seq

    cdef cppclass FastaReader:
        FastaReader(string& filename) except +
        size_t GetFilePos()
        size_t GetLineNum()
        FastaRecord* ReadNext()
        void Close()
