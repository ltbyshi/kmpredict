cimport fasta
from libc cimport stdio, stdlib
from stdio_unlocked import fgetc_unlocked, feof_unlocked
cimport libc.string
from utils cimport malloc_s, free_s

cdef inline size_t next_line(const char* s, size_t pos):
    """return position after the next newline
    """
    while s[pos] != '\n':
        pos += 1
    pos += 1
    return pos

cdef class FastaReader:
    cdef FILE* fp
    cdef const char* filename
    cdef char* buffer
    cdef char* last_kmer
    cdef int* is_nuc
    cdef size_t bufsize
    cdef int seq_start
    def __cinit__(self, const char* filename, Py_ssize_t bufsize=4*1024*1024):
        self.buffer = NULL
        self.fp = stdio.fopen(filename, 'rt')
        if self.fp == NULL:
            raise IOError()
        self.buffer = <char*>malloc_s(bufsize)
        stdio.setvbuf(fp, self.buffer, stdio._IOFBF, self.bufsize)
        self.bufsize = bufsize
        self.prev_kmer = <char*>malloc_s(k + 1)
        self.is_nuc = <int*>malloc_s(sizeof(int)*256)

    def __dealloc__(self):
        free_s(self.buffer)
        free_s(self.prev_kmer)
        free_s(self.is_nuc)
        if self.fp != NULL:
            stdio.fclose(self.fp)

    cdef void init_is_nuc(self):
        cdef int i
        cdef char nuc_chars[5] = ['A', 'T', 'G', 'C', 'U']
        libc.string.memset(self.is_nuc, 0, sizeof(int)*256)
        for i in range(5):
            self.is_nuc[nuc_chars[i]] = 1

    def close(self):
        stdio.fclose(self.fp)
        stdio.fp = NULL

    cdef Py_ssize_t get_kmers(self, Py_ssize_t k, char** kmers, Py_ssize_t n_kmers):
        cdef size_t n_read
        cdef Py_ssize_t i_kmer = 0
        cdef Py_ssize_t i
        cdef int c
        while True:
            if feof_unlocked(self.fp):
                break
            c = fgetc_unlocked(self.fp)
            if c == stdio.EOF:
                break
            if i_kmer >= n_kmers:
                break
            # skip header
            if c == '>':
                c = fgetc_unlocked(self.fp)
                while c != stdio.EOF:
                    if c == '\n':
                        break
                    c = fgetc_unlocked(self.fp)
                c = fgetc_unlocked(self.fp)
                seq_start = 1
            if seq_start:
                # read a whole k-mer
                for i in range(k):
                    c = fgetc_unlocked(self.fp)
                    if c == stdio.EOF:
                        break
                    if self.is_nuc[c]:
                        kmers[i_kmer][i] = c
                i_kmer += 1
            else:
                for i in range(k - 1):
                    kmers[i_kmer][i] = kmers[i_kmer - 1][i - 1]
                c = fgetc_unlocked(self.fp)
                if c == stdio.EOF:
                    break
                if self.is_nuc[c]:
                    kmers[i_kmer][k - 1] = c
