#! /usr/bin/env python
from kmpredict.kmercounter import KmerCounter
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('seqfile', type=str)
    args = parser.parse_args()

    k = 32
    counter = KmerCounter(0, k)
    counter.LSHCountFasta(args.seqfile, 28, 10000)
    counter.Save('kmer_counts.h5')
