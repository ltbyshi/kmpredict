#! /usr/bin/env python
from kmpredict.utils import random_sequence, choose_canonical, get_reverse_complement, \
    get_canonical_kmers
from kmpredict.kmercounter import FastaReader
import argparse
def test_canonical():
    for i in xrange(50):
        seq = random_sequence(20)
        rc_seq = get_reverse_complement(seq)
        choice = choose_canonical(seq)
        if (choice == 0) and (seq <= rc_seq):
            correct = '+'
        elif (choice == 1) and (seq > rc_seq):
            correct = '+'
        else:
            correct = '-'
        print '%s - %s: %d %s'%(seq, rc_seq, choice, correct)

def test_canonical_file(seqfile):
    reader = FastaReader(seqfile)
    for seq in reader:
        get_canonical_kmers(seq, 32)
    reader.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    group_action = parser.add_mutually_exclusive_group(required=True)
    group_action.add_argument('--canonical-file', type=str, required=False,
        help='get all canonical k-mers in the sequences in a fasta file')
    group_action.add_argument('--canonical', action='store_true', required=False,
        help='test the choose_canonical function')
    args = parser.parse_args()

    if args.canonical:
        test_canonical()
    elif args.canonical_file:
        test_canonical_file(args.canonical_file)
    else:
        raise ValueError('no action selected')
