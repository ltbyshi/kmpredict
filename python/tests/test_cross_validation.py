#! /usr/bin/env python
import argparse

if __name__ == '__main__':
    commands = ['create_cv_index', 'text_to_hdf5', 'feature_select', 'cv_inner', 'cv_outer', 'generate_data']
    parser = argparse.ArgumentParser(description='A general pipeline for cross-validation')
    parser.add_argument('command', type=str, choices=commands,
        help='a command to run')
    parser.add_argument('-n', '--nsamples', type=int, required=False,
        help='number of samples')
    parser.add_argument('--nfeatures', type=int, required=False,
        help='number of features')
    parser.add_argument('-nfolds-outer', type=int, required=False,
        help='number of folds in outer cross-validation')
    parser.add_argument('-nfolds-inner', type=int, required=False,
        help='number of folds in inner cross-validation')
    parser.add_argument('--cvindex', type=str, required=False,
        help='JSON file that provides indices of training and test samples')
    parser.add_argument('--text-matrix-file', type=str, required=False,
        help='data matrix file in text format')
    parser.add_argument('--matrix-file', type=str, required=False,
        help='data matrix file in HDF5 format')
    parser.add_argument('--labels-file', type=str, required=False,
        help='text file that provides true class labels (one per line)')
    parser.add_argument('--filter-metrics', type=str, required=False,
        help='a comma-separated list of metrics for feature selection')
    parser.add_argument('--classifiers', type=str, required=False,
        help='a comma-separated list of classifier names')
    parser.add_argument('--eval-metric', type=str, required=False,
        help='the evaluation metric for optimizing hyper-parameters')
    parser.add_argument('-j', '--njobs', type=int, required=False,
        default=1, help='number of jobs to be run in parallel')
    parser.add_argument('--parallel-engine', type=str, required=False,
        default='sge_array', help='the type of parallel engine to use')
    parser.add_argument('--rootdir', type=str, required=False,
        help='the root directory to store all output files')
    parser.add_argument('--all-features', action='store_true', required=False,
        help='use all features for cross-validation')
    parser.add_argument('--random-seed', type=int, required=False,
        help='specify random seed for generating cvindex')
    args = parser.parse_args()

    from kmpredict import cross_validation
    from kmpredict.utils import save_json, load_json, save_hdf5, load_hdf5, require_options

    if args.rootdir is not None:
        cross_validation.config['RootDir'] = args.rootdir
    # process commands
    if args.command == 'create_cv_index':
        require_options(args.command, args, ['--cvindex', '--nsamples'])
        cvindex = cross_validation.create_cv_index(args.nsamples)
        save_json(cvindex, 'cvindex.json')
    elif args.command == 'text_to_hdf5':
        require_options(args.command, args, ['--text-matrix', '--matrix-file'])
        import pandas as pd
        X = pd.read_table(args.text_matrix)
        save_hdf5(X, args.matrix_file, 'data')
    elif args.command == 'feature_select':
        require_options(args.command, args, ['--matrix-file', '--labels-file', '--cvindex', '--filter-metrics'])
        import pandas as pd
        import numpy as np
        cv_index = load_json(args.cvindex)
        X = load_hdf5(args.matrix_file, 'data')
        y = np.loadtxt(args.labels_file, dtype='int')
        #y = pd.read_table(args.labels_file, header=None)[0]
        filter_metrics = args.filter_metrics.split(',')
        cross_validation.run_feature_selection(X, y,
            filter_metrics=filter_metrics,
            cv_index=cv_index,
            n_jobs=args.njobs,
            parallel_engine=args.parallel_engine)
    elif args.command == 'cv_inner':
        require_options(args.command, args, ['--cvindex', '--matrix-file', '--labels-file'])
        cv_index = load_json(args.cvindex)
        filter_metrics = args.filter_metrics.split(',')
        classifiers = args.classifiers.split(',')
        eval_metric = args.eval_metric
    elif args.command == 'cv_outer':
        require_options(args.command, args, ['--matrix-file', '--cvindex'])
        cv_index = load_json(args.cvindex)
    elif args.command == 'generate_data':
        require_options(args.command, args, ['--nsamples', '--nfeatures', '--matrix-file', '--labels-file'])
        from sklearn.datasets import make_classification
        import numpy as np
        X, y = make_classification(args.nsamples, n_features=args.nfeatures)
        save_hdf5(X, args.matrix_file, 'data')
        np.savetxt(args.labels_file, y, fmt='%d')
        #save_hdf5(y, args.labels_file, 'data')
    else:
        raise ValueError('unknown command')
