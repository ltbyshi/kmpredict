#include <vector>
#include <map>
#include <fstream>
#include <iostream>
using namespace std;
#include "Kmer.h"
#include "Utils.h"
#include "Thread.h"
#include "KmerFilter.h"

#define MULTITHREAD 1
#define VERBOSE 0

void ReadGroupFile(const string& groupfile, vector<string>& groupmap,
                   vector<int>& groups, vector<string>& kmerfiles)
{
    ifstream fin(groupfile);
    if(!fin)
        IOError("cannot open the input file " + groupfile);
    int lineno = 1;
    while(!fin.eof())
    {
        string line;
        getline(fin, line);
        // skip empty lines
        if(line.size() < 1)
        {
            lineno ++;
            continue;
        }
        istringstream is(line);
        string groupname, kmerfile;
        is >> groupname >> kmerfile;
        if(!is)
            Die("invalid at line %d in groupfile %s", lineno, groupfile);
        
        kmerfiles.push_back(kmerfile);
        // find group id
        size_t groupid;
        for(groupid = 0; groupid < groupmap.size(); groupid ++)
        {
            if(groupmap[groupid] == groupname)
                break;
        }
        // create a new group ID
        if(groupid == groupmap.size())
        {
            groupmap.push_back(groupname);
            groups.push_back(groupid);
        }
        else
        {
            groups.push_back(groupid);
        }
        lineno ++;
    }
    fin.close();
}

void ReadGroupInfo(const string& filename, 
                   vector<string>& groupnames, vector<int>& nsamples_pg)
{
    groupnames.resize(0);
    nsamples_pg.resize(0);
    ifstream fin(filename);
    while(!fin.eof())
    {
        string line;
        string groupname;
        int n;
        getline(fin, line);
        istringstream is(line);
        is >> groupname >> n;
        groupnames.push_back(groupname);
        nsamples_pg.push_back(n);
    } 
    fin.close();
}

class CountSamplesPerKmerJob: public Job
{
public:
    CountSamplesPerKmerJob(vector<unsigned char>& nkmer_per_group)
    : nkmer_per_group(nkmer_per_group)
    {}
    virtual ~CountSamplesPerKmerJob() {}
    virtual int Start()
    {
        cerr << "CountSamplePerKmerJob [" << jobid << "] group=" << group << endl;
        DefaultKmerFilter counter;
        counter.Load(kmerfile.c_str());
        for(size_t kmer = startpoint, index = startpoint*ngroups + group;
            kmer < hashsize;
            kmer ++, index += ngroups)
        {
            if(counter.Get(kmer))
                __atomic_add_fetch(&(nkmer_per_group[index]), 1, __ATOMIC_SEQ_CST);
        }
        for(size_t kmer = 0, index = 0 + group;
            kmer < startpoint;
            kmer ++, index += ngroups)
        {
            if(counter.Get(kmer))
                __atomic_add_fetch(&(nkmer_per_group[index]), 1, __ATOMIC_SEQ_CST);
        }
          
        //for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
        //    (*nkmer_per_group)[kmer] = counter.Get(kmer);
        return 0;
    }
public:
    // input params
    int jobid;
    int group;
    size_t ngroups;
    size_t startpoint;
    string kmerfile;
    DefaultKmerType hashsize;
    // output
    vector<unsigned char>& nkmer_per_group;
};

void ReadKmerFiles(vector<int>& groups, int ngroups,
                   vector<string>& kmerfiles, 
                   DefaultKmerType hashsize, 
                   int nsamples,
                   vector<unsigned char>& nkmer_per_group)
{
#ifdef MULTITHREAD
    // multi-threading version for counting the number of samples
    const int nthreads = 16;
    ThreadPool pool(nthreads);
    vector<Job*> jobs(nsamples);
    for(int i = 0; i < nsamples; i ++)
    {
        CountSamplesPerKmerJob* job = new CountSamplesPerKmerJob(nkmer_per_group);
        job->jobid = i;
        job->group = groups[i];
        job->ngroups = ngroups;
        job->kmerfile = kmerfiles[i];
        job->hashsize = hashsize;
        //job->nkmer_per_group = &(nkmer_per_group[groups[i]]);
        job->startpoint = hashsize / nsamples * i;
        jobs[i] = (Job*)job;
        pool.Submit(job);
    }
    pool.Wait();
    for(int i = 0; i < nsamples; i ++)
        delete jobs[i];
    pool.Shutdown();
    pool.Join();
#else
    DefaultKmerFilter* counter = NULL;
    for(size_t i = 0; i < nsamples; i ++)
    {
        int group = groups[i];
        counter = new DefaultKmerFilter;
        counter->Load(kmerfiles[i].c_str());
        for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
            nkmer_per_group[group][kmer] += counter->Get(kmer);
        // counters[group].Merge(*counter);
        delete counter;
    }
    counter = NULL;
#endif
}

map<string, int> ReadCoverage(const string& fileName, char delim)
{
    map<string, int> coverage;
    ifstream fin(fileName);
    if(!fin)
        Die("cannot open the file: %s", fileName);
    string line;
    unsigned int lineno = 1;
    while(getline(fin, line))
    {
        istringstream is(line);
        string name;
        int value;
        is >> name >> value;
        if(!is)
        {
            if((is.rdstate() & ifstream::failbit) != 0)
                Die("invalid format at line %d in file %s", lineno, fileName);
            if((is.rdstate() & ifstream::eofbit) != 0)
                Die("not enough columns at line %d in file %s", lineno, fileName);
            if((is.rdstate() & ifstream::badbit) != 0)
                Die("errors happen at line %d in file %s", lineno, fileName);
        }
        coverage[name] = value;
    }
    fin.close();
    return coverage;
}


