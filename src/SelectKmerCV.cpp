#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <iomanip>
#include <stdexcept>
using namespace std;
#include "omp.h"
//#include <boost/filesystem.hpp>
//using boost::filesystem::create_directories;
//using boost::filesystem::path;


#define PROGNAME "SelectKmerCV"
// C library
#include <stdlib.h>
#include <math.h>
// rapidjson's DOM-style APIs
#include <rapidjson/document.h>     
using rapidjson::Document;
using rapidjson::Value;
using rapidjson::SizeType;
// user library
#include "Fasta.h"
#include "KmerFilter.h"
#include "Utils.h"
#include "Thread.h"
#include "Logging.h"
#include "HDF5Utils.h"
#include "ProgressBar.h"
#include "FeatureSelection.h"

class KmerMatrix
{
public:
    typedef float ValueType; // data type of matrix elements
    KmerMatrix(const string& kmermat_file, size_t nsamples)
    : kmermat_file(kmermat_file), nsamples(nsamples)
    {}
    

    void ReadKmerList(const string& kmerlist_file)
    {
        ReadVectorFromHDF5<DefaultKmerType>(kmerlist_file, "data", kmerlist);
        nkmers = kmerlist.size();
        m.resize(nkmers*nsamples);
    }
    // Normalize the k-mer counts in a sample by a factor avgcov and fill the k-mer matrix
    // only k-mers given in the k-mer list file will be included in the matrix
    // select all k-mers if the kmer list is empty
    // sample_index: index of the sample of the k-mer counts to read
    void SelectKmer(const vector<DefaultValType>& counts, int sample_index, ValueType avgcov)
    {
        // select all k-mers if the kmer list is empty
        if(kmerlist.size() == 0)
        {
            if(m.size() == 0)
            {
                nkmers = counts.size();
                m.resize(nkmers*nsamples);
            }
            for(size_t ki = 0, mi = sample_index;
                ki < counts.size();
                ki ++, mi += nsamples)
                m[mi] = ValueType(counts[ki])/avgcov;
        }
        else
        {
            for(size_t li = 0, mi = sample_index;
                li < kmerlist.size();
                li ++, mi += nsamples)
                m[mi] = ValueType(counts[kmerlist[li]])/avgcov;
        }
    }
    // scale the whole matrix by the maximum element
    void ScaleByMax()
    {
        ValueType maxval = m[0];
        for(size_t i = 0; i < m.size(); i ++)
        {
            if(m[i] > maxval)
                maxval = m[i];
        }
        ValueType scale_factor = 1.0/maxval;
        for(size_t i = 0; i < m.size(); i ++)
            m[i] *= scale_factor;
    }
    
    void WriteMatrix()
    {
        ScaleByMax();
        MakeDirs(DirName(kmermat_file));
        logger << "WriteKmerMatrix: " << kmermat_file << Endl;
        WriteMatrixToHDF5<ValueType>(kmermat_file, "data", nkmers, nsamples, m);
    }

private:
    string kmermat_file;
    size_t nsamples;
    size_t nkmers;
    
    vector<DefaultKmerType> kmerlist;
    vector<ValueType> m; // nsamples x nkmers
};

int CreateKmerMatrix(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Create k-mer matrices\n" 
             << "Read indices from k-mer list files and extract counts from k-mer counts files"<< endl;
        cerr << "Usage: " << PROGNAME << " mat param_file" << endl;
        cerr << "Optional environment variables: pe_task_id, pe_task_num, estimate_memory" << endl;
        exit(1);
    }
    string param_file(argv[1]);
    
    Document p;
    ReadJSON(param_file, p);
   
    // estimate memory usage (in GB) and exit
    const size_t min_memory = 1;
    int estimate_memory = false;
    GetEnv<int>("estimate_memory", estimate_memory);
    if(estimate_memory)
    {
        size_t mem = 0;
        vector<size_t> dims_counts = GetHDF5DatasetDims(p["SampleInfo"][0]["KmerCountFile"].GetString(), "data");
        mem += sizeof(DefaultValType)*dims_counts[0]; // size of k-mer counts
        if(p["Params"][0].HasMember("KmerListFile"))
        {
            vector<size_t> dims_kmerlist = GetHDF5DatasetDims(p["Params"][0]["KmerListFile"].GetString(), "data");
            mem += dims_kmerlist[0]*sizeof(float)*p["SampleInfo"].Size()*p["Params"].Size(); // size of k-mer matrix
            mem += dims_kmerlist[0]*sizeof(size_t)*p["Params"].Size(); // size of k-mer list
        }
        else
        {
            mem += dims_counts[0]*sizeof(float)*p["SampleInfo"].Size()*p["Params"].Size();
        }
        if(mem % (1UL << 30) > 0)
            mem = mem / (1UL << 30) + 1;
        else
            mem /= (1UL << 30);
        mem += min_memory;
        cout << mem << endl;
        return 0;
    }
    // estimate number of tasks given maximum memory (in GB)
    size_t max_memory = 0;
    int estimate_task_num = false;
    GetEnv<int>("estimate_task_num", estimate_task_num);
    if(estimate_task_num)
    {
        if(!GetEnv<size_t>("max_memory", max_memory))
            throw ValueError("Environment variable $max_memory is not found for estimation of task number");
        vector<size_t> dims_counts = GetHDF5DatasetDims(p["SampleInfo"][0]["KmerCountFile"].GetString(), "data");
        size_t a0 = sizeof(DefaultValType)*dims_counts[0];
        size_t a1 = 0;
        if(p["Params"][0].HasMember("KmerListFile"))
        {
            vector<size_t> dims_kmerlist = GetHDF5DatasetDims(p["Params"][0]["KmerListFile"].GetString(), "data");
            a1 += dims_kmerlist[0]*sizeof(float)*p["SampleInfo"].Size();
            a1 += dims_kmerlist[0]*sizeof(size_t);
        }
        else
        {
            a1 += dims_counts[0]*sizeof(float)*p["SampleInfo"].Size();
        }
        // memory required even no memory allocation is performed
        size_t n = p["Params"].Size();
        max_memory *= (1UL << 30);
        size_t task_num;
        for(task_num = 1; task_num <= n; task_num ++)
        {
            size_t task_size = n / task_num;
            if(n % task_num > 0)
                task_size ++;
            size_t mem = min_memory + a0 + a1*task_size;
            if(mem <= max_memory)
                break;
        }
        cout << task_num << endl;
        return 0;
    }
    // get parallel task parameters 
    int task_id = 0, task_num = 1;
    bool enable_pe = GetPETaskParams(task_id, task_num);

    ProcessStatus ps;

    const Value& sample_info = p["SampleInfo"];
    // read kmer list
    logger.Info() << "Read kmer lists" << Endl;
    vector<KmerMatrix*> matrices;
    const Value& params = p["Params"];
    for(SizeType i = 0; i < params.Size(); i ++)
    {
        // get PE tasks
        if(enable_pe && ((int(i) % task_num) != task_id))
            continue;
        KmerMatrix* mat = new KmerMatrix(params[i]["KmerMatrixFile"].GetString(),
                                                 sample_info.Size());
        if(params[i].HasMember("KmerListFile"))
        {
            string kmerlist_file = params[i]["KmerListFile"].GetString();
            mat->ReadKmerList(kmerlist_file);
            logger.Debug() << "ReadKmerList: " << kmerlist_file
                << ", virtual memory: " 
                << double(ps.Update().vsize)/double(1UL << 30) << " GB" << Endl;
        }
        else
            logger.Debug() << "SelectAllKmer: " << params[i]["KmerMatrixFile"].GetString() << Endl;
        matrices.push_back(mat);
    }
    // read average coverage
    const Value& average_coverage = p["AverageCoverage"];
    vector<float> avgcov(average_coverage.Size());
    for(SizeType i = 0; i < average_coverage.Size(); i ++)
        avgcov[i] = average_coverage[i].GetDouble();
    // read kmer counts
    vector<DefaultValType> counts;
    for(SizeType si = 0; si < sample_info.Size(); si ++)
    {
        string kmercounts_file = sample_info[si]["KmerCountFile"].GetString();
        logger.Info() << "Read kmer counts " << kmercounts_file << Endl;
        ReadVectorFromHDF5<DefaultValType>(kmercounts_file, "data", counts);
        for(size_t mi = 0; mi < matrices.size(); mi ++)
            matrices[mi]->SelectKmer(counts, si, avgcov[si]);
    }
    counts.resize(0);
 
    logger.Info() << "Write kmer matrices" << endl;
    // write matrix
    for(size_t i = 0; i < matrices.size(); i ++)
        matrices[i]->WriteMatrix();

    return 0;
}

int KmerMatrixToMRMR(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Convert the k-mer matrix from HDF5 format to CSV format for mRMR" << endl;
        cerr << "Usage: " << PROGNAME << " mat_mrmr param_file" << endl;
        cerr << "Optional environment variables: task_id, n_tasks" << endl;
        exit(1);
    }
    string param_file(argv[1]);
    
    Document p;
    ReadJSON(param_file, p);
    const Value& sample_info = p["SampleInfo"];
    const Value& params = p["Params"];

    size_t nsamples = sample_info.Size();
    vector<int> groups(nsamples);
    for(SizeType i = 0; i < sample_info.Size(); i ++)
        groups[i] = sample_info[i]["Group"].GetInt();

    // detect parallel settings
    int task_id = 0, n_tasks = 1;
    bool enable_parallel = GetPETaskParams(task_id, n_tasks);
    
    for(SizeType i = 0; i < params.Size(); i ++)
    {
        // select parallel tasks
        if(enable_parallel)
        {
            if((int(i) % n_tasks) != task_id)
                continue;
        }
        vector<float> m;
        // nrow = nkmers, ncol = nsamples
        hsize_t nrow, ncol;
        logger.Info() << "Read k-mer matrix " << params[i]["KmerMatrixFile"].GetString() << Endl;
        ReadMatrixFromHDF5<float>(params[i]["KmerMatrixFile"].GetString(),
                                  "data", nrow, ncol, m);
        const Value& p_train_index = params[i]["TrainIndex"];
        vector<int> train_index(p_train_index.Size());
        for(SizeType si = 0; si < p_train_index.Size(); si ++)
            train_index[si] = p_train_index[si].GetInt();
        // write to CSV file
        MakeDirs(DirName(params[i]["MRMRInputFile"].GetString()));
        FileOutputStream fout(params[i]["MRMRInputFile"].GetString());
        fout << "class";
        for(size_t ki = 0; ki < nrow; ki ++)
            fout << ",v" << ki;
        fout << endl;
        for(size_t si = 0; si < train_index.size(); si ++)
        {
            size_t sample = train_index[si];
            fout << groups[sample];
            for(size_t ki = 0; ki < nrow; ki ++)
                fout << ',' << m[ki*ncol + sample];
            fout << endl;
        }
        fout.close();
    }
    return 0;
}

class SelectKmerCV
{
public:
    SelectKmerCV();
    ~SelectKmerCV();
    int ParseArgs(int argc, char** argv);
    void ReadParamFile(const string& filename);
    void ReadKmerFilter();
    //void CreateJobs();
    //void RunJobs();
    void Contour();
    void BinCount();
    void DumpKmers();
    void DumpKmerRelevance();
    void DumpKmerRelevance(const string& filter_metric,
                           const vector<int>& samples,
                           const vector<DefaultKmerType>& selected);
    template <int FilterMetric>
    void DumpKmerRelevance(const vector<int>& samples,
                           const vector<DefaultKmerType>& selected);
    // first filter the kmers by the frequency among samples
    void FilterByFreq();
    void FilterByRelevance();
    void FeatureRobustness();
    template <int FilterMetric>
    void CalculateRelevance(const vector<int>& samples,
                            vector<float>& relevances);
    void CalculateRelevance(const string& filter_metric, 
                            const vector<int>& samples,
                            vector<float>& relevances);
    // from raw counts
    template <int FilterMetric>
    void CalculateRelevanceFromCounts(const vector<int>& samples, vector<float>& relevances);
    void CalculateRelevanceFromCounts(const string& filter_metric,
                                      const vector<int>& samples,
                                      vector<float>& relevances);
    void FilterByRelevance(const vector<float>& relevances,
                           vector<DefaultKmerType>& selected,
                           size_t kmer_rank_min,
                           size_t kmer_rank_max);
protected:
    const double oc_ratio_thresh;
    const static int pseudocount = 1;
    const static int ngroups = 2;
    int nsamples;
    map<int, int> groupname_to_index;
    vector<int> group_index;
    Document p;
    DefaultKmerType hashsize;
    vector<DefaultKmerFilter> kmerfilters;
    vector<DefaultKmerType> kmers;
    vector<Job*> select_kmer_jobs;
};

SelectKmerCV::SelectKmerCV()
: oc_ratio_thresh(0.10)
{
}

SelectKmerCV::~SelectKmerCV()
{
}

void SelectKmerCV::ReadKmerFilter()
{
    kmerfilters.resize(nsamples);
    logger.Info() << "Loading kmer filter files" << Endl;
    ProgressBar progressbar(nsamples);
    const Value& sample_info = p["SampleInfo"];
    for(SizeType i = 0; i < sample_info.Size(); i ++)
    {
        kmerfilters[i].Load(sample_info[i]["KmerFilterFile"].GetString()); 
        progressbar.Increment(1);
    }
    progressbar.Finish();
    hashsize = kmerfilters[0].GetHashSize();
    logger.Info() << "HashSize: " << hashsize << Endl;
}

void SelectKmerCV::ReadParamFile(const string& filename)
{
    ReadJSON(filename, p);
    // read sample info
    const Value& sample_info = p["SampleInfo"];
    nsamples = sample_info.Size();
    group_index.resize(nsamples);
    for(SizeType i = 0; i < sample_info.Size(); i ++)
    {
        int group = sample_info[i]["Group"].GetInt();
        if(groupname_to_index.find(group) == groupname_to_index.end())
        {
            groupname_to_index[group] = groupname_to_index.size() - 1;
        }
        group_index[i] = groupname_to_index[group];
    }
    if(groupname_to_index.size() != ngroups)
        throw std::logic_error("Number of groups differnent from 2");
    logger.Info() << "Groups: ";
    for(size_t i = 0; i < group_index.size(); i ++)
        logger << group_index[i] << " ";
    logger << Endl;
    logger.Info() << "Number of samples: " << nsamples << Endl;
}

int SelectKmerCV::ParseArgs(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Select feature kmers" << endl;
        cerr << "Usage: " << PROGNAME << " " << argv[0] << " param_file" << endl; 
        cerr << "\tprefix\tstring prepended to the output filenames" << endl;
        exit(1);
    }
    string command(argv[0]);
    string param_file(argv[1]);
    ReadParamFile(param_file);
    
    if(command == "select")
    {
        ReadKmerFilter();
        string kmerlist_freq_file = p["KmerListFilteredByFreq"].GetString();
        if(FileExists(kmerlist_freq_file))
        {
            logger.Info() << "Read cache: " << kmerlist_freq_file << Endl;
            ReadVectorFromHDF5<DefaultKmerType>(kmerlist_freq_file, "data", kmers);
        }
        else
        {
            FilterByFreq();
            logger.Info() << "Write cache: " << kmerlist_freq_file << Endl;
            MakeDirs(DirName(kmerlist_freq_file));
            WriteVectorToHDF5<DefaultKmerType>(kmerlist_freq_file, "data", kmers);
        }
        
        FilterByRelevance();
    }
    else if(command == "contour")
    {
        ReadKmerFilter();
        Contour(); 
    }
    else if(command == "dumpkmers")
    {
        ReadKmerFilter();
        DumpKmerRelevance();
    }
    else if(command == "bincount")
    {
        ReadKmerFilter();
        BinCount();
    }
    else
    {
        throw logic_error("Invalid command");
    }
    
    return 0;
}

void SelectKmerCV::Contour()
{
    logger.Info() << "Creating contour" << Endl;
    vector<int> nsamples_by_group(ngroups);
    for(int i = 0; i < nsamples; i ++)
        nsamples_by_group[group_index[i]] ++;
    vector<int> contour(nsamples_by_group[0]*nsamples_by_group[1]);
    vector<int> oc_by_group(ngroups);
    for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
    {
        oc_by_group.assign(ngroups, 0);
        for(int si = 0; si < nsamples; si ++)
        {
            if(kmerfilters[si][kmer])
                oc_by_group[group_index[si]] ++;
        }
        contour[nsamples_by_group[1]*oc_by_group[0] + oc_by_group[1]] ++;
    }
    WriteMatrixToHDF5<int>(p["ContourFile"].GetString(),
                           "data",
                           nsamples_by_group[0],
                           nsamples_by_group[1],
                           contour);
}

void SelectKmerCV::BinCount()
{
    logger.Info() << "BinCount" << Endl;
    vector<DefaultKmerType> bincounts(nsamples);
    for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
    {
        DefaultKmerType counts = 0;
        for(int si = 0; si < nsamples; si ++)
            counts += kmerfilters[si][kmer];
        bincounts[counts] ++;
    }
    WriteVectorToHDF5<DefaultKmerType>(p["BinCountFile"].GetString(),
            "data", bincounts);
}

void SelectKmerCV::FilterByFreq()
{
    
    int oc_thresh = nsamples*oc_ratio_thresh;
    logger.Info() << "Filter by frequency " << oc_thresh 
        << "(" << oc_ratio_thresh << ")" << Endl;
    kmers.resize(0);
    vector<int> nsamples_by_group(ngroups);
    for(int i = 0; i < nsamples; i ++)
        nsamples_by_group[group_index[i]] ++;
    
    const int ntasks = 100;
    DefaultKmerType tasksize = hashsize/ntasks;
    ProgressBar progressbar(ntasks);
    #pragma omp parallel for schedule(static, 1)
    for(int taskid = 0; taskid < ntasks; taskid ++)
    {
        DefaultKmerType kmer_start = tasksize*taskid;
        DefaultKmerType kmer_end = std::min(kmer_start + tasksize, hashsize);
        vector<DefaultKmerType> kmers_task;
        for(DefaultKmerType kmer = kmer_start; kmer < kmer_end; kmer ++)
        {
            int oc = 0;
            for(int si = 0; si < nsamples; si ++)
            {
                if(kmerfilters[si][kmer])
                    oc ++;
            }
            if(oc >= oc_thresh)
                kmers_task.push_back(kmer);
        }
        #pragma omp critical
        {
            kmers.insert(kmers.end(), kmers_task.begin(), kmers_task.end());
            progressbar.Increment(1);
        }
    }
    progressbar.Finish();
    logger.Info() << kmers.size() << " kmers selected" << Endl;
}

void SelectKmerCV::DumpKmerRelevance()
{
    vector<DefaultKmerType> selected;
    string filename = p["KmerListFile"].GetString();
    string filter_metric = p["FilterMetric"].GetString();
    ReadVectorFromHDF5<DefaultKmerType>(filename, "data", selected);
    vector<int> samples(p["TrainIndex"].Size());
    for(size_t i = 0; i < samples.size(); i ++)
        samples[i] = p["TrainIndex"][i].GetInt();
    DumpKmerRelevance(filter_metric, samples, selected);
}

void SelectKmerCV::DumpKmerRelevance(const string& filter_metric,
                                     const vector<int>& samples,
                                     const vector<DefaultKmerType>& selected)
{
    if(filter_metric == "InfoGain")              DumpKmerRelevance<0>(samples, selected);
    else if(filter_metric == "MutualInfo")       DumpKmerRelevance<1>(samples, selected);
    else if(filter_metric == "Accuracy")         DumpKmerRelevance<2>(samples, selected);
    else if(filter_metric == "BalancedAccuracy") DumpKmerRelevance<3>(samples, selected);
    else if(filter_metric == "ProbabilityRatio") DumpKmerRelevance<4>(samples, selected);
    else if(filter_metric == "OddsRatio")        DumpKmerRelevance<5>(samples, selected);
    else if(filter_metric == "Frequency")        DumpKmerRelevance<6>(samples, selected);
    else if(filter_metric == "ChiSquared")       DumpKmerRelevance<7>(samples, selected);
}

template <int FilterMetric>
void SelectKmerCV::DumpKmerRelevance(const vector<int>& samples,
                                     const vector<DefaultKmerType>& selected)
{
    vector<int> oc_by_group(ngroups);
    vector<int> nsamples_by_group(ngroups);
    // count total number of samples in each group
    for(size_t si = 0; si < samples.size(); si ++)
        nsamples_by_group[group_index[samples[si]]] ++;
    
    for(size_t i = 0; i < selected.size(); i ++)
    {
        DefaultKmerType kmer = selected[i];
        oc_by_group.assign(ngroups, 0);
        // count values of the confusion table
        for(size_t si = 0; si < samples.size(); si ++)
        {
            int sample = samples[si];
            if(kmerfilters[sample][kmer])
                oc_by_group[group_index[sample]] ++;
        }
        float tp = oc_by_group[1] + pseudocount;
        float fp = oc_by_group[0] + pseudocount;
        float fn = nsamples_by_group[1] - oc_by_group[1] + pseudocount;
        float tn = nsamples_by_group[0] - oc_by_group[0] + pseudocount;
        // reverse class labels
        #if ENABLE_REVERSE_CORRELATION
        float tpr = tp / oc_by_group[1];
        float fpr = fp / oc_by_group[0];
        if(tpr < fpr)
        {
            std::swap(tp, fp);
            std::swap(fn, tn);
        }
        #endif
        float relevance = 0;
        switch(FilterMetric)
        {
            case 0: relevance = InfoGain(tp, tn, fp, fn); break;
            case 1: relevance = MutualInfo(tp, tn, fp, fn); break;
            case 2: relevance = Accuracy(tp, tn, fp, fn); break;
            case 3: relevance = BalancedAccuracy(tp, tn, fp, fn); break;
            case 4: relevance = ProbabilityRatio(tp, tn, fp, fn); break;
            case 5: relevance = OddsRatio(tp, tn, fp, fn); break;
            case 6: relevance = Frequency(tp, tn, fp, fn); break;
            case 7: relevance = ChiSquared(tp, tn, fp, fn); break;
            default: throw std::logic_error("Invalid filter metric");
        }
        {
            cout << std::setfill('0') << std::setw(8) << std::hex << std::right
            << std::setiosflags(std::ios::uppercase) << kmer
            << std::resetiosflags(std::ios::uppercase);
            cout << std::setfill(' ') << std::dec;
            cout << "  " << std::setw(3) << int(tp)
                 << "  " << std::setw(3) << int(fp)
                 << "  " << std::setw(3) << int(fn)
                 << "  " << std::setw(3) << int(tn);
            cout << "  " << std::fixed << std::setprecision(5) << relevance << endl;
        }
    }
}

template <int FilterMetric>
void SelectKmerCV::CalculateRelevance(const vector<int>& samples,
                                      vector<float>& relevances)
{
    // store the values of TP, FP, TN, FN in one integer
    vector<int> nsamples_by_group(ngroups);
    
    // count total number of samples in each group
    for(size_t si = 0; si < samples.size(); si ++)
        nsamples_by_group[group_index[samples[si]]] ++;
    
    vector<int> oc_by_group(ngroups);
    for(size_t i = 0; i < kmers.size(); i ++)
    {
        DefaultKmerType kmer = kmers[i];
        
        oc_by_group.assign(ngroups, 0);
        // count occurrence of kmers in each group
        for(size_t si = 0; si < samples.size(); si ++)
        {
            int sample = samples[si];
            if(kmerfilters[sample][kmer])
                oc_by_group[group_index[sample]] ++;
        }
        // calculate confusion table
        float tp = oc_by_group[1] + pseudocount;
        float fp = oc_by_group[0] + pseudocount;
        float fn = nsamples_by_group[1] - oc_by_group[1] + pseudocount;
        float tn = nsamples_by_group[0] - oc_by_group[0] + pseudocount;
        // reverse class labels
        #if ENABLE_REVERSE_CORRELATION
        float tpr = tp / oc_by_group[1];
        float fpr = fp / oc_by_group[0];
        if(tpr < fpr)
        {
            std::swap(tp, fp);
            std::swap(fn, tn);
        }
        #endif
        // calculate relevance index
        float relevance = 0;
        switch(FilterMetric)
        {
            case 0: relevance = InfoGain(tp, tn, fp, fn); break;
            case 1: relevance = MutualInfo(tp, tn, fp, fn); break;
            case 2: relevance = Accuracy(tp, tn, fp, fn); break;
            case 3: relevance = BalancedAccuracy(tp, tn, fp, fn); break;
            case 4: relevance = ProbabilityRatio(tp, tn, fp, fn); break;
            case 5: relevance = OddsRatio(tp, tn, fp, fn); break;
            case 6: relevance = Frequency(tp, tn, fp, fn); break;
            case 7: relevance = ChiSquared(tp, tn, fp, fn); break;
            default: throw std::logic_error("Invalid filter metric");
        }
        relevances[i] = relevance;        
    }
    
}

void SelectKmerCV::CalculateRelevance(const string& filter_metric, 
                                      const vector<int>& samples,
                                      vector<float>& relevances
                                     )
{
    if(filter_metric == "InfoGain")              CalculateRelevance<0>(samples, relevances);
    else if(filter_metric == "MutualInfo")       CalculateRelevance<1>(samples, relevances);
    else if(filter_metric == "Accuracy")         CalculateRelevance<2>(samples, relevances);
    else if(filter_metric == "BalancedAccuracy") CalculateRelevance<3>(samples, relevances);
    else if(filter_metric == "ProbabilityRatio") CalculateRelevance<4>(samples, relevances);
    else if(filter_metric == "OddsRatio")        CalculateRelevance<5>(samples, relevances);
    else if(filter_metric == "Frequency")        CalculateRelevance<6>(samples, relevances);
    else if(filter_metric == "ChiSquared")       CalculateRelevance<7>(samples, relevances);
}

void SelectKmerCV::FilterByRelevance(const vector<float>& relevances,
                                     vector<DefaultKmerType>& selected,
                                     size_t kmer_rank_min,
                                     size_t kmer_rank_max)
{
    // indices of all kmers
    vector<DefaultKmerType> indices(kmers.size());
    for(size_t i = 0; i < kmers.size(); i ++)
        indices[i] = i;
    
    // remove k-mers with duplicated signatures
    /*
    std::sort(indices.begin(), indices.end(),
              ArgGreater<vector<unsigned int>& >(signatures));
    vector<DefaultKmerType>::iterator it = std::unique(indices.begin(), indices.end(), 
              ArgEqual<vector<unsigned int>& >(signatures));
    indices.resize(std::distance(indices.begin(), it));
    */
    // sort by relevances
    std::sort(indices.begin(), indices.end(), 
              ArgGreater<vector<float> >(relevances));
    // select top k-mers
    size_t nselected = std::min(indices.size(),
                                kmer_rank_max - kmer_rank_min);
    selected.resize(nselected);
    for(size_t i = kmer_rank_min; i < kmer_rank_max; i ++)
        selected[i - kmer_rank_min] = kmers[indices[i]];
    /*
    size_t iselected = 0;
    unsigned int last_signature = 0;
    set<unsigned int> unique_signatures;
    // exclude kmers with duplicated confusion table
    for(size_t i = 0; i < indices.size(); i ++)
    {
        size_t ikmer = indices[i];
        if(signatures[ikmer]
        if(signatures[ikmer] != last_signature)
        {
            selected[iselected] = kmers[ikmer];
            last_signature = signatures[ikmer];
            iselected ++;
        }
        if(iselected >= nselected)
            break;
    }
    selected.resize(iselected);*/
    //progressbar.Finish();
}



void SelectKmerCV::FilterByRelevance()
{
    logger.Info() << "Filter kmers by relevance" << Endl;
    const Value& params = p["Params"];
    
    unsigned int randseed;
    if(p.HasMember("RandomSeed"))
        randseed = p["RandomSeed"].GetUint();
    else
        randseed = time(0);
    srand(randseed);
    logger.Info() << "Set random seed: " << randseed << Endl;
    
    int ntasks = params.Size();
    #if PROGRESS
    ProgressBar progressbar(ntasks);
    #endif
    #pragma omp parallel for schedule(static, 1)
    for(int i = 0; i < ntasks; i ++)
    {
        const Value& train_index = params[i]["TrainIndex"];
        vector<int> samples(train_index.Size());
        // count total number of samples in each group
        for(SizeType si = 0; si < train_index.Size(); si ++)
            samples[si] = train_index[si].GetInt();
        // output file name
        string kmer_list_file = params[i]["KmerListFile"].GetString();
        string filter_metric = params[i]["FilterMetric"].GetString();
        MakeDirs(DirName(kmer_list_file));
        // calculate relevance
        vector<float> relevances(kmers.size());
        int n_bootstrap = params[i]["Bootstrap"].GetInt();

        if(n_bootstrap > 0)
        {
            vector<int> bsamples(samples.size());
            for(int bi = 0; bi < n_bootstrap; bi ++)
            {
                // generator bootstrap samples
                for(size_t si = 0; si < samples.size(); si ++)
                    bsamples[si] = samples[rand() % samples.size()];
                vector<float> brelevances(kmers.size());
                CalculateRelevance(filter_metric, bsamples, brelevances);
                for(size_t ri = 0; ri < relevances.size(); ri ++)
                    relevances[ri] += brelevances[ri];
            }
        }
        else
        {
            CalculateRelevance(filter_metric, samples, relevances);
        }
#if _DEBUG
        logger.Info() << "Bootstrap: " << n_bootstrap
            << ", FilterMetric: " << filter_metric
            << ", Relevance: ";
        for(size_t ri = 0; ri < 20; ri ++)
            logger << relevances[ri] << " ";
        logger << Endl;
#endif
        // filter k-mers by relevance
        vector<DefaultKmerType> selected;
        int kmer_rank_min = params[i]["KmerRankMin"].GetInt();
        int kmer_rank_max = params[i]["KmerRankMax"].GetInt();
        FilterByRelevance(relevances, selected, 
                          kmer_rank_min, kmer_rank_max);
        #pragma omp critical
        {
            //logger.Info() << "Write kmer list: " << kmer_list_file << Endl;
            WriteVectorToHDF5<DefaultKmerType>(kmer_list_file, "data", selected);
            #if VERBOSE
            DumpKmerRelevance(filter_metric, samples, selected);
            #endif
            #if PROGRESS
            progressbar.Increment(1);
            #endif
        }
    }
    #if PROGRESS
    progressbar.Finish();
    #endif
}


int FeatureRobustness(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Evaluate robustness of kmer selection" << endl;
        cerr << "Usage: " << PROGNAME << " robust param_file" << endl;
        exit(1);
    }
    string param_file(argv[1]);
    Document p;
    ReadJSON(param_file, p);
    
    const Value& params = p["Params"];
    FileOutputStream fout(p["FeatureRobustnessFile"].GetString());
    for(size_t taskid = 0; taskid < params.Size(); taskid ++)
    {
        //logger.Info() << params[taskid]["Name"].GetString() << Endl;
        const Value& kmerlistfiles = params[taskid]["KmerListFiles"];
        size_t n_folds = kmerlistfiles.Size();
        vector<vector<DefaultKmerType> > kmerlists(n_folds);
        for(size_t fi = 0; fi < kmerlistfiles.Size(); fi ++)
        {
            vector<DefaultKmerType>& kmerlist = kmerlists[fi];
            ReadVectorFromHDF5<DefaultKmerType>(kmerlistfiles[fi].GetString(), "data", kmerlist);
            std::sort(kmerlist.begin(), kmerlist.end());
        }
        // pairwise distance
        double distance = 0;
        for(size_t i = 0; i < n_folds; i ++)
            for(size_t j = i + 1; j < n_folds; j ++)
                distance += Jaccard<vector<DefaultKmerType>, DefaultKmerType>(kmerlists[i], kmerlists[j]);
        distance = distance*2/(n_folds*(n_folds-1));
        fout << params[taskid]["FilterMetric"].GetString()
             << "\t" << params[taskid]["Name"].GetString()
             << "\t" << distance << "\n";
    }
    fout.close();
    return 0;
}

int SelectKmers(int argc, char** argv)
{
    SelectKmerCV pipeline;
    return pipeline.ParseArgs(argc, argv);
}

int main(int argc, char** argv)
{
    HandlerMap handlers;
    handlers["select"]  = SelectKmers;
    handlers["contour"] = SelectKmers;
    handlers["mat"] = CreateKmerMatrix;
    handlers["robust"] = FeatureRobustness;
    handlers["dumpkmers"] = SelectKmers;
    handlers["bincount"] = SelectKmers;
    handlers["mat_mrmr"] = KmerMatrixToMRMR;

    ParseArguments(handlers, PROGNAME, argc, argv);
    //FindFeatureKmers(argc, argv);
    
    return 0;
}
