#ifndef __STATISTICS_H__
#define __STATISTICS_H__
#include <algorithm>
#include <vector>
#include <string>
#include <iterator>
#include <math.h>
#include <assert.h>
#include <iostream>

template <typename Container>
inline void PrintArray(Container x, size_t n)
{
    for(size_t i = 0; i < n; i ++, ++x)
    {
        if(i > 0)
            std::cout << ", ";
        std::cout << *x;
    }
}

// Statistics

// ArgLess, ArgGreater, ArgEqual
// take two values from the associated array by index and compare 
template <typename Container>
struct ArgLess
{
    const Container data;
    ArgLess(const Container data): data(data) {}
    bool operator()(size_t i, size_t j) const {
        return (data[i] < data[j]);
    }
};

template <typename Container>
struct ArgGreater
{
    const Container data;
    ArgGreater(const Container data): data(data) {}
    bool operator()(size_t i, size_t j) const {
        return (data[i] > data[j]);
    }
};

template <typename Container>
struct ArgEqual
{
    const Container data;
    ArgEqual(const Container data): data(data) {}
    bool operator()(size_t i, size_t j) const {
        return (data[i] == data[j]);
    }
};

// sort the array data and store the indices in index
template <class DataIt, class IndexIt>
void ArgSortAscend(DataIt data, size_t size, IndexIt index)
{
    std::sort(index, index + size, ArgLess<DataIt>(data));
}

template <class DataIt, class IndexIt>
void ArgSortDecend(DataIt data, size_t size, IndexIt index)
{
    std::sort(index, index + size, ArgGreater<DataIt>(data));
}

template <typename Type>
Type Quantile(const std::vector<Type>& x, size_t start, size_t end, double p)
{
    std::vector<Type> a(x.begin() + start, 
                        x.begin() + end);
    std::sort(a.begin(), a.end(), std::less<Type>());
    return a[size_t(a.size()*p)];
}

void Factorial(int n, long double* f);

class FisherExactTest
{
public:
    FisherExactTest(int maxn)
    {
        _maxn = maxn;
        _f = new long double[maxn + 1];
        Factorial(maxn, _f);
    }

    ~FisherExactTest()
    {
        if(_f) delete[] _f;
    }

    // draw k balls from m white balls and n black balls
    // x is the number of white balls in k balls
    inline double Density(int x, int m, int n, int k) const
    {
        return (_f[m]*_f[n]*_f[k]*_f[m+n-k]/(_f[x]*_f[m-x]*_f[k-x]*_f[n+x-k]*_f[m+n]));
    }

    inline double TestGreater(int x, int m, int n, int k) const
    {
        double p = 0;
        for(int ix = x; ix <= k; ix ++)
            p += Density(ix, m, n, k);
        return p;
    }

private:
    int _maxn;
    long double* _f;
};

template <typename Type>
inline Type NormalizedEntropy(const std::vector<Type>& x)
{
    Type sum = 0;
    for(size_t i = 0; i < x.size(); i ++)
        sum += x[i];
    Type result = 0;
    for(size_t i = 0; i < x.size(); i ++)
    {
        Type xn = x[i]/sum;
        result += xn*log(xn);
    }
    result = 1.0 + result/log(x.size());
    return result;
}

template <typename Type>
inline Type Entropy(Type p)
{
    return -p*log(p) - (1 - p)*log(1 - p);
}

/*
template <typename Type, unsigned int N>
inline Type Entropy(Type x)
{
    Type result = 0;
    for(unsigned int i = 0; i < N; i ++)
        result += x[i]*log(x[i]);
    return (-result);
}
*/

template <typename Type>
inline Type NormalizedMutualInfo(Type p00, Type p01, Type p10, Type p11)
{
    Type px1 = p10 + p11;
    Type px0 = 1 - px1;
    Type py1 = p01 + p11;
    Type py0 = 1 - py1;
    Type mi = p00*log(p00/(px0*py0)) + p01*log(p01/(px0*py1)) 
        + p10*log(p10/(px1*py0)) + p11*log(p11/(px1*py1));
    mi = mi/sqrt((px0*log(px0) + px1*log(px1))
        *(py0*log(py0) + py1*log(py1)));
    return mi;
}

template <typename Type, unsigned int NValue, unsigned int NClass>
inline Type NormalizedMutualInfo(const std::vector<Type>& x)
{
    Type result = 0;
    Type pv[NValue] = {};
    Type pc[NClass] = {};
    for(unsigned int i = 0; i < NValue; i ++)
    {
        for(unsigned int j = 0; j < NClass; j ++)
        {
            pv[i] += x[j + NClass*i];
            pc[j] += x[j + NClass*i];
        }
    }
    for(unsigned int i = 0; i < NValue; i ++)
        for(unsigned int j = 0; j < NClass; j ++)
            result += x[j + NClass*i]*log(x[j + NClass*i]/(pv[i]*pc[j]));
    Type Hv = 0;
    for(unsigned int i = 0; i < NValue; i ++)
        Hv += pv[i]*log(pv[i]);
    Type Hc = 0;
    for(unsigned int i = 0; i < NClass; i ++)
        Hc += pc[i]*log(pc[i]);
    result /= sqrt(Hv*Hc);
    
    return result;
}

// A pointer class that support pointer operations but does not change the pointer value
// The offset from the initial position is stored in pos
// The refered value is stored in value
template <typename T>
struct StaticPointer: public std::iterator<std::output_iterator_tag, T>
{
    T value;
    size_t pos;
    StaticPointer(const T& value): value(value), pos(0) {}
    T& operator*() { return value; }
    StaticPointer& operator--() { pos--; return *this; }
    StaticPointer& operator--(int) { pos--; return *this; }
    StaticPointer& operator-=(size_t n) { pos -= n; return *this; }
    StaticPointer& operator++() { pos++; return *this; }
    StaticPointer& operator++(int) { pos++; return *this; }
    StaticPointer& operator+=(size_t n) { pos += n; return *this; }
};

template <class Container, typename Type>
inline double Jaccard(const Container& a, const Container& b)
{
    StaticPointer<Type> i_size(0);
    StaticPointer<Type> u_size(0);
    i_size = std::set_intersection(a.begin(), a.end(), b.begin(), b.end(), i_size); 
    u_size = std::set_union(a.begin(), a.end(), b.begin(), b.end(), u_size);
    return 1 - double(i_size.pos)/u_size.pos;
}

template <class Type>
inline Type Sum(const Type* x, size_t n)
{
    Type result = 0;
    for(size_t i = 0; i < n; i ++)
        result += x[i];
    return result;
}

template <typename Container, class Type>
inline void Sum(Container x, size_t n, Type& result)
{
    result = 0;
    for(size_t i = 0; i < n; i ++, ++x)
        result += *x;
}

template <class Container, typename Type>
inline Type Mean(Container x, size_t n)
{
    Type sum = 0;
    for(size_t i = 0; i < n; i ++)
        sum += x[i];
    return sum/double(n);
}

// calculate correlation coefficient between array x and y
template <class Container, typename Type = double>
inline double Correlation(Container x, Container y, size_t n)
{
    double xmean = Mean<Container, Type>(x, n);
    double ymean = Mean<Container, Type>(y, n);
    double cov = 0, xsqr = 0, ysqr = 0;
    for(size_t i = 0; i < n; i ++)
    {
        cov += (x[i] - xmean)*(y[i] - ymean);
        xsqr += (x[i] - xmean)*(x[i] - xmean);
        ysqr += (y[i] - ymean)*(y[i] - ymean);
    }
    double r = cov/::sqrt(xsqr*ysqr);
    if(::isnan(r))
        return 0;
    else
        return r;
}


// Cummulative density function of standard normal distribution
inline double StandardNormalCDF(double x)
{
    return 0.5*erfc(-x/sqrt(2.0));
}

// Survival function of standard normal distribution (1 - CDF)
inline double StandardNormalSF(double x)
{
    return 0.5*erfc(x/sqrt(2.0));
}

// fill array x with a sequence of integers from range [0, length)
template <typename Container>
inline void RangeArray(Container x, size_t length)
{
    for(size_t i = 0; i < length; i ++, ++x)
        *x = i;
}

// fill array x with a sequence of numbers from range [start, end)
template <typename Container, typename Type>
inline void RangeArray(Container x, Type start, Type end, Type step = 1)
{
    for(size_t i = start; i < end; i += step, ++x)
        *x = i;
}

// take values from array x by index array indices and store them to array out
template <typename DataIt, typename IndexIt>
inline void ArrayIndex(DataIt x, size_t size, IndexIt indices, DataIt out)
{
    for(size_t i = 0; i < size; i ++, ++out)
        *out = x[indices++];
}

// Two-sided Mann Whitney U Test
// use continuty
struct MannWhitneyUTest
{
    size_t n_x, n_y, n;
    std::vector<double> indices;
    std::vector<double> ranks;
    
    MannWhitneyUTest() {}
    MannWhitneyUTest(size_t n_x, size_t n_y)
        : n_x(n_x), n_y(n_y) { Init(n_x, n_y); }
    inline void Init(size_t n_x, size_t n_y)
    {
        n = n_x + n_y;
        indices.resize(n);
        ranks.resize(n);
    }
   
    // first n_x elements are from group x, last n_y elements are from group y 
    template <class Container>
    inline double operator()(Container combined);
};

template <class Container>
double MannWhitneyUTest::operator()(Container combined)
{
    //std::copy(x, x + n_x, combined);
    //std::copy(y, y + n_y, combined + n_x);
    RangeArray(indices.begin(), n);
    ArgSortAscend(combined, n, indices.begin());
   
    double sigma = 0;
    size_t tie_start = 0;
    // get ranks and tie correct
    for(size_t i = 0; i < n; i ++)
    {
        double cur_value = combined[indices[i]];
        double next_value;
        if(i < n - 1)
            next_value = combined[indices[i + 1]];
        else
            next_value = cur_value + 1;
        if(next_value > cur_value)
        {
            if(i > tie_start)
            {
                double t = i - tie_start + 1;
                double avg_rank = 0;
                for(size_t j = tie_start; j <= i; j ++)
                    avg_rank += j + 1;
                avg_rank /= t;
                for(size_t j = tie_start; j <= i; j ++)
                    ranks[indices[j]] = avg_rank;
                sigma += t*t*t - t;
            }
            else
                ranks[indices[i]] = i + 1;
            tie_start = i + 1;
        }
    }
    // get rank sum of x
    double r_x;
    Sum(ranks.begin(), n_x, r_x);
#if 0
    std::cout << "ranks = ["; PrintArray(ranks.begin(), n); std::cout << "]" << std::endl;
    std::sort(ranks.begin(), ranks.end());
    std::cout << "ranks_sorted = ["; PrintArray(ranks.begin(), n); std::cout << "]" << std::endl;
#endif

    // U static
    double u1 = r_x - n_x*(n_x + 1)/2;
    double u2 = n_x*n_y - u1;
    double u = std::max(u1, u2);
    double mu = n_x*n_y/2 + 0.5; // mean
    // correct ties
    sigma = ::sqrt(double(n_x)*n_y/12.0*((n + 1) - sigma/(n*(n - 1))));
    //sigma = ::sqrt(double(n_x)*n_y*(n + 1)/12.0);
#if 0
    std::cout << "x = ["; PrintArray(combined, n_x); std::cout << "]" << std::endl;
    std::cout << "y = ["; PrintArray(combined) + n_x, n_y); std::cout << "]" << std::endl;
    std::cout << "mu = " << mu << ", sigma = " << sigma << std::endl;
    std::cout << "u1 = " << u1 << ", u2 = " << u2 << std::endl;
#endif
    double pvalue = 2.0*StandardNormalSF((u - mu)/sigma);
    
    return pvalue;
}
#endif
