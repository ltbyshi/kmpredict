#ifndef __KMERCOUNTER_H__
#define __KMERCOUNTER_H__
#include <vector>
#include <string>
#include <list>

#include "Nucleotide.h"
#include "Utils.h"
#include "Kmer.h"
#include "HDF5Utils.h"

class KmerError: public std::exception
{
public:
    explicit KmerError(const std::string& msg) {
        _msg = std::string("KmerError: ") + msg;
    }
    virtual const char* what() const noexcept{
        return _msg.c_str();
    }
private:
    std::string _msg;
};

struct KmerCounterHeader
{
    uint32_t magic;
    uint32_t headersize;
    uint32_t k;
    uint32_t kmersize;
    uint32_t valsize;
    uint64_t hashsize;
    uint64_t nRecords;

    static const uint32_t MagicNumber = 0x52454D4B;

    KmerCounterHeader()
    {
        magic = KmerCounterHeader::MagicNumber;
        headersize = GetSize();
    }

    void Read(std::istream& is)
    {
        ReadItem(is, magic);
        ReadItem(is, headersize);
        ReadItem(is, k);
        ReadItem(is, kmersize);
        ReadItem(is, valsize);
        ReadItem(is, hashsize);
        ReadItem(is, nRecords);
    }

    void Write(std::ostream& os) const
    {
        WriteItem(os, magic);
        WriteItem(os, headersize);
        WriteItem(os, k);
        WriteItem(os, kmersize);
        WriteItem(os, valsize);
        WriteItem(os, hashsize);
        WriteItem(os, nRecords);
    }
    uint32_t GetSize() const
    {
        return (sizeof(magic) + sizeof(headersize) + sizeof(k)
            + sizeof(kmersize) + sizeof(valsize) + sizeof(hashsize)
            + sizeof(nRecords));
    }
};

template <typename KmerType, typename ValType>
class KmerCounter
{
public:
    KmerCounter() : _method("exact")
    {
        _hash = NULL;
        _k = 0;
        _hashSize = 0;
    }

    KmerCounter(size_t hashSize, int k)  : _method("exact")
    {
        // _hash.resize(hashSize, 0);
        _hash = new ValType[hashSize];
        _k = k;
        _hashSize = hashSize;
    }

    KmerCounter(const KmerCounter& counter)  : _method("exact")
    {
        _k = counter._k;
        _hashSize = counter._hashSize;
        _hash = new ValType[_hashSize];
        memcpy(_hash, counter._hash, _hashSize*sizeof(ValType));
    }

    ~KmerCounter()
    {
        if(_hash)
            delete[] _hash;
    }

    inline void Add(KmerType kmer, ValType value)
    {
        //KmerType kmerrc = KmerRC(kmer, _k);
        //if(kmerrc < kmer)
        //    kmer = kmerrc;
        if(kmer < _hashSize)
        {
            _hash[kmer] += value;
        }
    }

    inline void Add(const char* seq, ValType value)
    {
        KmerType kmer = StringToKmer<KmerType>(seq, _k);
        Add(kmer, value);
    }

    inline ValType Get(KmerType kmer) const
    {
        return _hash[kmer];
    }

    inline ValType operator[](KmerType kmer) const
    {
        return _hash[kmer];
    }

    const ValType* GetData() const { return _hash; }

    inline int GetK() const { return _k; }

    inline KmerType GetHashSize() const { return _hashSize; }

    inline void Clear()
    {
        if(_hash)
            delete[] _hash;
        _hash = NULL;
        _k = 0;
        _hashSize = 0;
    }

    static void GetInfo(const char* filename);
    void LoadFromHDF5(const std::string& filename);
    void SaveToHDF5(const std::string& filename) const;
    void Load(const char* filename);
    void Load(const std::list<std::string>& filenames);
    void Save(const char* filename, bool mergerc = true) const;
    void DumpToTable() const;
    KmerType GetUniqueKmerCount() const;
public:
    static const uint32_t MagicNumber = 0x52454D4B;
private:
    ValType* _hash;
    KmerType _hashSize;
    int _k;
    const H5std_string _method = "exact";
};

template <typename KmerType, typename ValType>
void KmerCounter<KmerType, ValType>::GetInfo(const char* filename)
{
    ifstream fin(filename, ios::binary);
    if(!fin)
        Die("cannot open the input file %s", filename);

    KmerCounterHeader header;
    header.Read(fin);
    fin.close();

    cout << "K\t" << header.k << endl;
    cout << "HashSize\t" << header.hashsize << endl;
    cout << "KmerSize\t" << header.kmersize << endl;
    cout << "ValSize\t" << header.valsize << endl;
    cout << "NumValues\t" << header.nRecords << endl;
    cout << "HeaderSize\t" << header.headersize << endl;
}

template <typename KmerType, typename ValType>
void KmerCounter<KmerType, ValType>::Load(const char* filename)
{
    // clear data
    Clear();
    ifstream fin(filename, ios::binary);
    if(!fin)
        Die("cannot open the input file %s", filename);

    KmerCounterHeader header;
    header.Read(fin);
    KmerType nRecords = header.nRecords;
    //fin.read((char*)(&_k), sizeof(_k));
    // fin.read((char*)(&nRecords), sizeof(nRecords));
    _k = header.k;
    _hashSize = CalcKmerHashSize<KmerType>(_k);
    _hash = new ValType[_hashSize];

    // all possible kmers
    if(nRecords == _hashSize)
        fin.read((char*)_hash, sizeof(ValType)*_hashSize);
    // merged reverse complement kmers
    else
    {
        ValType* count = new ValType[nRecords];
        fin.read((char*)count, sizeof(ValType)*nRecords);
        KmerType i = 0;
        for(KmerType kmer = 0; kmer < _hashSize; kmer ++)
        {
            KmerType kmerrc = KmerRC(kmer, _k);
            if(kmer <= kmerrc)
            {
                // fin.read((char*)(&_hash[kmer]), sizeof(ValType));
                _hash[kmer] = count[i];
                i ++;
                if(kmer < kmerrc)
                    _hash[kmerrc] = 0;
            }
        }
        delete[] count;
    }
    if(!fin)
        Die("corrupted kmer file: %s", filename);
    fin.close();
}

template <typename KmerType, typename ValType>
void KmerCounter<KmerType, ValType>::Load(const std::list<std::string>& filenames)
{
    // clear the data
    Clear();
    // read first file to determine k and hash size
    const char* firstfile = filenames.front().c_str();
    ifstream fin(firstfile, ios::binary);
    if(!fin)
        Die("cannot open the input file %s", firstfile);
    KmerCounterHeader header;
    header.Read(fin);
    // fin.read((char*)(&_k), sizeof(_k));
    fin.close();

    _k = header.k;
    _hashSize = CalcKmerHashSize<KmerType>(_k);
    _hash = new ValType[_hashSize];
    memset(_hash, 0, sizeof(ValType)*_hashSize);

    for(std::list<std::string>::const_iterator it = filenames.begin();
        it != filenames.end(); ++it)
    {
        const char* filename = it->c_str();
        ifstream fin(filename, ios::binary);
        if(!fin)
            Die("cannot open the input file %s", filename);

        header.Read(fin);
        int k = header.k;
        KmerType nRecords = header.nRecords;
        //fin.read((char*)(&k), sizeof(k));
        //fin.read((char*)(&nRecords), sizeof(nRecords));
        if(k != _k)
            Die("the value of K in %s is not equal to the first file", filename);
        if((nRecords != CalcUniqueKmerCount<KmerType>(_k)) && (nRecords != _hashSize))
            Die("the number of values (%d) is neither the hash size (%d)"
                " nor the unique kmer counts (%d) in file %s",
                nRecords, _hashSize, CalcUniqueKmerCount<KmerType>(_k), filename);

        ValType* count = new ValType[nRecords];
        fin.read((char*)count, sizeof(ValType)*nRecords);
        // reverse complemented kmers are merged
        if(nRecords < _hashSize)
        {
            KmerType i = 0;
            for(KmerType kmer = 0; kmer < _hashSize; kmer ++)
            {
                if(kmer <= KmerRC(kmer, _k))
                {
                    _hash[kmer] += count[i];
                    i ++;
                }
            }
        }
        else
        {
            for(KmerType kmer = 0; kmer < _hashSize; kmer ++)
                _hash[kmer] += count[kmer];
        }
        fin.close();
        delete[] count;
    }
}

template <typename KmerType, typename ValType>
void KmerCounter<KmerType, ValType>::Save(const char* filename, bool mergerc) const
{
    ofstream fout(filename, ios::binary);
    if(!fout)
        Die("cannot open the output file %s", filename);
    KmerCounterHeader header;
    header.k = _k;
    header.hashsize = _hashSize;
    if(mergerc)
        header.nRecords = CalcUniqueKmerCount<KmerType>(_k);
    else
        header.nRecords = _hashSize;
    //header.nRecords = mergerc? CalcUniqueKmerCount<KmerType>(_k) : _hashsize;
    header.kmersize = sizeof(KmerType);
    header.valsize = sizeof(ValType);
    header.Write(fout);
    // fout.write(reinterpret_cast<const char*>(&_k), sizeof(_k));
    // merge counts of a kmer and its reverse complement kmer
    if(mergerc)
    {
        for(KmerType kmer = 0; kmer < _hashSize; kmer ++)
        {
            KmerType kmerrc = KmerRC(kmer, _k);
            if(kmer <= kmerrc)
            {
                ValType count;
                if(kmer < kmerrc)
                    count = _hash[kmer] + _hash[kmerrc];
                else
                    count = _hash[kmer];
                fout.write(reinterpret_cast<const char*>(&count), sizeof(ValType));
            }
        }
    }
    else
    {
        // fout.write(reinterpret_cast<const char*>(&_hashSize), sizeof(_hashSize));
        fout.write(reinterpret_cast<const char*>(_hash), sizeof(ValType)*_hashSize);
    }
    fout.close();
}

template <typename KmerType, typename ValType>
void KmerCounter<KmerType, ValType>::LoadFromHDF5(const H5std_string& filename)
{
    Clear();
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    hsize_t dims[1];
    const H5std_string dataname = "data";
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataSpace dataspace = dataset.getSpace();
    H5::DataType memtype = InferDataType<ValType>();
    dataspace.getSimpleExtentDims(dims, NULL);
    ReadAttributeFromHDF5<int>(filename, dataname, "k", _k);
    ReadAttributeFromHDF5<KmerType>(filename, dataname, "hashsize", _hashSize);
    _hash = new ValType[dims[0]];
    dataset.read(_hash, memtype);
    dataset.close();
    h5file.close();
}

template <typename KmerType, typename ValType>
void KmerCounter<KmerType, ValType>::SaveToHDF5(const H5std_string& filename) const
{
    H5::H5File h5file(filename, H5F_ACC_TRUNC);
    hsize_t dims[1];
    dims[0] = _hashSize;
    const H5std_string dataname = "data";
    H5::DataSpace dataspace(1, dims);
    H5::DataType datatype = InferDataType<ValType>();
    H5::DataSet dataset = h5file.createDataSet(dataname, datatype, dataspace);
    dataset.write(_hash, datatype);
    dataset.close();
    h5file.close();
    WriteAttributeToHDF5<int>(filename, dataname, "k", _k);
    WriteAttributeToHDF5<KmerType>(filename, dataname, "hashsize", _hashSize);
    WriteAttributeToHDF5<H5std_string>(filename, dataname, "method", _method);
}

template <typename KmerType, typename ValType>
void KmerCounter<KmerType, ValType>::DumpToTable() const
{
    for(KmerType kmer = 0; kmer < _hashSize; kmer ++)
    {
        KmerType kmerrc = KmerRC(kmer, _k);
        ValType count = 0;
        if(kmer <= kmerrc)
        {
            if(kmer < kmerrc)
                count = _hash[kmer] + _hash[kmerrc];
            else if(kmer == kmerrc)
                count = _hash[kmer];
            cout << KmerToString(kmer, _k)
                << "\t" << count << endl;
        }
    }
}

template <typename KmerType, typename ValType>
KmerType KmerCounter<KmerType, ValType>::GetUniqueKmerCount() const
{
    KmerType uniqc = 0;
    for(KmerType kmer = 0; kmer < _hashSize; kmer ++)
    {
        KmerType kmerrc = KmerRC(kmer, _k);
        if((kmer < kmerrc) && (_hash[kmer] + _hash[kmerrc] > 0))
            uniqc ++;
        else if((kmer == kmerrc) && (_hash[kmer] > 0))
            uniqc ++;
    }
    return uniqc;
}

typedef KmerCounter<DefaultKmerType, DefaultValType> DefaultKmerCounter;

#endif
