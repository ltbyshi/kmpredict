#include "HDF5Utils.h"

std::vector<size_t> GetHDF5DatasetDims(const H5std_string& filename,
                                       const H5std_string& dataname)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataSpace dataspace = dataset.getSpace();
    int rank = dataspace.getSimpleExtentNdims();
    hsize_t* dims = new hsize_t[rank];
    dataspace.getSimpleExtentDims(dims, NULL);
    std::vector<size_t> dims_v(rank);
    for(int i = 0; i < rank; i ++)
        dims_v[i] = dims[i];
    delete[] dims;
    dataset.close();
    h5file.close();
    return dims_v;
}

size_t GetHDF5DatasetTypeSize(const H5std_string& filename, 
                              const H5std_string& dataname)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataType datatype = dataset.getDataType();
    size_t type_size = datatype.getSize();
    dataset.close();
    h5file.close();
    return type_size;
}

H5::DataType GetHDF5DatasetDataType(const H5std_string& filename, 
                              const H5std_string& dataname)
{
    H5::H5File h5file(filename, H5F_ACC_RDONLY);
    H5::DataSet dataset = h5file.openDataSet(dataname);
    H5::DataType datatype = dataset.getDataType();
    dataset.close();
    h5file.close();
    return datatype;
}

