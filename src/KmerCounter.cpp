#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <sstream>
#include <map>
#include <list>
#include <algorithm>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
using namespace std;

#include "Fasta.h"
#include "KmerCounter.h"
#include "Utils.h"
#include "HyperplaneHash.h"

#define PROGNAME "KmerCounter"

// Kmer counter that does not need coverage information
int KmerCounterMain(int argc, char** argv)
{
    if(argc != 4)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmer frequencies" << endl;
        cerr << "Usage: " << PROGNAME << " count k seq_file kmer_file" << endl;
        cerr << "\tseq_file\tsequence in FASTA format." << endl;
        cerr << "\tkmer_file\toutput file containing kmer frequencies." << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    const char* seqfile = argv[2];
    const char* kmerfile = argv[3];
    
    // calculate hash size
    unsigned int hashSize = 1;
    for(int i = 0; i < k; i ++)
        hashSize *= 4;
    
    FastaReader reader(seqfile);
    if(!reader)
        Die("cannot open the fasta file: %s", seqfile);
    DefaultKmerCounter counter(hashSize, k);
    // count kmers
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        const char* seq = record->seq.c_str();
        size_t nkmer = record->seq.size() - k + 1;
        // skip if the sequence is shorter than kmer length
        if(nkmer <= 0)
            continue;
        DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
        counter.Add(kmer, 1);
        for(size_t i = 0; i < nkmer - 1; i++)
        {
            kmer = OverlapKmer(kmer, k, seq[k + i]);
            counter.Add(kmer, 1);
        }
        /* 
        for(size_t i = 0; i < nkmer; i ++)
            counter.Add(&(record->seq.c_str()[i]), 1);
        */
        delete record;
        record = reader.ReadNext();
    }
    reader.Close();
    counter.SaveToHDF5(kmerfile);
    // WriteKmersBinary(counter, kmerfile);
    return 0;
}

int WeightedKmerCounterMain(int argc, char** argv)
{
    if(argc != 5)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmers with sequence weight" << endl;
        cerr << "Usage: " << PROGNAME << " countw k seq_file coverage_file kmer_file" << endl;
        cerr << "\tseq_file\tsequence in FASTA format." << endl;
        cerr << "\tcoverage_file\tfile with two columns (name, coverage)." << endl;
        cerr << "\tkmer_file\toutput file containing kmer frequencies." << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    const char* seqfile = argv[2];
    const char* covfile = argv[3];
    const char* kmerfile = argv[4];
    
    // read coverage file
    map<string, int> coverage = ReadCoverage(covfile);
    
    // calculate hash size
    unsigned int hashSize = 1;
    for(int i = 0; i < k; i ++)
        hashSize *= 4;
    
    FastaReader reader(seqfile);
    DefaultKmerCounter counter(hashSize, k);
    // count kmers
    FastaRecord* record = reader.ReadNext();
    while(record)
    {
        map<string, int>::iterator it = coverage.find(record->name);
        if(it == coverage.end())
        {
            // cerr << "Warning: sequence " << record->name << " has no coverage value" << endl;
        }
        else
        {
            const char* seq = record->seq.c_str();
            size_t nkmer = record->seq.size() - k + 1;
            // skip if the sequence is shorter than kmer length
            if(nkmer <= 0)
                continue;
            DefaultKmerType kmer = StringToKmer<DefaultKmerType>(seq, k);
            counter.Add(kmer, it->second);
            for(size_t i = 0; i < nkmer - 1; i++)
            {
                kmer = OverlapKmer(kmer, k, seq[k + i]);
                counter.Add(kmer, it->second);
            }
         }
        delete record;
        record = reader.ReadNext();
    }
    counter.SaveToHDF5(kmerfile);
    return 0;
}

int DumpKmerMain(int argc, char** argv)
{
    if(argc < 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " dump kmer_file [kmer_file...]" << endl;
        cerr << "\tkmer_file\toutput file from k-mer counter." << endl;
        exit(1);
    }
    else if(argc == 2)
    {
        const char* kmerfile = argv[1];
        DefaultKmerCounter counter;
        counter.LoadFromHDF5(kmerfile);
        counter.DumpToTable();
        //DumpKmersToTable(kmerfile);
    }
    else
    {
        list<string> kmerfiles;
        for(int i = 1; i < argc; i ++)
            kmerfiles.push_back(argv[i]);
        //DumpKmersToTable(kmerfiles);
        DefaultKmerCounter counter;
        counter.Load(kmerfiles);
        counter.DumpToTable();
    }
    return 0;
}

int MergeRCMain(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " mergerc kmer_infile kmer_outfile" << endl;
        cerr << "\tkmer_infile,kmer_outfile\tkmer binary format file" << endl;
        exit(1);
    }
    else
    {
        const char* infile = argv[1];
        const char* outfile = argv[2];
        
        DefaultKmerCounter counter;
        counter.Load(infile);
        
        counter.Save(outfile, true);
    }
    return 0;
}

int GetInfoMain(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " info kmer_file" << endl;
        cerr << "\tkmer_file\tkmer binary file generated from count" << endl;
        exit(1);
    }
    else
    {
        const char* kmerfile = argv[1];
        
        DefaultKmerCounter::GetInfo(kmerfile);
    }
    return 0;
}

int UniqueKmerCountMain(int argc, char** argv)
{
    if(argc < 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Usage: " << PROGNAME << " uniqkmercount kmer_file [kmer_file...]" << endl;
        cerr << "\tkmer_file\tkmer binary file generated from count" << endl;
        exit(1);
    }
    else if(argc == 2)
    {
        const char* kmerfile = argv[1];
        
        DefaultKmerCounter counter;
        counter.Load(kmerfile);
        cout << counter.GetUniqueKmerCount() << endl;
    }
    else
    {
        list<string> kmerfiles;
        for(int i = 1; i < argc; i ++)
            kmerfiles.push_back(argv[i]);
        DefaultKmerCounter counter;
        counter.Load(kmerfiles);
        cout << counter.GetUniqueKmerCount() << endl;
    }
    return 0;
}


int CharKmerMain(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Get characteristic kmers for each group" << endl;
        cerr << "Usage: " << PROGNAME << " charkmer groupfile" << endl;
        cerr << "\tgroupfile\ta text file with group labels and kmerfile name per line" << endl;
        exit(1);
    }
    else
    {
        const char* groupfile = argv[1];
        vector<string> groupmap;
        vector<int> groups;
        vector<string> kmerfiles;
        
        // read groupfile
        ifstream fin(groupfile);
        if(!fin)
            Die("cannot open the input file: %s", groupfile);
        int lineno = 1;
        while(!fin.eof())
        {
            string line;
            getline(fin, line);
            // skip empty lines
            if(line.size() < 1)
            {
                lineno ++;
                continue;
            }
            istringstream is(line);
            string groupname, kmerfile;
            is >> groupname >> kmerfile;
            if(!is)
                Die("invalid at line %d in groupfile %s", lineno, groupfile);
            
            kmerfiles.push_back(kmerfile);
            // find group id
            size_t groupid;
            for(groupid = 0; groupid < groupmap.size(); groupid ++)
            {
                if(groupmap[groupid] == groupname)
                    break;
            }
            // create a new group ID
            if(groupid == groupmap.size())
            {
                groupmap.push_back(groupname);
                groups.push_back(groupid);
            }
            else
            {
                groups.push_back(groupid);
            }
            lineno ++;
        }
        fin.close();
        
        // detect K and hashsize
        vector<BitSet> indicator(groupmap.size());
        DefaultKmerCounter* counter = new DefaultKmerCounter;
        counter->Load(kmerfiles[0].c_str());
        DefaultKmerType hashsize = counter->GetHashSize();
        delete counter;
        for(size_t i = 0; i < indicator.size(); i ++)
            indicator[i].Resize(hashsize);
        // read kmerfiles
        for(size_t i = 0; i < kmerfiles.size(); i ++)
        {
            int group = groups[i];
            counter = new DefaultKmerCounter;
            counter->Load(kmerfiles[i].c_str());
            const DefaultValType* data = counter->GetData();
            for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
            {
                if(data[kmer] > 0)
                    indicator[group].Set(kmer);
            }
            delete counter;
        }
        // count characteristic kmers
        vector<DefaultKmerType> charkmers(indicator.size());
        for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
        {
            int chargroup = 0;
            int countOverGroups = 0;
            for(size_t i = 0; i < indicator.size(); i ++)
            {
                if(indicator[i][kmer])
                {
                    chargroup = i;
                    countOverGroups ++;
                }
            }
            if(countOverGroups == 1)
                charkmers[chargroup] ++;
        }
        cout << "Group\tCharCount" << endl;
        for(size_t i = 0; i < charkmers.size(); i ++)
            cout << groupmap[i] << "\t" << charkmers[i] << endl;
    }
    return 0;
}

#if ENABLE_PROGRESS
ProgressBar* ProgressBarUpdater::progressbar = NULL;
double ProgressBarUpdater::value = 0;
int ProgressBarUpdater::interval = 1;
#endif

template <unsigned int BinWidth>
static void KmerCountHistogram(const DefaultKmerCounter& counter,
        DefaultValType bincount, unsigned int binwidth = 0)
{
    DefaultKmerType hashsize = counter.GetHashSize();
    vector<size_t> hist(bincount + 1);
    for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
    {
        DefaultKmerType bin;
        if(BinWidth < 10)
            bin = counter.Get(kmer)/BinWidth;
        else
            bin = counter.Get(kmer)/binwidth;
        if(bin < bincount)
            hist[bin] ++;
        else
            hist[bincount] ++;
    }
    if(BinWidth < 10)
        binwidth = BinWidth;
    for(size_t c = 0; c < hist.size(); c ++)
        cout << c*binwidth << "\t" << hist[c] << endl;
}

int HistogramMain(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count the distribution of counts" << endl;
        cerr << "Usage: " << PROGNAME << " hist kmerfile" << endl;
        cerr << "Options:\n"
            << "bincount:\tnumber of bins\n"
            << "binwidth:\twidth of bins" << endl;
        exit(1);
    }
    const char* kmerfile = argv[1];
    DefaultValType bincount = 1000;
    DefaultValType binwidth = 1;
    GetEnv<DefaultValType>("bincount", bincount);
    GetEnv<DefaultValType>("binwidth", binwidth);

    DefaultKmerCounter counter;
    counter.LoadFromHDF5(kmerfile);
    // optimize for small bin width
    switch(bincount)
    {
        case 1: KmerCountHistogram<1>(counter, bincount); break;
        case 2: KmerCountHistogram<2>(counter, bincount); break;
        default: KmerCountHistogram<100>(counter, bincount, binwidth);
    }
    return 0;
}

int AverageCount(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Calculate (total number of kmers)/(total number of distinct kmers)" << endl;
        cerr << "Usage: average_count kmerfile" << endl;
        exit(1);
    }
    const char* kmerfile = argv[1];

    DefaultKmerCounter counter;
    counter.LoadFromHDF5(kmerfile);
    DefaultKmerType hashsize = counter.GetHashSize();
    DefaultKmerType nkmers = 0;
    for(DefaultKmerType kmer = 0; kmer < hashsize; kmer ++)
        nkmers += counter.Get(kmer);
    cout << setprecision(12) << double(nkmers)/double(hashsize) << endl;   
    return 0;
}

template <bool Weighted>
int HashedCount(int argc, char** argv)
{
    int nargs = Weighted? 7 : 6;
    if(argc != nargs)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count kmer frequencies" << endl;
        cerr << "Usage: " << PROGNAME;
        if(Weighted)
            cerr << " hashedcountw k hashsize hashfile seqfile covfile kmerfile" << endl;
        else
            cerr << " hashedcount k hashsize hashfile seqfile kmerfile" << endl;
        cerr << "\thashfile\tfile created by createhash" << endl;
        cerr << "\tseqfile\tsequence in FASTA format." << endl;
        cerr << "\tkmerfile\toutput file containing kmer frequencies." << endl;
        cerr << "Environment variables: file_size" << endl;
        exit(1);
    }
    int k = atoi(argv[1]);
    int hashsize = atoi(argv[2]);
    const char* hashfile = argv[3];
    const char* seqfile = argv[4];
    const char* covfile = NULL;
    string kmerfile;

    map<string, int> coverage;
    if(Weighted)
    {
        covfile = argv[5];
        kmerfile = argv[6];
        // read coverage file
        coverage = ReadCoverage(covfile);
    }
    else
        kmerfile = argv[5]; 
        
    HyperplaneHash<DefaultKmerType> hash(k, hashsize);
    if(!hash.LoadHyperplanes(hashfile))
        exit(1);
    
    FastaReader reader(seqfile);
    DefaultKmerCounter counter((1UL << hashsize), k);
    // count kmers
#if ENABLE_PROGRESS
    size_t filesize = 1;
    if(!GetEnv<size_t>("file_size", filesize))
        filesize = FileSize(seqfile);
    ProgressBar progressbar(filesize);
    ProgressBarUpdater::progressbar = &progressbar;
    ProgressBarUpdater::start();
#endif
    FastaRecord* record = reader.ReadNext();
    VectorXcd kmer_code(k);
    while(record)
    {
        if(Weighted)
        {
            map<string, int>::iterator it = coverage.find(record->name);
            if(it != coverage.end())
            {
                const char* seq = record->seq.c_str();
                size_t nkmers = record->seq.size() - k + 1;
                for(size_t i = 0; i < nkmers; i ++)
                    counter.Add(hash.Hash(&(seq[i]), kmer_code), it->second);
            }
        }
        else
        {
            const char* seq = record->seq.c_str();
            size_t nkmers = record->seq.size() - k + 1;
            for(size_t i = 0; i < nkmers; i ++)
                counter.Add(hash.Hash(&(seq[i]), kmer_code), 1);
        }
        delete record;
        record = reader.ReadNext();
#if ENABLE_PROGRESS
        ProgressBarUpdater::value = reader.GetFilePos();
#endif
    }
#if ENABLE_PROGRESS
    ProgressBarUpdater::stop();
    progressbar.Finish();
#endif
    reader.Close();
    //prevent parallel writing
#ifdef ENABLE_FILELOCK
    string lockfile;
    FileLock fl;
    bool lock_enabled = GetEnv<string>("lockfile", lockfile);
    if(lock_enabled)
    {
        fl.Open(lockfile);
        fl.Lock(true);
    }
#endif
    MakeDirs(DirName(kmerfile));
    counter.SaveToHDF5(kmerfile);
#ifdef ENABLE_FILELOCK
    if(lock_enabled)
        fl.Unlock();
#endif
    return 0;
}


typedef int (*CommandHandler)(int , char**);
typedef map<string, CommandHandler> HandlerMap;
void Usage(const HandlerMap& handlers)
{
    cerr << "Usage: " << PROGNAME << " command [options]" << endl;
    cerr << "Avaiable commands:";
    for(HandlerMap::const_iterator it = handlers.begin();
        it != handlers.end(); ++ it)
        cerr << " " << it->first;
    cerr << endl;
    exit(1);
}

int main(int argc, char** argv)
{
    // TestKmerRC();
    // TestKmerRC2();
    // TestKmerConversion();
    // TestFastaReader();
    // TestDefaultKmerCounter();
    // TestOverlapKmer();
    // TestCalcKmerSize();
    // exit (0);
    
    HandlerMap handlers;
    handlers["countw"] = WeightedKmerCounterMain;
    handlers["count"] = KmerCounterMain;
    handlers["dump"] = DumpKmerMain;
    handlers["mergerc"] = MergeRCMain;
    handlers["info"] = GetInfoMain;
    handlers["uniquekmercount"] = UniqueKmerCountMain;
    handlers["charkmer"] = CharKmerMain;
    handlers["hashedcountw"] = HashedCount<true>;
    handlers["hashedcount"] = HashedCount<false>;
    handlers["hist"] = HistogramMain;
    handlers["average_count"] = AverageCount;

    if(argc < 2)
    {
        Usage(handlers);
    }
    HandlerMap::const_iterator it = handlers.find(argv[1]);
    if(it != handlers.end())
        return it->second(argc - 1, argv + 1);
    else
    {
        cerr << "Error: invalid command: " << argv[1] << "\n" << endl;
        Usage(handlers);
    }
    
    return 0;
}
