#include <iostream>
#include <string>
#include <vector>
using namespace std;
#include "HDF5Utils.h"
#include "Logging.h"

void HDF5Merge(const std::vector<std::string>& input_files,
               const std::string& dataname,
               const std::string& output_file)
{
    hsize_t totalsize = 0;
    size_t n = input_files.size();
    vector<hsize_t> data_sizes(n);
    H5::DataType datatype;
    for(size_t i = 0; i < n; i ++)
    {
        logger.Debug() << "Read input file " << input_files[i];
        H5::H5File ifile(input_files[i], H5F_ACC_RDONLY);
        H5::DataSet idataset = ifile.openDataSet(dataname);
        H5::DataType idatatype = idataset.getDataType();
        H5::DataSpace ifile_space = idataset.getSpace();
        int rank = ifile_space.getSimpleExtentNdims();
        if(rank != 1)
            throw HDF5Error("HDF5 dataset is not a vector");
        if(i == 0)
            datatype = idatatype;
        if(!(datatype == idatatype))
            throw HDF5Error("HDF5 datasets are not of the same data type");
        hsize_t data_size;
        ifile_space.getSimpleExtentDims(&data_size, NULL);
        idataset.close();
        ifile.close();

        logger << ", data_size: " << data_size << Endl;
        data_sizes[i] = data_size;
        totalsize += data_sizes[i];
    }

    logger.Debug() << "Open output file " << output_file << Endl;
    size_t type_size = datatype.getSize();
    hsize_t offset = 0; 
    H5::H5File ofile(output_file, H5F_ACC_TRUNC);
    H5::DataSpace ofile_space(1, &totalsize);
    H5::DataSet odataset = ofile.createDataSet(dataname, datatype, ofile_space);
    for(size_t i = 0; i < n; i ++)
    {
        char* data = new char[data_sizes[i]*type_size];
        hsize_t count = data_sizes[i];
        // read input file
        H5::H5File ifile(input_files[i], H5F_ACC_RDONLY);
        H5::DataSet idataset = ifile.openDataSet(dataname);
        idataset.read(data, datatype);
        idataset.close();
        ifile.close();
        // write output file
        ofile_space.selectHyperslab(H5S_SELECT_SET, &count, &offset);
        H5::DataSpace mem_space(1, &count);
        odataset.write(data, datatype, mem_space, ofile_space);
        delete[] data;
        offset += data_sizes[i];
    }
    odataset.close();
    ofile.close();
}

void HDF5Select(const string& data_file,
                const string& data_dataset,
                const string& index_file,
                const string& index_dataset)
{
}

static const char* progname = NULL;

static void MainHelp()
{
    cerr << "Usage: " << progname << " command options" << endl;
    cerr << "Avaiable commands: merge" << endl;
}

int main(int argc, char** argv)
{
    progname = argv[0];
    if(argc < 2) 
    {
        MainHelp();
        exit(1);
    }
    string command(argv[1]);    
    argc -= 2;
    argv += 2;
    if(command == "merge")
    {
        if(argc < 3)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 << " dataset_name input_file1 [input_file2]... output_file" << endl;
            exit(1);
        }
        string dataname(argv[0]);
        vector<string> input_files(argc - 2);
        for(int i = 1; i < argc - 1; i ++)
            input_files[i - 1] = argv[i];
        string output_file(argv[argc - 1]);
        HDF5Merge(input_files, dataname, output_file);
    }
    else if(command == "select")
    {
        if(argc != 4)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 << " data_file data_dataset index_file index_dataset" << endl;
            exit(1);
        }
        string data_file(argv[0]);
        string data_dataset(argv[1]);
        string index_file(argv[2]);
        string index_dataset(argv[3]);
        HDF5Select(data_file, data_dataset, index_file, index_dataset);
    }
    else
    {
        cerr << "Error: invalid command: " << command << endl;
        MainHelp();
        exit(1);
    }

    return 0;
}
