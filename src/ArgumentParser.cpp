#include <iostream>
#include <string>
#include <map>
#include <exception>
#include <ctype.h>
using namespace std;

class ArgumentException: public std::exception
{
    ArgumentException(const std::string& msg): msg(msg) {}
    virtual const char* what() const throw()
    {
        return std::string("ArgumentParser error: ") + msg.c_str();
    }
    std::string msg;
};



template <typename ValType>
struct Argument
{
    std::string name;
    std::string metavar;
    std::string long_arg;
    std::string short_arg;
    std::string argstring;
    std::string help;
    bool given;
    bool flag;
    bool required;
    bool positional;
    int nargs_min;
    int nargs_max;
    ValType defval;
    vector<ValType> values;
    vector<ValType> choices;
    //ArgumentHandler* handler;
};

template <typename ValType>
struct NullArgumentHandler
{
    bool operator()(const Argument<ValType>& arg) { return true; }
};

class ArgumentParser
{
public:
    ArgumentParser(const std::string& description = "");
    ~ArgumentParser();
    
    template <typename ValType>
    void add_argument(const std::string& name, int nargs = 1, bool required = false);
    template <typename ValType>
    void add_argument(const Argument& arg);
    void add_positional(const std::string& name,
                        const string& help,
                        int nargs = 1);
    template <typename ValType>
    void add_argument(const std::string& name,
                      const std::string& long_arg,
                      const std::string& short_arg,
                      const std::string& help,
                      const ValType& defval,
                      int nargs = 1,
                      bool flag = false,
                      bool required = false);
    void add_flag(const std::string& name, 
                  const std::string& long_arg,
                  const std::string& short_arg,
                  const std::string& help,
                  bool defvalue = false);
    bool check_option(const std::string& name,
                      const std::string& long_arg,
                      const std::string& short_arg) const;
    template <typename ValType>
    ValType get_argument(const std::string& name) const;
    template <typename ValType>
    vector<ValType> get_arguments(const std::string& name) const;
    
    bool parse_args(int argc, char** argv);
    void help() const;
private:
    std::map<std::string, void*> options;
    std::map<std::string, void*> argmap;
    std::vector<void*> positionals;
    std::string description;
};

ArgumentParser::ArgumentParser(const std::string& description)
{
    if(description)
        this->description = description;
}

ArgumentParser::~ArgumentParser()
{
    for(std::map<std::string, void*>::iterator it = options.begin();
        it != options.end();
        ++ it)
        delete *it;
}

void ArgumentParser::check_option(const std::string& name,
                      const std::string& long_arg,
                      const std::string& short_arg)
{
    if((long_arg.size() <= 0) && (short_arg.size() <= 0))
        throw ArgumentException("at least one of long option or short option should be non-empty");
    if(long_arg.size() > 0)
    {
        if(argmap.find(long_arg) != argmap.end())
            throw ArgumentException(std::string("option ") + long_arg + " is duplicated");
        else if(long_arg[0] == '-')
            throw ArgumentException("long option should not start with -");
    }
    if(short_arg.size() > 0)
    {
        if(argmap.find(short_arg) != argmap.end()))
            throw ArgumentException("option " + short_arg + " is duplicated");
        else if(short_arg[0] == '-')
            throw ArgumentException(" short option should not start with -");  
    }
    if(name.size() <= 0)
        throw ArgumentException("option name is empty");
    else
    {
        if(name[0] == '-')
            throw ArgumentException("option name should not start with -");
        else if(options.find(name) != options.end())
            throw ArgumentException(std::string("option name ")s + name + " is duplicated");
    }
}

template <typename ValType>
void ArgumentParser::add_argument<ValType>(const std::string& name,
                      const std::string& long_arg,
                      const std::string& short_arg,
                      const std::string& help,
                      const ValType& defval,
                      int nargs = 1,
                      bool flag,
                      bool required = false)
{
    check_option(name, long_arg, short_arg);
    
    Argument<ValType>* arg = new Argument<ValType>;
    arg->name = name;
    arg->short_arg = std::string("-") + short_arg;
    arg->long_arg = std::string("--") + long_arg;
    arg->help = help;
    arg->given = false;
    arg->flag = flag;
    arg->required = required;
    arg_defvalue = defval;
    options[arg->name] = arg;
    if(arg->long_arg.size() > 0)
        argmap[arg->long_arg] = arg;
    if(arg->short_arg.size() > 0)
        argmap[arg->short_arg] = arg;
}

void ArgumentParser::add_flag(const std::string& name, 
                              const std::string& long_arg,
                              const std::string& short_arg,
                              const std::string& help,
                              bool defvalue = false)
{
    add_argument<bool>(name, long_arg, short_arg, help,
                       false, 1, true, false);
}

enum ArgParseStatus {ARG_START, ARG_OPTSTART, ARG_OPTEND};

void ParseArguments(int argc, char** argv)
{
    ArgParseStatus status = ARG_START;
    for(int i = 1; i < argc; i ++)
    {
        // empty argument
        size_t arglen = strlen(argv[i]);
        string option;
        string valud;
        bool valid = true;
        if(arglen == 0)
            valid = false;
        else if(argv[i][0] == '-')
        {
            if(arglen == 1)
                valid = false;
            // short option
            else if(argv[i][1] != '-')
                option = *(argv[i] + 1);
            // long option
            else
            {
                if(arglen == 2)
                    valid = false;
                else
                    option = *(argv[i] + 2);
            }
        }
        // value
        else
            value = argv[i];
            
    }
}
