#include <iostream>
#include <algorithm>
#include <string>
using namespace std;
#include <stdio.h>
#include <stdlib.h>
// KMC api
#include <kmc_api/kmc_file.h>
#include "HDF5Utils.h"
#include "Statistics.h"

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Dump k-mers in a KMC file from an index file" << endl;
        cerr << "Usage: " << argv[0] << " kmcfile index_file" << endl;
        cerr << "\tindex_file: an HDF5 file that contains a vector of type size_t" << endl;
        exit(1);
    }

    string kmcfile(argv[1]);
    string index_file(argv[2]);

    CKMCFile kmc_db;
    CKMCFileInfo kmcinfo;

    //cerr << "Read KMC database" << endl;
    if(!kmc_db.OpenForListing(kmcfile))
    {
        cerr << "Error: cannot open the KMC database " << kmcfile << endl;
        exit(1);
    }
    kmc_db.Info(kmcinfo);

    vector<size_t> indices;
    ReadVectorFromHDF5<size_t>(index_file, "data", indices);
    // sort the indices in ascending order
    vector<size_t> indices_sorted(indices);
    std::sort(indices_sorted.begin(), indices_sorted.end());
    // get the original indices of the sorted index array
    vector<size_t> indices_ind(indices.size());
    RangeArray(indices_ind.begin(), indices.size()); 
    ArgSortAscend(indices.begin(), indices.size(), indices_ind.begin());
    // sequences
    vector<string> seqs(indices.size());

    CKmerAPI kmer(kmcinfo.kmer_length);
    char* seq = new char[kmcinfo.kmer_length + 1];
    seq[kmcinfo.kmer_length] = 0;
    uint32 count;

    for(size_t ki = 0, ii = 0; ki < kmcinfo.total_kmers; ki ++)
    {
        if(!kmc_db.ReadNextKmer(kmer, count))
            break;
        if(indices_sorted[ii] == ki)
        {
            seqs[indices_ind[ii]] = kmer.to_string();
            ii ++;
            if(ii >= indices.size())
                break;
        }
    }
    kmc_db.Close();

    for(size_t i = 0; i < indices.size(); i ++)
        cout << indices[i] << '\t' << seqs[i] << "\n";
    return 0;
}
