#ifndef __EIGENIO_H__
#define __EIGENIO_H__
#include <iostream>
#include <fstream>
#include <string>

#include <Eigen/Dense>
#include <Eigen/SVD>
using namespace Eigen;
#include <string.h>
#include <errno.h>
#include <stdint.h>

struct EigenHeader
{
    uint32_t magic;
    uint32_t headersize;
    uint64_t rows;
    uint64_t cols;
    uint32_t valsize;
    uint32_t isRowMajor;
};

template <typename Type>
bool SaveEigenMatrix(const Type& m, const std::string& filename)
{
    if(m.rows()*m.cols() < 1)
    {
        std::cerr << "Error: cannot save empty matrix" << std::endl;
        return false;
    }
    std::ofstream fout(filename.c_str(), std::ios::binary);
    if(!fout)
    {
        std::cerr << "Error: cannot open the output file " << filename
            << ": " << strerror(errno) << std::endl;
        return false;
    }
    EigenHeader header;
    memset(&header, 0, sizeof(header));
    header.magic = 0x45474945;
    header.headersize = sizeof(header);
    header.rows = m.rows();
    header.cols = m.cols();
    header.valsize = sizeof(m.data()[0]);
    header.isRowMajor = m.IsRowMajor;
    size_t datasize = header.rows*header.cols*header.valsize;

    fout.write(reinterpret_cast<const char*>(&header), sizeof(header));
    fout.write(reinterpret_cast<const char*>(m.data()), datasize);
    fout.close();
    
    return true;
}

template <typename Type>
bool LoadEigenMatrix(Type& m, const std::string& filename)
{
    std::ifstream fin(filename.c_str(), std::ios::binary);
    if(!fin)
    {
        std::cerr << "Error: cannot open the input file " << filename
            << ": " << strerror(errno) << std::endl;
        return false;
    }
    EigenHeader header;
    fin.read((char*)&header, sizeof(header));
    m.resize(header.rows, header.cols);
    if(sizeof(m.data()[0]) != header.valsize)
    {
        cerr << "Error: matrix value size does not match the matrix file" << endl;
        return false;
    }
    size_t datasize = header.rows*header.cols*header.valsize;
    fin.read((char*)m.data(), datasize);
    fin.close();
    return true;
}

#endif

