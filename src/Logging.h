#ifndef __LOGGING_H__
#define __LOGGING_H__
#include <iostream>
#include <pthread.h>

class LoggerStream
{
    friend void Endl(LoggerStream& l);
public:
    enum LogLevel {NOTSET = 0, DEBUG = 10, INFO = 20, WARN = 30, ERROR = 40, CRITICAL = 50};
    LoggerStream(std::ostream& os) : _os(os), _level(DEBUG), _threshold(NOTSET) {
        pthread_mutex_init(&_mutex, NULL);
    }
    ~LoggerStream() {
        pthread_mutex_destroy(&_mutex);
    }
    
    template <typename T>
    LoggerStream& operator<< (T v) { _os << v; return *this; }
    LoggerStream& operator<< (const std::string& v) { _os << v; return *this; }
    LoggerStream& operator<< (std::streambuf* sb) { _os << sb; return *this; }
    LoggerStream& operator<< (std::ostream& (*pf)(std::ostream&)) { _os << pf; return *this; }
    LoggerStream& operator<< (std::ios& (*pf)(std::ios&)) { _os << pf; return *this; }
    LoggerStream& operator<< (std::ios_base& (*pf)(std::ios_base&)) { _os << pf; return *this; }
    LoggerStream& operator<< (void (*pf)(LoggerStream&)) { pf(*this); return *this; }
    
    LoggerStream& SetLevel(LogLevel level) { _threshold = level; return *this; }
    LoggerStream& Debug()    { pthread_mutex_lock(&_mutex); _level = DEBUG; return *this; }
    LoggerStream& Info()     { pthread_mutex_lock(&_mutex); _level = INFO; return *this; }
    LoggerStream& Warn()     { pthread_mutex_lock(&_mutex); _level = WARN; return *this; }
    LoggerStream& Error()    { pthread_mutex_lock(&_mutex); _level = ERROR; return *this; }
    LoggerStream& Critical() { pthread_mutex_lock(&_mutex); _level = CRITICAL; return *this; }
    
private:
    std::ostream& _os;
    int _level;
    int _threshold;
    pthread_mutex_t _mutex;
};

extern LoggerStream logger;

inline void Endl(LoggerStream& l) {
    l << std::endl;
    pthread_mutex_unlock(&(l._mutex));
}

#endif
