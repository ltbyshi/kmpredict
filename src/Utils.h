#ifndef __UTILS_H__
#define __UTILS_H__
#include <sstream>
#include <string>
#include <map>
#include <stdlib.h>

#include "ProgressBar.h"
#include "BitSet.h"
#include "Exception.h"
#include "IOUtils.h"
#include "Statistics.h"
#include "Logging.h"
#include "Exception.h"

void Die(const char* format, ...);

// Null reference
template <typename Type>
inline Type& NullRef()
{
    return *((Type*)0);
}

// command line parsing
typedef int (*CommandHandler)(int , char**);
typedef std::map<std::string, CommandHandler> HandlerMap;
void ShowUsage(const HandlerMap& handlers);
int ParseArguments(const HandlerMap& handlers, const char* progname,
                    int argc, char** argv);


inline std::string ToBinaryString(unsigned int a)
{
    std::string s(32 + 3, '0');
    
    for(int i = 0, si = 0; i < 32; i ++, si ++)
    {
        if(a & 0x80000000)
            s[si] = '1';
        a <<= 1;
        if((i % 8 == 7) && (i != 31))
        {
            si ++;
            s[si] = ' ';
        }
    }   
    return s;
}

inline std::vector<std::string>& Split(const std::string& s,
        std::vector<std::string>& v,
        char delimiter='\t')
{
    std::istringstream is(s);
    std::string tok;
    while(std::getline(is, tok, delimiter))
        v.push_back(tok);
    return v;
}

inline std::vector<std::string> Split(const std::string& s,
        char delimiter='\t')
{
    std::istringstream is(s);
    std::string tok;
    std::vector<std::string> v;
    while(std::getline(is, tok, delimiter))
        v.push_back(tok);
    return v;
}

template <typename Type>
inline std::string ToString(Type val)
{
    return std::to_string(val);
}

// JSON
#include <rapidjson/document.h> 
void ReadJSON(const std::string& filename, rapidjson::Document& json);

template <typename ValType>
inline bool GetEnv(const std::string& name, ValType& value)
{
    char* vs = getenv(name.c_str());
    if(vs)
    {
        ValType v;
        std::istringstream is(vs);
        is >> v;
        if(is)
        {
            value = v;
            return true;
        }
        else
            return false;
    }
    else
        return false;
}

template <> 
inline bool GetEnv<std::string>(const std::string& name, std::string& value)
{
    char* vs = getenv(name.c_str());
    if(vs)
    {
        value = vs;
        return true;
    }
    else
        return false;
}

inline bool GetPETaskParams(int& task_id, int& task_num)
{
    int pe_task_id, pe_task_num;
    if(GetEnv<int>("pe_task_id", pe_task_id) &&
       GetEnv<int>("pe_task_num", pe_task_num))
    {
        if(pe_task_num <= 0)
            throw ValueError("Environment variable 'pe_task_num' should be larger than 0");
        if((pe_task_id < 0) || (pe_task_id >= pe_task_num))
            throw ValueError("Environment variable 'pe_task_id' should be between 0 and (pe_task_num - 1)");
        task_id = pe_task_id;
        task_num = pe_task_num;
        return true;
    }
    else
        return false;
}

#endif
