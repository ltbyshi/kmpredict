#include <iostream>
#include <fstream>
#include <list>
using namespace std;
#include "omp.h"
// Standard C library
#include <stdlib.h>
// User library
#include "ProgressBar.h"
#include "Logging.h"
#include "Utils.h"
#include "HDF5Utils.h"
#include "IOUtils.h"

template <typename ValType, typename MatrixValType, bool RowMajor = true, bool Normalize = false>
void CreateMatrixFromVectors(const std::vector<std::string>& filenames,
                             const std::string& vector_dsname,
                             const std::string& matrixfile,
                             const std::string& matrix_dsname)
{
    logger.Debug() << "Options:\n"
        << "RowMajor = " << RowMajor << "\n"
        << "Normalize = " << Normalize << Endl;
    size_t nrow = 0, ncol = 0;
    if(RowMajor)
        nrow = filenames.size();
    else
        ncol = filenames.size();

    MatrixValType* data = NULL;

    ProgressBar progressbar(filenames.size());
    for(size_t i = 0; i < filenames.size(); i ++)
    {
        H5File h5file(filenames[i], H5F_ACC_RDONLY);
        DataSet dataset = h5file.openDataSet(vector_dsname);
        DataType memtype = InferDataType<ValType>();
        // determine the length of each vector
        hsize_t dims[1];
        DataSpace dataspace = dataset.getSpace();
        int ndim = dataspace.getSimpleExtentNdims();
        if(ndim != 1)
            Die("number of dimensions differs from 1 in HDF5 file (%s, '%s')",
                    filenames[i], vector_dsname);
        dataspace.getSimpleExtentDims(dims, NULL);
        if(i == 0)
        {
            if(RowMajor)
                ncol = dims[0];
            else
                nrow = dims[0];
            data = new MatrixValType[ncol*nrow];
        }
        else
        {
            bool consistent;
            if(RowMajor)
                consistent = (dims[0] == ncol);
            else
                consistent = (dims[0] == nrow);
            if(!consistent)
                Die("number of elements differs from the first file in (%s, '%s')",
                        filenames[i], vector_dsname);
        }
        if(RowMajor && !Normalize)
        {
            dataset.read(data + i*ncol, memtype);
        }
        else
        {
            ValType* tdata = new ValType[nrow];
            dataset.read(tdata, memtype);
            double sum = 0;
            if(Normalize)
            {
                unsigned long sumi = 0;
                for(size_t isrc = 0; isrc < dims[0]; isrc ++)
                    sumi += tdata[isrc];
                sum = double(sumi)/dims[0];
                //logger.Debug() << "\n" << sum << Endl;
            }
            if(RowMajor && Normalize)
            {
                size_t offset = i*ncol;
                for(size_t isrc = 0; isrc < ncol; isrc ++)
                    data[isrc + offset] = double(tdata[isrc])/sum;
            }
            else if(!RowMajor && !Normalize)
            {
                for(size_t isrc = 0, idest = i; isrc < nrow; isrc ++, idest += ncol)
                    data[idest] = tdata[isrc];
            }
            else if(!RowMajor && Normalize)
            {
                for(size_t isrc = 0, idest = i; isrc < nrow; isrc ++, idest += ncol)
                    data[idest] = double(tdata[isrc])/sum;
            }
            delete[] tdata;
        }
        dataset.close();
        h5file.close();
        progressbar.Increment(1);
    }
    progressbar.Finish();

#if 0
    for(size_t i = 0; i < 30; i ++)
    {
        for(size_t j = 0; j < 4; j ++)
        {
            if(RowMajor)
                cout << data[i*ncol + j] << " ";
            else
                cout << data[j*nrow + i] << " ";
        }
        cout << endl;
    }
    return;
#endif

    if(data)
    {
        // write matrix
        H5File h5file(matrixfile, H5F_ACC_TRUNC);
        hsize_t dims[2];
        dims[0] = nrow;
        dims[1] = ncol;
        DataSpace dataspace(2, dims);
        DataType memtype = InferDataType<MatrixValType>();
        DataSet dataset = h5file.createDataSet(matrix_dsname, memtype, dataspace);
        dataset.write(data, memtype);
        dataset.close();
        h5file.close();
        delete[] data;
    }
}

typedef void (*CreateMatrixFromVectors_func)(const std::vector<std::string>& filenames,
                             const std::string& vector_dsname,
                             const std::string& matrixfile,
                             const std::string& matrix_dsname);
CreateMatrixFromVectors_func CreateMatrixFromVectors(bool row_major, bool normalize)
{
    if(row_major && !normalize)
        return CreateMatrixFromVectors<unsigned int, unsigned int, true, false>;
    else if(!row_major && !normalize)
        return CreateMatrixFromVectors<unsigned int, unsigned int, false, false>;
    else if(row_major && normalize)
        return CreateMatrixFromVectors<unsigned int, float, true, true>;
    else if(!row_major && normalize)
        return CreateMatrixFromVectors<unsigned int, float, false, true>;
    return NULL;
}

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments" << endl;
        cerr << "Usage: CreateKmerMatrix input_filelist matrix_file" << endl;
        cerr << "Environment variables: vector_dsname, matrix_dsname, datatype" << endl;
        exit(1);
    }
    string input_filelist(argv[1]);
    string matrix_file(argv[2]);
    // options
    string vector_dsname = "data";
    string matrix_dsname = "data";
    string vector_dtype = "uint";
    string matrix_dtype = "uint";
    int row_major = 0;
    int normalize = 1;
    
    GetEnv<string>("vector_dsname", vector_dsname);
    GetEnv<string>("matrix_dsname", matrix_dsname);
    GetEnv<string>("vector_dtype", vector_dtype);
    GetEnv<string>("matrix_dtype", matrix_dtype);
    GetEnv<int>("row_major", row_major);
    GetEnv<int>("normalize", normalize);

    logger.Info() << "Options:\n"
        << "vector_dsname = " << vector_dsname << "\n"
        << "matrix_dsname = " << matrix_dsname << "\n"
        << "row_major = " << row_major << "\n"
        << "normalize = " << normalize << Endl;

    vector<string> filenames;
    FileInputStream fin(input_filelist.c_str());
    while(!fin.eof())
    {
        string line;
        getline(fin, line);
        if(line.size() > 0)
            filenames.push_back(line);
    }
    fin.close();
    
    CreateMatrixFromVectors(row_major, normalize)(filenames, vector_dsname, matrix_file, matrix_dsname);
    return 0;
}
