#ifndef __HYPERPLANEHASH_H__
#define __HYPERPLANEHASH_H__
#include <iostream>
#include <string>
#include <complex>
#include <cstdlib>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/SVD>
using namespace Eigen;

#include "Nucleotide.h"
#include "EigenUtils.h"

template <typename KmerType>
class HyperplaneHash
{
public:
    HyperplaneHash(int k, int hashsize);
    ~HyperplaneHash() {}
    void CreateHyperplanes(int nRandomKmers = 1000);
    void GenRandomKmers(MatrixXcd& m);
    bool SaveHyperplanes(const std::string& filename) const;
    bool LoadHyperplanes(const std::string& filename);
    KmerType Hash(const char* s) const;
    KmerType Hash(const std::vector<const char*>& seqs) const;
    KmerType Hash(const char* s, VectorXcd& code) const;
    inline KmerType operator()(const char* s) const {return Hash(s);}
    VectorXcd Encode(const char* s) const;
    void Encode(const char* s, VectorXcd& code) const;
protected:
    void InitTranstable();
private:
    int _k;
    int _hashsize;
    KmerType _tablesize;
    MatrixXcd _W;
    VectorXcd _w0;
    VectorXcd _transtable;
};

template <typename KmerType>
HyperplaneHash<KmerType>::HyperplaneHash(int k, int hashsize)
{
    _k = k;
    _hashsize = hashsize;
    _tablesize = (1 << (2*hashsize));
    //_W.resize(hashsize, k);
    //_w0.resize(hashsize);
    InitTranstable();
}

template <typename KmerType>
void HyperplaneHash<KmerType>::CreateHyperplanes(int nRandomKmers)
{
    _W.resize(_hashsize, _k);
    _w0.resize(_hashsize);
    for(int h = 0; h < _hashsize; h ++)
    {
        MatrixXcd m(nRandomKmers + 1, _k + 1);
        GenRandomKmers(m);
        for(int i = 0; i < nRandomKmers; i ++)
            m(i, _k) = std::complex<double>(-1, 0);
        for(int i = 0; i <= _k; i ++)
            m(nRandomKmers, i) = std::complex<double>(0, 0);
        // PCA
        JacobiSVD<MatrixXcd> svd(m, ComputeThinV);
        MatrixXcd V = svd.matrixV();
        for(int i = 0; i < _k; i ++)
            _W(h, i) = V(i, _k);
        _w0(h) = V(_k, _k);
    }
}

template <typename KmerType>
void HyperplaneHash<KmerType>::GenRandomKmers(MatrixXcd& m)
{
    for(int i = 0; i < m.rows(); i ++)
        for(int j = 0; j < m.cols(); j ++)
            m(i, j) = _transtable(int(DNAStr[rand() % 3]));
}

template <typename KmerType>
KmerType HyperplaneHash<KmerType>::Hash(const char* s) const
{
    VectorXcd code = Encode(s);
    VectorXcd t = _W*code - _w0;
    KmerType hashed = 0;
    for(int i = 0; i < _hashsize; i ++)
    {
        hashed <<= 1;
        if((t(i).real() > 0) && (t(i).imag() > 0))
            hashed += 1;
    }
    return hashed;
}

template <typename KmerType>
KmerType HyperplaneHash<KmerType>::Hash(const char* s, VectorXcd& code) const
{
    Encode(s, code);
    VectorXcd t = _W*code - _w0;
    KmerType hashed = 0;
    for(int i = 0; i < _hashsize; i ++)
    {
        hashed <<= 1;
        if((t(i).real() > 0) && (t(i).imag() > 0))
            hashed += 1;
    }
    return hashed;
}

template <typename KmerType>
void HyperplaneHash<KmerType>::InitTranstable()
{
    _transtable.resize(256);
    _transtable.fill(std::complex<double>(0, 0));
    _transtable(int('A')) = std::complex<double>(0.995, 0);
    _transtable(int('C')) = std::complex<double>(0, 0.995);
    _transtable(int('G')) = std::complex<double>(0, -0.995);
    _transtable(int('T')) = std::complex<double>(-0.995, 0);
}

template <typename KmerType>
VectorXcd HyperplaneHash<KmerType>::Encode(const char* s) const
{
    VectorXcd code(_k);
    for(int i = 0; i < _k; i ++)
        code(i) = _transtable(int(s[i]));
    return code;
}

template <typename KmerType>
void HyperplaneHash<KmerType>::Encode(const char* s, VectorXcd& code) const
{
    for(int i = 0; i < _k; i ++)
        code(i) = _transtable(int(s[i]));
}

template <typename KmerType>
bool HyperplaneHash<KmerType>::LoadHyperplanes(const std::string& filename)
{
    bool status = true;
    status &= LoadEigenMatrix(_W, filename + ".W");
    status &= LoadEigenMatrix(_w0, filename + ".w0");
    status &= (_W.rows() == _hashsize);
    status &= (_W.cols() == _k);
    status &= (_w0.rows() == _hashsize);
    if(!status)
    {
        std::cerr << "Error: cannot load the hyperplanes from " << filename << std::endl;
        return false;
    }
    else
        return true;
}

template <typename KmerType>
bool HyperplaneHash<KmerType>::SaveHyperplanes(const std::string& filename) const
{
    bool status = true;
    status &= SaveEigenMatrix(_W, filename + ".W");
    status &= SaveEigenMatrix(_w0, filename + ".w0");
    if(!status)
        std::cerr << "Error: cannot save the hyperplanes to " << filename << std::endl;
    return status;
}


#endif
