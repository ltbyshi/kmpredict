#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
#include <stdlib.h>
#include <time.h>

// rapidjson's DOM-style APIs
#include <rapidjson/document.h>     
using rapidjson::Document;
using rapidjson::Value;
using rapidjson::SizeType;

// user library
#include "Kmer.h"
#include "Utils.h"
#include "Logging.h"
#include "HDF5Utils.h"
#include "ProgressBar.h"
#include "Statistics.h"
#include "Exception.h"

#define PROGNAME "CalculateRelevance"
template <typename Type>
static ostream& operator<<(ostream& out, const vector<Type>& x)
{
    for(size_t i = 0; i < x.size(); i ++)
    {
        if(i > 0)
            out << ", ";
        out << x[i];
    }
    return out;
}

typedef float NormedCountType;
// m: kmer frequent matrix (nkmers, nsamples)
// nsamples: total number of samples
// samples: sample indices for the task
// groups: group labels (-1's and 1's) for the task
// FilterMetric: 0 - Correlation, 1 - MannWhitneyUTest
// 2 - MannWhitneyUTest+Frequency (requires at least half of the samples that belong to a class contain the k-mer)
// 3 - Difference of relative frequency |x1 - x2| x1, x2: relative frequency in the group
template <int FilterMetric>
void CalculateRelevanceForCounts(const NormedCountType* m,
                                 int nsamples, size_t nkmers,
                                 const vector<int>& samples,
                                 const vector<int>& groups,
                                 vector<float>& relevances)
{
    vector<NormedCountType> counts(samples.size());
    vector<float> group_values(samples.size());
    for(size_t i = 0; i < samples.size(); i ++)
        group_values[i] = groups[i];

    // for MannWhitneyUTest
    size_t group_size1 = 0, group_size2 = 0;
    vector<double> data_mwu;
    // calculate group sizes
    for(size_t i = 0; i < samples.size(); i ++)
    {
        if(groups[i] == -1)
            group_size1 ++;
        else
            group_size2 ++;
    }
    // create two vectors for each group that store the indices of the samples
    vector<size_t> indices1(group_size1);
    vector<size_t> indices2(group_size2);
    for(size_t i = 0, i1 = 0, i2 = 0; i < samples.size(); i ++)
    {
        if(groups[i] == -1)
            indices1[i1++] = i;
        else
            indices2[i2++] = i;
    }
    if((FilterMetric == 1) || (FilterMetric == 2))
    {
        data_mwu.resize(samples.size());
#if _DEBUG
        cout << "group_size1 = " << group_size1 << endl;
        cout << "group_size2 = " << group_size2 << endl;
        cout << "group1 = [" << indices1 << "]" << endl;
        cout << "group2 = [" << indices2 << "]" << endl;
#endif
    }
    // for MWUTestFreq
    int min_freq1 = group_size1/2;
    int min_freq2 = group_size2/2;
#if _DEBUG
    size_t nshow = 0;
    cout << "samples: " << samples << endl;
#endif
    MannWhitneyUTest mwutest(group_size1, group_size2);
    for(size_t kmer = 0, im = 0; kmer < nkmers; kmer ++, im += nsamples)
    {
        for(size_t is = 0; is < samples.size(); is ++)
            counts[is] = m[im + samples[is]];
        
        float relevance;
        if(FilterMetric == 0)
            relevance = Correlation(counts, group_values, samples.size());
        else if((FilterMetric == 1) || (FilterMetric == 2))
        {
            for(size_t i = 0; i < group_size1; i ++)
                data_mwu[i] = counts[indices1[i]];
            for(size_t i = 0; i < group_size2; i ++)
                data_mwu[i + group_size1] = counts[indices2[i]];
            relevance = mwutest(data_mwu.begin());
            if(FilterMetric == 2)
            {
                int freq1 = 0, freq2 = 0;
                for(size_t i = 0; i < group_size1; i ++)
                    if(counts[indices1[i]] > 0)
                        freq1 ++;
                for(size_t i = 0; i < group_size2; i ++)
                    if(counts[indices2[i]] > 0)
                        freq2 ++;
                // set p-value to a large value to exclude the k-mer during filtering
                if((freq1 < min_freq1) && (freq2 < min_freq2))
                    relevance = 10.0;
            }

        }
        else if(FilterMetric == 3)
        {
            int freq1 = 0, freq2 = 0;
            for(size_t i = 0; i < group_size1; i ++)
                if(counts[indices1[i]] > 0)
                    freq1 ++;
            for(size_t i = 0; i < group_size2; i ++)
                if(counts[indices2[i]] > 0)
                    freq2 ++;
            double freq_r1 = double(freq1)/group_size1;
            double freq_r2 = double(freq2)/group_size2;
            relevance = fabs(freq_r1 - freq_r2);
        }
        relevances[kmer] = relevance;
#if _DEBUG
        cout << "[" << kmer << "]" << endl;
        cout << "counts = [" << counts << "]" << endl;
        cout << "groups = [" << group_values << "]" << endl;
        cout << "relevance = " << relevance << endl;
        nshow ++;
        if(nshow > 10)
            break;
#endif
    }
}

typedef void (*CalculateRelevanceForCounts_f)(const NormedCountType* m,
                int nsamples, size_t nkmers,
                const vector<int>& samples,
                const vector<int>& groups,
                vector<float>& relevances);

CalculateRelevanceForCounts_f CalculateRelevanceForCounts(const string& filter_metric)
{
    if(filter_metric == "Correlation") return CalculateRelevanceForCounts<0>;
    else if(filter_metric == "MannWhitneyUTest") return CalculateRelevanceForCounts<1>;
    else if(filter_metric == "MWUTestFreq") return CalculateRelevanceForCounts<2>;
    else if(filter_metric == "FreqDiff") return CalculateRelevanceForCounts<3>;
    return NULL;
}

void GetTaskRange(size_t n, size_t n_tasks, size_t taskid, size_t& start, size_t& end)
{
    size_t unitsize = size_t(ceil(double(n)/n_tasks));
    start = unitsize*taskid;
    end = std::min(n, start + unitsize);
}

void CalculateRelevanceTask(const string& param_file,
                            size_t n_tasks,
                            size_t taskid)
{
    Document p;
    ReadJSON(param_file, p);
    // read kmerfile lists, groups
    const Value& sample_info = p["SampleInfo"];
    size_t nsamples = sample_info.Size();
    vector<string> kmerfiles(nsamples);
    vector<int> groups(nsamples);
    for(SizeType i = 0; i < sample_info.Size(); i ++)
    {
        kmerfiles[i] = sample_info[i]["KmerCountFile"].GetString();
        groups[i] = sample_info[i]["Group"].GetInt();
    }
    // get hash size
    vector<size_t> dims = GetHDF5DatasetDims(kmerfiles[0], "data");
    size_t hashsize = dims[0];
    logger.Info() << "HashSize: " << hashsize << Endl;

    // read average counts or total number of k-mers
    const Value& avgcounts_p = p["AverageCoverage"];
    vector<float> avgcounts(avgcounts_p.Size());
    for(SizeType i = 0; i < avgcounts_p.Size(); i ++)
        avgcounts[i] = avgcounts_p[i].GetDouble();

    // get task range
    size_t start, end;
    GetTaskRange(hashsize, n_tasks, taskid, start, end);
    size_t nkmers = end - start;
    logger.Info() << "Task range: " << nkmers << "(" << start << ", " << end << ")" << Endl;
    // read matrix (only part of the counts for the task)
    logger.Info() << "Read kmer counts: started" << Endl;
    NormedCountType* m = new NormedCountType[nsamples*nkmers]; // shape: (nkmers, nsamples)
    DefaultValType* counts = new DefaultValType[nkmers];
    for(size_t i = 0; i < nsamples; i ++)
    {
        ReadVectorFromHDF5<DefaultValType>(kmerfiles[i], "data", counts, start, nkmers);
        for(size_t kmer = 0, im = i; kmer < nkmers; kmer ++, im += nsamples)
            m[im] = double(counts[kmer])/avgcounts[i];
    }
#if _DEBUG
    cout << "m =" << endl;
    for(size_t i = 0; i < 50; i ++)
    {
        cout << "[" << i << "] ";
        for(size_t j = 0; j < nsamples; j ++)
            cout << m[i*nsamples + j] << " ";
        cout << endl;
    }
#endif
    logger.Info() << "Read kmer counts: finished" << Endl;
    delete[] counts;

    for(SizeType i = 0; i < p["Params"].Size(); i ++)
    {
        const Value& params = p["Params"][i];
#if DEBUG_RUN_ONE_PARAM
        string kmerlist_file;
        if(!GetEnv("kmerlist_file", kmerlist_file))
            throw ValueError("Environment variable 'kmerlist_file' has not been set");
        if(params["KmerListFile"].GetString() != kmerlist_file)
            continue;
        cout << "Only run " << kmerlist_file << endl;
#endif
        size_t nsamples_task = params["TrainIndex"].Size();
        vector<int> samples_task(nsamples_task);
        vector<int> groups_task(nsamples_task);
        for(SizeType sample = 0; sample < nsamples_task; sample ++)
        {
            samples_task[sample] = params["TrainIndex"][sample].GetInt();
            groups_task[sample] = groups[sample];
        }
        vector<float> relevances(nkmers);
        string filter_metric = params["FilterMetric"].GetString();

        CalculateRelevanceForCounts(filter_metric)(m, nsamples, nkmers, samples_task, groups_task, relevances);
        // save to file
        ostringstream rele_file_s;
        rele_file_s << params["RelevanceFile"].GetString() << "_" << taskid;
        string rele_file = rele_file_s.str();
        logger.Info() << "WriteRelevanceFile: " << rele_file << Endl;
        MakeDirs(DirName(rele_file));
        WriteVectorToHDF5<float>(rele_file, "data", relevances);
    }
    delete[] m;
} 

// n_tasks: number of splited relevance files
// pe_task_num, pe_task_id: split tasks and only run tasks that belong to the task id
void SelectKmers(string& param_file, int n_tasks)
{
    Document p;
    ReadJSON(param_file, p);
    // read kmerfile lists, groups
    const Value& sample_info = p["SampleInfo"];
    size_t nsamples = sample_info.Size();
    vector<string> kmerfiles(nsamples);
    vector<int> groups(nsamples);
    for(SizeType i = 0; i < sample_info.Size(); i ++)
    {
        kmerfiles[i] = sample_info[i]["KmerCountFile"].GetString();
        groups[i] = sample_info[i]["Group"].GetInt();
    }
    // get hash size
    vector<size_t> dims = GetHDF5DatasetDims(kmerfiles[0], "data");
    size_t hashsize = dims[0];
    logger.Info() << "HashSize: " << hashsize << Endl;

    // check environment to determine whether to run as parallel jobs
    int pe_task_num, pe_task_id;
    bool enable_pe = false;
    if(GetEnv<int>("pe_task_num", pe_task_num) && 
       GetEnv<int>("pe_task_id", pe_task_id))
    {
        logger.Info() << "Parallel environment enabled" << Endl;
        enable_pe = true;
    }

    // check if to create random list
    bool all_random = true;
    for(SizeType i = 0; i < p["Params"].Size(); i ++)
    {
        if(!p["Params"][i].HasMember("Random"))
            all_random = false;
        else
        {
            if(!p["Params"][i]["Random"].GetBool())
                all_random = false;
        }
        if(!all_random)
            break;
    }

    float* relevances = NULL;
    DefaultKmerType* indices = NULL;
    if(!all_random)
    {
        relevances = new float[hashsize];
        indices = new DefaultKmerType[hashsize];
    }
    for(SizeType i = 0; i < p["Params"].Size(); i ++)
    {
        if(enable_pe)
        {
            if((int(i) % pe_task_num) != pe_task_id)
                continue;
        }
        const Value& params = p["Params"][i];
        int kmer_rank_min = params["KmerRankMin"].GetInt();
        int kmer_rank_max = params["KmerRankMax"].GetInt();
    
#if _DEBUG
        string kmerlist_file;
        if(!GetEnv<string>("kmerlist_file", kmerlist_file))
            throw ValueError("Environment variable 'kmerlist_file' is not set");
        if(params["KmerListFile"].GetString() != kmerlist_file)
            continue;
        cout << "KmerListFile: " << kmerlist_file << endl;
#endif
        if(all_random)
        { 
            vector<DefaultKmerType> selected(kmer_rank_max);
            for(size_t ik = 0; ik < kmer_rank_max; ik ++)
                selected[ik] = random() % hashsize;
            WriteVectorToHDF5<DefaultKmerType>(params["KmerListFile"].GetString(),
                    "data", selected);
        }
        else
        {
            logger.Info() << "Process param set " << i << "\n"
                          << "Read relevance files" << Endl;
            for(int taskid = 0; taskid < n_tasks; taskid ++)
            {
                ostringstream rele_file_s;
                rele_file_s << params["RelevanceFile"].GetString() << "_" << taskid;
                string rele_file = rele_file_s.str();

                size_t start, end;
                GetTaskRange(hashsize, n_tasks, taskid, start, end);
                size_t nkmers = end - start;
                vector<size_t> dssize = GetHDF5DatasetDims(rele_file, "data");
                if(dssize[0] != nkmers)
                    throw ValueError("Wrong size in relevance file: " + rele_file);
                ReadVectorFromHDF5<float>(rele_file, "data", (float*)(relevances + start));
            }
            // initial indices
            for(size_t i = 0; i < hashsize; i ++)
                indices[i] = i;
            if(params.HasMember("SortReleAscend") && params["SortReleAscend"].GetBool())
            {
                logger.Info() << "Sort by relevance (ascending order)" << Endl;
                std::sort(indices, indices + hashsize, ArgLess<float*>(relevances));
            }
            else
            {
                logger.Info() << "Sort by relevance (descending order)" << Endl;
                std::sort(indices, indices + hashsize, ArgGreater<float*>(relevances));
            }
            vector<DefaultKmerType> selected(kmer_rank_max - kmer_rank_min);
            for(int ki = kmer_rank_min, si = 0; ki < kmer_rank_max; ki ++, si ++)
                selected[si] = indices[ki];
#if _DEBUG
            cout << "Sorted relevances" << endl;
            for(size_t si = 0; si < 10; si ++)
                cout << "[" << selected[si] << "] " << relevances[selected[si]] << endl;
            continue;
#endif
            string kmer_list_file = params["KmerListFile"].GetString();
            logger.Info() << "Write kmer list: " << kmer_list_file << Endl;
            MakeDirs(DirName(kmer_list_file));
            WriteVectorToHDF5<DefaultKmerType>(params["KmerListFile"].GetString(),
                   "data", selected);
        }
    }
    if(!all_random)
    {
        delete[] relevances;
        delete[] indices;
    }
}

// read a k-mer matrix file and calculate relevances based on a given filter metric
void CheckKmerMatrix(const string& param_file, const string& kmermat_file, size_t n_kmers)
{
    Document p;
    ReadJSON(param_file, p);
    // read kmerfile lists, groups
    const Value& sample_info = p["SampleInfo"];
    size_t nsamples = sample_info.Size();
    vector<string> kmerfiles(nsamples);
    vector<int> groups(nsamples);
    for(SizeType i = 0; i < sample_info.Size(); i ++)
    {
        groups[i] = sample_info[i]["Group"].GetInt();
    }

    for(SizeType i = 0; i < p["Params"].Size(); i ++)
    {
        const Value& params = p["Params"][i];
        if(kmermat_file != params["KmerMatrixFile"].GetString())
            continue;
        size_t nsamples_task = params["TrainIndex"].Size();
        vector<int> samples_task(nsamples_task);
        vector<int> groups_task(nsamples_task);
        
        for(SizeType sample = 0; sample < nsamples_task; sample ++)
        {
            samples_task[sample] = params["TrainIndex"][sample].GetInt();
            groups_task[sample] = groups[sample];
        }
        
        vector<NormedCountType> kmer_mat;
        hsize_t nrow, ncol;
        // nrow: number of samples, ncol: number of k-mers
        ReadMatrixFromHDF5<NormedCountType>(kmermat_file, "data",
                                            ncol, nrow,  kmer_mat);
        cout << "Number of samples in kmer matrix: " << ncol << endl;
        vector<float> relevances(n_kmers);
        string filter_metric = params["FilterMetric"].GetString();
        cout << "FilterMetric: " << filter_metric << endl;
        CalculateRelevanceForCounts(filter_metric)(kmer_mat.data(), nsamples, n_kmers,
                samples_task, groups_task, relevances);
        for(size_t ki = 0; ki < n_kmers; ki ++) 
        {
            cout << "[" << ki << "]";
            for(size_t si = 0; si < 10; si ++)
                cout << " " << kmer_mat[ki*nsamples + si];
            cout << " (" << relevances[ki] << ")" << endl;
        }
    }
}

// read a given k-mer list file and k-mer count files
// calculate relevances of the first n_kmers based on the given filter metric
void CheckKmerList(const string& param_file, const string& kmerlist_file, size_t n_kmers)
{
    Document p;
    ReadJSON(param_file, p);
    // read kmerfile lists, groups
    const Value& sample_info = p["SampleInfo"];
    size_t nsamples = sample_info.Size();
    vector<string> kmerfiles(nsamples);
    vector<int> groups(nsamples);
    for(SizeType i = 0; i < sample_info.Size(); i ++)
        groups[i] = sample_info[i]["Group"].GetInt();
    
    // read average counts
    const Value& avgcounts_p = p["AverageCoverage"];
    vector<float> avgcounts(avgcounts_p.Size());
    for(SizeType i = 0; i < avgcounts_p.Size(); i ++)
        avgcounts[i] = avgcounts_p[i].GetDouble();
    
    for(SizeType i = 0; i < p["Params"].Size(); i ++)
    {
        const Value& params = p["Params"][i];
        if(kmerlist_file != params["KmerListFile"].GetString())
            continue;
        size_t nsamples_task = params["TrainIndex"].Size();
        vector<int> samples_task(nsamples_task);
        vector<int> groups_task(nsamples_task);
        
        for(SizeType sample = 0; sample < nsamples_task; sample ++)
        {
            samples_task[sample] = params["TrainIndex"][sample].GetInt();
            groups_task[sample] = groups[sample];
        }
        
        hsize_t* coord = new hsize_t[n_kmers];
        vector<size_t> kmerlist;
        bool get_top = false;
        if(GetEnv<bool>("get_top", get_top) & get_top)
        {
            kmerlist.resize(n_kmers);
            for(size_t ki = 0; ki < n_kmers; ki ++)
                kmerlist[ki] = ki;
        }
        else
        {
            ReadVectorFromHDF5<size_t>(kmerlist_file, "data", kmerlist);
        }

        std::copy(kmerlist.begin(), kmerlist.begin() + n_kmers, coord);
        vector<NormedCountType> kmer_mat(nsamples*n_kmers);
        DefaultValType* counts = new DefaultValType[n_kmers];
        for(size_t si = 0; si < nsamples; si ++)
        {
            ReadVectorFromHDF5<DefaultValType>(p["SampleInfo"][si]["KmerCountFile"].GetString(),
                    "data", counts, n_kmers, coord);
            for(size_t ki = 0; ki < n_kmers; ki ++)
                kmer_mat[ki*nsamples + si] = double(counts[ki])/avgcounts[si];
        }
        delete[] counts;
        delete[] coord;

        vector<float> relevances(n_kmers);
        string filter_metric = params["FilterMetric"].GetString();
        cout << "FilterMetric: " << filter_metric << endl;
        CalculateRelevanceForCounts(filter_metric)(kmer_mat.data(), nsamples, n_kmers,
                samples_task, groups_task, relevances);
        for(size_t ki = 0; ki < n_kmers; ki ++) 
        {
            cout << "[" << ki << "] (" << kmerlist[ki] << ") ";
            for(size_t si = 0; si < 10; si ++)
                cout << " " << kmer_mat[ki*nsamples + si];
            cout << " (" << relevances[ki] << ")" << endl;
        }
    }
}

static const char* progname = NULL;

int main(int argc, char** argv)
{
    progname = argv[0];

    srandom(time(0));
    srand(time(0));

    if(argc < 2)
    {
        cerr << "Error: missing commands" << endl;
        cerr << "Usage: " << PROGNAME << " {calc,select,checkmat,checkkmerlist} args..." << endl;
        exit(1);
    }
    string command(argv[1]);
    argc -= 2;
    argv += 2;

    if(command == "calc")
    {
        if(argc != 3)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 << " param_file n_tasks taskid" << endl;
            exit(1);
        }
        string param_file(argv[0]);
        int n_tasks = atoi(argv[1]);
        int taskid = atoi(argv[2]);
        CalculateRelevanceTask(param_file, n_tasks, taskid);
    }
    else if(command == "select")
    {
        if(argc != 2)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 <<  " param_file n_tasks" << endl;
            cerr << "Environment variables: pe_task_id pe_task_num" << endl;
            cerr << "\tpe_task_id: range from 0 to (pe_task_num - 1)" << endl;
            exit(1);
        }
        string param_file(argv[0]);
        int n_tasks = atoi(argv[1]);
        SelectKmers(param_file, n_tasks);
    }
    else if(command == "checkmat")
    {
        if(argc != 3)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 <<  " param_file kmermat_file n_kmers" << endl;
            exit(1);
        }
        string param_file(argv[0]);
        string kmermat_file(argv[1]);
        int n_kmers = atoi(argv[2]);
        CheckKmerMatrix(param_file, kmermat_file, n_kmers);
    }
    else if(command == "checkkmerlist")
    {
        if(argc != 3)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 <<  " param_file kmerlist_file n_kmers" << endl;
            exit(1);
        }
        string param_file(argv[0]);
        string kmerlist_file(argv[1]);
        int n_kmers = atoi(argv[2]);
        CheckKmerList(param_file, kmerlist_file, n_kmers);
    }
    else
    {
        cerr << "Error: invalid command: " << command << endl;
        exit(1);
    }

    return 0;
}
