#include <iostream>
#include <vector>
#include <string>
using namespace std;
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
// KMC api
#include <kmc_api/kmc_file.h>

int main(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Usage: " << argv[0] << " kmcfile1" << endl;
        exit(1);
    }
    string kmcfile(argv[1]);

    CKMCFile kmc_db;
    CKMCFileInfo kmcinfo;

    //cerr << "Read KMC database" << endl;
    if(!kmc_db.OpenForListing(kmcfile))
    {
        cerr << "Error: cannot open the KMC database " << kmcfile << endl;
        exit(1);
    }
    kmc_db.Info(kmcinfo);
    // get total k-mer counts
    CKmerAPI kmer(kmcinfo.kmer_length);
    uint32 count;
    size_t total_counts = 0;
    while(kmc_db.ReadNextKmer(kmer, count))
        total_counts += count;
    
    cout << "kmer_length\t" << kmcinfo.kmer_length << "\n"
         << "counter_size\t" << kmcinfo.counter_size << "\n"
         << "lut_prefix_length\t" << kmcinfo.lut_prefix_length << "\n"
         << "signature_len\t" << kmcinfo.signature_len << "\n"
         << "min_count\t" << kmcinfo.min_count << "\n"
         << "max_count\t" << kmcinfo.max_count << "\n"
         << "both_strands\t" << kmcinfo.both_strands << "\n"
         << "total_kmers\t" << kmcinfo.total_kmers << "\n"
         << "total_counts\t" << total_counts
         << endl;
    kmc_db.Close();

    return 0;
}
