#ifndef __EXCEPTION_H__
#define __EXCEPTION_H__
#include <exception>
#include <string>
#include <string.h>
#include <errno.h>

class Exception: public std::exception {
public:
    explicit Exception(const std::string& what_arg, 
                       const std::string& except_type = "Exception") {
        msg += except_type + std::string(": ") + what_arg;
    }
    virtual const char* what() const noexcept { return msg.c_str(); }
private:
    std::string msg;
};

#define DEFINE_EXCEPTION(except_name) \
    class except_name: public Exception { \
    public: \
        explicit except_name(const std::string& what_arg) \
        : Exception(what_arg, #except_name) {} \
    };


class ValueError: public Exception {
public:
    explicit ValueError(const std::string& what_arg)
        : Exception(what_arg, "ValueError") {}
};

class TypeError: public Exception {
public:
    explicit TypeError(const std::string& what_arg)
        : Exception(what_arg, "TypeError") {}
};

class IOError: public std::exception {
public:
    explicit IOError(const std::string& what_arg, bool show_errno = true) {
        msg = std::string("IOError: ") + what_arg;
        if(show_errno)
            msg += std::string(": ") + strerror(errno); 
    }
    virtual const char* what() const noexcept { return msg.c_str(); }
private:
    std::string msg;
};

class JSONError: public std::exception {
public:
    explicit JSONError(const std::string& what_arg) { 
        msg = std::string("JSONError: ") + what_arg; 
    }
    virtual const char* what() const noexcept { return msg.c_str(); }
private:
    std::string msg;
};

class OSError: public std::exception {
public:
    explicit OSError(const std::string& what_arg, bool show_errno = true) { 
        msg = std::string("OSError: ") + what_arg;
        if(show_errno)
            msg += std::string(": ") + strerror(errno); 
    }
    virtual const char* what() const noexcept { return msg.c_str(); }
private:
    std::string msg;
};

#endif
