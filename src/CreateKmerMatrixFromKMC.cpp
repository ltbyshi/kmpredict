#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <ctype.h>

// KMC api
#include <kmc_api/kmc_file.h>
// User library
#include "BitSet.h"
#include "HDF5Utils.h"
#include "Logging.h"
#include "Utils.h"

const char* progname = NULL;

static bool IsSpace(char c) { return isspace(c); }

static vector<string> ReadLines(istream& is)
{
    vector<string> lines;
    while(!is.eof())
    {
        string line;
        getline(is, line);
        if(line.size() > 0 && !std::all_of(line.begin(), line.end(), IsSpace))
            lines.push_back(line);
    }
    return lines;
}

static void test_query(const string& kmcfile)
{
    CKMCFile kmc_db;
    CKMCFileInfo kmcinfo;

    cerr << "Read KMC database" << endl;
    if(!kmc_db.OpenForListing(kmcfile))
    {
        cerr << "Error: cannot open the KMC database " << kmcfile << endl;
        exit(1);
    }
    kmc_db.Info(kmcinfo);
    /*
    if(!(kmc_db.SetMinCount(1)))
    {
        cerr << "Error: cannot set min count" << endl;
        exit(1);
    }
    if(!kmc_db.SetMaxCount(UINT_MAX))
    {
        cerr << "Error: cannot set max count" << endl;
        exit(1);
    }*/
    CKmerAPI kmer_object(kmcinfo.kmer_length);

    uint32 counter;
    string kmerstr;
    cerr << "Read Kmers" << endl;
    uint64 nkmers = 0;

    vector<CKmerAPI*> queries(1000000);
    for(size_t i = 0; i < queries.size(); i ++)
        queries[i] = new CKmerAPI(kmcinfo.kmer_length);
    uint64 stride = kmcinfo.total_kmers / queries.size() + 1;
    uint64 iquery = 0;
    while(true)
    {
        bool status;
        //kmer_object.to_string(kmerstr);
        //cout << kmerstr << "\t" << counter << endl;
        if(nkmers % stride == 0)
            status = kmc_db.ReadNextKmer(*(queries[iquery++]), counter);
        else
            status = kmc_db.ReadNextKmer(kmer_object, counter);
        if(!status)
            break;
        nkmers += 1;
    }
    queries.resize(iquery);
    cerr << "Query KMC database" << endl;
    kmc_db.Close();
    if(!kmc_db.OpenForRA(kmcfile))
    {
        cerr << "Error: cannot open the KMC database " << kmcfile << endl;
        exit(1);
    }
    uint64 nhits = 0;
    for(size_t i = 0; i < queries.size(); i ++)
        if(kmc_db.IsKmer(*(queries[i])))
            nhits ++;
    cerr << "Number of hits: " << nhits << endl;
    for(size_t i = 0; i < queries.size(); i ++)
        delete queries[i];
    cerr << "Total number of kmers: " << nkmers << endl;
    kmc_db.Close();
}

void Query(const string& kmcfile_q,
                const string& kmcfile_r,
                const string& output_file)
{
    CKMCFile kmcdb_q, kmcdb_r;
    CKMCFileInfo kmcinfo_q, kmcinfo_r;

    cerr << "Read query KMC database: " << kmcfile_q << endl;
    if(!kmcdb_q.OpenForListing(kmcfile_q))
    {
        cerr << "Error: cannot open the query KMC database " << kmcfile_q << endl;
        exit(1);
    }
    cerr << "Read reference KMC database: " << kmcfile_r << endl;
    if(!kmcdb_r.OpenForRA(kmcfile_r))
    {
        cerr << "Error: cannot open the reference KMC database " << kmcfile_r << endl;
        exit(1);
    }

    kmcdb_q.Info(kmcinfo_q);
    kmcdb_r.Info(kmcinfo_r);

    if(kmcinfo_q.kmer_length != kmcinfo_r.kmer_length)
    {
        cerr << "Error: kmer length is unequal between query and reference database" << endl;
        exit(1);
    }
    cerr << "Query against reference KMC database" << endl;
    uint32 counter;
    CKmerAPI kmer(kmcinfo_q.kmer_length);
    BitSet kmer_exists(kmcinfo_q.total_kmers);
    for(size_t i = 0; i < kmcinfo_q.total_kmers; i ++)
    {
        if(!kmcdb_q.ReadNextKmer(kmer, counter))
            break;
        if(kmcdb_r.IsKmer(kmer))
            kmer_exists.Set(i);
    }

    cerr << "Write kmer existence to " << output_file << endl;
    kmer_exists.SaveToHDF5(output_file, "data");
    kmcdb_q.Close();
    kmcdb_r.Close();
}

void DumpQuery(const string& filename)
{
    BitSet kmer_exists;
    kmer_exists.LoadFromHDF5(filename, "data");
    for(size_t i = 0; i < kmer_exists.Size(); i ++)
        cout << kmer_exists.Get(i) << endl;
}

template <bool EnableFreqHist>
void Filter(const vector<string>& filenames,
            const string& output_file,
            size_t n_tasks = 1, size_t task_id = 0,
            int min_freq = 1, int max_freq = 0)
{
    string freq_hist_file, kmer_list_file;
    if(EnableFreqHist)
        freq_hist_file = output_file;
    else
        kmer_list_file = output_file;
    int nsamples = filenames.size();
    vector<BitSet> kmer_exists(nsamples);
    size_t nkmers_total = BitSet::SizeFromHDF5(filenames[0], "data");
    
    size_t nkmers_per_task = nkmers_total / n_tasks / BitSet::BitsPerUnit * BitSet::BitsPerUnit;
    size_t offset = task_id*nkmers_per_task;
    size_t nkmers = nkmers_per_task;
    if(task_id == (n_tasks - 1))
        nkmers = nkmers_total - (n_tasks - 1)*nkmers_per_task;
   
    logger.Info() << "n_tasks=" << n_tasks << ", task_id=" << task_id
        << ", nkmers_total=" << nkmers_total
        << ", offset=" << offset << ", nkmers=" << nkmers << Endl;

    for(int i = 0; i < nsamples; i ++)
    {
        //logger.Debug() << "Read " << filenames[i] << Endl;
        kmer_exists[i].LoadFromHDF5(filenames[i], "data", offset, nkmers);
    } 
    vector<int> v(nsamples);
    vector<size_t> freq_hist;
    vector<size_t> kmer_list;
    size_t nkmers_pass = 0;
    if(EnableFreqHist)
        freq_hist.resize(nsamples);
    else
        kmer_list.resize(nkmers);
    min_freq = (min_freq > 0)? min_freq : 1;
    max_freq = (max_freq > 0)? max_freq : nsamples;

    for(size_t ki = 0; ki < nkmers; ki ++)
    {
        int freq = 0;
        for(int si = 0; si < nsamples; si ++)
        {
            v[si] = kmer_exists[si].Get(ki);
            freq += v[si];
        }
        if(EnableFreqHist)
            freq_hist[freq] ++;
        else
        {
            if((freq >= min_freq) && (freq <= max_freq))
                kmer_list[nkmers_pass ++] = ki + offset;
        }
    }
    if(EnableFreqHist)
    {
        logger.Info() << "Write frequency histogram file: " << freq_hist_file << Endl;
        WriteVectorToHDF5<size_t>(freq_hist_file, "data", freq_hist);
    }
    else
    {
        kmer_list.resize(nkmers_pass);
        logger.Info() << "Write kmer list file: " << kmer_list_file << Endl;
        WriteVectorToHDF5<size_t>(kmer_list_file, "data", kmer_list);
    }
}


void ExtractCounts(const string& kmc_file,
                   const string& kmerset_file,
                   const string& index_file,
                   vector<uint32>& counts)
{
    vector<size_t> indices;
    logger.Info() << "Read index file " << index_file << Endl;
    ReadVectorFromHDF5<size_t>(index_file, "data", indices);
    
    logger.Info() << "Read KMC database " << kmc_file << Endl;
    CKMCFile kmcdb;
    CKMCFileInfo kmcinfo;
    if(!kmcdb.OpenForRA(kmc_file))
    {
        cerr << "Error: cannot open the query KMC database " << kmc_file << endl;
        exit(1);
    
    }
    kmcdb.Info(kmcinfo);

    logger.Info() << "Read KmerSet database " << kmerset_file << Endl;
    CKMCFile kmersetdb;
    CKMCFileInfo kmersetinfo;
    if(!kmersetdb.OpenForListing(kmerset_file))
    {
        cerr << "Error: cannot open the KmerSet file " << kmerset_file << endl;
        exit(1);
    }
    kmersetdb.Info(kmersetinfo);

    logger.Info() << "Extract k-mer counts" << Endl;
    CKmerAPI kmer(kmcinfo.kmer_length);
    size_t i_index = 0;
    uint32 counter;
    counts.resize(indices.size());
    for(size_t i = 0; i < kmersetinfo.total_kmers; i ++)
    {
        if(i_index == indices.size())
            break;
        if(!kmersetdb.ReadNextKmer(kmer, counter))
            break;
        if(i == indices[i_index])
        {
            if(kmcdb.CheckKmer(kmer, counter))
                counts[i_index] = counter;
            else
                counts[i_index] = 0;
            i_index ++;
        }
    }
    
    kmersetdb.Close(); 
    kmcdb.Close();
}

void ExtractCounts(const string& kmc_file,
                   const string& kmerset_file,
                   const string& index_file,
                   const string& output_file)
{
    vector<uint32> counts;
    ExtractCounts(kmc_file, kmerset_file, index_file, counts);
    WriteVectorToHDF5<uint32>(output_file, "data", counts);
}

static void MainHelp()
{
    cerr << "Usage: " << progname << " command options" << endl;
    cerr << "Avaiable commands: extract_counts query test dump_query size_query filter" << endl;
}

int main(int argc, char** argv)
{
    progname = argv[0];
    if(argc < 2)
    {
        MainHelp();
        exit(1);
    }
    string command(argv[1]);
    
    argc -= 2;
    argv += 2;
    if(command == "query")
    {
        if(argc != 3)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command 
                 << " kmcfile_query kmcfile_ref output_file" << endl;
            exit(1);
        }
        string kmcfile_q(argv[0]);
        string kmcfile_r(argv[1]);
        string output_file(argv[2]);

        Query(kmcfile_q, kmcfile_r, output_file);
    }
    else if(command == "test")
    {
        if(argc != 1)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command 
                 << " kmcfile" << endl;
            exit(1);
        }
        string kmcfile(argv[0]);
        test_query(kmcfile);
    }
    else if(command == "size_query")
    {
        if(argc != 1)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command 
                 << " filename" << endl;
            exit(1);
        }
        string filename(argv[0]);
        cout << BitSet::SizeFromHDF5(filename, "data") << endl;
    }
    else if(command == "dump_query")
    {
        if(argc != 1)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 << " filename" << endl;
            exit(1);
        }
        string filename(argv[0]);
        DumpQuery(filename);
    }
    else if(command == "filter")
    {
        if(argc != 5)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 << " kmer_list_file min_freq max_freq n_tasks task_id" << endl;
            exit(1);
        }
        string kmer_list_file(argv[0]);
        int min_freq = atoi(argv[1]);
        int max_freq = atoi(argv[2]);
        int n_tasks = atoi(argv[3]);
        int task_id = atoi(argv[4]);
        vector<string> filenames = ReadLines(cin);
        Filter<false>(filenames, kmer_list_file, n_tasks, task_id, min_freq, max_freq);
    }
    else if(command == "freq_hist")
    {
        if(argc != 3)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 << " freq_hist_file n_tasks task_id" << endl;
            cerr << "Input files are read from standard input" << endl;
            exit(1);
        }
        string freq_hist_file(argv[0]);
        int n_tasks = atoi(argv[1]);
        int task_id = atoi(argv[2]);
        vector<string> filenames = ReadLines(cin);
        Filter<true>(filenames, freq_hist_file, n_tasks, task_id);
    }
    else if(command == "extract_counts")
    {
        if(argc != 4)
        {
            cerr << "Error: invalid number of arguments" << endl;
            cerr << "Usage: " << progname << " " << command
                 << " kmc_file kmerset_file index_file output_file" << endl;
            exit(1);
        }
        string kmc_file(argv[0]);
        string kmerset_file(argv[1]);
        string index_file(argv[2]);
        string output_file(argv[3]);

        ExtractCounts(kmc_file, kmerset_file, index_file, output_file);
    }
    else
    {
        cerr << "Error: invalid command: " << command << endl;
        MainHelp();
        exit(1);
    }

    return 0;
}
