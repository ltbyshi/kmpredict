#ifndef __BITSET_H__
#define __BITSET_H__
#include <vector>
#include <string>
#include <string.h>
#define HAVE_POPCOUNT 1
#include "HDF5Utils.h"
#include "Exception.h"

class BitSet
{
public:    
    typedef uint64_t UnitType;
    const static size_t BitsPerUnit = 64;

    BitSet(size_t size = 0) { _data = NULL; Resize(size); }
    
    BitSet(const BitSet& b)
    {
        if(b._data)
        {
            _size = b._size;
            _nunits = b._nunits;
            _data = new UnitType[_nunits];
            memcpy(_data, b._data, _nunits*8);
        }
        else
        {
            _data = NULL;
            _size = 0;
            _nunits = 0;
        }
    }
    
    ~BitSet()
    {
        if(_data)
            delete[] _data;
    }
   
    // only check the HDF file for number of bits
    static size_t SizeFromHDF5(const std::string& filename,
                               const std::string& dataname)
    {
        size_t size;
        ReadAttributeFromHDF5<size_t>(filename, dataname, "size", size);
        return size;
    }

    // load from a HDF5 file
    void LoadFromHDF5(const std::string& filename,
                      const std::string& dataname)
    {
        if(_data)
            delete[] _data;
        std::vector<size_t> dims = GetHDF5DatasetDims(filename, dataname);
        _nunits = dims[0];
        _data = new UnitType[_nunits];
        ReadAttributeFromHDF5<size_t>(filename, dataname, "size", _size);
        ReadVectorFromHDF5<UnitType>(filename, dataname, _data);
    }

    // Partially load from HDF file
    // offset: start position of the bit
    // length: number of bits to load
    void LoadFromHDF5(const std::string& filename,
                      const std::string& dataname,
                      size_t offset, size_t count)
    {
        if((offset % BitsPerUnit) != 0)
            throw ValueError("LoadFromHDF5: offset is not aligned to unit size");
        if(_data)
           delete[] _data;
        _data = NULL;
        _nunits = count / BitsPerUnit;
        if((count % BitsPerUnit) != 0)
            _nunits += 1;
        _size = count;
        _data = new UnitType[_nunits];
        ReadVectorFromHDF5<UnitType>(filename, dataname, _data, 
                offset / BitsPerUnit, _nunits);
    }

    void SaveToHDF5(const std::string& filename,
                    const std::string& dataname) const
    {
        hsize_t dims[1];
        dims[0] = _nunits;
        WriteHDF5<UnitType>(filename, dataname, 1, dims, _data);
        WriteAttributeToHDF5<size_t>(filename, dataname, "size", _size);
    }

    void Resize(size_t size)
    {
        if(_data)
        {
            delete[] _data;
            _data = NULL;
        }
        _size = size;
        _nunits = size/64;
        if(size % 64 != 0)
            _nunits = size/64 + 1;
        if(size > 0)
        {
            _data = new UnitType[_nunits];
            memset(_data, 0, _nunits*8);
        }
    }
    
    bool operator[](size_t i) const
    {
        return Get(i);
    }
    
    bool Get(size_t i) const
    {
        return bool((_data[i >> 6] >> (i & 0x3FL)) & 1L);
    }
    
    void Set(size_t i)
    {
        _data[i >> 6] |= (1L << (i & 0x3FL));
    }
    
    void Clear(size_t i)
    {
        _data[i >> 6] &= ~(1L << (i & 0x3FL));
    }
    
    size_t Sum() const
    {
        size_t result = 0;
#ifdef HAVE_POPCOUNT
        size_t int_nunits = (_size >> 6);
        size_t rem_nunits = _size - (int_nunits << 6);
        for(size_t i = 0; i < int_nunits; i ++)
            result += __builtin_popcountl(_data[i]);
        for(size_t i = (int_nunits << 6); i < rem_nunits; i ++)
                result += (*this)[i];
#else
        for(size_t i = 0; i < _size; i ++)
            result += (*this)[i];
#endif
        return result;
    }
    
    BitSet& And(const BitSet& b)
    {
        if((_size == b._size) && (_size != 0))
        {
            for(size_t i = 0; i < _nunits; i ++)
                _data[i] &= b._data[i];
        }
        return *this;
    }
    
    BitSet& Or(const BitSet& b)
    {
        if((_size == b._size) && (_size != 0))
        {
            for(size_t i = 0; i < _nunits; i ++)
                _data[i] |= b._data[i];
        }
        return *this;
    }
    
    bool All() const
    {
        bool result = true;
        for(size_t i = 0; i < _size; i ++)
            result &= (*this)[i];
        return result;
    }
    
    void Unpack(std::vector<bool>& unpacked) const
    {
        unpacked.resize(_size);
        size_t int_nunits = (_size >> 6);
        size_t rem_nbits = _size - (int_nunits << 6);
        size_t index = 0;
        for(size_t i = 0; i < int_nunits; i ++)
        {
            for(int j = 0; j < 64; j ++)
                unpacked[index++] = ((_data[i] >> j) & 1);
        }
        for(size_t j = 0; j < rem_nbits; j ++)
            unpacked[index++] = ((_data[_nunits - 1] >> j) & 1);
    }
    
    const UnitType* GetData() const { return _data; }
    UnitType* GetData() { return _data; }
    size_t GetDataSize() { return _nunits; }
    size_t Size() const { return _size; }
    size_t GetSize() const { return _size; }
    size_t GetNumUnits() const { return _nunits; }
    size_t GetUnitSize() const { return 8; }
private:
    UnitType* _data;
    size_t _nunits;
    size_t _size;
};

#endif
