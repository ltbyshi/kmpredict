#ifndef __FASTA_H__
#define __FASTA_H__

#include <fstream>
#include <string>

#include "Nucleotide.h"

struct FastaRecord
{
    std::string name;
    std::string seq;
};

class FastaReader
{
public:
    FastaReader(const std::string& fileName)
    : _fin(fileName),
    _lastRecord(NULL)
    {
        if(_fin)
            _valid = true;
        else
            _valid = false;
        _filepos = 0;
        _lineno = 0;
    }
    
    ~FastaReader()
    {
        if(_lastRecord)
            delete _lastRecord;
        _fin.close();
    }
    
    operator bool() const { return _valid; }
    size_t GetFilePos() const { return _filepos; }
    size_t GetLineNum() const { return _lineno; }
    
    FastaRecord* ReadNext()
    {
        string line;
        
        FastaRecord* record = NULL;
        if(_lastRecord)
        {
            record = _lastRecord;
            _lastRecord = NULL;
        }
        while(getline(_fin, line))
        {
            _lineno ++;
            _filepos += line.size() + 1;
            if(line.size() > 0)
            {
                if(!record)
                {
                    // new record
                    if(line[0] == '>')
                    {
                        record = new FastaRecord;
                        record->name = line.substr(1, line.size() - 1);
                    }
                    // skip this line
                }
                else
                {
                    if(line[0] == '>')
                    {
                        // new record, save this line
                        if(record->seq.size() > 0)
                        {
                            _lastRecord = new FastaRecord;
                            _lastRecord->name = line.substr(1, line.size() - 1);
                            break;
                        }
                        // empty sequence, skip
                        else
                        {
                            record->name = line.substr(1, line.size() - 1);
                        }
                    }
                    else
                    {
                        // sequence line
                        if(DNACode[int(line[0])] >= 0)
                            record->seq += line;
                        // skip invalid lines
                    }
                }   
            }
            //skip empty lines
        }
        return record;
    }
    
    void Close()
    {
        _fin.close();
    }
    
    
private:
    std::ifstream _fin;
    FastaRecord* _lastRecord;
    bool _valid;
    size_t _lineno;
    size_t _filepos;
};

#endif