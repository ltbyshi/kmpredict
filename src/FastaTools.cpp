#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
#include <stdlib.h>
#include <math.h>

#include "Fasta.h"
#include "Utils.h"

#define PROGNAME "FastaTools"

int CountBases(int argc, char** argv)
{
    if(argc != 2)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count the number of bases per sequence" << endl;
        cerr << "Usage: " << PROGNAME << " countbases fastafile" << endl;
        exit(1);
    }
    else
    {
        const char* filename = argv[1];
        FastaReader reader(filename);
        if(!reader)
            Die("cannot open the fasta file %s", filename);
        FastaRecord* record = reader.ReadNext();
        while(record)
        {
            cout << record->seq.size() << endl;
            delete record;
            record = reader.ReadNext();
        }
        record = NULL;
        reader.Close();
    }
    return 0;
}

int CountKmers(int argc, char** argv)
{
    if(!((argc == 3) || (argc == 4)))
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count the number of kmers per sequence" << endl;
        cerr << "Usage: " << PROGNAME << " countkmers fastafile mink [maxk]" << endl;
        exit(1);
    }
    else
    {
        const char* filename = argv[1];
        unsigned long mink = atoi(argv[2]);
        unsigned long maxk = mink;
        if(argc == 4)
            maxk = atoi(argv[3]);
        if(mink > maxk)
            Die("mink cannot be greater than maxk");
        vector<unsigned long> nkmers(maxk - mink + 1, 0);
        
        FastaReader reader(filename);
        if(!reader)
            Die("cannot open the fasta file %s", filename);
        FastaRecord* record = reader.ReadNext();
        while(record)
        {
            unsigned long seqlen = record->seq.size();
            for(size_t k = mink; k <= maxk; k ++)
                nkmers[k-mink] += max(seqlen - k + 1, 0UL);
            delete record;
            record = reader.ReadNext();
        }
        record = NULL;
        reader.Close();
        
        cout << "K\tKmerNum\tTotalKmer\tCoverage\tP(n>=1)" << endl;
        for(unsigned long k = mink; k <= maxk; k ++)
        {
            unsigned long totalkmer = (1UL << (2*k));
            unsigned long nkmer = nkmers[k - mink];
            cout << k << "\t" << nkmer 
                << "\t" << totalkmer
                << "\t" << double(nkmer)/totalkmer
                << "\t" << (1.0 - pow(1.0 - 1.0/totalkmer, nkmer)) << endl;
        }
    }
    return 0;
}

int MakeIndex(int argc, char** argv)
{
    if(argc != 3)
    {
        cerr << "Error: invalid arguments\n" << endl;
        cerr << "Count the number of kmers per sequence" << endl;
        cerr << "Usage: " << PROGNAME << " makeindex fastafile indexfile" << endl;
        exit(1);
    }
    string fastafile(argv[1]);
    string indexfile(argv[2]);
    
    ofstream fout(indexfile);
    if(!fout)
        Die("cannot open the index file %s", indexfile.c_str());
    ifstream fin(fastafile);
    if(!fin)
        Die("cannot open the fasta file %s", fastafile.c_str());
    size_t filepos = 0;
    string line;
    while(getline(fin, line))
    {
        if(line.size() > 0)
        {
            if(line[0] == '>')
                fout << filepos << endl;
        }
        filepos += line.size() + 1;
    }
    fin.close();  
    fout.close();
    
    return 0;
}

int main(int argc, char** argv)
{
    HandlerMap handlers;
    handlers["countbases"] = CountBases;
    handlers["countkmers"] = CountKmers;
    handlers["makeindex"] = MakeIndex;
    
    ParseArguments(handlers, PROGNAME, argc, argv);
    
    return 0;
}