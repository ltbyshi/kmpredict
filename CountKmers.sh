#! /bin/bash

. Configuration.sh

makedir(){
    local d
    for d in $@;do
        [ -d "$d" ] || mkdir -p "$d"
    done
}

count_kmers_scaffold(){
    DataId=$1
    KmerCounterOption=$2
    mkdir -p "$kmerCountsDir/$DataId" 2>/dev/null
    if [ "$KmerCounterOption" = "countw" ];then
        $BINPATH/KmerCounter countw $K $ScaffoldDir/$DataId/scaffold.fa $KmerCountsDir/$DataId/AverageCoverage.txt \
            $KmerCountsDir/$DataId/kmer.$K.counts
    elif [ "$KmerCounterOption" = "count" ];then
        $BINPATH/KmerCounter count $K $ScaffoldDir/$DataId/scaffold.fa \
            $KmerCountsDir/$DataId/kmer.$K.counts
    elif [ "$KmerCounterOption" = "hashedcount" ];then
        $BINPATH/KmerCounter hashedcountw $K $HashSize Hyperplanes/$K.$HashSize \
            $ScaffoldDir/$DataId/scaffold.fa \
            $KmerCountsDir/$DataId/kmer.$K.counts
    elif [ "$KmerCounterOption" = "hashedcountw" ];then
        $BINPATH/KmerCounter hashedcountw $K $HashSize Hyperplanes/$K.$HashSize \
            $ScaffoldDir/$DataId/scaffold.fa \
            $KmerCountsDir/$DataId/AverageCoverage.txt \
            $KmerCountsDir/$DataId/kmer.$K.counts
    fi
}

count_kmers_raw(){
    local dataid=$1
    local method=$2
    KmerCountsDir=KmerCountRawReads
    : ${K?:} ${RawReadsDir:?} ${dataid:?} ${method:?}
    mkdir -p "$OutDir/$dataid" 2>/dev/null
    seqfile=$(ls $RawReadsDir/$dataid/*.fasta)
    if [ "$method" = "count" ];then
        $BINPATH/KmerCounter count $K $seqfile $KmerCountsDir/$dataid/kmer.${K}.counts
    elif [ "$method" = "check" ];then
        if [ ! -f "$seqfile" ];then
            echo "Error: seq file $seqfile for $dataid does not exist"
        fi
    fi
}

count_kmers_lsh(){
    local dataid=$1
    local hash_size=$2
    KmerCountsDir=KmerCountLSH
    : ${K?:} ${RawReadsDir:?} ${dataid?:} ${hash_size?:}
    mkdir -p "$OutDir/$dataid" 2>/dev/null
    seqfile=$(ls $RawReadsDir/$dataid/*.fasta)
    if [ "$method" = "count" ];then
        $BINPATH/KmerCounter $K $hash_size $coef_file $seqfile $KmerCountsDir/kmer.${K}.counts
    fi
}

get_rawreads_list(){
    for dataid in $(select_dataid);do
        printf "$dataid\t$(ls $RawReadsDir/$dataid/*.fasta)\n"
    done
}

count_kmers_kmc(){
    local dataid=$1
    memory=${memory:=32}
    # max value of k-mer counts
    if [ "$K" -lt 24 ];then
        max_value=${max_value:=65535}
    else
        max_value=${max_value:=255}
    fi
    threads=${threads:=16}
    OutDir=KMC
    : ${K?:} ${RawReadsDir:?} ${dataid:?}
    mkdir -p "$OutDir/$dataid" $TmpDir/KMC/$dataid 2>/dev/null
    seqfile=$(ls $RawReadsDir/$dataid/*.fasta)
    echo "Count k-mers in $seqfile"
    $SURUN kmc -cs$max_value -t$threads -fa -k$K -m$memory $seqfile $OutDir/$dataid/kmer.${K} $TmpDir/KMC/$dataid
}

# Calculate a histogram of k-mer counts for each KMC output file
# The histogram is used for selecting a threshold for filtering low-frequency k-mers
kmc_hist(){
    OutDir=KmerMarker/$K/KMCHist
    mkdir -p $OutDir 2>/dev/null
    for dataid in $(select_dataid);do
        echo "KMC histogram $dataid"
        kmc_tools histogram KMC/$dataid/kmer.${K} $OutDir/${dataid}.txt
    done
}

# merge the files from job_FreqHistKMC
# read the total number of kmers
# select min_freq so that the number of selected k-mers is no more than $nkmers_select
select_freq_thresh(){
    : ${min_count?}
    OutDir=KmerMarker/$K/Filter/ci${min_count}
    n_tasks=$(ls $OutDir/freq_hist.t*.h5 | wc -l)
    # select 2**28 kmers
    nkmers_select=268435456
    nkmers_total=$($BINPATH/KMCInfo KmerSet/$K/ci${min_count}/merged | grep total_kmers | awk '{print $2}')
    R --vanilla <<RSCRIPT
library(rhdf5)
freq_hist <- h5read('$OutDir/freq_hist.t0.h5', 'data')
for(i in 1:(${n_tasks} - 1)){
    freq_hist <- freq_hist + h5read(sprintf('$OutDir/freq_hist.t%d.h5', i), 'data')
}
freq_hist <- as.numeric(freq_hist)[2:length(freq_hist)]
freq_quan <- cumsum(freq_hist)/sum(freq_hist)
write.table(data.frame(n=1:length(freq_quan), freq_quan),
    '$OutDir/freq_quan.txt', row.names=FALSE, col.names=FALSE)
quantile_select <- 1.0 - $nkmers_select/$nkmers_total
min_freq <- which(freq_quan >= quantile_select)[1]
write(min_freq, '$OutDir/min_freq.txt')
message('quantile_select: ', quantile_select)
message('min_freq: ', min_freq)
RSCRIPT
}

kmer_set_kmc(){
    : ${min_count?}
    local enable_step1=${enable_step1:=1}
    local enable_step2=${enable_step2:=1}
    local n_tasks=${n_tasks:=30}
    local memory=${memory:=30}
    OutDir=KmerSet/$K/ci${min_count}
    $SURUN mkdir -p $OutDir 2>/dev/null
    mapfile -t dataids < <(select_dataid)
    if [ "$n_tasks" -gt "${#dataids[@]}" ];then
        n_tasks=${#dataids[@]}
    fi
    local indices
    set -e
    if [ "$enable_step1" -eq 1 ];then
        for task_id in $(seq $n_tasks);do
            mapfile -t indices < <(seq $task_id $n_tasks ${#dataids[@]})
            {
            printf "INPUT:\n"
            for i in ${indices[@]};do
                printf "s$i=$PWD/KMC/${dataids[$(($i-1))]}/kmer.${K} -ci$min_count\n"
            done
            printf "OUTPUT:\n"
            printf "t$task_id="
            for i in ${indices[@]};do
                if [ "$i" -gt "$n_tasks" ];then
                    printf "+";
                fi
                printf "s$i"
            done
            printf "\n"
            } | $SURUN tee $OutDir/t${task_id}.kmc >/dev/null
            pushd $OutDir >/dev/null
                echo "KMC merge ${indices[@]}"
                $SURUN kmc_tools complex t${task_id}.kmc
            popd >/dev/null
        done
    fi
    if [ "$enable_step2" -eq 1 ];then
        {
        mapfile -t indices < <(seq $n_tasks)
        printf "INPUT:\n"
        for i in ${indices[@]};do
            printf "s$i=t$i\n"
        done
        printf "OUTPUT:\n"
        printf "merged="
        for i in ${indices[@]};do
            if [ "$i" -gt 1 ];then
                printf "+"
            fi
            printf "s$i"
        done
        printf "\n"
        } | $SURUN tee $OutDir/merged.kmc >/dev/null
        pushd $OutDir >/dev/null
            echo "Merge ${indices[@]}"
            $SURUN kmc_tools complex merged.kmc
        popd >/dev/null
    fi
}

seqfile_totalsize(){
    local size=0
    for dataid in $(select_dataid);do
        seqfile=$(echo $RawReadsDir/$dataid/*.fasta)
        if [ -f "$seqfile" ];then
            local fsize=$(stat -c '%s' $seqfile)
            size=$(($size + $fsize))
        fi
    done
    echo $size
}

select_count_threshold(){
    local min_ratio=$1
    local outfile=KmerMarker/$K/CountThreshold/${min_ratio}.txt
    mkdir -p KmerMarker/$K/CountThreshold 2>/dev/null
    R --vanilla <<RSCRIPT
min_ratio=$min_ratio/100.0
dataids <- strsplit('$(select_dataid | xargs)', ' ')[[1]]
mincount <- NULL
q <- NULL
for(dataid in dataids){
    h <- read.table(sprintf('$KmerCountsDir/%s/kmer.${K}.hist', dataid), header=FALSE)
    qt <- cumsum(h[,2])/sum(h[,2])
    i <- findInterval(min_ratio, qt)
    mincount <- c(mincount, i)
    q <- c(q, qt[i])
}
df <- data.frame(DataId=dataids, MinCount=mincount, Quantile=q)
write.table(df, '$outfile',
    sep='\t', quote=FALSE, row.names=FALSE)
RSCRIPT
}

# calculate (total number of k-mers in the sample)/(total number of possible k-mers)
average_coverage(){
    [ -d KmerMarker/$K ] || mkdir -p KmerMarker/$K
    {
    for dataid in $(select_dataid);do
        echo "Get total counts of k-mers: $dataid" >&2
        printf "$dataid\t$($BINPATH/KmerCounter average_count $KmerCountsDir/$dataid/kmer.${K}.counts)\n"
    done
    } > KmerMarker/$K/AverageCoverage.txt
}

# total number of k-mers for normalization
average_coverage_kmc(){
    {
    for dataid in $(select_dataid);do
        echo "Get total number of k-mers: $dataid" >&2
        cov=$($BINPATH/KMCInfo KMC/$dataid/kmer.$K | grep total_counts | awk '{print $2}')
        printf "$dataid\t$cov\n"
    done
    } > KmerMarker/$K/AverageCoverage.txt
}

merge_filter_kmc(){
    : ${min_count?}
    OutDir=KmerMarker/$K/Filter/ci${min_count}
    n_tasks=$(ls $OutDir/freq_hist.t*.h5 | wc -l)
    $BINPATH/HDF5Tools merge data $(seq -f "$OutDir/filter.t%.0f.h5" 0 $(($n_tasks - 1))) $OutDir/filter.h5
}

# make symbolic link to the k-mer counts with a specific parameter
symlink_kmc_counts(){
    : ${min_count?}
    for dataid in $(select_dataid);do
        $SURUN ln -s -f kmer.${K}.ci${min_count}.counts KMC/$dataid/kmer.${K}.counts
    done
}

if [ -n "$source_only" ];then
    return 0
fi

# main script
usage(){
    echo "Usage: $0 command [args ...]"
    echo "Available commands: symlink_kmc_counts average_coverage_kmc merge_filter_kmc select_freq_thresh kmc_hist get_rawreads_list kmer_set_kmc count_kmers_scaffold count_kmers_raw count_kmers_kmc seqfile_totalsize counter_to_filter unique_kmers average_coverage"
    echo "Environment variables:"
    echo -e "\tDRYRUN: dry run for parallel"
    echo -e "\tNPROC: number of processes"
}

if [ "$#" -lt 1 ];then
    echo "Error: insufficient number of arguments"
    usage
    exit 1
fi
cmd=$1
shift
case "$cmd" in
    count_kmers_scaffold|count_kmers_raw)
        if [ "$#" -ne 1 ];then
            echo "Error: invalid number of arguments (1 expected, $# given)"
            echo "Usage: $0 count_kmers method"
            echo "method: countw count hashedcountw"
            exit 1
        fi
        method=$1
        set -e
        {
            for dataid in $(select_dataid);do
                echo "source_only=1 . $0; $cmd $dataid $method"
            done
        } | parallel -v $DRYRUN -j $NPROC
        ;;
    count_kmers_kmc)
        for dataid in $(select_dataid);do
            count_kmers_kmc $dataid
        done
        ;;
    get_rawreads_list)
        get_rawreads_list
        ;;
    kmer_set_kmc)
        kmer_set_kmc
        ;;
    seqfile_totalsize)
        seqfile_totalsize
        ;;
    kmc_hist)
        kmc_hist
        ;;
    counter_to_filter)
        threshold=${threshold:=1}
        set -e
        for dataid in $(select_dataid);do
            echo "CounterToFilter $dataid"
            $BINPATH/CounterToFilter $KmerCountsDir/$dataid/kmer.${K}.counts \
                $KmerCountsDir/$dataid/kmer.${K}.filter $threshold
        done
        ;;
    unique_kmers)
        set -e
        for dataid in $(select_dataid);do
            echo $dataid $($BINPATH/KmerFilter unikmercount $KmerCountsDir/$dataid/kmer.${K}.filter)
        done
        ;;
    select_count_threshold)
        if [ "$#" -ne 1 ];then
            echo "Error: invalid number of arguments (1 expected, $# given)"
            echo "Usage: $0 select_count_threshold min_ratio"
            exit 1
        fi
        select_count_threshold "$1"
        ;;
    average_coverage)
        average_coverage
        ;;
    select_freq_thresh)
        select_freq_thresh
        ;;
    merge_filter_kmc)
        merge_filter_kmc
        ;;
    *)
        $cmd
        ;;
esac
