#! /usr/bin/env python

import argparse
import sys, os, re, json, copy, itertools, math, multiprocessing

def DeferredImport():
    '''Accelerate startup time
    '''
    global np, pd
    import numpy as np
    import pandas as pd
    
class ParameterGenerator(object):
    def __init__(self, k, outer_fold, inner_fold, 
                 base_dir='.', param_dir='Param', cvindex_file=False,
                 classifiers=[], filter_metrics=[], misc_conds=[],
                 min_count_ratio=None, kmer_count_dir='output'):
        self.outer_fold = outer_fold
        self.inner_fold = inner_fold
        self.base_dir = base_dir
        self.param_dir = os.path.join(base_dir, param_dir)
    
        self.InitTemplates()

        metadata = pd.read_table('metadata/metadata.txt')
        self.p = {}
        self.p['DataId'] = list(metadata['DataId'])
        self.p['BaseDir'] = base_dir
        self.p['NumKmers'] = 10000
        self.p['ContourFile'] = '{BaseDir}/Contour.h5'.format(**self.p)
        self.p['KmerCountsDir'] = kmer_count_dir
        self.groups = ['Normal', 'Few_polyps', 'Stage_0', 'Stage_I_II', 'Stage_III_IV']
        self.state_to_group = {'Normal': -1, 'Few_polyps': -1, 
                               'Stage_0': 1, 'Stage_I_II': 1, 'Stage_III_IV': 1}
        self.pairs = {'Normal-Stage_0', 'Normal-Stage_I_II', 
                      'Normal-Stage_III_IV', 'Normal-Cancer'}
        self.pair_state = {'Normal-Stage_0': ('Normal', 'Few_polyps', 'Stage_0'),
                           'Normal-Stage_I_II': ('Normal', 'Few_polyps', 'Stage_I_II'),
                           'Normal-Stage_III_IV': ('Normal', 'Few_polyps', 'Stage_III_IV'),
                           'Normal-Cancer': ('Normal', 'Few_polyps', 'Stage_0', 'Stage_I_II', 'Stage_III_IV')}
        KmerCountDir = kmer_count_dir
        K = k
        # sample info
        info = []
        for i, row in metadata.iterrows():
            DataId = row['DataId']
            item = {'DataId': row['DataId'],
                    'State': row['State'],
                    'Group': self.state_to_group[row['State']],
                    'CoverageFile': self.templates['CoverageFile'].format(**locals()),
                    'KmerCountFile': self.templates['KmerCountFile'].format(**locals())}
            if k > 15:
                if min_count_ratio:
                    item['KmerFilterFile'] = self.templates['KmerFilterFileHashedMinCountRatio']
                else:
                    item['KmerFilterFile'] = self.templates['KmerFilterFileHashed']
            else:
                if min_count_ratio:
                    item['KmerFilterFile'] = self.templates['KmerFilterFileMinCountRatio']
                else:
                    item['KmerFilterFile'] = self.templates['KmerFilterFile']
            info.append(item)
        self.p['SampleInfo'] = info
        # generate CV folds
        if(cvindex_file):
            self.p['CVIndex'] = self.LoadFromJSON('CVIndex.json')
        else:
            cvindex = {}
            samples_all = np.arange(len(metadata))
            for pair in self.pairs:
                samples = samples_all[metadata['State'].isin(self.pair_state[pair]).values]
                cvindex[pair] = self.GenerateCVIndex(samples)
            self.p['CVIndex'] = cvindex
        
        self.p['RandomSeed'] = 1389
        
        self.filter_metrics = ('InfoGain', 'MutualInfo', 'BalancedAccuracy',
                               'ProbabilityRatio', 'Frequency',
                               'Correlation', 'MannWhitneyUTest',
                               'MWUTestFreq', 'FreqDiff')
        filter_metrics = filter(lambda x: x in self.filter_metrics, filter_metrics)
        if len(filter_metrics) > 0:
            self.filter_metrics = filter_metrics
        print 'FilterMetrics: ', self.filter_metrics
        
        self.hyperparams = {'SVM': {'C': [math.pow(10, i) for i in xrange(-5, 6)]},
                            'SVMLinear': {'C': [math.pow(10, i) for i in xrange(-5, 6)]},
                            'LogRegL2': {'C': [math.pow(10, i) for i in xrange(-5, 6)]},
                            'RandomForest': {'N': [100*i for i in xrange(1, 11)]},
                            'MultinomialNB': {'alpha': [1]},
                            'GaussianNB': {'dummy': [1]}
                            }
        self.classifiers = self.hyperparams.keys()
        classifiers = filter(lambda x: x in self.classifiers, classifiers)
        if len(classifiers) > 0:
            self.classifiers = classifiers
        print 'Classifiers: ', self.classifiers
        
        self.misc_cond_vars = {'All': {'KmerRankMin': 0, 'KmerRankMax': 0},
                               'Random_10000': {'KmerRankMax': 10000},
                               'Top_1000000': {'KmerRankMax': 1000000},
                               'Top_500000': {'KmerRankMax': 500000},
                               'Top_100000': {'KmerRankMax': 100000},
                               'Top_10000': {'KmerRankMax': 10000},
                               'Top_5000':  {'KmerRankMax': 5000},
                               'Top_2500':  {'KmerRankMax': 2500},
                               'Top_1000':  {'KmerRankMax': 1000},
                               'Top_500':   {'KmerRankMax': 500},
                               'Top_400':   {'KmerRankMax': 400},
                               'Top_300':   {'KmerRankMax': 300},
                               'Top_200':   {'KmerRankMax': 200},
                               'Top_100':   {'KmerRankMax': 100},
                               'Top_50':    {'KmerRankMax': 50},
                               'Sub1_10000': {'KmerRankMin': 2000, 'KmerRankMax': 12000},
                               'Sub2_10000': {'KmerRankMin': 4000, 'KmerRankMax': 14000},
                               'Sub3_10000': {'KmerRankMin': 6000, 'KmerRankMax': 16000},
                               'Bootstrap_10000': {'KmerRankMax': 10000, 'Bootstrap': 10},
                               'MRMR_500': {'MRMRMiscCond': 'Top_10000', 'MaxFeatureNum': 10000, 'FeatureNum': 500},
                               'MRMR_400': {'MRMRMiscCond': 'Top_10000', 'MaxFeatureNum': 10000, 'FeatureNum': 400},
                               'MRMR_300': {'MRMRMiscCond': 'Top_10000', 'MaxFeatureNum': 10000, 'FeatureNum': 300},
                               'MRMR_200': {'MRMRMiscCond': 'Top_10000', 'MaxFeatureNum': 10000, 'FeatureNum': 200},
                               'MRMR_100': {'MRMRMiscCond': 'Top_10000', 'MaxFeatureNum': 10000, 'FeatureNum': 100},
                               'MRMR_500_100000': {'MRMRMiscCond': 'Top_100000', 'MaxFeatureNum': 100000, 'FeatureNum': 500},
                               'MRMR_400_100000': {'MRMRMiscCond': 'Top_100000', 'MaxFeatureNum': 100000, 'FeatureNum': 400},
                               'MRMR_300_100000': {'MRMRMiscCond': 'Top_100000', 'MaxFeatureNum': 100000, 'FeatureNum': 300},
                               'MRMR_200_100000': {'MRMRMiscCond': 'Top_100000', 'MaxFeatureNum': 100000, 'FeatureNum': 200},
                               'MRMR_100_100000': {'MRMRMiscCond': 'Top_100000', 'MaxFeatureNum': 100000, 'FeatureNum': 100}
                               }
        # set default values for misc conds
        misc_cond_defaults = {
            'KmerRankMin': 0,
            'KmerRankMax': 0,
            'Bootstrap': 0
        }
                            
        for misc_cond in self.misc_cond_vars.itervalues():
            for name in misc_cond_defaults:
                if misc_cond.get(name) is None:
                    misc_cond[name] = misc_cond_defaults[name]
        
        self.misc_conds = ('Top_10000',)
        misc_conds = filter(lambda x: x in self.misc_cond_vars.keys(), misc_conds)
        if len(misc_conds) > 0:
            self.misc_conds = misc_conds
        print 'MiscConds: ', self.misc_conds
        
        
        if not os.path.exists(self.base_dir):
            os.makedirs(self.base_dir)
        if not os.path.exists(self.param_dir):
            os.makedirs(self.param_dir)
            
    def InitTemplates(self):
        # template variables
        self.templates = {}
        # sample info
        self.templates['KmerCountFile'] = '{KmerCountDir}/{DataId}/kmer.{K}.counts'
        self.templates['KmerFilterFile'] = '{KmerCountDir}/{DataId}/kmer.{K}.filter'
        self.templates['KmerFilterFileHashed'] = '{KmerCountDir}/{DataId}/kmer.{K}.hashed'
        self.templates['KmerFilterFileMinCountRatio'] = '{KmerCountDir}/{DataId}/kmer.{K}.{MinCountRatio}.filter'
        self.templates['KmerFilterFileHashedMinCountRatio'] = '{KmerCountDir}/{DataId}/kmer.{K}.{MinCountRatio}.hashed'
        self.templates['CoverageFile'] = '{KmerCountDir}/{DataId}/AverageCoverage.txt'

        relevance_misc_cond = 'Top_10000'
        self.templates['RelevanceFileOuter'] = '{BaseDir}/Relevance/{Pair}/' + relevance_misc_cond + '/{FilterMetric}/outer/{OuterIndex}.h5'
        self.templates['RelevanceFileInner'] = '{BaseDir}/Relevance/{Pair}/' + relevance_misc_cond + '/{FilterMetric}/inner/{OuterIndex}.{InnerIndex}.h5'
        self.templates['KmerListFileOuter'] = '{BaseDir}/KmerList/{Pair}/{MiscCond}/{FilterMetric}/outer/{OuterIndex}.h5'
        self.templates['KmerListFileInner'] = '{BaseDir}/KmerList/{Pair}/{MiscCond}/{FilterMetric}/inner/{OuterIndex}.{InnerIndex}.h5'
        self.templates['KmerListFileRandom'] = '{BaseDir}/KmerList/Random/{RandomIndex}.h5'
        self.templates['KmerMatrixFileOuter'] = '{BaseDir}/KmerMatrix/{Pair}/{MiscCond}/{FilterMetric}/outer/{OuterIndex}.h5'
        self.templates['KmerMatrixFileInner'] = '{BaseDir}/KmerMatrix/{Pair}/{MiscCond}/{FilterMetric}/inner/{OuterIndex}.{InnerIndex}.h5'
        self.templates['KmerMatrixFileAll'] = '{BaseDir}/KmerMatrix/All/0.h5'
        self.templates['KmerMatrixFileRandom'] = '{BaseDir}/KmerMatrix/Random/{RandomIndex}.h5'
        # MRMR files
        # the condition that MRMR is selected based on
        self.templates['MRMRInputFileOuter'] = '{BaseDir}/MRMR/{Pair}/{MRMRMiscCond}/{FilterMetric}/outer/{OuterIndex}.csv'
        self.templates['MRMRInputFileInner'] = '{BaseDir}/MRMR/{Pair}/{MRMRMiscCond}/{FilterMetric}/inner/{OuterIndex}.{InnerIndex}.csv'
        self.templates['MRMROutputFileOuter'] = '{BaseDir}/MRMR/{Pair}/{MRMRMiscCond}/{FilterMetric}/outer/{FeatureNum}/{OuterIndex}.mrmr.txt'
        self.templates['MRMROutputFileInner'] = '{BaseDir}/MRMR/{Pair}/{MRMRMiscCond}/{FilterMetric}/inner/{FeatureNum}/{OuterIndex}.{InnerIndex}.mrmr.txt'
        self.templates['MRMRKmerListFileOuter'] = '{BaseDir}/KmerList/{Pair}/{MiscCond}/{FilterMetric}/outer/{OuterIndex}.h5'
        self.templates['MRMRKmerListFileInner'] = '{BaseDir}/KmerList/{Pair}/{MiscCond}/{FilterMetric}/inner/{OuterIndex}.{InnerIndex}.h5'
        
        self.templates['ResultFileOuter'] = '{BaseDir}/CV/{Pair}/{MiscCond}/{FilterMetric}/{Classifier}/outer/{OuterIndex}.json'
        self.templates['ResultFileInner'] = '{BaseDir}/CV/{Pair}/{MiscCond}/{FilterMetric}/{Classifier}/inner/{OuterIndex}.{InnerIndex}.{ParamIndex}.json'
        self.templates['FeatureRobustnessFile'] = '{BaseDir}/FeatureRobustness.txt'
        self.templates['KmerListFilteredByFreq'] = '{BaseDir}/Cache/KmerListFilteredByFreq.h5'

    def GenerateCVFolds(self, n):
        from sklearn.cross_validation import KFold
        outer = []
        for outer_train, outer_test in KFold(n, n_folds=self.outer_fold, shuffle=True):
            inner = []
            for inner_train, inner_test in KFold(len(outer_train),
                                             n_folds=self.inner_fold, shuffle=True):
                inner.append({'TrainIndex': list(inner_train), 
                              'TestIndex': list(inner_test)})
            outer.append({'TrainIndex': list(outer_train), 
                          'TestIndex': list(outer_test),
                          'Inner': inner})
        return outer
 
    
    def GenerateCVIndex(self, samples):
        '''samples: an array of sample indicies
        '''
        from sklearn.cross_validation import KFold
        outer = []
        for outer_train, outer_test in KFold(len(samples), n_folds=self.outer_fold, shuffle=True):
            inner = []
            for inner_train, inner_test in KFold(len(outer_train),
                                             n_folds=self.inner_fold, shuffle=True):
                inner.append({'TrainIndex': list(samples[inner_train]), 
                              'TestIndex': list(samples[inner_test])})
            outer.append({'TrainIndex': list(samples[outer_train]), 
                          'TestIndex': list(samples[outer_test]),
                          'Inner': inner})
        return outer
        
    
    def SaveToJSON(self, obj, filename):
        outfile = os.path.join(self.param_dir, filename)
        print 'WriteFile:', outfile
        json.dump(obj, open(outfile, 'w'))
        
    def LoadFromJSON(self, filename):
        infile = os.path.join(self.param_dir, filename)
        print 'ReadFile:', infile
        return json.load(open(infile, 'r'))
        
    def CVIndex(self):
        self.SaveToJSON(self.p['CVIndex'], 'CVIndex.json')
        
    def SelectKmerCV(self):
        p = copy.deepcopy(self.p)
        BaseDir = self.p['BaseDir']
        p['BinCountFile'] = '{BaseDir}/BinCounts.h5'.format(**locals())
        p['KmerListFilteredByFreq'] = self.templates['KmerListFilteredByFreq'].format(**locals())
        params_list = []
        for MiscCond in self.misc_conds:
            KmerRankMin = self.misc_cond_vars[MiscCond]['KmerRankMin']
            KmerRankMax = self.misc_cond_vars[MiscCond]['KmerRankMax']
            Bootstrap = self.misc_cond_vars[MiscCond]['Bootstrap']

            KmerMatrixFileOuter = self.templates['KmerMatrixFileOuter']
            KmerMatrixFileInner = self.templates['KmerMatrixFileInner']
            KmerListFileOuter = self.templates['KmerListFileOuter']
            KmerListFileInner = self.templates['KmerListFileInner']
            # use the previous k-mer matrix as the input for MRMR
            FeatureNum = 0
            if MiscCond.startswith('MRMR_'):
                MRMRMiscCond = self.misc_cond_vars[MiscCond]['MRMRMiscCond']
                KmerMatrixFileOuter = re.sub(r'{MiscCond}', MRMRMiscCond, self.templates['KmerMatrixFileOuter'])
                KmerMatrixFileInner = re.sub(r'{MiscCond}', MRMRMiscCond, self.templates['KmerMatrixFileInner'])
                KmerListFileOuter = re.sub(r'{MiscCond}', MRMRMiscCond, self.templates['KmerListFileOuter'])
                KmerListFileInner = re.sub(r'{MiscCond}', MRMRMiscCond, self.templates['KmerListFileInner'])
                FeatureNum = self.misc_cond_vars[MiscCond]['FeatureNum']
            for FilterMetric in self.filter_metrics:
                for Pair in self.pairs:
                    for OuterIndex, outer in enumerate(self.p['CVIndex'][Pair]):
                        param = {'Pair': Pair,
                                 'FilterMetric': FilterMetric,
                                 'KmerRankMin': KmerRankMin,
                                 'KmerRankMax': KmerRankMax,
                                 'Bootstrap': Bootstrap,
                                 'RelevanceFile': self.templates['RelevanceFileOuter'].format(**locals()),
                                 'KmerListFile': KmerListFileOuter.format(**locals()),
                                 'KmerMatrixFile': KmerMatrixFileOuter.format(**locals()),
                                 'TrainIndex': outer['TrainIndex'],
                                 'TestIndex': outer['TestIndex']
                                 }
                        if MiscCond.startswith('MRMR_'):
                            param['MRMRInputFile'] = self.templates['MRMRInputFileOuter'].format(**locals())
                            param['MRMROutputFile'] = self.templates['MRMROutputFileOuter'].format(**locals())
                            param['MRMRKmerListFile'] = self.templates['MRMRKmerListFileOuter'].format(**locals())
                            param['FeatureNum'] = self.misc_cond_vars[MiscCond]['FeatureNum']
                            param['MaxFeatureNum'] = self.misc_cond_vars[MiscCond]['MaxFeatureNum']
                        param['SortReleAscend'] = True if FilterMetric in ('MannWhitneyUTest', 'MWUTestFreq') else False
                        params_list.append(param)
                        for InnerIndex, inner in enumerate(outer['Inner']):
                            param = {'Pair': Pair,
                                     'FilterMetric': FilterMetric,
                                     'KmerRankMin': KmerRankMin,
                                     'KmerRankMax': KmerRankMax,
                                     'Bootstrap': Bootstrap,
                                     'RelevanceFile': self.templates['RelevanceFileInner'].format(**locals()),
                                     'KmerListFile': KmerListFileInner.format(**locals()),
                                     'KmerMatrixFile': KmerMatrixFileInner.format(**locals()),
                                     'TrainIndex': inner['TrainIndex'],
                                     'TestIndex': inner['TestIndex']
                                     }
                            if MiscCond.startswith('MRMR_'):
                                param['MRMRInputFile'] = self.templates['MRMRInputFileInner'].format(**locals())
                                param['MRMROutputFile'] = self.templates['MRMROutputFileInner'].format(**locals())
                                param['MRMRKmerListFile'] = self.templates['MRMRKmerListFileInner'].format(**locals())
                                param['FeatureNum'] = self.misc_cond_vars[MiscCond]['FeatureNum']
                                param['MaxFeatureNum'] = self.misc_cond_vars[MiscCond]['MaxFeatureNum']
                            param['SortReleAscend'] = True if FilterMetric in ('MannWhitneyUTest', 'MWUTestFreq') else False
                            params_list.append(param)
        p['Params'] = params_list
        avgcov = pd.read_table(os.path.join(BaseDir, 'AverageCoverage.txt'), header=None)
        p['AverageCoverage'] = list(avgcov.ix[:, 1])
        self.SaveToJSON(p, 'SelectKmerCV.json')
   
    def DumpKmerRelevance(self):
        p = copy.deepcopy(self.p)
        BaseDir = self.p['BaseDir']
        FilterMetric = 'Frequency'
        OuterIndex = 0
        InnerIndex = 0
        p['FilterMetric'] = FilterMetric
        p['KmerListFile'] = self.templates['KmerListFileInner'].format(**locals())
        p['TrainIndex'] = self.p['CVIndex'][OuterIndex]['Inner'][InnerIndex]['TrainIndex']
        self.SaveToJSON(p, 'DumpKmerRelevance.json')
        
    def FeatureRobustness(self):
        p = copy.deepcopy(self.p)
        BaseDir = self.p['BaseDir']
        p['FeatureRobustnessFile'] = self.templates['FeatureRobustnessFile'].format(**locals())
        params = []
        for MiscCond in self.misc_conds:
            for FilterMetric in self.filter_metrics:
                pouter = []
                for OuterIndex, outer in enumerate(self.p['CVIndex']):
                    pouter.append(self.templates['KmerListFileOuter'].format(**locals()))
                params.append({'Name': 'outer',
                               'FilterMetric': FilterMetric,
                               'KmerListFiles': pouter})
                for OuterIndex, outer in enumerate(self.p['CVIndex']):
                    pinner = []
                    for InnerIndex, inner in enumerate(outer['Inner']):
                        pinner.append(self.templates['KmerListFileInner'].format(**locals()))
                    params.append({'Name': 'inner_%d'%(OuterIndex),
                                   'FilterMetric': FilterMetric,
                                   'KmerListFiles': pinner})
        p['Params'] = params
        self.SaveToJSON(p, 'FeatureRobustness.json')
        
    def Contour(self):
        p = copy.deepcopy(self.p)
        p['ContourFile'] = '{BaseDir}/Contour.h5'.format(**p)
        self.SaveToJSON(p, 'Contour.json')
        
    def CreateKmerMatrix(self, filter_by_freq=False, output_file='CreateKmerMatrix.json'):
        '''
        filter_by_freq: output the whole matrix filtered by frequency
        '''
        n_random = 10
        p = copy.deepcopy(self.p)
        BaseDir = self.p['BaseDir']
        params = []
        if filter_by_freq:
            output_file = 'CreateKmerMatrixFilteredByFreq.json'
            params.append({'KmerListFile': '{BaseDir}/Cache/KmerListFilteredByFreq.h5'.format(**locals()),
                    'KmerMatrixFile': '{BaseDir}/Cache/KmerMatrixFilteredByFreq.h5'.format(**locals())})
        else:
            for MiscCond in self.misc_conds:
                if MiscCond.startswith('Random'):
                    for RandomIndex in xrange(n_random):
                        params.append({'KmerListFile': self.templates['KmerListFileRandom'].format(**locals()),
                                       'KmerMatrixFile': self.templates['KmerMatrixFileRandom'].format(**locals()),
                                       'Random': True})
                        continue
                # if we want to select all k-mers, we do not need a k-mer list file
                # we also do not need to generate a matrix for each CV fold
                if MiscCond == 'All':
                    param = {}
                    param['KmerMatrixFile'] = self.templates['KmerMatrixFileAll'].format(**locals())
                    params.append(param)
                    continue
                for Pair in self.pairs:
                    for FilterMetric in self.filter_metrics:
                        for OuterIndex, outer in enumerate(self.p['CVIndex'][Pair]):
                            param = {}
                            param['KmerMatrixFile'] = self.templates['KmerMatrixFileOuter'].format(**locals())
                            param['KmerListFile'] = self.templates['KmerListFileOuter'].format(**locals())
                            params.append(param)
                            for InnerIndex, inner in enumerate(outer['Inner']):
                                param = {}
                                param['KmerMatrixFile'] = self.templates['KmerMatrixFileInner'].format(**locals())
                                param['KmerListFile'] = self.templates['KmerListFileInner'].format(**locals())
                                params.append(param)
        p['Params'] = params
        avgcov = pd.read_table(os.path.join(BaseDir, 'AverageCoverage.txt'), header=None)
        p['AverageCoverage'] = list(avgcov.ix[:, 1])
        
        self.SaveToJSON(p, output_file)
        
    def CrossValidation(self, pair, classifier, filter_metric, misc_cond):
        p = copy.deepcopy(self.p)
        BaseDir = self.p['BaseDir']
        Classifier = classifier
        Pair = pair
        p['Classifier'] = classifier
        p['FilterMetric'] = filter_metric
        p['HyperParams'] = self.hyperparams[classifier]
        p['MiscCond'] = misc_cond
        p['Pair'] = pair
        params_outer = []
        params_inner = []
        FilterMetric = filter_metric
        MiscCond = misc_cond
        
        Y = [info['Group'] for info in p['SampleInfo']]
        KmerRankMin = self.misc_cond_vars[MiscCond]['KmerRankMin']
        KmerRankMax = self.misc_cond_vars[MiscCond]['KmerRankMax']
        params_common = {'Y': Y,
                         'KmerRankMin': KmerRankMin,
                         'KmerRankMax': KmerRankMax}
        # Top_* conditions use the same matrix
        KmerMatrixFileInner = self.templates['KmerMatrixFileInner']
        KmerMatrixFileOuter = self.templates['KmerMatrixFileOuter']
        if misc_cond.startswith('Top_'):
            if int(misc_cond.split('_')[1]) < 10000:
                KmerMatrixFileInner = re.sub(r'{MiscCond}', 'Top_10000', 
                                             KmerMatrixFileInner)
                KmerMatrixFileOuter = re.sub(r'{MiscCond}', 'Top_10000',
                                             KmerMatrixFileOuter)
        elif misc_cond.startswith('All'):
            KmerMatrixFileInner = self.templates['KmerMatrixFileAll']
            KmerMatrixFileOuter = KmerMatrixFileInner

        for OuterIndex, outer in enumerate(self.p['CVIndex'][Pair]):
            ParamNames, ParamValueList = zip(*p['HyperParams'].iteritems())
            pouter_hyper = []
            for ParamIndex, ParamValues in enumerate(itertools.product(*ParamValueList)):
                ParamDict = {name:value for name, value in zip(ParamNames, ParamValues)}
                pouter_inner= []
                for InnerIndex, inner in enumerate(outer['Inner']):
                    param_inner = copy.deepcopy(params_common)
                    param_inner.update({'OuterIndex': OuterIndex,
                                   'InnerIndex': InnerIndex,
                                   'TrainIndex': inner['TrainIndex'],
                                   'TestIndex':  inner['TestIndex'],
                                   'HyperParams': ParamDict,
                                   'KmerMatrixFile': KmerMatrixFileInner.format(**locals()),
                                   'ResultFile': self.templates['ResultFileInner'].format(**locals())})
                    params_inner.append(param_inner)
                    pouter_inner.append({'ResultFile': param_inner['ResultFile']})
                pouter_hyper.append({'Inner': pouter_inner,
                                     'HyperParams': ParamDict})
            param_outer = copy.deepcopy(params_common)
            param_outer.update({'OuterIndex': OuterIndex,
                                'TrainIndex': outer['TrainIndex'],
                                'TestIndex': outer['TestIndex'],
                                'Hyper': pouter_hyper,
                                'KmerMatrixFile': KmerMatrixFileOuter.format(**locals()),
                                'ResultFile': self.templates['ResultFileOuter'].format(**locals())})
            params_outer.append(param_outer)
        p['CVInner'] = params_inner
        p['CVOuter'] = params_outer
        self.SaveToJSON(p, 'CV.{Pair}.{MiscCond}.{FilterMetric}.{Classifier}.json'.format(**locals()))
    
    def CrossValidationAll(self):
        for pair, classifier, filter_metric, misc_cond in itertools.product(self.pairs, self.classifiers, self.filter_metrics, self.misc_conds):
            self.CrossValidation(pair, classifier, filter_metric, misc_cond)
        
    def RunAll(self):
        ##self.CVIndex()
        self.SelectKmerCV()
        self.CreateKmerMatrix()
        self.Contour()
        self.CrossValidationAll()
        self.FeatureRobustness()
                
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('command', type=str,
                        choices=('All', 'SelectKmerCV', 'CreateKmerMatrix', 'Contour',
                                 'CrossValidation', 'CVIndex', 'FeatureRobustness',
                                 'DumpKmerRelevance'))
    parser.add_argument('-k', type=int, required=True)
    parser.add_argument('-d', '--base-dir', type=str, required=False)
    parser.add_argument('--inner-fold', type=int, required=False, default=5)
    parser.add_argument('--outer-fold', type=int, required=False, default=10)
    parser.add_argument('--param-dir', type=str, required=False, default='Param')
    parser.add_argument('--cvindex-file', action='store_false', required=False, default=True)
    parser.add_argument('--classifiers', type=str, required=False, default='')
    parser.add_argument('--filter-metrics', type=str, required=False, default='')
    parser.add_argument('--misc-conds', type=str, required=False, default='')
    parser.add_argument('--filter-by-freq', action='store_true', required=False, default=False)
    parser.add_argument('--min-count-ratio', type=str, required=False)
    parser.add_argument('--kmer-count-dir', type=str, required=False, default='output')
    args = parser.parse_args()
    
    k = args.k
    if not args.base_dir:
        args.base_dir = 'KmerMarker/%d'%k
        
    DeferredImport()
    # split comma-separated arguments into lists
    args.classifiers = args.classifiers.split(',')
    args.filter_metrics = args.filter_metrics.split(',')
    args.misc_conds = args.misc_conds.split(',')

    paramgen = ParameterGenerator(args.k, args.outer_fold, args.inner_fold,
                                  args.base_dir, args.param_dir, args.cvindex_file,
                                  classifiers=args.classifiers,
                                  filter_metrics=args.filter_metrics,
                                  misc_conds=args.misc_conds,
                                  min_count_ratio=args.min_count_ratio,
                                  kmer_count_dir=args.kmer_count_dir)

    if args.command == 'All':
        paramgen.RunAll()
    elif args.command == 'CVIndex':
        paramgen.CVIndex()
    elif args.command == 'SelectKmerCV':
        paramgen.SelectKmerCV()
    elif args.command == 'CreateKmerMatrix':
        paramgen.CreateKmerMatrix(args.filter_by_freq)
    elif args.command == 'Contour':
        paramgen.Contour()
    elif args.command == 'CrossValidation':
        paramgen.CrossValidationAll()
    elif args.command == 'FeatureRobustness':
        paramgen.FeatureRobustness()
    elif args.command == 'DumpKmerRelevance':
        paramgen.DumpKmerRelevance()
    else:
        raise ValueError('invalid command: ' + args.command)
    
    
    
    
