#! /usr/bin/env python
import json,sqlite3,sys
from collections import namedtuple
import itertools, os, copy, argparse
#Performance = namedtuple('Performance', ['K', 'Pair', 'MiscCond', 'FilterMetric', 'Classifier', 'Mean', 'Std'])

def LoadJSON(filename):
    f = open(filename, 'r')
    t = json.load(f)
    f.close()
    return t

class ReportDatabase(object):
    column_names = ['Id', 'K', 'Pair', 'MiscCond', 'FilterMetric', 'Classifier', 'Mean', 'Std']
    column_name_list = ','.join(column_names[1:])
    value_columns = ['Mean', 'Id']

    def record_to_str(self, record):
        return '''{K},'{Pair}','{MiscCond}','{FilterMetric}','{Classifier}',{Mean},{Std}'''.format(**record)

    def __init__(self, dbfile):
        self.dbfile = dbfile
        self.dbcon = sqlite3.connect(self.dbfile)
        self.dbcon.row_factory = sqlite3.Row
        self.initdb()
        self.cursor = self.dbcon.cursor()

    def initdb(self):
        cursor = self.dbcon.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS Performance (Id INTEGER PRIMARY KEY AUTOINCREMENT,
K INTEGER NOT NULL,
Pair TEXT, MiscCond TEXT, FilterMetric TEXT,
Classifier TEXT, Mean REAL, Std REAL)''')
        self.dbcon.commit()

    def close(self):
        self.dbcon.close()
    
    def search(self, **kwargs):
        if 'Mean' in kwargs:
            del kwargs['Mean']
        if 'Std' in kwargs:
            del kwargs['Std']
        sql = 'SELECT * FROM Performance WHERE '
        sql_filter = ' AND '.join([key + ' = \'%s\''%value for key, value in kwargs.iteritems()])
        sql += sql_filter
        print sql
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def to_table(self):
        self.cursor.execute('SELECT * FROM Performance')
        row = self.cursor.fetchone()
        print '\t'.join(self.column_names)
        while row:
            print '\t'.join([str(row[key]) for key in self.column_names])
            row = self.cursor.fetchone()

    def insert(self, record):
        self.cursor.execute('INSERT INTO Performance (%s) VALUES (%s)'%(self.column_name_list, self.record_to_str(record)))

    def update(self, record):
        #self.cursor.execute('INSERT OR REPLACE INTO Performance VALUES (?)', (self.record_to_str(record),))
        cond = copy.deepcopy(record)
        for name in ('Mean', 'Std'):
            if name in cond:
                del name[cond]
        results = self.search(**cond)
        if len(results) == 0:
            self.cursor.execute('INSERT INTO Performance (%s) VALUES (%s)'%(self.column_name_list, self.record_to_str(record)))
        elif len(results) == 1:
            self.cursor.execute('UPDATE Performance SET %s WHERE Id=%d'%(', '.join(['%s=%s'%(key, record[key]) for key in self.value_columns]),
                results[0]['Id']))
        else:
            raise ValueError('More than one records matched: ' + self.record_to_str(record))
        self.dbcon.commit()

class DetailDatabase(ReportDatabase):
    column_names = ['Id', 'K', 'Pair', 'MiscCond', 'FilterMetric', 'Classifier', 'OuterIndex', 'InnerIndex', 'EvalMetric', 'EvalValue']
    column_name_list = ','.join(column_names[1:])
    value_columns = ['EvalValue']

    def initdb(self):
        cursor = self.dbcon.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS Performance (Id INTEGER PRIMARY KEY AUTOINCREMENT,
K INTEGER NOT NULL,
Pair TEXT, MiscCond TEXT, FilterMetric TEXT,
Classifier TEXT, OuterIndex INTEGER, InnerIndex INTEGER, EvalMetric TEXT, EvalValue REAL)''')
        self.dbcon.commit()

def Report(param_files, eval_metric):
    print 'Pair\tMiscCond\tFilterMetric\tClassifier\tMean\tStd'
    for param_file in param_files:
        p = LoadJSON(param_file)
        perf = []
        try:
            for outer in p['CVOuter']:
                result = LoadJSON(outer['ResultFile'])
                perf.append(result[eval_metric])
            print '{0}\t{1}\t{2}\t{3}\t{4}\t{5}'.format(p['Pair'], p['MiscCond'],
                            p['FilterMetric'], p['Classifier'],
                            np.mean(perf), np.std(perf))
        except IOError:
            pass

def TestReportDatabase():
    db = ReportDatabase('tmp/reportdb.sqlite')
    record = {'K': '30', 'Pair': 'Normal-Cancer', 'MiscCond': 'Top_10000', 'FilterMetric': 'Frequency', 'Classifier': 'SVM', 'Mean': 0.6, 'Std': 0.1}
    print 'Update record:', db.record_to_str(record)
    db.update(record)
    print 'Search', db.search(**record)
    print 'Convert to table'
    db.to_table()
    db.close()

class CVReport(object):
    def __init__(self):
        self.init_params()

    def init_params(self):
        self.pairs = ['Normal-Cancer', 'Normal-Stage_0', 'Normal-Stage_I_II', 'Normal-Stage_III_IV']
        self.misc_conds = {}
        self.filter_metrics = {}
        self.classifiers = {}
        self.nfolds_outer = 10
        self.nfolds_inner = 5
        
        for k in (20, 32):
            self.misc_conds[k] = ['Top_100', 'Top_200', 'Top_300', 'Top_400', 'Top_500', 'Top_1000', 'Top_10000', 'Top_100000',
                             'MRMR_100', 'MRMR_200', 'MRMR_300', 'MRMR_400', 'MRMR_500']
            self.filter_metrics[k] = ['MannWhitneyUTest', 'FreqDiff', 'MWUTestFreq']
            self.classifiers[k] = ['LogRegL2', 'RandomForest', 'GaussianNB']

        for k in range(4, 10):
            self.misc_conds[k] = ['All']
            self.filter_metrics[k] = ['Frequency']
            self.classifiers[k] = ['LogRegL2', 'RandomForest', 'GaussianNB'] 

        self.misc_conds[28] = ['Top_10000']
        self.filter_metrics[28] = ['InfoGain', 'MutualInfo', 'ProbabilityRatio']
        self.classifiers[28] = ['LogRegL2', 'RandomForest', 'GaussianNB'] 
        self.klist = (4, 5, 6, 7, 8, 9, 20, 28, 32) 
    # make archive
    def make_archive(self, filename):
        import zipfile
        zf = zipfile.ZipFile(filename, 'w', compression=zipfile.ZIP_STORED, allowZip64=True)
        for result_dir in self.iter_result_dir():
            for outer_index in range(self.nfolds_outer):
                result_file = os.path.join(result_dir, 'outer', '%d.json'%outer_index)
                try:
                    zf.write(result_file)
                except IOError as e:
                    print >>sys.stderr, 'Warning: cannot open %s: %d'%(result_file, e.errno)
                    pass
        zf.close()
    
    def iter_result_dir(self):
        for k in self.misc_conds.keys():
            for pair, misc_cond, filter_metric, classifier in itertools.product(
                    self.pairs, self.misc_conds[k], self.filter_metrics[k], self.classifiers[k]):
                result_dir = os.path.join('KmerMarker', str(k), 'CV', pair, misc_cond, filter_metric, classifier)
                if os.path.exists(result_dir):
                    yield result_dir

    def make_table(self, filename=None):
        # generate CV report
        import numpy as np
        from sklearn import metrics

        fout = None
        if filename == '-':
            fout = sys.stdout
        else:
            fout = open(filename, 'w')
        header = '\t'.join(('K', 'Pair', 'MiscCond', 'FilterMetric', 'Classifier', 'OuterIndex', 'EvalMetric', 'EvalValue'))
        fout.write(header + '\n')
        for k in self.misc_conds.keys():
            for pair, misc_cond, filter_metric, classifier in itertools.product(
                    self.pairs, self.misc_conds[k], self.filter_metrics[k], self.classifiers[k]):
                result_dir = os.path.join('KmerMarker', str(k), 'CV', pair, misc_cond, filter_metric, classifier, 'outer')
                if not os.path.exists(result_dir):
                    continue
                try:
                    lines = []
                    for outer_index in xrange(self.nfolds_outer):
                        result_file = '%s/%d.json'%(result_dir, outer_index)
                        result = LoadJSON(result_file)
                        # calculate other metrics
                        y_pred = np.asarray(result['PredictedLabels'])
                        y_true = np.array(result['TrueLabels'])
                        result['Sensitivity'] = metrics.recall_score(y_true, y_pred)
                        result['Precision'] = metrics.precision_score(y_true, y_pred)
                        result['F1Score'] = metrics.f1_score(y_true, y_pred)
                        if np.isnan(result['AUC']):
                            print >>sys.stderr, 'Warning: AUC is nan in %s'%(result_file)
                            result['AUC'] = 0.5
                        for eval_metric in ('Accuracy', 'AUC', 'Sensitivity', 'Precision', 'F1Score'):
                            lines.append('\t'.join((str(k), pair, misc_cond, filter_metric, classifier, 
                                         str(outer_index), eval_metric, str(result[eval_metric]))) + '\n')
                    map(fout.write, lines)
                except IOError:
                    print >>sys.stderr, result_dir, '(not found)'
                    continue

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Read cross-validation results (outer loop) and report')
    parser.add_argument('--table', type=str, required=False, help='generate a detail table')
    parser.add_argument('--archive', type=str, required=False, help='save results files to a zip file')
    args = parser.parse_args()

    report = CVReport()
    if args.table:
        report.make_table(args.table)
    if args.archive:
        report.make_archive(args.archive)


