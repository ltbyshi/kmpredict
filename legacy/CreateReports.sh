#! /bin/bash

. Configuration.sh

report_CV(){
    OutDir=KmerMarker/$K/Report/CV
    mkdir -p KmerMarker/$K/Report/CV 2>/dev/null
    R --vanilla <<RSCRIPT
library(cowplot)
t <- read.table('KmerMarker/$K/Report/CrossValidation.txt', header=TRUE)
pairs <- unique(t[['Pair']])
misc_conds <- unique(t[['MiscCond']])
for(pair in pairs){
    for(misc_cond in misc_conds){
        st <- subset(t, (t[['Pair']] == pair) & (t[['MiscCond']] == misc_cond))
        plt <- ggplot(st) + geom_bar(aes(x=Classifier, y=Mean, fill=FilterMetric),
            stat='identity', position='dodge') + ylim(0, 1)
        filename <- sprintf('KmerMarker/$K/Report/CV/%s.%s.png', pair, misc_cond)
        print(filename)
        save_plot(filename, plt, base_aspect_ratio = 1.5)
    }
}
RSCRIPT
}

report_CV
