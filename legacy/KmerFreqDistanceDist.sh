#! /bin/bash

. Configuration.sh

OutDir=analysis/KmerFreqDistanceDist
[ -d "$OutDir" ] || mkdir -p $OutDir

unused(){
R --vanilla <<RSCRIPT
kmerfreq <- read.table('analysis/KmerFreqMatrix/kmer.$K.normedcounts.txt', header=TRUE, row.names=1);
kmerfreq <- as.matrix(kmerfreq);

distmat <- cor(t(kmerfreq));
write.table(distmat, file='$OutDir/pearsonr.kmer.$K.txt', sep='\t');
pdf('$OutDir/pearsonr.kmer.$K.pdf');
hist(distmat[upper.tri(distmat)], breaks=30, freq=FALSE,
    main='Histogram of PCC between samples (k=$K)',
    xlab='PCC', ylab='Density');
dev.off();
RSCRIPT
}

select_dataid > $OutDir/kmer.$K.dataid.txt
R --vanilla <<RSCRIPT
source('common.R');
metadata <- read.table('metadata/metadata.txt', header=TRUE);
dataids <- read.table('$OutDir/kmer.$K.dataid.txt', header=FALSE)[,1];
m <- read.kmerfiles(sprintf('output/%s/kmer.$K.counts', dataids),
    row.names=dataids, normalize=TRUE);

distmat <- cor(t(m));
write.table(distmat, file='$OutDir/pearsonr.kmer.$K.txt', sep='\t');
pdf('$OutDir/pearsonr.kmer.$K.pdf');
hist(distmat[upper.tri(distmat)], breaks=30, freq=FALSE,
    main='Histogram of PCC between samples (k=$K)',
    xlab='PCC', ylab='Density',
    xlim=c(0, 1));
dev.off();
RSCRIPT

