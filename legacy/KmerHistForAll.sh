#! /bin/bash

. Configuration.sh

kmer_hist(){
    DataId=$1
    R --vanilla <<RSCRIPT
t <- read.table('$OutDir/$DataId/kmer.$K.counts.txt', header=FALSE);
pdf('$OutDir/$DataId/kmer.$K.counts.hist.pdf');
hist(t[, 2], breaks=20,
    main='Distribution of Kmer($K) counts', xlab='Kmer Counts', ylab='Counts');
dev.off();
RSCRIPT
}

if [ -n "$1" ];then
    kmer_hist $1
    exit 0
fi

if [ "$NPROC" -gt 1 ];then
    select_dataid | xargs -l -P $NPROC $0
else
    for DataId in `select_dataid`;do
        kmer_hist $DataId
    done
fi
    