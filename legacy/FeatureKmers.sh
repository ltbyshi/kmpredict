#! /bin/bash

. Configuration.sh

CounterProgram=hashedcount

if [ "$CounterProgram" = "counter" ];then
    FileExtension=counts
    SubDir=KmerCounter
elif [ "$CounterProgram" = "filter" ];then
    FileExtension=filter
    SubDir=KmerFilter
elif [ "$CounterProgram" = "hashedcount" ];then
    FileExtension=hashed
    SubDir=Hashed
else
    echo "Invalid CounterProgram: $CounterProgram"
    exit 1
fi

OutDir=FeatureKmers
[ -d "$OutDir" ] || mkdir -p $OutDir
FeatureKmers(){
    [ -d "$OutDir/groupfiles" ] || mkdir -p "$OutDir/groupfiles"
    GroupFile=$OutDir/groupfiles/kmer.$K.txt
    sed '1 d' metadata/metadata.txt \
        | awk -v K=$K 'BEGIN{OFS="\t"}{print $2,sprintf("output/%s/kmer.%s.counts",$1,K)}' \
        > $GroupFile
    $BINPATH/KmerCounter charkmer $GroupFile > $OutDir/kmer.$K.txt
    echo "FeatureKmers K=$K"
    cat $OutDir/kmer.$K.txt
}

FeatureKmers_KmerFilter(){
    [ -d "$OutDir/$SubDir" ] || mkdir -p "$OutDir/$SubDir"
    GroupFile=$OutDir/$SubDir/kmer.$K.groups.txt
    sed '1 d' metadata/metadata.txt \
        | awk -v K=$K -v ext=$FileExtension 'BEGIN{OFS="\t"}{print $2,sprintf("output/%s/kmer.%s.%s",$1,K,ext)}' \
        > $GroupFile
    #return 0
    export ReadKmerFiles=1
    #$BINPATH/FindFeatureKmers count $GroupFile $OutDir/$SubDir/kmer.$K.
    #$BINPATH/FindFeatureKmers entropy $GroupFile $OutDir/$SubDir/kmer.$K.
    $BINPATH/FindFeatureKmers select $GroupFile $OutDir/$SubDir/kmer.$K.
    echo "FeatureKmers K=$K"
}

create_groupfiles() {
    [ -d "$OutDir/$SubDir" ] || mkdir -p "$OutDir/$SubDir"
    GroupFile=$OutDir/$SubDir/kmer.$K.groups.txt
    sed '1 d' metadata/metadata.txt \
        | awk -v K=$K -v ext=$FileExtension 'BEGIN{OFS="\t"}{print $2,sprintf("output/%s/kmer.%s.%s",$1,K,ext)}' \
        > $GroupFile
}

entropy_distribution(){
    Prefix=$OutDir/$SubDir/kmer.$K
    echo "Calculating entropy distribution $Prefixs"
    R --vanilla <<RSCRIPT
source('common.R');
proportion <- 0.2;
ent <- read.vector('$Prefix.entropies', 'double');
ent <- ent[1:(length(ent)*proportion)];

breaks = c(seq(0, 0.5, 0.02), 0.5);
h <- hist(ent, plot=FALSE, breaks=breaks);
df <- data.frame(Entropy=h[['breaks']][1:(length(h[['breaks']])-1)],
    Counts=h[['counts']]/proportion, 
    Quantile=cumsum(h[['counts']]/length(ent)));
write.table(df, '$Prefix.entropy.dist.txt', sep='\t',
    row.names=FALSE, quote=FALSE);

maxratio <- read.vector('$Prefix.maxratio', 'double');
maxratio <- maxratio[1:(length(maxratio)*proportion)];
breaks <- seq(0, 1, 0.05);
h <- hist(maxratio, plot=FALSE, breaks=breaks);
df <- data.frame(MaxRatio=h[['breaks']][1:(length(h[['breaks']])-1)],
    Counts=h[['counts']]/proportion,
    Quantile=cumsum(h[['counts']]/length(ent)));
write.table(df, '$Prefix.maxratio.dist.txt', sep='\t',
    row.names=FALSE, quote=FALSE); 
RSCRIPT
    cat $Prefix.entropy.dist.txt
    cat $Prefix.maxratio.dist.txt
}

select_feature_kmers(){
    Prefix=$OutDir/$SubDir/kmer.$K
    echo "Select feature kmers for $Prefix"
    R --vanilla <<RSCRIPT
source('common.R');
proportion <- 0.2;
ent <- read.vector('$Prefix.entropies', 'double');
maxratio <- reac.vector('$Prefix.maxratio', 'double');
n <- length(ent);
ent_thresh <- quantile(ent[1:(n*proportion)], 0.005);
maxratio_thresh <- quantile(maxratio[1:(n*proportion)], 0.2);

RSCRIPT
}

normalize_freq_matrix(){
    for DataId in $(select_dataid);do
        echo "Norm=\$($BINPATH/KmerFilter avgcountw $K Scaffold/$DataId/scaffold.fa output/$DataId/AverageCoverage.txt output/$DataId/kmer.$K.$FileExtension); echo -e \"$DataId\t\$Norm\"" 
    done | parallel -k -t -P $NPROC \
        > FeatureKmers/$K/AverageCounts.txt
}

#if [ "$CounterProgram" = "filter" ] || [ "$CounterProgram" = "hashedcount" ];then
    #FeatureKmers_KmerFilter
    #entropy_distribution
    #normalize_freq_matrix
#fi
normalize_freq_matrix
#create_groupfiles

        
        