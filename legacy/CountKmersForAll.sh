#! /bin/bash

. Configuration.sh

calc_avg_coverage(){
    DataId=$1
    [ -d "$OutDir/$DataId" ] || mkdir -p "$OutDir/$DataId"
    python CalcAvgCovForScaffold.py $ScaffoldDir/$DataId/scaffold_bowtied.sam.genehits \
        $OutDir/$DataId/AverageCoverage.txt
}

count_kmers(){
    DataId=$1
    [ -d "$OutDir/$DataId" ] || mkdir -p "$OutDir/$DataId"
    if [ "$KmerCounterOption" = "countw" ];then
        $BINPATH/KmerCounter countw $K $ScaffoldDir/$DataId/scaffold.fa $OutDir/$DataId/AverageCoverage.txt \
            $OutDir/$DataId/kmer.$K.counts
    elif [ "$KmerCounterOption" = "count" ];then
        $BINPATH/KmerCounter count $K $ScaffoldDir/$DataId/scaffold.fa \
            $OutDir/$DataId/kmer.$K.counts
    elif [ "$KmerCounterOption" = "hashedcountw" ];then
        $BINPATH/KmerCounter hashedcountw $K $HashSize Hyperplanes/$K.$HashSize \
            $ScaffoldDir/$DataId/scaffold.fa \
            $OutDir/$DataId/AverageCoverage.txt \
            $OutDir/$DataId/kmer.$K.counts
    fi
}

if [ -n "$1" ];then
    #calc_avg_coverage $1
    count_kmers $1
    exit 0
fi

if [ "$NPROC" -gt 1 ];then
    select_dataid | xargs -t -l -P $NPROC $0
else
    for DataId in `select_dataid`;do
        #calc_avg_coverage $DataId
        count_kmers $DataId
    done
fi

