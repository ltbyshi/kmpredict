#! /bin/bash

echo -e "DataId\tState1\tState2\tSubjectId\tPreserve" > metadata/metadata.txt
awk 'BEGIN{OFS="\t"} 
    {split($2, a, "[_-]"); printf($1); \
    for(i=1;i<=length(a);i++){printf("\t%s", a[i])} printf("\n")}' \
    metadata/SampleId.txt >> metadata/metadata.txt
    