#! /bin/bash

. Configuration.sh

OutDir=analysis/KmerFreqHeatmap
[ -d "$OutDir" ] || mkdir -p $OutDir

unused(){
R --vanilla << RSCRIPT
library('RColorBrewer');
kmerfreq <- read.table('analysis/KmerFreqMatrix/kmer.$K.normedcounts.txt', header=TRUE, row.names=1);
png('$OutDir/kmer.$K.png', width=2000, height=1000);
heatmap(as.matrix(kmerfreq), labRow=NA, labCol=NA,
    col=colorRampPalette(brewer.pal(5, 'BuGn'))(100),
    hclustfun=function(x) hclust(x, method='average'));
dev.off();
RSCRIPT
}

select_dataid > $OutDir/kmer.$K.dataid.txt
R --vanilla <<RSCRIPT
library('RColorBrewer');
source('common.R');
dataids <- read.table('$OutDir/kmer.$K.dataid.txt', header=FALSE)[,1];
m <- read.kmerfiles(sprintf('output/%s/kmer.$K.counts', dataids),
    row.names=dataids, normalize=TRUE);
png('$OutDir/kmer.$K.png', width=4500, height=4000);
#pdf('$OutDir/kmer.$K.pdf', title='Heatmap of Kmer($K) frequencies');
heatmap(m, labCol=NA, Colv=NA, Rowv=NA,
    col=colorRampPalette(brewer.pal(5, 'BuGn'))(100),
    hclustfun=function(x) hclust(x, method='average'),
    main='Heatmap of Kmer($K) frequencies');
dev.off();

RSCRIPT
