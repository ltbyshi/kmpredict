#! /bin/bash

. Configuration.sh

OutDir=FeatureKmers

gen_kmer_freq_matrix(){
    R --vanilla <<RSCRIPT
metadata <- read.table('metadata/metadata.txt', header=TRUE);
m <- data.frame();
kmerhash <- NULL;
for(filename in sprintf('output/%s/kmer.%d.featkmers.counts.txt', metadata[['DataId']], $K)){
    print(filename);
    tmp <- read.table(filename, header=FALSE);
    kmerhash <- tmp[, 1];
    m <- rbind(m, tmp[, 2]);
}
rownames(m) <- kmerhash;
colnames(m) <- metadata[['DataId']];
write.table(m, file='$OutDir/FreqMatrix/kmer.$K.txt', quote=FALSE, sep='\t');
RSCRIPT
}

gen_kmer_freq_matrix(){
    KmerFreqFiles=$(select_dataid \
        | awk -v k=$K '{printf("output/%s/kmer.%d.featkmers.counts.txt\n",$1,k)}')
    KmerFreqFiles=$(echo $KmerFreqFiles)
    #echo $KmerFreqFiles
    $BINPATH/ScanKmers matrix $KmerFreqFiles > $OutDir/FreqMatrix/kmer.$K.txt
}

clustering_heatmap(){
    R --vanilla <<RSCRIPT
library(RColorBrewer);
library(gplots);
metadata <- read.table('metadata/metadata.txt', header=TRUE);
datanorm <- read.table('$OutDir/FreqMatrix/kmer.$K.norm', header=FALSE);
states <- c('Few_polyps','Multiple_polyps', 'Normal',
    'Stage_0', 'Stage_I_II', 'Stage_III_IV');
m <- as.matrix(read.table('$OutDir/FreqMatrix/kmer.$K.txt', header=FALSE));
m <- m/datanorm[,2];
colnames(m) = metadata[['State']];
m <- m[, order(colnames(m))];
featgroup <- apply(m, 1, which.max);
#m <- m[order(featgroup), ];
m <- log2(m + 1);
png('$OutDir/FreqMatrix/kmer.$K.png', width=4500, height=4000);
heatmap(m, labCol=NA, labRow=NA, Colv=NA, Rowv=NA,
    col=colorRampPalette(brewer.pal(5, 'Greys'))(100),
    hclustfun=function(x) hclust(x, method='average'));
dev.off();
RSCRIPT
}

pca_analysis(){
    R --vanilla << RSCRIPT
library(RColorBrewer);
library(gplots);
library(cowplot);
metadata <- read.table('metadata/metadata.txt', header=TRUE);
state <- metadata[['State']];
state[state == 'Few_polyps'] <- 'Normal';
state[state == 'Stage_0'] <- 'Cancer';
state[state == 'Stage_I_II'] <- 'Cancer';
state[state == 'Stage_III_IV'] <- 'Cancer';
datanorm <- read.table('$OutDir/FreqMatrix/kmer.$K.norm', header=FALSE);
m <- as.matrix(read.table('$OutDir/FreqMatrix/kmer.$K.txt', header=FALSE));
# normalize
m <- m/datanorm[,2];
colnames(m) = state;
m <- t(m);
m <- log2(m + 1);
fit <- prcomp(m);
plot.data <- data.frame(State=rownames(m),
    PC1=fit[['x']][, 1], PC2=fit[['x']][, 2]);
plt <- ggplot(data=plot.data) +
    geom_point(aes(x=PC1, y=PC2, color=State), size=1) +
    ggtitle('PCA analysis of Kmer($K) frequency');
save_plot('$OutDir/FreqMatrix/kmer.$K.pca.pdf', plot=plt,
    base_aspect_ratio=1.6);
RSCRIPT
}

logistic_reg(){
    R --vanilla --args 0.05 $OutDir/FreqMatrix/kmer.$K \
        < Mizutani/command.9-2.R
}

#gen_kmer_freq_matrix
#clustering_heatmap
#pca_analysis
logistic_reg
