#! /bin/bash

OutDir=Random

generate_random_sequences(){
    DataId=$1
    local OutDir=$OutDir/$DataId
    echo "generating $OutDir"
    rm -rf $OutDir
    mkdir $OutDir
    ./SequenceGenerator > $OutDir/scaffold.fa
    seq 5000 | awk '{printf("RN_%010d\tgene_id_%d\t1000\t1.00\n",$1,$1)}' \
        > $OutDir/scaffold_bowtied.sam.genehits
    rm -f Scaffold/$DataId
    ln -f -s $PWD/$OutDir Scaffold/
}

for i in $(seq 5);do
    generate_random_sequences random_0$i
done
