#! /usr/bin/env python

import argparse
import random
import numpy as np
import itertools

DNACode = {'A': -0.9995+0j, 'T': 0.9995+0j, 'C': 0-0.9995j, 'G': 0+0.9995j}

def RandomSequence(size, alphabet='ATGC'):
    s = bytearray([random.choice(alphabet) for i in xrange(size)])
    return str(s)

def EncodeDNA(s):
    return list(map(lambda x: DNACode.get(x, 0+0j), s))

def AffineHull(m):
    for i in xrange(len(m)):
        m[i].append(-1)
    m.append([0]*len(m[0]))
    m = np.array(m)
    u, w, v = np.linalg.svd(m)
    return list(v[-1,:])

def HyperplaneHash(seq, hp, reverse_complement=False):
    x = np.array(EncodeDNA(seq))
    W, Wc = hp
    hashsize = W.shape[0]
    pow2 = [2**i for i in xrange(hashsize)]
    code = np.dot((np.dot(x, W.transpose().conjugate()) > Wc), pow2)
    if reverse_complement:
        x_rc = x[::-1]*-1
        code_rc = np.dot((np.dot(x, W.transpose().conjugate()) > Wc), pow2)
        if code <= code_rc:
            return code
        else:
            return code_rc
    else:
        return code
    
def RandomHyperplane(k, hashsize):
    W = []
    Wc = []
    for i in xrange(hashsize):
        kmers = []
        for j in xrange(k*20):
            kmers.append(EncodeDNA(RandomSequence(k)))
        w = AffineHull(kmers)
        Wc.append(w.pop())
        W.append(w)
    
    return (np.array(W), np.array(Wc))
        
def TestCollisionRatio(k, hashsize):
    hp = RandomHyperplane(k, hashsize)
    print hp[0]
    print hp[1]
    t = np.full(2**hashsize, False, dtype=bool)
    alphabet = 'ATCG'
    collisions = 0
    groups = {}
    for i in xrange(4**k):
        seq = bytearray(['A']*k)
        code = i
        for j in xrange(k):
            seq[k - 1 - j] = alphabet[(i >> (j*2)) & 0x03]
        seq = str(seq)
        hashed = HyperplaneHash(seq, hp)
        #print '%s => %d'%(seq, hashed)
        if t[hashed]:
            collisions += 1
            groups[hashed].append(seq)
        else:
            t[hashed] = True
            groups[hashed] = [seq]
    #for hashed in groups.iterkeys():
    #    print '{:08b} => {:s}'.format(hashed, ','.join(groups[hashed]))
    print 'CollisionRatio (k={:d}, hashsize={:d}): {:f} ({:d}/{:d})'.format(k,
                hashsize, float(collisions)/(4**k), collisions, 4**k)
    
if __name__ == '__main__':
    
    seq = RandomSequence(8)
    print 'Sequence:', seq
    print 'Code: ', EncodeDNA(seq)
    hp = RandomHyperplane(k=8, hashsize=5)
    print hp[0]
    print hp[1]
    hashed = HyperplaneHash(seq, hp)
    print 'Hashed code:', hashed
    
    #TestCollisionRatio(4, 8)
    
    
