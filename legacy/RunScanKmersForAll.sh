#! /bin/bash

. Configuration.sh

OutDir=output

ScanKmerOption=hashed

run_scankmers_exact(){
    DataId=$1
    $BINPATH/ScanKmers exact $K \
        FeatureKmers/KmerFilter/kmer.$K.featkmers.txt \
        Scaffold/$DataId/scaffold.fa \
        $OutDir/$DataId/AverageCoverage.txt \
        $OutDir/$DataId/kmer.$K.featkmers.counts.txt
}

run_scankmers_hashed(){
    DataId=$1
    $BINPATH/ScanKmers hashed $K $HashSize \
        FeatureKmers/Hashed/kmer.$K.featkmers.txt \
        Hyperplanes/$K.$HashSize \
        Scaffold/$DataId/scaffold.fa \
        $OutDir/$DataId/AverageCoverage.txt \
        $OutDir/$DataId/kmer.$K.featkmers.counts.txt
}

if [ -n "$1" ];then
    if [ "$ScanKmerOption" = "exact" ];then
        run_scankmers_exact $1
    elif [ "$ScanKmerOption" = "hashed" ];then
        run_scankmers_hashed $1
    else
        echo "Error: invalid ScanKmerOption: $ScanKmerOption"
        exit 1
    fi
    exit 0
fi

select_dataid | xargs -t -l -P $NPROC $0
