#! /bin/bash

. Configuration.sh

AnalysisDir=analysis/KmerFreqMatrix
[ -d "$AnalysisDir" ] || mkdir -p $AnalysisDir

normalize_kmer_freq(){
    # read kmer counts
    DataId=$1
    ./KmerCounter dump $OutDir/$DataId/kmer.$K.counts \
        > $OutDir/$DataId/kmer.$K.counts.txt
    awk '{t+=$2}END{print t}' $OutDir/$DataId/kmer.$K.counts.txt \
        > $OutDir/$DataId/kmer.$K.totalcounts.txt
    # normalization
    echo $DataId > $OutDir/$DataId/kmer.$K.normedcounts.txt
    awk -v t=$(cat $OutDir/$DataId/kmer.$K.totalcounts.txt) \
        '{if(t>0){print $2/t} else{print 0}}' $OutDir/$DataId/kmer.$K.counts.txt \
        >> $OutDir/$DataId/kmer.$K.normedcounts.txt
}

calc_kmer_freq_matrix(){
    # generate header
    FirstDataId=$(select_dataid | head -n 1)
    cat <(echo 'DataId') <(cut -f1 $OutDir/$FirstDataId/kmer.$K.counts.txt) \
        | tr '\n' '\t' \
        > $AnalysisDir/kmer.$K.normedcounts.txt
    echo >> $AnalysisDir/kmer.$K.normedcounts.txt
    for DataId in `select_dataid`;do
        cat $OutDir/$DataId/kmer.$K.normedcounts.txt | tr '\n' '\t' \
            >> $AnalysisDir/kmer.$K.normedcounts.txt
        echo >> $AnalysisDir/kmer.$K.normedcounts.txt
    done
}

if [ -n "$1" ];then
    normalize_kmer_freq $1
    exit 0
fi

if [ "$NPROC" -gt 1 ];then
    select_dataid | xargs -t -l -P $NPROC $0
else
    for DataId in `select_dataid`;do
        normalize_kmer_freq $DataId
    done
fi
calc_kmer_freq_matrix
    