#! /bin/bash

. Configuration.sh

DataId=random_01
MinK=4
MaxK=12
OutDir=analysis/TimeBenchmark
[ -d "$OutDir" ] || mkdir -p $OutDir

time_kmercounter(){
for K in $(seq 4 12);do
    echo "Time benchmark for $K on $DataId" >&2
    Time=$(/usr/bin/time -f %e ./KmerCounter countw $K \
        $ScaffoldDir/$DataId/scaffold.fa \
        output/$DataId/AverageCoverage.txt \
        tmp/$DataId.kmer.$K.counts 2>&1 >/dev/null)
    echo -e "$K\t$Time"
done
}

time_kmerfilter(){
for K in 10 11 12 13 14 15;do
    echo "Time benchmark for $K on $DataId" >&2
    Time=$(/usr/bin/time -f %e ./KmerFilter count $K $ScaffoldDir/$DataId/scaffold.fa \
        tmp/$DataId.kmer.$K.filter 2>&1 >/dev/null)
    echo -e "$K\t$Time"
done
}

#time_kmercounter >> $OutDir/KmerCounter.txt
time_kmerfilter > $OutDir/KmerFilter.txt

