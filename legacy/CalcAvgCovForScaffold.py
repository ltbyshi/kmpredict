#! /usr/bin/env python

import argparse

def CalcAvgCovForScaffold(infile, outfile):
    import pandas as pd
    from pandas import DataFrame, Series
    
    t = pd.read_table(infile, header=None, 
                      names=['ScaffoldId', 'GeneId', 'Length', 'Coverage'])
    t['CovSum'] = t['Length'] * t['Coverage']
    avgcov = t.groupby('ScaffoldId', as_index=False)[['Length', 'CovSum']].sum()
    avgcov['AvgCov'] = avgcov['CovSum'] / avgcov['Length']
    avgcov = avgcov[['ScaffoldId', 'AvgCov']]
    avgcov.to_csv(outfile, index=False, header=False, sep='\t')
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Calculate average coverage of each scaffold')
    parser.add_argument('infile', type=str)
    parser.add_argument('outfile', type=str)
    args = parser.parse_args()
    
    CalcAvgCovForScaffold(args.infile, args.outfile)
    