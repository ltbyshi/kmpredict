#! /bin/bash
. Configuration.sh

FileExtension=filter
normalize_coverage(){
    for DataId in $(select_dataid);do
        echo "Norm=\$($BINPATH/KmerFilter avgcountw $K Scaffold/$DataId/scaffold.fa output/$DataId/AverageCoverage.txt output/$DataId/kmer.$K.$FileExtension); echo -e \"$DataId\t\$Norm\"" 
    done | parallel -k -t -P $NPROC \
        > KmerMarker/$K/AverageCoverage.txt
}

normalize_coverage
