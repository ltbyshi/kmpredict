#! /bin/bash

. Configuration.sh

OutDir=analysis/PCAOnKmerFreq
[ -d "$OutDir" ] || mkdir -p $OutDir

concat_kmerfiles()
{
    for DataId in `select_dataid`;do
        dd if=output/$DataId/kmer.$K.counts skip=12 conv=
    done
}
rm -f $OutDir/FreqMatrix.kmer.$K
mkfifo $OutDir/FreqMatrix.kmer.$K

unused(){
R --vanilla << RSCRIPT
library(ggplot2);
m <- read.table('analysis/KmerFreqMatrix/kmer.$K.normedcounts.txt', header=TRUE, row.names=1);
fit <- princomp(m);
metadata <- read.table('metadata/metadata.txt', header=TRUE, row.names=1);
metadata[['TwoState']] <- 'Normal';
metadata[['TwoState']][metadata[['State1']] > 2] <- 'Cancer';
plot.data <- data.frame(DataId=rownames(fit[['scores']]),
    x=fit[['scores']][, 1], y=fit[['scores']][, 2],
    State=metadata[['TwoState']]);
plt <- ggplot(data=plot.data) + geom_point(aes(x=x, y=y, color=State)) +
    ggtitle('PCA analysis of Kmer($K) frequency');
ggsave('$OutDir/kmer.$K.pdf', plot=plt);
RSCRIPT
}

pca_on_kmer_freq(){
nSamples=$(select_dataid | wc -l)
select_dataid > $OutDir/kmer.$K.dataid.txt
KmerMatrixFile=analysis/KmerFreqMatrix/kmer.$K.counts
R --vanilla << RSCRIPT
library(cowplot);
source('common.R');
metadata <- read.table('metadata/metadata.txt', header=TRUE, row.names=1);
metadata[['TwoState']] <- 'Normal';
metadata[['TwoState']][metadata[['State1']] > 2] <- 'Cancer';
dataids <- read.table('$OutDir/kmer.$K.dataid.txt', header=FALSE)[,1];
m <- read.kmerfiles(sprintf('output/%s/kmer.$K.counts', dataids),
    row.names=dataids, normalize=TRUE);

fit <- prcomp(m);
plot.data <- data.frame(DataId=rownames(m),
    PC1=fit[['x']][, 1], PC2=fit[['x']][, 2],
    State=metadata[dataids, 'TwoState']);
plt <- ggplot(data=plot.data) + geom_point(aes(x=PC1, y=PC2, color=State)) +
    ggtitle('PCA analysis of Kmer($K) frequency');
save_plot('$OutDir/kmer.$K.pdf', plot=plt);
RSCRIPT
}

pca_on_kmer_freq
