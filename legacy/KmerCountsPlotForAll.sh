#! /bin/bash

. Configuration.sh

kmer_counts_plot(){
    DataId=$1
    ./WeightedKmerCounter dump $OutDir/$DataId/kmer.$K.counts \
        > $OutDir/$DataId/kmer.$K.counts.txt
    R --vanilla <<RSCRIPT
t <- read.table('$OutDir/$DataId/kmer.$K.counts.txt', header=FALSE);
#pdf('$OutDir/$DataId/kmer.$K.counts.pdf', width=8, height=5);
png('$OutDir/$DataId/kmer.$K.counts.png', width=800, height=500);
barplot(t[, 2], main='Kmer($K) counts in sample $DataId', xlab='Kmer($K) index', ylab='Counts');
dev.off();
RSCRIPT
}

if [ -n "$1" ];then
    kmer_counts_plot $1
    exit 0
fi

if [ "$NPROC" -gt 1 ];then
    select_dataid | xargs -l -P $NPROC $0
else
    for DataId in `select_dataid`;do
        kmer_counts_plot $DataId
    done
fi

