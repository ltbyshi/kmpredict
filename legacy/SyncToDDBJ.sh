#! /bin/bash
#DRYRUN=--dry-run
{
for K in `dir KmerMarker`;do
    dir KmerMarker/$K/AverageCoverage.txt
    find KmerMarker/$K/KmerList -type f
    find KmerMarker/$K/KmerMatrix -type f
    ##find KmerMarker/$K/Param -type f
done   
} | rsync -rAvP $DRYRUN --files-from=- \
    . shibinbin@gw.ddbj.nig.ac.jp:projects/kmer/ \
  | tee Logs/SyncToDDBJ.log

