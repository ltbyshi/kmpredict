#! /bin/bash

. Configuration.sh

OutDir=analysis/KmerDiversity
[ -d "$OutDir" ] || mkdir -p $OutDir
SampleSize=5
MaxNSamples=10
MinK=4
MaxK=12

unused(){
echo -e "SampleCount\tNo\tKmerCount" > $OutDir/kmer.$K.txt
for nSamples in $(seq $MaxNSamples);do
    for i in $(seq $SampleSize);do
        echo "Sample size $nSamples ($i)"
        KmerFiles=$(sed '1 d' metadata/metadata.txt | cut -f1  | shuf \
            | head -n $nSamples | awk -v K=$K '{printf("output/%s/kmer.%s.counts\n",$1,K)}')
        nKmers=$($BINPATH/KmerCounter dump $KmerFiles | awk '$2>0' | wc -l)
        echo -e "$nSamples\t$i\t$nKmers" >> $OutDir/kmer.$K.txt
    done
done
}

kmer_diversity(){
    echo -e "SampleCount\tNo\tKmerCount" > $OutDir/kmer.$K.txt
    for nSamples in $(seq $MaxNSamples);do
        for i in $(seq $SampleSize);do
            echo "K=$K, Sample size $nSamples ($i)"
            KmerFiles=$(select_dataid | shuf \
                | head -n $nSamples | awk -v K=$K '{printf("output/%s/kmer.%s.counts\n",$1,K)}')
            nKmers=$($BINPATH/KmerCounter uniquekmercount $KmerFiles)
            echo -e "$nSamples\t$i\t$nKmers" >> $OutDir/kmer.$K.txt
        done
    done
}

kmer_diversity_plot(){
R --vanilla <<RSCRIPT
library(plyr);
library(cowplot);
dat <- data.frame();
for(k in $MinK:$MaxK){
    tmp <- read.table(sprintf('$OutDir/kmer.%d.txt', k), header=TRUE);
    tmp[['K']] <- factor(k);
    dat <- rbind(dat, tmp);
}
dat <- ddply(dat, .(K, SampleCount), summarize, AvgKmerCount=log10(mean(KmerCount)));
plt <- ggplot(data=dat) + 
    geom_line(aes(x=SampleCount, y=AvgKmerCount, color=K)) +
    xlab('Number of samples') +
    ylab('Number of unique kmers (log10)') +
    ggtitle('Kmer diversity analysis') +
    scale_x_discrete(breaks=1:$MaxNSamples);
save_plot('$OutDir/KmerDiversity.pdf', plt, base_aspect_ratio=1.3);
RSCRIPT
}

#for K in $(seq $MinK $MaxK);do
#    kmer_diversity
#done
kmer_diversity_plot

