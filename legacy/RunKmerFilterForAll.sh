#! /bin/bash

. Configuration.sh

OutDir=output

run_kmerfilter(){
    DataId=$1
    [ -d "$OutDir/$DataId" ] || mkdir -p "$OutDir/$DataId"
    $BINPATH/KmerFilter count $K $ScaffoldDir/$DataId/scaffold.fa \
            $OutDir/$DataId/kmer.$K.filter
}

run_hashedcount(){
    DataId=$1
    [ -d "$OutDir/$DataId" ] || mkdir -p "$OutDir/$DataId"
    $BINPATH/KmerFilter hashedcount $K $HashSize \
        Hyperplanes/$K.$HashSize \
        $ScaffoldDir/$DataId/scaffold.fa \
        $OutDir/$DataId/kmer.$K.hashed   
}

run_filtered_hashedcount(){
    DataId=$1
    MinKmerCount=$2
    [ -d "$OutDir/$DataId" ] || mkdir -p "$OutDir/$DataId"
    $BINPATH/KmerFilter hashedcount $K $HashSize \
        Hyperplanes/$K.$HashSize \
        $ScaffoldDir/$DataId/scaffold.fa \
        $OutDir/$DataId/kmer.$K.$MinKmerCount.hashed 
}

if [ -n "$1" ];then
    if [ "$KmerFilterOption" = count ];then
        run_kmerfilter $1
    elif [ "$KmerFilterOption" = hashedcount ];then
        run_hashedcount $1
    else
        echo "Invalid KmerFilterOption: $KmerFilterOption"
        exit 1
    fi
    exit 0
fi

# run in parallel
select_dataid | xargs -t -l -P $NPROC $0

unused(){
if [ "$NPROC" -gt 1 ];then
    select_dataid | xargs -t -l -P $NPROC $0
else
    for DataId in `select_dataid`;do
        run_kmerfilter $DataId
    done
fi
}
