#! /bin/bash

OutDir=Hyperplanes
rm -rf $OutDir
mkdir $OutDir

for HashSize in 24 26 28 30;do
    for K in 16 20 24 26 28 30 32;do
        echo "CreateHash K=$K HashSize=$HashSize"
        ./KmerFilter createhash $K $HashSize $OutDir/$K.$HashSize
    done
done
