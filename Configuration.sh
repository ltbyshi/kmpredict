#! /bin/bash

#NPROC=${NPROC?}
#ScaffoldDir=/sdat2/KurokawaLab/Share/151002_scaffold_coverage/151001_scaffold_test
# symbolic link
ScaffoldDir=Scaffold
K=${K?}
HashSize=${HashSize=28}
OutDir=output
KmerCountsDir=${KmerCountsDir:=output}
if [ "$K" -le 15 ];then
    KmerCounterOption=countw
else
    KmerCounterOption=hashedcountw
fi
KmerFilterOption=count
select_dataid(){
    sed '1 d' metadata/metadata.txt | cut -f1
    #ls $ScaffoldDir | grep -v -f metadata/excluded.txt
    #cat <(ls $ScaffoldDir | grep random)
}

export BINPATH=./bin
export LogDir=Logs
TemplateDir=$HOME/Templates
export ScriptDir=Scripts
export JobDir=Jobs

mkdir -p $JobDir 2>/dev/null
mkdir -p $LogDir 2>/dev/null

# override with local settings
if [ -f "bashrc" ];then
    . bashrc
fi

get_seq_file(){
    local dataid=$1
    local seqfile=$(ls $RawReadsDir/$dataid/*.fasta)
    if [ ! -f "$seqfile" ];then
        echo "Error: sequence file for $dataid does not exist"
        exit 1
    fi
    printf $seqfile
}
